import cv2, sys
import skvideo.io
import skvideo.datasets
import numpy as np
import matplotlib.pyplot as plt
import pandas as df

#command Line
filename = sys.argv[1]

# read the video
vid = skvideo.io.vreader(filename)

greens=[]
blues=[]
reds=[]

print(filename)

outfile = open('titation.csv', 'w')
"""
LOOKING AT VIDEO 

take out a clip of the image that is the same for every snapshot
then pull out all blue, green, and red channels

find the averages of each of the channels and append to their appropriate lists
(greens, blues, reds) 

add these averaged values to the csv file 

"""
framecount = 0 
for frame in vid:
	clip = frame[233:413, 184:265, :]
	framecount += 1

	blue = clip[:,:,0] 
	green = clip[:,:,1] 
	red = clip[:,:,2] 

	avgRed = np.mean(red) 
	reds.append(avgRed)
	avgGrn = np.mean(green)
	greens.append(avgGrn)
	avgBlue = np.mean(blue)
	blues.append(avgBlue)

	
	outfile.write('%0.2F,%0.2F, %0.2F, %0.2F\n'%(avgRed,avgGrn,avgBlue, framecount))

"""
TINY MATH and TINY ANALYSIS 

- finds minimum green (meaning most purple/pink) 
- finds frame number min green is at
- finds size of dataset 
- finds unstablegreen (aka not at equillibrium)
- finds unstablered
- finds stablegreen
- finds stable red


""" 

greenloc = np.min(greens) #finds location of min green on numpy 
framenumber = int(greens.index(greenloc)) + int(1) #finds framenumber (index goes up to but does include, that's why there's +1 necessary 
seconds = framenumber/25
size = np.ma.size(greens, axis = 0) #looks at all rows in greens dataset, finds size

unstablegreen = np.mean(greens[0:framenumber]) 

unstablered = np.mean(reds[0:framenumber]) 

stablegreen = np.mean(greens[framenumber:-1])

stablered = np.mean(reds[framenumber:-1]) 

overallunstable = unstablered - unstablegreen
overallstable = stablered - stablegreen

"""Prints Statements 
- Finds min value of reds, greens, and blues
- Total Frames 
- Location of min green 
- Location pH change in seconds 

"""

print("min red value: ",np.min(reds))
print("min green value: ",np.min(greens)) 
print("min blue value: ",np.min(blues))
print("the number of total frames is:", size)
print("you will find the min green value at row: ",framenumber)
print("you will find the pH change at: ", seconds, "seconds")

#print("this will print all unstables: ", unstable)
#print ("this will print average of unstables:", avgUnstable) 
print ("Average of unstables for Green:", unstablegreen, "Red:", unstablered) 
print ("Average of stables for Green:", stablegreen, "Red:", stablered) 

print("BEFORE pH is stablized", overallunstable) 
print ("AFTER pH is stablized", overallstable)


"""GRAPH"""

plt.xlabel('Frames')
plt.ylabel('Channel Value')

plt.plot (reds, 'r')
plt.plot (blues, 'b')
plt.plot (greens, 'g')
plt.ylim(0, 256)
plt.xlim(0, 600)
plt.show()

outfile.close()
#the correct frame, set as variable in case needed to be changed
video24 = 271
video25 = 300
video26 = 250
video27 = 336
video28 = 421
video29 = 257

if '24TimeLapse.wmv' in filename:
	difference = abs(framenumber - video24)
	print('The number of frame(s) off is:', difference)

if '25TimeLapse.wmv' in filename:
	difference = abs(framenumber - video25)
	print('The number of frame(s) off is:', difference)

if '26TimeLapse.wmv' in filename:
	difference = abs(framenumber - video26)
	print('The number of frame(s) off is:', difference)

if '27TimeLapse.wmv' in filename:
	difference = abs(framenumber - video27)
	print('The number of frame(s) off is:', difference)

if '28TimeLapse.wmv' in filename:
	difference = abs(framenumber - video28)
	print('The number of frame(s) off is:', difference)

if '29TimeLapse.wmv' in filename:
	difference = abs(framenumber - video29)
	print('The number of frame(s) off is:',difference)



	
