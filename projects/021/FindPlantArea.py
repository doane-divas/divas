#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 31 14:57:04 2018

@author: Jennifer Pashby and Grace Su
This code isolates the plants and finds the area for each one.
"""
import cv2
import sys
import numpy as np
img = cv2.imread(sys.argv[1])
k=5
# Convert BGR to HSV
#better way to show only a certain color, much faster and clearer
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV) #HSV=hue, saturation, value

# define range of green color in HSV
lower_green = np.array([30,50,20])
upper_green= np.array([75,255,255])

# Threshold the HSV image to get only green colors
#shows only plant, masks over all other colors
mask = cv2.inRange(hsv, lower_green, upper_green)

# Bitwise-AND mask and original image
res = cv2.bitwise_and(img,img, mask= mask)

#grayscale and blur
newgray= cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(newgray, (k, k), 0)

#threshold to get rid of the last bit of gray
(t, maskLayer) = cv2.threshold(blur, 35, 255, cv2.THRESH_BINARY)
mask = cv2.merge([maskLayer, maskLayer, maskLayer])
plant = cv2.bitwise_and(res, mask)

#show final image
cv2.namedWindow('plant', cv2.WINDOW_NORMAL)
cv2.imshow('plant',plant)
cv2.waitKey(0)

#make it grayscale again in order to find contours
plantgray= cv2.cvtColor(plant, cv2.COLOR_BGR2GRAY)

#find contours
(_, cnts, _)=cv2.findContours(plantgray,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
arealist=[]
centroid=[]

#scroll through the contours to find the centroid, and the area in pixels to later be
#able to sort
for c in cnts:
    centroidpoint=[]
    area = cv2.contourArea(c)
    if area> 4500:
        arealist.append(area)
        M = cv2.moments(c)
        if M['m00']!=0:
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
        centroidpoint.append(cx)
        centroidpoint.append(cy)
        centroid.append(centroidpoint)
print (arealist)
print (centroid)
place =0

#sort by matching each contour with its plant/section of the image
for point in centroid:
    if point[0] <789 and point[1]< 756:
        print ("Row 1 Plant 1", "Area=", arealist[place])
    elif point[0] <1539 and point[1]< 756:
        print ("Row 1 Plant 2", "Area=", arealist[place])
    elif point[0] <2253 and point[1]< 756:
        print ("Row 1 Plant 3", "Area=", arealist[place])
    elif point[0] <789 and point[1]< 1488:
        print ("Row 2 Plant 1", "Area=", arealist[place])
    elif point[0] <1539 and point[1]< 1488:
        print ("Row 2 Plant 2", "Area=", arealist[place])
    elif point[0] <2253 and point[1]< 1488:
        print ("Row 2 Plant 3", "Area=", arealist[place])
    elif point[0] <789 and point[1]< 2235:
        print ("Row 3 Plant 1", "Area=", arealist[place])
    elif point[0] <1539 and point[1]< 2235:
        print ("Row 3 Plant 2", "Area=", arealist[place])
    elif point[0] <2253 and point[1]< 2235:
        print ("Row 3 Plant 3", "Area=", arealist[place])
    place=place+1
        
