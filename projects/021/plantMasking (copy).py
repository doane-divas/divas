#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 29 14:39:18 2018

@author: diva
"""
#import stuff
import cv2
import numpy as np
import sys

# get filename and kernel size values from command line
filename = sys.argv[1]
k = int(sys.argv[2])

# read the original image
img = cv2.imread(filename)
'''cv2.namedWindow("original", cv2.WINDOW_NORMAL)
cv2.imshow("original", img)
cv2.waitKey(0)'''

# blur before thresholding
blur = cv2.GaussianBlur(img, (k, k), 0)
blur= cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
'''cv2.namedWindow("blur", cv2.WINDOW_NORMAL)
cv2.imshow("blur", blur)
cv2.waitKey(0)'''

#invert for white circles and QR code
(t, maskL) = cv2.threshold(blur, 100, 255, cv2.THRESH_BINARY)
cv2.namedWindow('mask3', cv2.WINDOW_NORMAL)
cv2.imshow('mask3', maskL)
cv2.waitKey(0)

# perform inverse binary thresholding to create a mask that will remove 
# the white circle and label.
(t, mask) = cv2.threshold(blur, 105, 255, cv2.THRESH_BINARY_INV)

'''cv2.namedWindow("mask", cv2.WINDOW_NORMAL)
cv2.imshow("mask", mask) 
cv2.waitKey(0)'''

#edge detection stuff
edgeX = cv2.Sobel(mask, cv2.CV_16S, 1, 0)
edgeY = cv2.Sobel(mask, cv2.CV_16S, 0, 1)

# convert back to 8-bit, unsigned numbers and combine edge images
edgeX = np.uint8(np.absolute(edgeX))
edgeY = np.uint8(np.absolute(edgeY))
edge = cv2.bitwise_or(edgeX, edgeY)

# display edges
"""cv2.namedWindow("edges", cv2.WINDOW_NORMAL)
cv2.imshow("edges", edge)
cv2.waitKey(0)"""

#find contours
(_, contours, _) = cv2.findContours(edge, cv2.RETR_TREE, 
    cv2.CHAIN_APPROX_SIMPLE)
# draw contours over original image
cv2.drawContours(mask, contours, -1, (0, 0, 255), 5)

# display original image with contours
'''cv2.namedWindow("output", cv2.WINDOW_NORMAL)
cv2.imshow("output", mask)
cv2.waitKey(0)'''


# perform inverse binary thresholding 
(t, maskLayer) = cv2.threshold(mask, 25, 255, cv2.THRESH_BINARY)


mask = cv2.merge([maskLayer, maskLayer, maskLayer])

# display the mask image
"""cv2.namedWindow("mask", cv2.WINDOW_NORMAL)
cv2.imshow("mask", mask)
cv2.waitKey(0)"""

#splitting colors into channels
rows,column,channels=img.shape
for x in range(rows):
    for y in range(column):
        if img [x,y,2] >100 and img[x,y,1]<100: #numbers in bgr
            img[x,y,:]=0#gets rid of red border
        if img [x,y,0]>75:#gets rid of glare on background
            img[x,y,:]=0

# use the mask to select the "interesting" part of the image
sel = cv2.bitwise_and(img, mask)

# display the result
'''cv2.namedWindow("selected", cv2.WINDOW_NORMAL)
cv2.imshow("selected", sel)
cv2.waitKey(0)'''

# changes back to grayscale
newgray= cv2.cvtColor(sel, cv2.COLOR_BGR2GRAY)
# perform adaptive thresholding 
(t, maskLayer) = cv2.threshold(newgray, 0, 255, cv2.THRESH_BINARY + 
    cv2.THRESH_OTSU)

# make a mask suitable for color images
mask2 = cv2.merge([maskLayer, maskLayer, maskLayer])

# use the mask to select the "interesting" part of the image
select = cv2.bitwise_and(sel, mask2)

# display the result
'''cv2.namedWindow("select", cv2.WINDOW_NORMAL)
cv2.imshow("select", select)
cv2.waitKey(0)'''

#goes back to grayscale
newselect= cv2.cvtColor(select, cv2.COLOR_BGR2GRAY)
# perform binary thresholding 
(t, maskLayers) = cv2.threshold(newselect, 75, 255, cv2.THRESH_BINARY) #THRESH_BINARY for white mask THRESH_BINARY_INV for black mask

# make a mask suitable for color images
NewImg = cv2.merge([maskLayers, maskLayers, maskLayers])

# display the mask image
'''cv2.namedWindow("NewImg", cv2.WINDOW_NORMAL)
cv2.imshow("NewImg", NewImg)
cv2.waitKey(0)'''

# use the mask to select the "interesting" part of the image
sel2 = cv2.bitwise_and(select, NewImg)

# display the final result
cv2.namedWindow("selectedImg", cv2.WINDOW_NORMAL)
cv2.imshow("selectedImg", sel2)
cv2.waitKey(0)
