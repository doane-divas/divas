'''
 * Python program to create a color histogram.
 *
 * Usage: python ColorHistogram.py <filename>
'''
import cv2
import sys
from matplotlib import pyplot as plt

# read original image, in full color, based on command
# line argument
img = cv2.imread(sys.argv[1])

'''
g = img.copy()
# set blue and red channels to 0
g[:, :, 0] = 0
g[:, :, 2] = 0

# RGB - Green
cv2.namedWindow('G-RGB', cv2.WINDOW_NORMAL)
cv2.imshow('G-RGB', g)
cv2.waitKey(0)

(t, maskL) = cv2.threshold(g, 75, 255, cv2.THRESH_BINARY)
cv2.namedWindow('gthresh', cv2.WINDOW_NORMAL)
cv2.imshow('gthresh', maskL)
cv2.waitKey(0)
'''
r = img.copy()
# set blue and green channels to 0
r[:, :, 0] = 0
#r[:, :, 1] = 0

cv2.namedWindow('R-RGB', cv2.WINDOW_NORMAL)
cv2.imshow('R-RGB', r)
cv2.waitKey(0)


(t, maskL) = cv2.threshold(r, 75, 255, cv2.THRESH_BINARY)
cv2.namedWindow('gthresh', cv2.WINDOW_NORMAL)
cv2.imshow('gthresh', maskL)
cv2.waitKey(0)



'''
# display the image 
cv2.namedWindow("Original Image", cv2.WINDOW_NORMAL)
cv2.imshow("Original Image", img)
cv2.waitKey(0)

# split into channels
channels = cv2.split(img)

# list to select colors of each channel line
colors = ("b", "g", "r") 

# create the histogram plot, with three lines, one for
# each color
plt.xlim([0, 256])
for(channel, c) in zip(channels, colors):
    histogram = cv2.calcHist([channel], [0], None, [256], [0, 256])
    plt.plot(histogram, color = c)

plt.xlabel("Color value")
plt.ylabel("Pixels")

plt.show()
'''