<<<<<<< HEAD:projects/012/standard.py
'''
###############################################################################
The purpose of this code is to normalize ninhydrin paper so that they can be
compared. 

Authors: Truc Doan, AdreAnna Ernest, Danny Tran
Date: June 16, 2017
###############################################################################
'''
import cv2
import sys
import numpy as np

#load the image through the terminal system argument
filename = sys.argv[1]
img = cv2.imread(filename)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


#Showing the image with the gray scale
cv2.namedWindow("Grayscale", cv2.WINDOW_NORMAL)
cv2.imshow("Grayscale",gray)
cv2.waitKey(0)

#Values for the standard ninhydrin paper that
m2 = -22.042
b2 = 208.26

#These user imputs allow us to specify vaules that need to be adjusted 
m1 = input('input slope for nonstandard image')
m1 = float(m1)
b1 = input('input y-intercept for nonstandard image')
b1 = float(b1)

# working with individual pixel for conversion 
adjustedYList = []
outlierAdjustedY = []
'''
def outlier(pixelValue):
    for value in pixelValue:
        if int(value) > 255:
            outlierAdjustedY.append(value)
'''
black = np.empty_like(gray)

for (i, row) in enumerate(gray):
    for (p, pixel) in enumerate(row):
        y1 = pixel
        #print('pixel: ', y1)
        #This equation will adjust the intesity so that it is normalized to the standard
        adjustedY = m2 * ((y1 - b1)/m1) + b2
        adjustedYList.append(adjustedY)
        black[i, p] = adjustedY

cv2.imwrite('AdjustedIntensity_Day_' + filename[-5] + '.tif', black)

#Showing the image with the gray scale
cv2.namedWindow("ReformedGray", cv2.WINDOW_NORMAL)
cv2.imshow("ReformedGray",black)
cv2.waitKey(0)


#print('the outlierAdjustedY:', len(outlierAdjustedY))
#print('adjustedY Values: ', adjustedYList)
#print('min: ', str(min(adjustedYList)))
#print('max: ', str(max(p, adjustedYList)))
=======
'''
###############################################################################
The purpose of this code is to normalize ninhydrin papers so that they can be
compared. 

Authors: Truc Doan, AdreAnna Ernest, Danny Tran
Date: June 16, 2017
###############################################################################
'''
import cv2, sys
import numpy as np

#load the image through the terminal system argument
filename = sys.argv[1]
img = cv2.imread(filename)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


#Showing the image with the gray scale
cv2.namedWindow("Grayscale", cv2.WINDOW_NORMAL)
cv2.imshow("Grayscale",gray)
cv2.waitKey(0)

#Values for the standard ninhydrin paper - previously calculated
m2 = -22.042
b2 = 208.96

#These user imputs allow us to specify vaules that need to be adjusted 
m1 = input('input slope for nonstandard image')
m1 = float(m1)
b1 = input('input y-intercept for nonstandard image')
b1 = float(b1)

# working with individual pixel for conversion 
adjustedYList = []
outlierAdjustedY = []

#make a black image the same size as the gray image. This will be used for the 
#new, adjusted intesities to be written over
black = np.empty_like(gray)

#This for loop selects the area of the image that we want to adjust the pixel
#intensities. It then applies an equation to normalize the unadjusted paper
#so that the intensities and concentrations are comparable.
for (i, row) in enumerate(gray):
    for (p, pixel) in enumerate(row):
        y1 = pixel
        #print('pixel: ', y1)
        #This equation will adjust the intesity so that it is normalized to the standard
        adjustedY = m2 * ((y1 - b1)/m1) + b2
        adjustedYList.append(adjustedY)
        black[i, p] = adjustedY

#Save the black image with new, adjusted intensities applied to the image
cv2.imwrite('AdjustedIntensity_Day_' + filename[-5] + '.tif', black)

#Showi the new image
cv2.namedWindow("ReformedGray", cv2.WINDOW_NORMAL)
cv2.imshow("ReformedGray",black)
cv2.waitKey(0)


#print('the outlierAdjustedY:', len(outlierAdjustedY))
#print('adjustedY Values: ', adjustedYList)
#print('min: ', str(min(adjustedYList)))
#print('max: ', str(max(p, adjustedYList)))
>>>>>>> eb92edc19d9558033a8ef67ab30a088a07865266:projects/012/normalize.py
