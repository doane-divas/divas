'''
##############################################################################
The purpose of this code is to create a colormap image to easily estimate the 
concentration of free amines secreted by maize root. This quantification occurs
when the user compares the color of the maize root to the corresponding color 
on the intensity/concentration scale.

Truc Doan, AdreAnna Ernest, Danny Tran
Edited: 6/29/2017
##############################################################################
'''

#Import libraries needed to run the code
import cv2, sys
import matplotlib.pyplot as plt
import matplotlib.ticker
import matplotlib as mpl
import numpy as np

#Enter image/filename into command line
filename = sys.argv[1]

#load the image and convert it to a grayscale image     
img = cv2.imread(filename)

#To get a single number instead of three individual channel values
intensity = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#Make plot with vertical (default) colorbar
fig, ax = plt.subplots()

#Creating a custom colorbar
colors = [(0,0,0), (1,0.2,0.2), (1,0.4,0.4), (1,0.6,0.2), (1,0.7,0.4), (1,1,0.2), (1,1,0.4), (0.6,1,0.2), (0.2,1,0.2), (0.2,1,0.6), (0.2,1,1), (0.2,0.6,1), (0.2,0.2,1), (0.6,0.2,1), (0.7,0.4,1), (0.8,0.6,1), (0.9,0.8,1), (1,1,1)]  # R -> G -> B
n_bins = 23  # Discretizes the interpolation into bins. Fewer bins will result in "coarser" colormap interpolation
cmap_name = 'my_list'
cm1 = mpl.colors.LinearSegmentedColormap.from_list(
    cmap_name, colors, N=n_bins)

#Simply defining cmap as our custom bar
cmap=cm1

#Define the bins and normalize
bounds = np.linspace(115.6836, 212.6684, 23)
norm = mpl.colors.BoundaryNorm(bounds, cmap.N)

#Create the colormap image
colormapImg = ax.imshow(intensity, interpolation='nearest', cmap=cmap, norm=norm)

#Create two color bars - 1 for intensity and 1 for concentration 
#Acknowledgement to Michael Kangas
cs = plt.colorbar(colormapImg, spacing = 'proportional', drawedges=True, extend='both', format = '%1i', norm=norm) 
cs2 = plt.colorbar(colormapImg, spacing = 'proportional', drawedges=True, extend='both', format = '%1i', norm=norm)

#Placing the equivalent concentration that the given intensity value on the colorbar 
#and displaying the colormap

#for Concentration colorbar (cs.)
tick_loc = [208.26, 203.8516, 199.4432, 195.0348, 190.6264, 186.218, 181.8096,
            177.4012, 172.9928, 168.5844, 164.176, 159.7676, 155.3592, 150.9508, 
            146.5424, 142.134, 137.7256, 133.3127, 128.9088, 124.5004, 120.092]
            
tick_labels = [0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 3.6, 3.8, 4.0]

#Placing the tick labels according to the location provided in tick_loc
cs.locator     = matplotlib.ticker.FixedLocator(tick_loc)
cs.formatter   = matplotlib.ticker.FixedFormatter(tick_labels)
cs.update_ticks()

#Adding labels and displaying the image
cs2.ax.set_ylabel('Intensity Value')
cs.ax.set_ylabel('Concentration of free amines (mM)')
plt.xlabel('X Pixel Coordinates')
plt.ylabel('Y Pixel Coordinates')

#Flipping y axis so 0 starts at the bottom. This would allow the colormap to look similar to a traditional (x,y) coordinate graph
#Finally, displaying the heatmap
plt.gca().invert_yaxis()
plt.show()



