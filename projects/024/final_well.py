#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 13:16:33 2018

@author: Justin and Catie
"""

#import
import numpy as np
import cv2
import sys
import operator
import matplotlib.pylab as plt

#Load and read image
filename = sys.argv[1]
img = cv2.imread(filename)

#create grayscale images to find grayscale avg
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

'''
Loop through the wells and find average color
sort the wells by lowest color and assign them
in the dictionary
'''

#create dictionaries for each color channel
colorR = {}
colorG = {}
colorB = {}
colorAvg = {}
#starting coordinates of the first well
startX = 85
startY = 106
#create a second pair (x,y) that will change while (startX,startY) stays the same
x,y = startX, startY

#Letters will be used to add the key to dictionaries
letters = ['A','B','C','D','E','F','G','H']

#loop through the columns
for column in range(8):
	#these will hold the sum of values for each channel
    avgR = 0
    avgG = 0
    avgB = 0
    avg = 0
	#loop down wells 1-6
    for row in range(6):
        #avgeraging color values in each channel
        avgB += np.mean(img[y:y+12,x:x+15,0])
        avgG += np.mean(img[y:y+12,x:x+15,1])
        avgR += np.mean(img[y:y+12,x:x+15,2])
		#grayscale avg	
        avg += np.mean(gray[y:y+12,x:x+15])
        y += 71
	#add the average with the correct key
	#calling the letters list at the indice column to find the key for dic
	#avg/6 to find avg per well
    colorR[letters[column] + ' 1-6'] = (avgR/6)
    colorG[letters[column] + ' 1-6'] = (avgG/6)
    colorB[letters[column] + ' 1-6'] = (avgB/6)
    colorAvg[letters[column] + ' 1-6'] = (avg/6)
#reset the y value and add to x to move to top of second column
    y = startY
    x += 69

#same loop but for the bottom six
#refer to the one above for comments
    #reset x for second loop
x = startX
#start y is lower so it starts on the 7th well in row
startY2 = 532
y = startY2

for column in range(8):
    avgR = 0
    avgG = 0
    avgB = 0
    avg = 0
	#loop down wells 7-12
    for row in range(6):
        avgB += np.mean(img[y:y+12,x:x+15,0])
        avgG += np.mean(img[y:y+12,x:x+15,1])
        avgR += np.mean(img[y:y+12,x:x+15,2])	
        avg += np.mean(gray[y:y+12,x:x+15])
        y += 71
	#add the average with the correct key
    colorR[letters[column] + ' 7-12'] = (avgR/6)
    colorG[letters[column] + ' 7-12'] = (avgG/6)
    colorB[letters[column] + ' 7-12'] = (avgB/6)
    colorAvg[letters[column] + ' 7-12'] = (avg/6)

    y = startY2
    x += 69

#takes each number in the dic and turns it into a string and keeps the first 
#five numbers; turns back to float
#done for each color
#this is rounding it to the hundreth decimal point
for k,v in colorR.items():
	colorR[k] = float(str(v)[:6])

for k,v in colorG.items():
	colorG[k] = float(str(v)[:6])

for k,v in colorB.items():
	colorB[k] = float(str(v)[:6])
    
for k,v in colorAvg.items():
	colorAvg[k] = float(str(v)[:6])

#sort the wells by the values in each channel
'''
https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value 
'''
#turns dictionary into a list of tuples and sorts them
sColorsR = sorted(colorR.items(), key=operator.itemgetter(1))
sColorsG = sorted(colorG.items(), key=operator.itemgetter(1))
sColorsB = sorted(colorB.items(), key=operator.itemgetter(1))
sGray = sorted(colorAvg.items(), key=operator.itemgetter(1))

'''
print("Blue color values:",sColorsB)
#newline character \n, makes it easier to read
print("\n")
print("Green color values:",sColorsG)
print("\n")
print("Red color values:",sColorsR)
print("\n")
print("Gray values:",sGray)
''' 
#take first 10 values and put them into a temporary list
#done for each list: red, green, blue, and gray
counter = 0 #counter in order to take only the first 10
tempB = []
#k is the key and v is the value in the dictionary
for k,v in sColorsB:
	if counter <10:
		#only add the first 10 values
		tempB.append(v)
		counter += 1


counter = 0
tempG = []
for k,v in sColorsG:
	if counter <10:
		tempG.append(v)
		counter += 1


counter = 0
tempR = []
for k,v in sColorsR:
	if counter <10:
		tempR.append(v)
		counter += 1


counter = 0
tempGray = []
for k,v in sGray:
	if counter <10:
		tempGray.append(v)
		counter += 1



#make list of concentrations
conc = [0.1,0.09,0.08,0.07,0.06,0.05
,0.04,0.03,0.02,0.01]



#plotting the concentration and the list of the first ten values for each channel
plt.scatter(conc,tempGray,color = 'black',s = 5, label = 'gray')


#Online function that finds the line of best fit
#https://stackoverflow.com/questions/22239691/code-for-line-of-best-fit-of-a-scatter-plot-in-python

def best_fit(X, Y):

	#finds the average per list
    xbar = sum(X)/len(X)
    ybar = sum(Y)/len(Y)
    n = len(X) # or len(Y)

    numer = sum([xi*yi for xi,yi in zip(X, Y)]) - n * xbar * ybar
    denum = sum([xi**2 for xi in X]) - n * xbar**2

    b = numer / denum
    a = ybar - b * xbar

    print('best fit line:\ny = {:.2f} + {:.2f}x'.format(a, b))

    return a, b

# solution
a, b = best_fit(conc, tempG)
yfit = [a + b * xi for xi in conc]
plt.plot(conc, yfit)


#plt.plot(conc,tempR,color='r', label = 'Red')
#plt.plot(conc,tempG,color='g', label = 'Green')
#plt.plot(conc,tempB,color='b', label = 'blue')

plt.xlabel("Concentration x 10,000")
plt.ylabel("Color Intenistiy")
plt.legend()

plt.show()
