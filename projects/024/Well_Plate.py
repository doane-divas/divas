#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 13:16:33 2018

@author: Justin and Catie
"""

#import
import numpy as np
import cv2
import sys
import operator

#Load and read image
filename = sys.argv[1]
img = cv2.imread(filename)

#convert to grayscale
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#cv2.namedWindow("image2", cv2.WINDOW_NORMAL)
#cv2.imshow("image2", gray)
#cv2.waitKey(0)

'''
Loop through the wells and find average color
sort the wells by lowest color and assign them
in the dictionary
'''

#create dictionary
colorAvg = {}

#starting coordinates of the first well
startX = 85
startY = 106

x,y = startX, startY

#Letters will be used to add the key to dictionary
letters = ['A','B','C','D','E','F','G','H']

#loop through the columns
for column in range(8):
    avg = 0
	#loop down wells 1-6
    for row in range(6):
        avg += np.mean(gray[y:y+12,x:x+15])
        y += 71
	#add the average with the correct key
    colorAvg[letters[column] + ' 1-6'] = (avg/6)
    y = startY
    x += 69

#same loop but for the bottom six
x = startX
y = 532

for column in range(8):
    avg = 0
    for row in range(6):
        avg += np.mean(gray[y:y+12,x:x+15])
        y += 71
    colorAvg[letters[column] + ' 7-12'] = (avg/6)
    y = 532
    x += 69

#takes each number in the dic and turns it into a string and keeps the first five numbers; turns back to float
for k,v in colorAvg.items():
	colorAvg[k] = float(str(v)[:6])

#sort the wells by the values 
'''
https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value 
'''
sortColors = sorted(colorAvg.items(), key=operator.itemgetter(1))


print(sortColors)
    

