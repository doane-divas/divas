#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 11 14:05:06 2018

@author: diva
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 28 13:48:47 2018

@author: Catie

This code is used to find possible breaks in vertical limb images

Images can be found in Google Drive folder under Catie Images
"""

#import needed tools
import cv2
import numpy as np
import sys

#get image name from command line
filename = sys.argv[1]

#open image
img = cv2.imread(filename)
'''
This is where the code can only work on vertical images
This creates masks over the labels in the four corners because the labels were
getting contoured and be counted, which was messing with the results
'''
#set variables for the top right and bottom left corners of the image
x0,y0 = 0,0
x,y = 2844,3584

#create a mask like a frame over the labels
mask = np.zeros(img.shape, dtype = "uint8")
cv2.rectangle(mask, (x0+454, y0+219), (x-554,y-329), (255,255,255), -1)

masked = cv2.bitwise_and(img, mask)

#show the maked image
cv2.namedWindow("Mask", cv2.WINDOW_NORMAL)
cv2.imshow("Mask", masked)
cv2.waitKey(0)
#grayscale the image that has the masks placed over them
gray = cv2.cvtColor(masked, cv2.COLOR_BGR2GRAY)

#blur and threshold the image so it can be contoured
blur = cv2.GaussianBlur(gray, (5,5), 0)
(t, binary) = cv2.threshold(blur, 140, 255, cv2.THRESH_BINARY_INV)

#find contours (hierarchy because the contours wouldn't show otherwise)
(_, contours, hierarchy) = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

#draw the contours on the gray image: this makes the outline of the bones easier to see
cv2.drawContours(gray, contours, -1, (0,0,255), 5)

#show the image
cv2.namedWindow("New Image", cv2.WINDOW_NORMAL)
cv2.imshow("New Image", gray)
cv2.waitKey(0)

#save the image as filename-clear.jpg
dot = filename.index(".")
newFileName = filename[:dot] + "-clear" + filename[dot:]
cv2.imwrite(newFileName, gray)
