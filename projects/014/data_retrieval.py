#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 19:33:25 2017

@author: Benjamin Zwiener
"""

#imports for this program
import sys, cv2
import tkinter as tk
import pandas as pd
from PIL import Image
from PIL import ImageTk
import tkinter.messagebox as tkmb

MY_NAME = 'BensOwnComp'

filename = sys.argv[1]
k = 5
t = 120

img = cv2.imread(filename)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
label_gray = gray[1900:3550,:]
'''
Description: This function is used to find the left x-value of the first 
             tube on the plate.
             
Parameters:  label - the labels on the top of each tube in a plate

find_first_x_point function algorithm:
    
    I.  blur, threshold and contour the label image
    II. loop through all the contours
    III.loop through all the points in each individual contour
    IV. now look at the y value of the points
        1. if the y value is between 1340 & 1375, keep the x point
        2. if the x value is between 100 & 600, save this pixel
    V.  return the x_point of the saved pixel
'''
def find_first_x_point(label):
    
    # blur the image
    blur = cv2.GaussianBlur(label, (k,k), 0)
    
    # threshold the image
    (_, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    
    # contour the image, only keeping the external contours
    (_, contours, _) = cv2.findContours(binary, cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_SIMPLE)
    
    # loops through each contour in the contours list
    for contour in contours:
        
        # loops through the points in each contour
        for points in contour:
            
            # this loop is needed because points is a list inside a list *e.g. [[10 10]]
            for point in points:
                
                # pull out the y point for comparison
                y = point[1]

                # see if the y point is between these two points
                if y > 1340 and y < 1375:
                    
                    # pull out the x point for comparison
                    x = point[0]

                    # see if the x point is between these two points
                    if x < 600 and x > 100:
                        
                        # if so, return the x value
                        return x
'''    
Description: This function is used to break apart the filename to create new
             filenames to save each tube.
             
Parameters:  filename - the filename of the image being processed
             
get_subfilenames function algorithm:
    
    I.  create a list by splitting filename with dashes (-)
    II. create empty subfilenames list to be returned
    III.loop through list from index 1 to the second last index
        1. index 0 is the plate number while the last index is the day.tif
    IV. create the file using the plate number (index 1), current item in
        the loop, and the day.tif
        1. append this to the subfilenames list
    V.  return the subfilenames list   
'''  
def get_subfilenames(filename):
    
    # create a list by splitting filename
    list = filename.split('-')
    
    # empty list to put new filenames in
    subfilenames = []

    # loop through list from index 1 up to but not including the last index
    for item in list[1:-1]:
        
        # create new filename
        file = list[0] + '-' + item + '-' + list[-1]
        # e.g. file = '001-045-D2.tif'
        
        # append the new filename to subfilenames
        subfilenames.append(file)
    
    # return the list to be used later
    return subfilenames
'''
Description: This function is used to save the tubes as separate images.

Parameters:  x_point - the x point found from the find_first_x_point function
             subfilenames - the list created from get_subfilenames function
             
save_tube_images function algorithm:
    
    I.  loop through a range on 9
        1. crop out the test tube area 
    II. write the cropped out img to a new file using the subfilenames list
    III.increment x_point by 1200 since the tube widths are around 1200
'''
def save_tube_images(x_point, subfilenames):
    
    # loops through 9 times with i = 0,1,2..7,8
    for i in range(0,9):
        
        # crop out the test_tube
        test_tube = img[6750:10700, x_point-80 : x_point + 1280]

        # write the test tube to a new tif image using current subfilename
        cv2.imwrite(subfilenames[i], test_tube)
        
        # increment x_point by 1200
        x_point += 1200
'''
Description: This function is used to find the contours using OTSU thresholding
             and then slim those contours down to the five largest.
             
Parameters:  subfilenames - the list created from get_subfilenames function
                * used to load images to find their contours
             
slim_contour_size function algorithm:
    
    I.  create slimmed_contours_list and thres_list to add to later
    II. loop through the files in subfilenames
        1. read in the image as grayscale using current filename
        2. blur, threshold (OTSU) and contour the image
        3. create top_five_largest_contours_list
        4. sort contours by their length and put it into sorted_contours
        5. loop through the last five contours in sorted_contours
            A. append those to top_five_largest_contours_list
        6. add the top_five_largest_contours_list to slimmed_contours_list
        7. add threshold number to thres_list
    III.return slimmed_contours_list, thres_list
'''
def slim_contour_size(subfilenames):
    
    # list to be returned
    slimmed_contours_list = []

    # thres_list to be returned
    thres_list = []
    
    # loop to go through the subfilenames
    for file in subfilenames:
        
        # read image in as grayscale
        gray_tube_img = cv2.imread(file, cv2.IMREAD_GRAYSCALE)
        
        # blur the image
        blur = cv2.GaussianBlur(gray_tube_img, (k,k), 0)
        
        # threshold using OTSU
        (t, binary) = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        
        # contour the binary image, keeping external contours
        (_, contours, _) = cv2.findContours(binary, cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)
        
        # list for top five largest contours
        top_five_largest_contours_list = []
          
        # sort contours by length      
        sorted_contours = sorted(contours, key=len)
        
        # loop through last five items of sorted_contours
        for contour in sorted_contours[-5:]:
            
            # append to top_five_largest_contours_list
            top_five_largest_contours_list.append(contour)
            
        # append the top five largest contours to slimmed contours
        slimmed_contours_list.append(top_five_largest_contours_list)
        
        # append the t pulled from the OTSU thresholding to thres_list
        thres_list.append(t)
        
    # return these two lists
    return slimmed_contours_list, thres_list
'''
Description: This function is used to find the contours using OTSU thresholding
             and then slim those contours down to the five largest.
             
Parameters:  slimmed_contours_list - list of top five larg. contours for each tube
             thres_list - list of the threshold num used for each tube
             subfilenames - the list created from get_subfilenames function
             
create_dataframe function algorithm:
    
    I.  intialize empty list root_dataframes
    II. loop through slimmed_contours_list, thres_list, and subfilenames
        1. create a dataframe called single_root_dataframe
        2. loop through the contours_list
            A. calculate the area of the contour
            B. create a row with index number, filename, area, and threshold number
            C. append that to the single_root_dataframe
            D. reset the index of single_root_dataframe
        3. append single_root_dataframe to root_dataframes
    III.return root_dataframes
'''
def create_dataframe(slimmed_contours_list, thres_list, subfilenames):
    
    # make empty list to hold all the dataframes
    root_dataframes = []
    
    # loop through the three lists
    for (contours_list, thres, subfile) in zip(slimmed_contours_list, thres_list, subfilenames):
    
        # create a dataframe
        single_root_dataframe = pd.DataFrame(columns = ['Index','Filename', 
                                                 'Area', 'Threshold Number'])
        # loop through the five contours
        for (i, contour) in enumerate(contours_list):
            
            # calculate the area of the contour
            area = cv2.contourArea(contour)
            
            # create a row to add to single_root_dataframe
            current_contour = pd.DataFrame(
                            {'Index': [i+1],
                             'Filename': [subfile],
                             'Area': [area],
                             'Threshold Number': [thres]})
            
            # append current_contour
            single_root_dataframe = single_root_dataframe.append(current_contour)
            
            # reset the index
            single_root_dataframe.reset_index()
        
        # append the new dataframe to the list
        root_dataframes.append(single_root_dataframe)
    
    # return root_dataframes list with all nine dataframes
    return root_dataframes
'''
Description: This function is used to find the center of the contour so a number
                can be placed there later
             
Parameters:  contour - this is the contour the function will find the center of
             
findCentroid function algorithm:
    
    I.  find the moments and set them equal to M
    II. find the x coordinate
    III.find the y coordinate
    IV. return the (x,y) coordinates
'''
def findCentroid(contour):
    
    # find moments
    M = cv2.moments(contour)
    
    # use the moments to get the x value
    x = int(M['m10'] / M['m00'])
    
    # use the moments to get the y value
    y = int(M['m01'] / M['m00'])
    
    # return the x and y values
    return (x, y)
    
'''
Description: This function is used to create the Graphical User Interface.
             It is more or less the main functionality of the script.
             
Parameters:  contents -
             num - 
             
GUI function algorithm:
    
    I.  
'''
def GUI(contents, num): 
    
    # function that combines with get_data button
    def get_data_now():
        
        # ask the user a yes or no question
        answer = tkmb.askquestion('Window', 'Did you mean to press that?')
        
        # if answer is yes
        if answer == 'yes':
            
            # get the number from the text box
            index = get_data_entry_txtbox.get()
            
            # if the index is in this range
            if index in ['1','2','3','4','5']:
                
                # turn index to an int
                index = int(index)
                
                # print index in terminal
                print(index)
                
                # pull out row using the index number
                data = dataframe[(dataframe['Index'] == index)]
                data = data.iloc[0].values

                # pull out area from data
                area = float(data[0])
                
                # tell the user that their data has been saved and they can move on
                tkmb.showinfo('Window', 'Your data has been saved. Exit the window.')
                
                # create string for datasheet
                datasheet = MY_NAME + 'seedlingData.csv'
    
                # open the file and use a for appending
                Outfile = open(datasheet, "a")
                
                # write the plate, type, day, threshold, and area to the file
                Outfile.write('\n' + file[-14:-11] + ',' + file[-10:-7] + ',' + 
                 file[-6:-4] + ',' + str(t) + ',' + str(area))
    
                # close file
                Outfile.close()
                
            # if the index entry is not right    
            else:
                
                # tell the user the entry is invalid
                tkmb.showinfo('Window', 'Invalid entry. Try again.')
        
        # if user presses no
        else:
            
            # tell user to go back to window
            tkmb.showinfo('Window', 'Okay. Exit and go back to window')
    
    
    def get_fifth_index():
        
        answer = tkmb.askquestion('Window', 'Did you mean to press that?')
        
        if answer == 'yes':
        
            index = 5
            
            print(index)
                    
            data = dataframe[(dataframe['Index'] == index)]
    
            data = data.iloc[0].values
    
            area = float(data[0])
            
            t = int(data[3])
            
            tkmb.showinfo('Window', 'Your data has been saved. Exit the window.')
            
            datasheet = MY_NAME + 'seedlingData.csv'
    
            Outfile = open(datasheet, "a")
            
            Outfile.write('\n' + file[-14:-11] + ',' + file[-10:-7] + ',' + 
             file[-6:-4] + ',' + str(t) + ',' + str(area))
    
            Outfile.close()
            
        else:
            
            tkmb.showinfo('Window', 'Okay. Go back to the window')
        
    def no_root_on_pic():
        
        answer = tkmb.askquestion('Window', 'Yes for no root. No for bad contour.')
        
        if answer == 'yes':
        
            tkmb.showinfo('Window', 'Note taken. Output to root file. Exit the window.')
            
            data = dataframe[(dataframe['Index'] == 2)]
    
            data = data.iloc[0].values
            
            t = int(data[3])
            
            datasheet = MY_NAME + 'noRoot.csv'
    
            Outfile = open(datasheet, "a")
            
            Outfile.write('\n' + file[-14:-11] + ',' + file[-10:-7] + ',' + 
             file[-6:-4] + ',' + str(t) + ',' + 'BAD TESTTUBE')
    
            Outfile.close()
            
        else:
            
            tkmb.showinfo('Window', 'Note taken. Output to contour file. Exit the window please.')
            
            data = dataframe[(dataframe['Index'] == 2)]
    
            data = data.iloc[0].values
            
            t = int(data[3])
            
            datasheet = MY_NAME + 'badContour.csv'
    
            Outfile = open(datasheet, "a")
            
            Outfile.write('\n' + file[-14:-11] + ',' + file[-10:-7] + ',' + 
             file[-6:-4] + ',' + str(t) + ',' + 'BAD CONTOUR')
    
            Outfile.close()
        
    # pull out the tube contents
    tube_contents = contents[num]

    # pull the file name from tube_contents
    file = tube_contents[0]

    # pull contours from tube_contents
    contours = tube_contents[1]

    # pull the dataframe from tube_contents
    dataframe = tube_contents[2]

    # variables for width and height of widgets
    x_width = 250
    y_height = 500
        
    # initalize the window
    root = tk.Tk()
    
    # panel for normal image
    panelA = None
    # panel for contoured image
    panelB = None
    # panel for buttons
    panelC = None
    # panel for dataframe
    panelD = None
    
    # third panel will store buttons
    panelC = tk.Frame(root, height=y_height, width=x_width)
    panelC.pack(side="left", padx=10, pady=10)
    
    # change contour button to be put in panelC
    auto_choose_five_btn = tk.Button(panelC, text='Index 5?', command=get_fifth_index)
    auto_choose_five_btn.grid(row=0, pady=10)
    
    # get data button to be put in panelC
    get_data_btn = tk.Button(panelC, text='Get Data', command=get_data_now)
    get_data_btn.grid(row=1, pady=10)
    
    # get data entry Entry to be put in panelC by get data button
    get_data_entry_txtbox = tk.Entry(panelC)
    get_data_entry_txtbox.grid(row=1,column=1, pady=10)
    
    # no root button to be put in panelC
    no_root_btn = tk.Button(panelC, text='No Root', command=no_root_on_pic)
    no_root_btn.grid(row=2, pady=10)

    # read in the image
    image = cv2.imread(file)
    
    # read in the image again to put contours on
    contoured_image = cv2.imread(file)
    
    # draw the contours on the contoured_image
    cv2.drawContours(contoured_image, contours, -1, (0, 0, 255), 5)
    
    # loop through contours to put numbers on them
    for (i, contour) in enumerate(contours):
        
        # put numbers on image
        cv2.putText(contoured_image, str(i+1), findCentroid(contour), 
                    cv2.FONT_HERSHEY_PLAIN, 7.5, (0, 0, 0), 5)
    
    # turn images from BGR to RGB for tkinter
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    contoured_image = cv2.cvtColor(contoured_image, cv2.COLOR_BGR2RGB)
    
    # make PIL Images from an array
    image = Image.fromarray(image)
    contoured_image = Image.fromarray(contoured_image)
    
    # resize the images
    image = image.resize((x_width,y_height), Image.ANTIALIAS)
    contoured_image = contoured_image.resize((x_width,y_height), Image.ANTIALIAS)
    
    # make the images PhotoImages
    image = ImageTk.PhotoImage(image)
    contoured_image = ImageTk.PhotoImage(contoured_image)

    # the first panel will store our original image
    panelA = tk.Label(image=image)
    panelA.image = image
    panelA.pack(side="left", padx=10, pady=10)
     
    # the second panel will store the original image with contours on it
    panelB = tk.Label(image=contoured_image)
    panelB.image = contoured_image
    panelB.pack(side="left", padx=10, pady=10)
    
    # fourth contour will hold the dataframe
    panelD = tk.Frame(root, height=y_height, width=x_width)
    panelD.pack(side="left", padx=10, pady=10)
    
    panelD_text = tk.Text(panelD, height=int(y_height/10), width=int(x_width/7))
    
    panelD_text.insert(index=tk.INSERT, chars=file[-10:-4])
    panelD_text.insert(index=tk.INSERT, chars='\n\n')
    
    for row in dataframe.itertuples():
        
        index = int(row[3])
        
        area = float(row[1])
        
        panelD_text.insert(index=tk.INSERT, chars='Index = ')
        panelD_text.insert(index=tk.INSERT, chars=index)
        panelD_text.insert(index=tk.INSERT, chars='\n\nArea = ')
        panelD_text.insert(index=tk.INSERT, chars=area)
        panelD_text.insert(index=tk.INSERT, chars='\n___________________\n\n')
        
    panelD_text.pack()

    # kick off the GUI
    root.mainloop()
'''

'''
def Main(): 
    # call find_first_x_point function
    x_point = find_first_x_point(label=label_gray)
    
    # call get_subfilenames function (9 items)
    subfilenames = get_subfilenames(filename=filename)
    
    # call save_tube_images function
    save_tube_images(x_point=x_point, subfilenames=subfilenames)
    
    # call slim_contour_size function (9 items)
    slimmed_contours_list, thres_list = slim_contour_size(subfilenames=subfilenames)
    
    # create the root_dataframes (9 items)
    root_dataframes = create_dataframe(slimmed_contours_list=slimmed_contours_list, thres_list=thres_list, subfilenames=subfilenames)
    
    contents = [[],   [],   [],   [],    [],   [],   [],   [],     []]
            
    for i in range(0,9):
        
        contents[i].append(subfilenames[i])
        contents[i].append(slimmed_contours_list[i])
        contents[i].append(root_dataframes[i])
    
    # generate the GUI
    for i in range(0,9):
        
        GUI(contents, i)
        
Main()
