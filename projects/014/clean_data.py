#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 11 21:25:09 2018

@author: benjamin
"""

#imports for this program
import sys, cv2
import tkinter as tk
import pandas as pd
import numpy as np

filename = sys.argv[1]
save_filename = sys.argv[2]

df = pd.read_csv(filename)

plate = 0

trial = 0

accum = 0

day = 0

cleaned_data = pd.DataFrame(columns = ['Plate', 
                                                 'Trial', 'Day', 'Area'])

for row in df.itertuples():
    
    #print(plate, row[1])
    #print(plate != row[1])
    #print(trial != row[2])
    if ((plate == row[1]) and (trial == row[2]) and (day == row[3])):
        accum += row[5]
        print(1)
    else:
        if row[0] != 0:
            print(accum)
            
            # create a row to add to single_root_dataframe
            current_contour = pd.DataFrame(
                            {'Plate': [plate],
                             'Trial': [trial],
                             'Day': [day],
                             'Area': [accum]})
            
            # append current_contour
            cleaned_data = cleaned_data.append(current_contour)
            
            # reset the index
            cleaned_data.reset_index()
    
        plate = row[1]
        trial = row[2]
        day = row[3]
        accum = row[5]
        print(0)
        print()
        
    print(row[1], row[2], row[3])
    
print(accum)





print(cleaned_data)

cleaned_data.to_csv(save_filename, encoding='utf-8', index=False)