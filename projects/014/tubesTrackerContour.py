#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 24 08:11:17 2017
Objectives:
1. find a way to pull out the white labels from the image
2. determine the x value of the left side of the first tube
3. use that x value to split up test tubes (approx 1200 pixels wide) 
4. save each test tube as its own .tif
5. save file format as plate-seed-day.tif

@author: AdreAnna Ernest & Benjamin Zwiener
"""

#imports
import sys, cv2
import trackbar as tb
import sortContours as sc

filename = sys.argv[1]
k = 5
t = 120
#########################################################
####Section
####This section: loads the img as a BGR image; pulls the label out as BGR;
####loads the img as a grayscale image; pulls the label out as grayscale
#########################################################
#load image and pull our label
img = cv2.imread(filename)
labelColor = img[1900:3550,:]
#load grayscale image and pull out label
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
label = gray[1900:3550,:]
#########################################################
####Section
####This section: blurs image; thresholds image with OTSU; finds image 
####contours; then most importantly, determines a contour point for first tube
#########################################################
# blur image and use simple inverse binary thresholding to create
# a binary image
blur = cv2.GaussianBlur(label, (k, k), 0)
(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

#find contours of the white label
(_, contours, _) = cv2.findContours(binary, cv2.RETR_EXTERNAL, 
    cv2.CHAIN_APPROX_SIMPLE)

#this determines the contour point to make first test tube image based on x
#value to draw vertical line to slice first image
#   ***this is done on the white label of each plate image***
#this for loops through each contour of the large image
for contour in contours:
    #this for loops through each point in contour
    for points in contour:
        #this for loops through the points x/y values
        for point in points:
            y = point[1]
            if y > 1340 and y < 1375:
                x = point[0]
                if x < 600 and x  > 100:
                    xOriginPixel  = point
#########################################################
####Section
####This section: splits up initial filename; makes new filenames; 
####splits up testtubes and saves them as individual images
#########################################################
#split up filename and put it in a list 
#(e.g. 001-022-045-267-D2.tif --> [001,022,045,267,D2.tif]
list = filename.split('-')

#initialize subfilenames as an empty list
subfilenames = []

#use the list to create each pictures new file name
for label in list[1:-1]:
    file = list[0] + '-' + label + '-' + list[-1]
    #e.g. file = '001-045-D2.tif'
    subfilenames.append(file)

#this selects the x value of the xOriginPixel
xPoint = xOriginPixel[0]

#loop to save each test tube as a different file
#this does what was done above for each test tube   
for i in range(0, 9):
    #this allows us to get individual test tube images
    #we overlapped images by about 40 pixels  
    #this was done to make up for potential error in test tube edge detection
    testTube = img[6000:10700, xPoint-40 : xPoint + 1240]
    cv2.imwrite(subfilenames[i], testTube)
    #increment xPoint by 1200 since test tubes were around that wide
    xPoint = xPoint + 1200
#########################################################
####Section
####This section: 
#########################################################
goodContoursList = []
    
for file in subfilenames:
    #call trackbarContour function from trackbar.py file
    #parameters are filename and kernel size
    contours, t = tb.trackbarContour(file,k)
    
    #determine average contour size and get rid of small contours
    goodContours = []
    avg = 0
    for c in contours:
        avg += len(c)
       
    #print('total number of points', avg)
    avg /= len(contours)
    #print('the average number of points in contours is:', avg)
    
    for c in contours:
        
        # add a contour to goodContours if it is greater than 1.5 * avg
        if len(c) > avg * 1.5:
            goodContours.append(c)
    #append goodContours to a list to be used in the next for loop
    goodContoursList.append(goodContours)
            
    print('the number of good contours is', (len(goodContours)))
#########################################################
####Section
####This section:
#########################################################  
print(subfilenames)
for (i, file) in enumerate(subfilenames):
    sc.sortContours(file, goodContoursList[i], t)