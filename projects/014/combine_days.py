#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 11 23:02:12 2018

@author: benjamin
"""

#imports for this program
import sys, cv2
import tkinter as tk
import pandas as pd
import numpy as np

filename = sys.argv[1]
save_filename = sys.argv[2]

df = pd.read_csv(filename)

plate = 0

trial = 0

accum = 0

day = 0

cleaned_data = pd.DataFrame(columns = ['Plate', 'Trial', 'D2-Area', 'D4-Area', 'Growth'])

df_list = []

for row in df.itertuples():
    
    df_list.append(row)

length = int(len(df_list))
accum = 0
    
for i in range(length-1):
    if df_list[i][3] == 'D2' and df_list[i+1][3] == 'D4' and df_list[i][1] == df_list[i+1][1] and df_list[i][2] == df_list[i+1][2]:
            
        growth = df_list[i+1][4]-df_list[i][4]
        
        # create a row to add to single_root_dataframe
        current_contour = pd.DataFrame(
                        {'Plate': [df_list[i][1]],
                         'Trial': [df_list[i][2]],
                         'D2-Area': [df_list[i][4]],
                         'D4-Area': [df_list[i+1][4]],
                         'Growth': [growth]})
        
        # append current_contour
        cleaned_data = cleaned_data.append(current_contour)
        
        # reset the index
        cleaned_data.reset_index()
            
            
cleaned_data.to_csv(save_filename, encoding='utf-8', index=False)

    
    
    
    