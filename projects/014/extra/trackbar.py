#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 24 09:40:20 2017
Objectives:
1. 

@author: AdreAnna Earnest & Benjamin Zwiener
"""

import cv2
import numpy as np
#########################################################
####Function
####This function:
#########################################################
def adjustContour():
    global img, blur, t, mask, contours
    #create contours
    mask2 = mask.copy()
    (_, contours, _) = cv2.findContours(mask2, cv2.RETR_EXTERNAL, 
    cv2.CHAIN_APPROX_SIMPLE)
    # draw contours over original image
    cv2.drawContours(img, contours, -1, (0, 0, 255), 5)
#########################################################
####Function
####This function:
#########################################################
def fixedThresh():
    global img, blur, t, mask
    (t, mask) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY)
    img = np.copy(img2)
    adjustContour()
    cv2.imshow('threshold image', mask)
    cv2.imshow('image', img)
#########################################################
####Function
####This function:
#########################################################
def adjustThresh(v):
    global img, blur, t, mask
    t = v
    fixedThresh()
#########################################################
####Function
####This function:
#########################################################   
#THIS IS THE MAIN FUNCTION OF THE FILE!!!!
def trackbarContour(filename,kernel):
    global img, blur, t, img2
    img = cv2.imread(filename)
    img2 = cv2.imread(filename)
    gray = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
    blur = cv2.GaussianBlur(gray, (kernel,kernel), 0)
    cv2.namedWindow('threshold image', cv2.WINDOW_NORMAL)
    cv2.namedWindow('image', cv2.WINDOW_NORMAL)
    (t, mask) = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    t = int(t)
    cv2.createTrackbar('thresh', 'threshold image', t, 255, adjustThresh)
    fixedThresh()
    cv2.waitKey(0)
    #return contour values and threshold number
    return contours, t



