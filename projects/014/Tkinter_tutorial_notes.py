# -*- coding: utf-8 -*-
"""
Spyder Editor

This code was created while watching 'thenewboston' on youtube. He has
a series of videos introducing someone to Tkinter. The video series is named
Python GUI with Tkinter.
"""

import tkinter as tk

"""
# Chapter 1 - Introduction to Tkinter

# this creates the window
root = tk.Tk()

# this labels the window
theLabel = tk.Label(root, text = "This is too easy")

# this is the layout of theLabel
theLabel.pack()

# this makes the window show up continuously
root.mainloop()
"""
#######################################
"""
# Chapter 2 - Organizing your Layout

# this creates the window
root = tk.Tk()

# a frame is something like a container
topFrame = tk.Frame(root)
topFrame.pack() # pack the topFrame into the window

# a second container to hold widgets
bottomFrame = tk.Frame(root)
bottomFrame.pack(side=tk.BOTTOM) # pack this to the bottom of the window

# creates a button with button1 as its text
button1 = tk.Button(topFrame, text = 'button1', fg='red') # puts the button in topFrame with foreground color red
button1.pack(side=tk.LEFT)

# creates a button with button2 as its text
button2 = tk.Button(topFrame, text = 'button2', fg='blue') # puts the button in topFrame with foreground color blue
button2.pack(side=tk.TOP)

# creates a button with button3 as its text
button3 = tk.Button(bottomFrame, text = 'button3', fg='green') # puts the button in bottomFrame with foreground color green
button3.pack(side=tk.BOTTOM)

# creates a button with button4 as its text
button4 = tk.Button(bottomFrame, text = 'button4', fg='purple') # puts the button in bottomFrame with foreground color purple
button4.pack()

# this makes the window show up continuously
root.mainloop()
"""
#######################################
"""
# Chapter 3 - Fitting Widgets in your Layout

# this creates the window
root = tk.Tk()

# this creates a lable on root with text of one, background of red, and foreground of white
one = tk.Label(root, text="One", bg = "red", fg = "white")
one.pack()

# this creates a lable on root with text of two, background of green, and foreground of black
two = tk.Label(root, text="Two", bg="green", fg="black")
two.pack(fill=tk.X) # the fill param in pack makes your label grow to the x or y range

# this creates a lable on root with text of three, background of blue, and foreground of white
three = tk.Label(root, text="three", bg="blue", fg="white")
three.pack(side=tk.LEFT, fill=tk.Y) # the fill param in pack makes your label grow to the x or y range

# this makes the window show up continuously
root.mainloop()
"""
#######################################
"""
# Chapter 4 - 5 - Grid Layout

# this creates the window
root = tk.Tk()

# create labels and entries
label_1 = tk.Label(root, text='name')
label_2 = tk.Label(root, text='password')
entry_1 = tk.Entry(root)
entry_2 = tk.Entry(root)

# put labels into a grid layout
label_1.grid(row=0, sticky=tk.E) # this label is in row one and right (E) aligned
label_2.grid(row=1, sticky=tk.E) # this label is in row two and right (E) aligned

# put entries into a grid layout
entry_1.grid(row=0, column=1)
entry_2.grid(row=1, column=1)

# create a checkbox and add it to the grid layout
check_box = tk.Checkbutton(root, text='Keep me logged in')
check_box.grid(columnspan=2)

# this makes the window show up continuously
root.mainloop()
"""
#######################################
"""
# Chapter 6 - Binding Functions to Layouts

# this creates the window
root = tk.Tk()

# function to be bound to a widget
def printName():
    print('Hola')

# button that is bounded to the printName function
button_1 = tk.Button(root, text='print my name', command=printName)
button_1.pack()

# this makes the window show up continuously
root.mainloop()
"""
#######################################
"""
# Chapter 7 - Mouse Click Events

# this creates the window
root = tk.Tk()

# function for a left mouse click
def leftClick(event):
    print('left')

# function for a middle mouse click
def middleClick(event):
    print('middle')

# function for a right mouse click    
def rightClick(event):
    print('right')
    
# make a frame with a width of 300 and height of 300    
frame = tk.Frame(root, width=300, height=300)

# bind the left, middle, and right mouse click to their functions
frame.bind('<Button-1>', leftClick) # '<Button-1>' means left click
frame.bind('<Button-2>', middleClick) # '<Button-2>' means middle click
frame.bind('<Button-3>', rightClick) # '<Button-3>' means right click

# pack the frame into the window
frame.pack()

# this makes the window show up continuously
root.mainloop()
"""
#######################################
"""
# Chapter 8 - Using Classes

# this creates a class with buttons
class firstButtons:
    
    # intialize the object with this function
    def __init__(self, master):
        # create a frame from master...master becomes root later
        frame = tk.Frame(master)
        frame.pack()
        
        # create a button on the page and bind it to printMessage function
        self.printButton = tk.Button(frame, text='Print Message', command=self.printMessage)
        self.printButton.pack()
        
        # create a button to quit the GUI
        self.quitButton = tk.Button(frame, text='Quit', command=frame.quit)
        self.quitButton.pack()
        
    # function that prints a message    
    def printMessage(self):
        print('It worked')

# this creates the window
root = tk.Tk()

# creates the object using the first buttons class
b = firstButtons(root)

# this makes the window show up continuously
root.mainloop()
"""
#######################################
"""
# Chapter 9 - Creating Drop Down Menus

def doNothing():
    print('ok ok I won\'t...')
    
root = tk.Tk()

menu = tk.Menu(root)
root.config(menu=menu)

subMenu = tk.Menu(menu)
menu.add_cascade(label='File', menu=subMenu)

subMenu.add_command(label='New Project...', command=doNothing)
subMenu.add_command(label='New...', command=doNothing)
subMenu.add_separator()

subMenu.add_command(label='Save', command=doNothing)
subMenu.add_command(label='Save All', command=doNothing)

editMenu = tk.Menu(menu)
menu.add_cascade(label='Edit', menu=editMenu)
editMenu.add_command(label='Undo', command=doNothing)

# this makes the window show up continuously
root.mainloop()
"""
#######################################
"""
# Chapter 10 - Creating a Toolbar

def doNothing():
    print('ok ok I won\'t...')
    
root = tk.Tk()

menu = tk.Menu(root)
root.config(menu=menu)

subMenu = tk.Menu(menu)
menu.add_cascade(label='File', menu=subMenu)

subMenu.add_command(label='New Project...', command=doNothing)
subMenu.add_command(label='New...', command=doNothing)
subMenu.add_separator()

subMenu.add_command(label='Save', command=doNothing)
subMenu.add_command(label='Save All', command=doNothing)

editMenu = tk.Menu(menu)
menu.add_cascade(label='Edit', menu=editMenu)
editMenu.add_command(label='Undo', command=doNothing)

# **** Toolbar ****

toolbar = tk.Frame(root, bg='blue')

insertButt = tk.Button(toolbar, text='Insert Image', command=doNothing)
insertButt.pack(side=tk.LEFT, padx=2, pady=2)
printButt = tk.Button(toolbar, text='Print', command=doNothing)
printButt.pack(side=tk.LEFT, padx=2, pady=2)

toolbar.pack(side=tk.TOP, fill=tk.X)

# this makes the window show up continuously
root.mainloop()
"""
#######################################
"""
# Chapter 11 - Adding a Status Bar

def doNothing():
    print('ok ok I won\'t...')
    
root = tk.Tk()

menu = tk.Menu(root)
root.config(menu=menu)

subMenu = tk.Menu(menu)
menu.add_cascade(label='File', menu=subMenu)

subMenu.add_command(label='New Project...', command=doNothing)
subMenu.add_command(label='New...', command=doNothing)
subMenu.add_separator()

subMenu.add_command(label='Save', command=doNothing)
subMenu.add_command(label='Save All', command=doNothing)

editMenu = tk.Menu(menu)
menu.add_cascade(label='Edit', menu=editMenu)
editMenu.add_command(label='Undo', command=doNothing)

# **** Toolbar ****

toolbar = tk.Frame(root, bg='blue')

insertButt = tk.Button(toolbar, text='Insert Image', command=doNothing)
insertButt.pack(side=tk.LEFT, padx=2, pady=2)
printButt = tk.Button(toolbar, text='Print', command=doNothing)
printButt.pack(side=tk.LEFT, padx=2, pady=2)

toolbar.pack(side=tk.TOP, fill=tk.X)

# **** Status Bar ****

status = tk.Label(root, text='Preparing to do nothing...', bd=1, relief=tk.SUNKEN, anchor=tk.W)
status.pack(side=tk.BOTTOM, fill=tk.X)

# this makes the window show up continuously
root.mainloop()
"""
#######################################
"""
# Chapter 12 - Messagebox
import tkinter.messagebox as tkmb

root = tk.Tk()

tkmb.showinfo('Window Title', 'Monkeys can live up to 300 years.')

answer = tkmb.askquestion('Question 1', 'Do you like silly faces?')

if answer == 'yes':
    print(':D')

# this makes the window show up continuously
root.mainloop()
"""
#######################################
"""
# Chapter 13 - Shapes and Graphics

root = tk.Tk()

canvas = tk.Canvas(root, width=200, height=100)
canvas.pack()

blackLine = canvas.create_line(0,0,200,50)
redLine = canvas.create_line(0,100,200,50, fill='red')
greenBox = canvas.create_rectangle(25,25,130,60, fill='green')

# deletes the redLine when called
canvas.delete(redLine)
# deletes everything in the canvas when called
canvas.delete(tk.ALL)

# this makes the window show up continuously
root.mainloop()
"""
#######################################
"""
# Chapter 14 - Images and Icons
root = tk.Tk()

photo = tk.PhotoImage(file='name of image')
label = tk.Label(root, image=photo)
label.pack()

# this makes the window show up continuously
root.mainloop()
"""