#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 20 12:26:02 2017

@author: Benjamin Zwiener
"""

##############################################
import sys, cv2

filename = sys.argv[1]
x_start = int(sys.argv[2])
less = int(sys.argv[3])

img = cv2.imread(filename)
img = img[:,x_start:]

'''    
Description: This function is used to break apart the filename to create new
             filenames to save each tube.
             
Parameters:  filename - the filename of the image being processed
             
get_subfilenames function algorithm:
    
    I.  create a list by splitting filename with dashes (-)
    II. create empty subfilenames list to be returned
    III.loop through list from index 1 to the second last index
        1. index 0 is the plate number while the last index is the day.tif
    IV. create the file using the plate number (index 1), current item in
        the loop, and the day.tif
        1. append this to the subfilenames list
    V.  return the subfilenames list   
'''  
def get_subfilenames(filename):
    
    # create a list by splitting filename
    list = filename.split('-')
    
    # empty list to put new filenames in
    subfilenames = []

    # loop through list from index 1 up to but not including the last index
    for item in list[1:-1]:
        
        # create new filename
        file = list[0] + '-' + item + '-' + list[-1]
        # e.g. file = '001-045-D2.tif'
        
        # append the new filename to subfilenames
        subfilenames.append(file)
    
    # return the list to be used later
    return subfilenames
    
'''
Description: This function is used to save the tubes as separate images.

Parameters:  x_point - the x point found from the find_first_x_point function
             subfilenames - the list created from get_subfilenames function
             
save_tube_images function algorithm:
    
    I.  loop through a range on 9
        1. crop out the test tube area 
    II. write the cropped out img to a new file using the subfilenames list
    III.increment x_point by 1200 since the tube widths are around 1200
'''
def save_tube_images(subfilenames):
    x_point = 0
    
    # loops through 9 times with i = 0,1,2..7,8
    for i in range(0,9):
        
        if i == 0:
            subtract = 0
            add = 80
        elif i in (1,2,3,4,5,6,7):
            subtract = 80
            add = 80 
        else:
            subtract = 80
            add = less*(-1)
        
        # crop out the test_tube
        test_tube = img[6750:10700, x_point-subtract : x_point + 1200 + add]

        cv2.namedWindow("img", cv2.WINDOW_NORMAL)
        cv2.imshow("img", test_tube)
        cv2.waitKey(0)

        # write the test tube to a new tif image using current subfilename
        cv2.imwrite(subfilenames[i], test_tube)
        
        # increment x_point by 1200
        x_point += 1200
        
subfilenames = get_subfilenames(filename=filename)
save_tube_images(subfilenames=subfilenames)