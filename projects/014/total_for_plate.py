#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 11 22:47:43 2018

@author: benjamin
"""

#imports for this program
import sys, cv2
import tkinter as tk
import pandas as pd
import numpy as np

filename = sys.argv[1]
save_filename = sys.argv[2]

df = pd.read_csv(filename)

plate = 0

trial = 0

accum = 0

day = 0

cleaned_data = pd.DataFrame(columns = ['Plate', 
                                                 'Total'])

for row in df.itertuples():
    
    if plate == row[1]:
        accum+=1
        
    else:
        
        if row[0] != 0:
            
            # create a row to add to single_root_dataframe
            current_contour = pd.DataFrame(
                            {'Plate': [plate],
                             'Total': [accum]})
            
            # append current_contour
            cleaned_data = cleaned_data.append(current_contour)
            
            # reset the index
            cleaned_data.reset_index()
        plate = row[1]
        accum = 1
        
        if plate != i:
            missing_list.append(i)
        
cleaned_data.to_csv(save_filename, encoding='utf-8', index=False)
print(missing_list)