
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 31 13:06:21 2018

@author: diva

Authors: Brea and Catie 
This code is for measuring the plants area to compare the controls and the ones being touched. 

"""
#import the things
import numpy as np
import sys
import cv2

 
#get this from the command line
filename = sys.argv[1]

#found this kernel size worked well for all the images
k = 5

#open the image in color
img = cv2.imread(filename)

# Convert BGR to HSV to get the lower limits and upper limits of the color green
#researched on the google
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

lower_limit = np.array([30,60,20]) 
upper_limit = np.array([75,255,255])

mask = cv2.inRange(hsv, lower_limit, upper_limit)

mak = cv2.bitwise_and(img, img, mask = mask)

#show the images so we can make sure it is doing what we want it to do
#cv2.namedWindow('mak', cv2.WINDOW_NORMAL)
#cv2.imshow('mak', mak)
#cv2.waitKey(0)

#gray scale and blur so we can threshold
gray = cv2.cvtColor(mak, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(gray, (k,k), 0)

#create threshold to get rid of all extra gray 
(t, masklayer) = cv2.threshold(blur, 35, 255, cv2.THRESH_BINARY)

mask = cv2.merge([masklayer, masklayer, masklayer])

#putting the mask on the image
plant = cv2.bitwise_and(mak, mask)

#show the image to check that everything is correct
#cv2.namedWindow("thresh", cv2.WINDOW_NORMAL)
#cv2.imshow("thresh", plant)
#cv2.waitKey(0)

#converting it to gray scale so we can contour the image
gplant = cv2.cvtColor(plant, cv2.COLOR_BGR2GRAY)

#spilt the gray scale image into different rows to find the area, perimeter, and pixel count easier and so you can find the more direct numbers.
row1 = cv2.countNonZero(gplant[:780, :])
row2 = cv2.countNonZero(gplant[780:1530, :])
row3 = cv2.countNonZero(gplant[1530:, :])

print (filename, 'Pixel Count', int(row1/3), int(row2/3), int(row3/3), sep = ',')

#contours so we can find the plants in each row 

(_, contoursTop, _) = cv2.findContours(gplant[:780,:], cv2.RETR_EXTERNAL, 
    cv2.CHAIN_APPROX_SIMPLE)
(_, contoursMid, _) = cv2.findContours(gplant[780:1530, :], cv2.RETR_EXTERNAL, 
    cv2.CHAIN_APPROX_SIMPLE)
(_, contoursBot, _) = cv2.findContours(gplant[1530:, :], cv2.RETR_EXTERNAL, 
    cv2.CHAIN_APPROX_SIMPLE)

#adding mask on plant image to make surrounding things all black. 
BLplant = np.zeros(plant.shape, dtype = 'uint8')

#combining the BLplant mask to the plant image
Bplant = cv2.bitwise_and(plant, BLplant)

#creating a list of the top, middle, and bottom rows of the image so that it is easier to read. 
Clist = [contoursTop,contoursMid,contoursBot]

#counting average perimeter of the plants for each row in the image (top, middle, and bottom rows)
perimeter = []
for contours in Clist:
    avg = 0
    for c in contours:
        avg += cv2.arcLength(c, True)
    avg /= len(contours)
    perimeter.append(avg)
    
print(filename, 'perimeter', int(perimeter[0]), int(perimeter[1]), int(perimeter[2]), sep = ',')

#counting average areas of the plants for each row in the image 
cA = []
for contours in Clist:
	avg = 0
	for c in contours:
		M = cv2.moments(c)
		avg += cv2.contourArea(c)
	avg /= len(contours)/3
	cA.append(avg)

print (filename, 'Areas', int(cA[0]), int(cA[1]), int(cA[2]), sep = ',')

















































