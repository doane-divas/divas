#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 31 13:06:21 2018

@author: diva
"""

import numpy as np
import sys
import cv2

filename = sys.argv[1]
t = int(sys.argv[2])

img = cv2.imread(filename)
'''
mask = np.zeros(img.shape, dtype = "uint8")

#Draw white rectangles over the plants and apply the mask
#2460x2460

#starting location for mask to put on the white image
x = 240
y = 100


#draw rectangles over boxes
for row in range(3):
	for column in range(3):
		cv2.rectangle(mask, (x,y), (x + 580, y + 660), (255,255,255), -1)
		x += 720
	
	y += 720
	x = 200	

# Apply the mask and display the result
maskedImg = cv2.bitwise_and(img, mask)
'''

# Convert BGR to HSV
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

lower_limit = np.array([30,60,20]) 
upper_limit = np.array([75,255,255])

mask = cv2.inRange(hsv, lower_limit, upper_limit)

mak = cv2.bitwise_and(img,img,mask= mask)

cv2.namedWindow('img', cv2.WINDOW_NORMAL)
cv2.imshow('img', img)

cv2.namedWindow('mak', cv2.WINDOW_NORMAL)
cv2.imshow('mak', mak)
cv2.waitKey(0)

blur = cv2.cvtColor(mak, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(blur, (5,5), 0)

#create threshold
(t, masklayer) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY_INV)


cv2.namedWindow("thresh", cv2.WINDOW_NORMAL)
cv2.imshow("thresh", blur)
cv2.waitKey(0)


(_, contours, _) = cv2.findContours(blur, cv2.RETR_EXTERNAL, 
    cv2.CHAIN_APPROX_SIMPLE)
'''
cv2.drawContours(img, contours, -1, (255,255,255), 5)

cv2.namedWindow('output', cv2.WINDOW_NORMAL)
cv2.imshow('output', blur)
cv2.waitKey(0)
'''
# create all-black mask image
mask = np.zeros(img.shape, dtype="uint8")

# draw white rectangles for each object's bounding box
for c in contours:
    (x, y, w, h) = cv2.boundingRect(c)
    cv2.rectangle(mask, (x, y), (x + w, y + h), (255, 255, 255), -1)
    
    #if len(c) > avg / 2:
        
# apply mask to the original image
img = cv2.bitwise_and(mak, mask)

# display masked image
cv2.namedWindow("output2", cv2.WINDOW_NORMAL)
cv2.imshow("output2", img)
cv2.waitKey(0)


























































'''
r = maskedImg.copy()
# set blue and green channels to 0
r[:, :, 0] = 0
r[:, :, 1] = 0

# RGB - Red
cv2.namedWindow('R-RGB', cv2.WINDOW_NORMAL)
cv2.imshow('R-RGB', r)

cv2.waitKey(1000)
'''
'''
edgeX = cv2.Sobel(r, cv2.CV_16S, 1, 0)
edgeY = cv2.Sobel(r, cv2.CV_16S, 0, 1)

#convert back to 8-bit, unsigned numbers and combine edge images
edgeX = np.uint8(np.absolute(edgeX))
edgeY = np.uint8(np.absolute(edgeY))
edge = cv2.bitwise_or(edgeX, edgeY) #if one has both or either witih another then their is a edge 

 #display edges
cv2.namedWindow('Xedges', cv2.WINDOW_NORMAL)
cv2.imshow('Xedges', edgeX)
cv2.waitKey(1000)
cv2.namedWindow('Yedges', cv2.WINDOW_NORMAL)
cv2.imshow('Yedges', edgeY)
cv2.waitKey(1000)
cv2.namedWindow("edges", cv2.WINDOW_NORMAL)
cv2.imshow("edges", edge)
cv2.waitKey(1000)

blur = cv2.cvtColor(edge, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(blur, (k,k), 0)

#create threshold
(t, masklayer) = cv2.threshold(blur, 180, 255, cv2.THRESH_BINARY_INV)
blur = cv2.bitwise_and(blur, masklayer)

cv2.namedWindow("final", cv2.WINDOW_NORMAL)
cv2.imshow("final", blur)
cv2.waitKey(0)


(_, contours, hierarchy) = cv2.findContours(blur, cv2.RETR_TREE, 
    cv2.CHAIN_APPROX_SIMPLE)

output = cv2.drawContours(edge, contours, -1, (255,0,0), 5)

cv2.namedWindow('output', cv2.WINDOW_NORMAL)
cv2.imshow('output', output)
cv2.waitKey(0)
'''