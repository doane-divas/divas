# imports
import cv2
import sys
import numpy as np
from matplotlib import pyplot as plt

filename = sys.argv[1]
k = 5
#t = int(sys.argv[2])

# read the image
img = cv2.imread(filename)
img2 = cv2.imread(filename)


rows, columns, channels = img.shape

for x in range(rows):
	for y in range(columns):
		if img2[x,y,0] < 200 and img2[x,y,1] < 200 and img2[x,y,2] < 230:
			#img2[x,y,:] = 0
		if img[x,y,1] < 60 or img[x,y,2] > 120 or img[x,y,0] > 50:
			img[x,y,:] = 0


cv2.namedWindow('original', cv2.WINDOW_NORMAL)
cv2.imshow('original', img)
cv2.waitKey(0)
#cv2.namedWindow('white', cv2.WINDOW_NORMAL)
#cv2.imshow('white', img2)
#cv2.waitKey(0)



#create the basic black image

mask = np.zeros(img.shape, dtype = "uint8")

#Draw white rectangles over the plants and apply the mask
#2460x2460


x = 240
y = 100
x2 = 780
y2 = 750

for row in range(3):
	for column in range(3):
		cv2.rectangle(mask, (x,y), (x + 580, y + 660), (255,255,255), -1)
		x += 720
	
	y += 720
	x = 200	

# Apply the mask and display the result
maskedImg = cv2.bitwise_and(img, mask)

	

#Grayscale and blur the new maskedImg
blur = cv2.cvtColor(maskedImg, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(blur, (k, k), 0)

#cv2.namedWindow("Masked Image Gray", cv2.WINDOW_NORMAL)
#cv2.imshow("Masked Image Gray", blur)
#cv2.waitKey(0)

#Threshold for the plant

(t, maskLayer) = cv2.threshold(blur, 180, 255, cv2.THRESH_BINARY_INV)

blur = cv2.bitwise_and(blur, maskLayer)

(t, binary) = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + 
	cv2.THRESH_OTSU)

