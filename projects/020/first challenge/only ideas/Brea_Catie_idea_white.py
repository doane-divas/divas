import numpy as np
import sys
import cv2

#define
filename = sys.argv[1]
k = int(sys.argv[2])
t = int(sys.argv[3])


#open image and read 
img = cv2.imread(filename)
cv2.namedWindow('original', cv2.WINDOW_NORMAL)
cv2.imshow('original', img)
cv2.waitKey(0) 




#create blur 
blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(blur, (k,k), 0)



#create threshold
(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY)

#combind threshold on image 
masks = cv2.merge([binary, binary, binary])

edgeX = cv2.Sobel(binary, cv2.CV_16S, 1, 0)
edgeY = cv2.Sobel(binary, cv2.CV_16S, 0, 1)

#convert back to 8-bit, unsigned numbers and combine edge images
edgeX = np.uint8(np.absolute(edgeX))
edgeY = np.uint8(np.absolute(edgeY))
edge = cv2.bitwise_or(edgeX, edgeY) #if one has both or either witih another then their is a edge 

# display edges
cv2.namedWindow('Xedges', cv2.WINDOW_NORMAL)
cv2.imshow('Xedges', edgeX)
cv2.waitKey(0)
cv2.namedWindow('Yedges', cv2.WINDOW_NORMAL)
cv2.imshow('Yedges', edgeY)
cv2.waitKey(0)
cv2.namedWindow("edges", cv2.WINDOW_NORMAL)
cv2.imshow("edges", edge)
cv2.waitKey(0)

#xreate contours 
(_, contours, hierarchy) = cv2.findContours(binary, cv2.RETR_TREE, 
    cv2.CHAIN_APPROX_SIMPLE)

cv2.drawContours(img, contours, -1, (255,0,0), 5)
cv2.namedWindow('output', cv2.WINDOW_NORMAL)
cv2.imshow('output', masks)
cv2.waitKey(0)
#display the mask image 
cv2.namedWindow('masks', cv2.WINDOW_NORMAL)
cv2.imshow('masks', masks)
cv2.waitKey(0)

#use the mask to select the interesting part of the image 
sel = cv2.bitwise_and(img, masks)

#display
cv2.namedWindow('selected', cv2.WINDOW_NORMAL)
cv2.imshow('selected', sel)
cv2.waitKey(0)


#crete a mask 
mask = np.zeros(img.shape, dtype = 'uint8')

#draw white triangle 
cv2.rectangle(mask, (219,99), (784,760), (255,255,255), -1)
cv2.rectangle(mask, (240,844), (800,1484), (255,255,255), -1)
cv2.rectangle(mask, (248,1608), (808,2236), (255,255,255), -1)
cv2.rectangle(mask, (942,102), (1536,736), (255,255,255), -1)
cv2.rectangle(mask, (960,844), (1540,1476), (255,255,255), -1)
cv2.rectangle(mask, (976, 1580), (1556, 2216), (255,255,255), -1)
cv2.rectangle(mask, (1704,72), (2244,704), (255,255,255), -1)
cv2.rectangle(mask, (1708,848), (2252, 1460), (255,255,255), -1)
cv2.rectangle(mask, (1716,1584), (2272,2204), (255,255,255), -1)

#apply mask 
maskedImg = cv2.bitwise_and(sel, mask)
#display
cv2.namedWindow('Masked Image', cv2.WINDOW_NORMAL)
cv2.imshow('Masked Image', maskedImg)
cv2.waitKey(0)
