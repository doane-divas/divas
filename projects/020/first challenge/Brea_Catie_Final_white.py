import numpy as np
import sys
import cv2

#define
filename = sys.argv[1]
k = 5
#t = int(sys.argv[3])


#open image and read 

img2 = cv2.imread(filename)

#Create mask
mask = np.zeros(img2.shape, dtype = 'uint8')

#draw white triangle 
cv2.rectangle(mask, (219,99), (784,760), (255,255,255), -1)
cv2.rectangle(mask, (240,844), (800,1484), (255,255,255), -1)
cv2.rectangle(mask, (248,1608), (808,2236), (255,255,255), -1)
cv2.rectangle(mask, (942,102), (1536,736), (255,255,255), -1)
cv2.rectangle(mask, (960,844), (1540,1476), (255,255,255), -1)
cv2.rectangle(mask, (976, 1580), (1556, 2216), (255,255,255), -1)
cv2.rectangle(mask, (1704,72), (2244,704), (255,255,255), -1)
cv2.rectangle(mask, (1708,848), (2252, 1460), (255,255,255), -1)
cv2.rectangle(mask, (1716,1584), (2272,2204), (255,255,255), -1)

#apply mask 

maskedImg2 = cv2.bitwise_and(img2, mask)


#cv2.namedWindow('Masked Image', cv2.WINDOW_NORMAL)
#cv2.imshow('Masked Image', maskedImg)
#cv2.waitKey(0)

#Spilt image into different color channels 
# Was a way to try to get the color spilt but took forever and might have not work. 
rows, columns, channels = img2.shape
#B_mean1 = list()
#G_mean1 = list()
#R_mean1 = list()
#for x in range(rows):
	#for y in range(columns):
		#b, g, r = cv2.split(img2)

		#ttl = img2.size / 3

		#B = float(np.sum(b)) / ttl
		#G = float(np.sum(g)) / ttl
		#R = float(np.sum(r)) / ttl

		
		#B_mean1.append(B)
		#G_mean1.append(G)
		#R_mean1.append(R)


#spliting the colors to get the green to stand out. 
#rows, columns, channels = img2.shape
for x in range(rows):
	for y in range(columns):
		if maskedImg2[x,y,0] < 200 and maskedImg2[x,y,1] < 200 and maskedImg2[x,y,2] < 230:
			maskedImg2[x,y,:] = 0
		#if maskedImg[x,y,1] < 60 or maskedImg[x,y,2] > 120 or maskedImg[x,y,0] > 50:
			#maskedImg[x,y,:] = 0

#display
cv2.namedWindow('original', cv2.WINDOW_NORMAL)
cv2.imshow('original', maskedImg2)
cv2.waitKey(0)




#create blur 
blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(blur, (k,k), 0)



#create threshold
(t, masklayer) = cv2.threshold(blur, 180, 255, cv2.THRESH_BINARY_INV)
blur = cv2.bitwise_and(blur, masklayer)
(t, binary) = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)























