# solutions to exercises on page 99 of Python Programming in Context
#
# Mark M. Meysenburg
# 11/17/2016
def scramble2Encrypt(plainText):
	evens = ''
	odds = ''
	for i in range(len(plainText)):
		if i % 2 == 0:
			evens = evens + plainText[i]
		else:
			odds = odds + plainText[i]
	return odds + evens
	
def scramble2Decrypt(cipherText):
	halfLength = len(cipherText) // 2
	odds = cipherText[:halfLength]
	evens = cipherText[halfLength:]
	plain = ''
	
	for i in range(halfLength):
		plain = plain + evens[i]
		plain = plain + odds[i]
		
	if len(odds) < len(evens):
		plain = plain + evens[-1]
	
	return plain
	
def scramble3Encrypt(plain):
	zero = ''
	one = ''
	two = ''
	for i in range(len(plain)):
		if i % 3 == 0:
			zero = zero + plain[i]
		elif i % 3 == 1:
			one = one + plain[i]
		else:
			two = two + plain[i]
			
	return two + one + zero
	
def scramble3Decrypt(cipher):
	oneT = len(cipher) // 3
	twoT = oneT + oneT
	if len(cipher) % 3 == 2:
		twoT = twoT + 1
	two = cipher[:oneT]
	one = cipher[oneT:twoT]
	zero = cipher[twoT:]
	plain = ''
	
	for i in range(oneT):
		plain = plain + zero[i]
		plain = plain + one[i]
		plain = plain + two[i]

	if len(cipher) % 3 == 1:
		for ch in zero[oneT:]:
			plain = plain + ch
	elif len(cipher) % 3 == 2:
		for i in range(oneT, len(zero)):
			plain = plain + zero[i]
			plain = plain + one[i]
	
	return plain
	
print(scramble2Encrypt('abababababab'))
print(scramble2Decrypt(scramble2Encrypt('abababababab')))
print(scramble2Encrypt('ababababababc'))
print(scramble2Decrypt(scramble2Encrypt('ababababababc')))
print(scramble2Encrypt('I do not like green eggs and ham'))
print(scramble2Decrypt(scramble2Encrypt('I do not like green eggs and ham')))
print(scramble2Encrypt('a'))
print(scramble2Decrypt(scramble2Encrypt('a')))
print(scramble2Encrypt(''))
print(scramble2Decrypt(scramble2Encrypt('')))

print()
print()

plain = 'The quick brown fox jumps over the lazy dog'
ciper = scramble2Encrypt(plain)
print('ciper = ' + ciper)
plain2 = scramble2Decrypt(ciper)
print('decrypted version = ' + plain2)

print()
print()

plain = 'abc'*4
print('plain = ' + plain)
cipher = scramble3Encrypt(plain)
print('encrypted = ' + cipher)
plain2 = scramble3Decrypt(cipher)
print('decrypted = ' + plain2)

print()
print()

plain = 'The quick brown fox jumps over the lazy dog'
print('plain = ' + plain)
cipher = scramble3Encrypt(plain)
print('encrypted = ' + cipher)
plain2 = scramble3Decrypt(cipher)
print('decrypted = ' + plain2)

print()
print()
plain = input('Enter plaintext: ')
print('plain = ' + plain + ", " + str(len(plain)) + ' characters')
cipher = scramble3Encrypt(plain)
print('encrypted = ' + cipher)
plain2 = scramble3Decrypt(cipher)
print('decrypted = ' + plain2)