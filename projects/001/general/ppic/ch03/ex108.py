# solutions to exercises on page 108 of Python Programming in Context
#
# Mark M. Meysenburg
# 11/18/2016

def rot13(message):
	cipher = ''
	for ch in message:
		ascii = (( (ord(ch) - ord('a')) + 13) % 26) + ord('a')
		cipher = cipher + chr(ascii)
	return cipher
	
def rotNEncrypt(plain, n):
	cipher = ''
	for ch in plain:
		ascii = (( (ord(ch) - ord('a')) + n) % 26) + ord('a')
		cipher = cipher + chr(ascii)
	return cipher
	
def rotNDecrypt(cipher, n):
	plain = ''
	
	for ch in cipher:
		ascii = (( (ord(ch) - ord('a')) - n) % 26) + ord('a')
		plain = plain + chr(ascii)
	return plain
	
message = input('Enter plaintext: ').lower().replace(' ', '')
print('ciphering ' + message)
cipher = rot13(message)
print('ciphered message: ' + cipher)
plain = rot13(cipher)
print('decrypted message: ' + plain)

print()
print()
message = input('Enter plaintext for rotN: ').lower().replace(' ', '')
n = int(input('Enter n for rotN: '))
cipher = rotNEncrypt(message, n)
print('ciphertext = ' + cipher)
plain = rotNDecrypt(cipher, n)
print('plaintext = ' + plain)
