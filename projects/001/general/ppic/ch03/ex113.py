# solutions to exercises on page 113 of Python Programming in Context
#
# Mark M. Meysenburg
# 11/18/2016

def letterToIndex(ch):
	idx = ord(ch) - ord('a')
	if idx < 0:
		print('ERROR! ' + ch + " isn't in lowercase alphabet!")
	return idx
	
def indexToLetter(idx):
	ch = ''
	if idx < 0 or idx > 25:
		print('ERROR! idx value must be in [0, 25]')
	else:
		ch = chr(ord('a') + idx)
	return ch
	
def vignereIndex(keyLetter, plainTextLetter):
	keyIndex = letterToIndex(keyLetter)
	ptIndex = letterToIndex(plainTextLetter)
	newIdx = (ptIndex + keyIndex) % 26
	return indexToLetter(newIdx)
	
def encryptVignere(key, plainText):
	cipherText = ''
	keyLen = len(key)
	for i in range(len(plainText)):
		ch = plainText[i]
		if ch == ' ':
			cipherText = cipherText + ch
		else:
			cipherText = cipherText + vignereIndex(key[i % keyLen], ch)
	return cipherText
	
def undoVig(keyLetter, ctLetter):
	keyIndex = letterToIndex(keyLetter)
	ctIndex = letterToIndex(ctLetter)
	newIdx = (ctIndex - keyIndex) % 26
	return indexToLetter(newIdx)
	
def decryptVignere(key, cipherText):
	plainText = ''
	keyLen = len(key)
	for i in range(len(cipherText)):
		ch = cipherText[i]
		if ch == ' ':
			plainText = plainText + ' '
		else:
			plainText = plainText + undoVig(key[i % keyLen], ch)
	return plainText
	
message = input('Enter message: ').lower()
key = input('Enter key: ')
cipher = encryptVignere(key, message)
print('Ciphertext: ' + cipher)
plain = decryptVignere(key, cipher)
print('Recovered plaintext: ' + plain)