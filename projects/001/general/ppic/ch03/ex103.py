# solutions to exercises on page 103 of Python Programming in Context
#
# Mark M. Meysenburg
# 11/17/2016
import random

def removeDuplicates(password):
	rv = ''
	
	for ch in password:
		if ch not in rv:
			rv = rv + ch
	
	return rv
	
def removeChar(string, idx):
	return string[:idx] + string[idx + 1:]
	
def makeKeyFromPassword(password):
	alpha = 'abcdefghijklmnopqrstuvwxyz'
	password = removeDuplicates(password)
	last = password[-1]
	lastIdx = alpha.index(last)
	beforeLast = alpha[:lastIdx]
	afterLast = alpha[lastIdx + 1:]
	key = password
	for ch in afterLast:
		if ch not in password:
			key = key + ch
	for ch in beforeLast:
		if ch not in password:
			key = key + ch
			
	return key

def makeKey():
	chars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
	for i in range(0, 26):
		t = chars[i]
		idx = random.randint(0, 25)
		chars[i] = chars[idx]
		chars[idx] = t
	return ''.join(chars)
	
def substitutionEncrypt(plain, key):
	alpha = 'abcdefghijklmnopqrstuvwxyz'
	plain = plain.lower().replace(' ','')
	cipher = ''
	
	for ch in plain:
		i = alpha.find(ch)
		cipher = cipher + key[i]
	
	return cipher
	
def substitutionDecrypt(cipher, key):
	alpha = 'abcdefghijklmnopqrstuvwxyz'
	plain = ''
	
	for ch in cipher:
		i = key.find(ch)
		plain = plain + alpha[i]
		
	return plain
	
random.seed()
password = input('enter encryption password: ')
key = makeKeyFromPassword(password)
print('key = ' + key)