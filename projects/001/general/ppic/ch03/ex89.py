# solutions to exercises on page 89 of Python Programming in Context
#
# Mark M. Meysenburg
# 11/17/2016
name = 'mark m. meysenburg'
print('first name: ' + name[:4])
print('last name: ' + name[8:])
print('military name: ' + name[8:] + ', ' + name[:4])
print('length of first name: ' + str(len(name[:4])))

s = 's'
p = 'p'
mis = 'm' + ( 'i' + s*2)*2 + 'i' + p*2 + 'i'
print(mis)

name = 'Roy G Biv'
for i in range(len(name) + 1):
	print(name[:i])
