# solutions to exercises on page 89 of Python Programming in Context
#
# Mark M. Meysenburg
# 11/17/2016
mis = 'mississippi'
print('there are ' + str(mis.count('s')) + " s's in " + mis)

mox = mis.replace('iss', 'ox')
print('mox = ' + mox)

print("the first occurrence of 'p' in " + mis + ' is at location ' + str(mis.index('p')))

py = 'python'
print(py.upper().center(20))