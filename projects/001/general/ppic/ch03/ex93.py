# solutions to exercises on page 93 of Python Programming in Context
#
# Mark M. Meysenburg
# 11/17/2016

def charToInt(ch):
	if len(ch) != 1:
		print('ERROR! charToInt() only accepts single digits!')
		return -1
	
	return ord(ch) - ord('0')

def letterToIndex(ch):
	idx = ord(ch) - ord('a')
	if idx < 0:
		print('ERROR! ' + ch + " isn't in lowercase alphabet!")
	return idx
	
def indexToLetter(idx):
	ch = ''
	if idx < 0 or idx > 25:
		print('ERROR! idx value must be in [0, 25]')
	else:
		ch = chr(ord('a') + idx)
	return ch
	
def scoreToLetter(score):
	letter = ''
	if score >= 90:
		letter = 'A'
	elif score >= 80:
		letter = 'B'
	elif score >= 70:
		letter = 'C'
	elif score >= 60:
		letter = 'D'
	else:
		letter = 'F'
	return letter


print("ord('A') = " + str(ord('A')) + "; ord('a') = " + str(ord('a')))

digs = '0123456789'
for i in range(len(digs)):
	print('value for ' + digs[i] + " = " + str(charToInt(digs[i])))
	
print()
alpha = 'abcdefghijklmnopqrstuvwxyz'
for i in range(len(alpha)):
	print('index of ' + alpha[i] + ' = ' + str(letterToIndex(alpha[i])))
	
print()
for i in range(26):
	print('char for ' + str(i) + ' = ' + indexToLetter(i))
	
print()
for i in range(101):
	print('grade for score of ' + str(i) + ' = ' + scoreToLetter(i))