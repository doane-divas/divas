import math
import random
import turtle

# Listing 2.2 of PPIC: Archimedes' method of estimating pi
def archimedes(numSides):
	innerAngleB = 360.0 / numSides
	halfAngleA = innerAngleB / 2
	
	oneHalfSideS = math.sin(math.radians(halfAngleA))
	
	sideS = oneHalfSideS * 2
	
	polygonCircumference = numSides * sideS
	
	pi = polygonCircumference / 2
	
	return pi

# inspired by Listing 2.3 of PPIC	
def leibniz(n):
	acc = 0
	pm = 1
	num = 4
	den = 1
	
	for i in range(n):
		acc = acc + pm * (num / den)
		
		den = den + 2
		pm = -pm
		
	return acc

# inspired by Listing 2.4 of PPIC	
def wallis(n):
	prod = 1.0
	num = 2.0
	den = 1.0
	
	for i in range(n):
		lt = num / den
		den = den + 2.0
		rt = num / den
		
		prod = prod * lt * rt
		
		num = num + 2.0
		
	return 2 * prod

# Monte Carlo pi estimation inspired by Listing 2.5 from PPIC	
def montePi(n):
	numIn = 0
	
	for i in range(n):
		x = random.random()
		y = random.random()
		
		d = x * x + y * y
		
		if d <= 1.0 :
			numIn = numIn + 1
			
	pi = numIn / n * 4.0
	
	return pi
	
def graphicMontePi(n) :
	# graphical set up, axis lines
	scr = turtle.Screen()
	t = turtle.Turtle()
	
	scr.setworldcoordinates(-2, -2, 2, 2)
	
	t.up()
	t.goto(-1, 0)
	t.down()
	t.goto(1, 0)
	t.up()
	t.goto(0, -1)
	t.down()
	t.goto(0, 1)
	t.up()
	
	numIn = 0
	
	for i in range(n):
		x = random.random()
		y = random.random()
		
		d = x * x + y * y
		t.goto(x, y)
		
		if d <= 1.0 :
			numIn = numIn + 1
			t.color("blue")	
		else:
			t.color("red")
			
		t.dot()
			
	pi = numIn / n * 4.0
	
	scr.exitonclick()
	
	return pi
	
