# inspired by listing 1.3 of PPIC
def dSquare(turt, len) :
	for i in range(4):
		turt.forward(len)
		turt.right(90)

# inspired by listing 1.4 of PPIC
def dSpiral(turt, maxSide) :
	for sl in range(1, maxSide + 1, 5) :
		turt.forward(sl)
		turt.right(90)


# inspired by listing 1.5 of PPIC
def dTriangle(turt, length) :
	for i in range(3) :
		turt.forward(length)
		turt.right(120)

# inspired by listing 1.6 of PPIC
def dPolygon(turt, length, sides) :
	angle = 360 / sides
	for i in range(sides) :
		turt.forward(length)
		turt.right(angle)

# inspired by listing 1.7 of PPIC
def dCircle(turt, radius) :
	circ = 2 * 3.141592653589793 * radius
	length = circ / 360
	dPolygon(turt, length, 360)
