# -*- coding: utf-8 -*-
"""
dicprint.py -- noodling with printing a dictionary

@author: Mark M. Meysenburg
@version: 11/22/2016
"""
import random

def freq(rands):
    counts = {}
    for i in rands:
        counts[i] = counts.get(i, 0) + 1

    return counts

random.seed()
rands = []
for i in range(1000000):
    rands.append(random.randint(1, 100))

counts = freq(rands)

for i in counts:
    print('%d\t%d\n' % (i, counts[i]))