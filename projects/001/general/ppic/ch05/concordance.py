# -*- coding: utf-8 -*-
"""
Program to create a concordance for the novel 'War and Peace'

@author: Mark M. Meysenburg
@version 11/22/16
"""
import time

start = time.clock()
# read input file and build concordance dictionary
infile = open('wap.txt', 'r')
conc = {}
i = 0
for line in infile:
    i = i + 1
    words = line.split()
    for w in words:
        if w in conc:
            conc[w].append(i)
        else:
            conc[w] = [i]

infile.close()

# write concordance file
outfile = open('wap-conc.txt', 'w')
awords = list(conc.keys())
awords.sort()
for w in awords:
    outfile.write(w + ':' + str(conc[w]) + '\n')
outfile.close()
stop = time.clock()
elapsed = stop - start
print('completed in', elapsed)