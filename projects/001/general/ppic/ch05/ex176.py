# -*- coding: utf-8 -*-
"""
Link extraction exercise from page 176 of PPiC

@author: Mark M. Meysenburg
@version: 11/22/16
"""

import urllib.request

def makeLinkList(url):
    page = urllib.request.urlopen(url)
    pageText = (page.read()).decode('utf-8')
    linkList = []
    idx1 = pageText.find('<a')
    while idx1 >= 0:
        idx2 = pageText.find('/a>')
        link = pageText[idx1:idx2+3]
        if 'href' in link:
            linkList.append(link)
        pageText = pageText[idx2+3:]
        idx1 = pageText.find('<a')
    
    return linkList
    
linkList = makeLinkList('http://bethlehemlutherancrete.org/')
for link in linkList:
    print(link)