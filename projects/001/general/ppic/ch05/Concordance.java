import java.io.*;
import java.util.*;

public class Concordance {
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		try {
			Scanner in = new Scanner(new FileInputStream(new File("wap.txt")));
			TreeMap<String, LinkedList<Integer>> conc = new TreeMap<>();
			int i = 0;
			
			while(in.hasNextLine()) {
				String line = in.nextLine();
				i++;
				Scanner sline = new Scanner(line);
				while(sline.hasNext()) {
					String w = sline.next();
					if(conc.containsKey(w)) {
						conc.get(w).addLast(i);
					} else {
						LinkedList<Integer> list = new LinkedList<>();
						list.addLast(i);
						conc.put(w, list);
					}
				}
			}
			in.close();
			
			PrintWriter pw = new PrintWriter(new File("wap-c-j.txt"));
			Set<String> keys = conc.keySet();
			for(String w : keys) {
				pw.println(w + ": " + conc.get(w));
			}
			pw.close();
		}
		catch(FileNotFoundException fnfe) {
			System.err.println(fnfe.toString());
		}
		long stop = System.currentTimeMillis();
		
		System.out.println("Complete in " + (stop - start) + " ms");
	} // main
} // Concordance