# -*- coding: utf-8 -*-
"""
Exercises from page 163 of PPiC

@author: Mark M. Meysenburg
@version 11/22/16
"""

def makeTempTable():
    outf = open('tempconv.txt', 'w')
    mult = 5.0 / 9.0
    for f in range(-300, 213):
        c = (f - 32) * mult
        outf.write('%3.2f\t%3.2f\n' % (f, c))
    outf.close()
        
makeTempTable()