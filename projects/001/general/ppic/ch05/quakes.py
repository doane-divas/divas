# -*- coding: utf-8 -*-
"""
Program to examine earthquake data

@author: Mark M. Meysenburg
@version 11/22/2016
"""
import math

def makeMagnitudeList():
    qfile = open('all_month.csv', 'r')
    maglist = []
    qfile.readline() # eat the header line
    for line in qfile:
        vals = line.split(',')
        if len(vals[4]) > 0:  # don't try to add empty strings
            maglist.append(float(vals[4]))

    qfile.close()
    return maglist
    
def makeFreqTable(maglist):
    freqs = {}
    for x in maglist:
        freqs[x] = freqs.get(x, 0) + 1
    return freqs
    
def mean(maglist):
    return sum(maglist) / len(maglist)
    
def median(maglist):
    copylist = maglist[:]
    copylist.sort()
    if len(copylist) % 2 == 0:
        rm = len(copylist) // 2
        lm = rm - 1
        med = (copylist[lm] + copylist[rm]) / 2
    else:
        med = copylist[len(copylist)//2]
    return med
    
def mode(freqs):
    countlist = freqs.values()
    maxcount = max(countlist)
    modes = []
    for i in freqs:
        if freqs[i] == maxcount:
            modes.append(i)
    return modes
    
def stddev(maglist):
    xbar = mean(maglist)
    num = 0.0
    for x in maglist:
        v = x - xbar
        num = num + v * v
    num = num / (len(maglist) - 1)
    return math.sqrt(num)
    
maglist = makeMagnitudeList()
freqs = makeFreqTable(maglist)
fkeys = list(freqs.keys())
fkeys.sort()
for k in fkeys:
    print(k, freqs[k])
    
print('max:', max(maglist))
print('min:', min(maglist))
print('mean:', mean(maglist))
print('median:', median(maglist))
print('mode:', str(mode(freqs)))
print('std dev:', stddev(maglist))