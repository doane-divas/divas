# -*- coding: utf-8 -*-
"""
Exercises on page 170 of PPiC

@author: Mark M. Meysenburg
@version: 11/22/2016
"""
def makeQuakesByDate():
    qfile = open('all_month.csv', 'r')
    qfile.readline()    # eat the first line
    qbd = {}
    for line in qfile:
        vals = line.split(',')
        if len(vals[4]) > 0:    # only proceed for non-null mag
            date = (vals[0])[:10]
            mag = float(vals[4])
            if mag > 0:
                if date in qbd:
                    qbd[date].append(mag)
                else:
                    qbd[date] = [mag]
    return qbd
    
def makeCategorizedTable():
    qfile = open('all_month.csv', 'r')
    qfile.readline()    # eat the first line
    freqs = {}
    for line in qfile:
        vals = line.split(',')
        if len(vals[4]) > 0:    # only proceed if non-null
            mag = float(vals[4])
            if mag >= 8:
                freqs['6 great'] = freqs.get('6 great',0) + 1
            elif mag >= 7:
                freqs['5 strong'] = freqs.get('5 strong',0) + 1
            elif mag >= 6:
                freqs['4 major'] = freqs.get('4 major',0) + 1
            elif mag >= 5:
                freqs['3 moderate'] = freqs.get('3 moderate',0) + 1
            elif mag >= 4:
                freqs['2 light'] = freqs.get('2 light',0) + 1
            elif mag >= 3:
                freqs['1 minor'] = freqs.get('1 minor',0) + 1
            elif mag >= 0:
                freqs['0 micro'] = freqs.get('0 micro',0) + 1
    qfile.close()
    return freqs

freqs = makeCategorizedTable()
flist = list(freqs.keys())
flist.sort()
for f in flist:
    print(f, freqs[f])
    
qbd = makeQuakesByDate()
dlist = list(qbd.keys())
dlist.sort()
for d in dlist:
    print(d, str(qbd[d]))