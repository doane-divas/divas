# solutions to exercises on page 139 of Python Programming in Context
#
# Mark M. Meysenburg
# 11/21/2016
import random

def mode(list):
    counts = {}
    
    for i in list:
        counts[i] = counts.get(i, 0) + 1
    
    countlist = counts.values()
    maxcount = max(countlist)
    
    modelist = []
    for i in counts:
        if counts[i] == maxcount:
            modelist.append(i)
            
    return modelist

def mean(list):
    mean = sum(list) / len(list)
    return mean
    
def makeDictionary(names, scores):
    dic = {}

    for i in range(len(names)):
        dic[names[i]] = scores[i]
    
    return dic
    
def printAlphaTable(dic):
    names = list(dic.keys())
    names.sort()
    for n in names:
        print(n,dic[n],sep=':\t',end='\n')
        
def getScore(name, dic):
    score = -1
    if name in dic:
        score = dic[name]
    else:
        print('\tERROR!', name, 'is not in the dictionary!', sep=' ', end='\n')
    return score

names =     ['joe', 'tom', 'barb', 'sue', 'sally']
scores =    [   10,    23,     13,    18,      12] 

dic = makeDictionary(names, scores)

print("barb's score is:", dic['barb'], sep=' ', end='\n')

dic['john'] = 19
print('\nadding john:', str(dic), sep=' ', end='\n')

sortedlist = list(dic.values())
sortedlist.sort()
print('\nsorted list of scores:', str(sortedlist))

print('\naverage of scores: %2.2f\n' % mean(sortedlist))

dic['sally'] = 13
print("\nupdating sally's score:", str(dic), sep = ' ', end='\n')

del dic['tom']
print('dropping tom:', str(dic), sep=' ', end='\n')

print()
printAlphaTable(dic)

print('\nscore for sally:', getScore('sally', dic), sep=' ', end='\n')
print('\nscore for mark:', getScore('mark', dic), sep=' ', end='\n')


rlist = []
for i in range(1000):
    rlist.append(random.randint(0, 100))
    
print('\nmode for random list:', mode(rlist), sep = ' ', end= '\n')