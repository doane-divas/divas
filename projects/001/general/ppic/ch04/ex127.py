# solutions to exercises on page 127 of Python Programming in Context
#
# Mark M. Meysenburg
# 11/18/2016
import random

def shuffleList(list):
	nList = []
	
	# deep copy list
	olist = []
	for x in list:
		olist.append(x)
		
	for i in range(0, len(olist)):
		nList.append(olist.pop(random.randint(0, len(olist) - 1)))
	
	
	return nList

def wordsInSentence(sentence):
	wlist = sentence.split()
	return len(wlist)

# myList = [7, 9, 'a', 'cat', False]
# print(myList)

# myList.append(3.14)
# myList.append(7)
# myList.insert(3, 'dog')
# print(myList)
# print("'cat' is at: " + str(myList.index('cat')))
# print('there are ' + str(myList.count(7)) + ' 7s in the list')
# myList.remove(7)
# print(myList)
# print('there are now ' + str(myList.count(7)) + ' 7s in the list')
# idx = myList.index('dog')
# myList.pop(idx)
# print(myList)

# print() 
# print()
# print('the quick brown fox'.split())
# print('mississippi'.split('i'))

# print()
# print()
# sentence = input('Enter a sentence: ')
# print('there are ' + str(wordsInSentence(sentence)) + ' words in that.')

random.seed()
myList = [1,2,3,4,5,6,7,8,9,10]
list = shuffleList(myList)
print(myList)
print(list)
