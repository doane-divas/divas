# program to determine complexity differences between iteration by index
# versus interation by item
#
# Mark M. Meysenburg
# 11/18/2016
import random
import time

# create random list of n floats in [0, 1)
def makeRandoms(n):
	list = []
	i = 0
	while i < n:
		list.append(random.random())
		i = i + 1
	return list


def maxByIndexRange(list): 
	n = len(list)
	max = list[0]
	for i in range(1, n):
		if list[i] > max:
			max = list[i]
	return max
	
def maxByIndexWhile(list):
	n = len(list)
	i = 1
	max = list[0]
	while i < n:
		if list[i] > max:
			max = list[i]
		i = i + 1
	return max
	
def maxByItem(list):
	max = list[0]
	for x in list:
		if x > max:
			max = x
	return max


random.seed()	
list = makeRandoms(1000000)
start = time.time()
for i in range(1000):
	m = maxByIndexRange(list)
end = time.time()
print('max by index, range: ' + str(end - start))

start = time.time()
for i in range(1000):
	m = maxByIndexWhile(list)
end = time.time()
print('max by index, while: ' + str(end - start))

start = time.time()
for i in range(1000):
	m = maxByItem(list)
end = time.time()
print('max by item: ' + str(end - start))

