# solutions to exercises on page 134 of Python Programming in Context
#
# Mark M. Meysenburg
# 11/21/2016

import random

def mean(list):
    mean = sum(list) / len(list)
    return mean
    
def median(list):
    copylist = list[:]
    copylist.sort()
    if len(copylist) % 2 == 0 :
        rightmid = len(copylist) // 2
        leftmid = rightmid - 1
        median = (copylist[leftmid] + copylist[rightmid]) / 2
    else:
        mid = len(copylist) // 2
        median = copylist[mid]
    return median
    
def makeList():
    list = []
    for i in range(10):
        list.append(random.randint(10, 90))
    return list
    
random.seed()
ages = makeList()
print('Here are our ages: ' + str(ages))
print('\tmean = %3.2f\tmedian = %3.2f\n' % (mean(ages), median(ages)))

ages.append(39)
print('Here are our new ages: ' + str(ages))
print('\tmean = %3.2f\tmedian = %3.2f\n' % (mean(ages), median(ages)))
