# -*- coding: utf-8 -*-
"""
Noodling with exercises on page 145 of Python Programming in Context

@author: mark.meysenburg
@version: 11/21/2016
"""
import random
import time
import turtle

def frequencyChart(alist):
    countdic = {}

    for i in alist:
        countdic[i] = countdic.get(i, 0) + 1

    itemlist = list(countdic.keys())
    minitem = 0
    maxitem = len(itemlist) - 1

    countlist = countdic.values()
    maxcount = max(countlist)
    
    wn = turtle.Screen()
    chartT = turtle.Turtle()
    wn.setworldcoordinates(-1, -1, maxitem + 1, maxcount + 1)
    
    chartT.up()
    chartT.goto(0, 0)
    chartT.down()
    chartT.goto(maxitem, 0)
    chartT.up()
    
    chartT.goto(-1, 0)
    #chartT.write("0", font=("Helvetica", 16, "bold"))
    
    for index in range(len(itemlist)):
        chartT.goto(index, -1)
       #chartT.write(str(itemlist[index]), font=("Helvetica", 16, "bold"))
        
        chartT.goto(index, 0)
        chartT.down()
        chartT.goto(index, countdic[itemlist[index]])
        chartT.up()
    wn.exitonclick()

def frequencyTableList(alist):
    counts = {}
    rlist = []

    for i in alist:
        counts[i] = counts.get(i, 0) + 1

    theitems = list(counts.keys())
    theitems.sort()
    
    for i in theitems:
        rlist.append((i, counts[i]))
        
    return rlist
        
def frequencyTablePrint(alist):
    counts = {}

    for i in alist:
        counts[i] = counts.get(i, 0) + 1

    theitems = list(counts.keys())
    theitems.sort()
    
    print("ITEM", "FREQUENCY")
    for i in theitems:
        print(i, "\t", counts[i])
        
def frequencyTableAltPrint(alist):
    print("ITEM", "FREQUENCY")
    slist = alist[:] # copy list
    slist.sort()  
    
    prev = slist[0]
    n= 0
    for curr in slist:
            if curr == prev:
                n = n + 1
                prev = curr
            else:
                print(prev, '\t', n)
                prev = curr
                n = 1
                
    print(curr, '\t', n)
    

def makeScores():
    scores = []
    names = ['Eric', 'Ross', 'Bryce', 'Adam', 'Rhett', 'Camden', 'Yianni', 'Alex', 'Mark', 'Ashby', 'Tram', 'Dominique', 'Maggie', 'Samantha', 'Madeline', 'Alan', 'Brendan', 'Christy', 'Jordan', 'Benjamin']
    for name in names:
        for i in range(5):
            scores.append((name, random.randint(0, 100)))
    return scores
    
def computeAvg(scores):
    gradeDic = {}
    for s in scores:
        gradeDic[s[0]] = gradeDic.get(s[0],0) + s[1]

    names = list(gradeDic.keys())
    names.sort()
    
    for name in names:
        print(name, '\t', str(gradeDic[name] / 5.0))
        

random.seed()
'''
scores = makeScores()
computeAvg(scores)
'''


alist = []
for i in range(1000000):
    d = int(random.gauss(0.5, 0.25) * 50)
    alist.append(d)
    
frequencyChart(alist)

'''
start = time.time()
frequencyTablePrint(alist)
stop = time.time()
elapsed = (stop - start)
print('time taken', elapsed)

start = time.time()
frequencyTableAltPrint(alist)
stop = time.time()
elapsed = (stop - start)
print('time taken', elapsed)

rlist = frequencyTableList(alist)
for i in rlist:
    print(str(i))
'''

