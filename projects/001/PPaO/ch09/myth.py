import numpy as np
import argparse
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True, help = "Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image, (5, 5), 0)

cv2.imshow("Image", image)

cv2.waitKey(0)

thresh = blurred.copy()
thresh[thresh < 137] = 0
thresh[thresh > 0] = 255
thresh = cv2.bitwise_not(thresh)

cv2.imshow("My Threshold", thresh)

cv2.waitKey(0)