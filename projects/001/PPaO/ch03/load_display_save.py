from __future__ import print_function # grabs print function if using Python 2.7
import argparse
import cv2

# parse arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True, help = "Path to the image")
args = vars(ap.parse_args()) # dictionary holding arguments

# load image
image = cv2.imread(args["image"]) # returns a NumPy array 

# display image information
print("width: {} pixels".format(image.shape[1]))
print("height: {} pixels".format(image.shape[0]))
print("channels: {}".format(image.shape[2]))

# display image and wait
cv2.imshow("Image", image)
cv2.waitKey(0)

# output image as a jpg
cv2.imwrite("newimage.jpg", image)
