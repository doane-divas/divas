import numpy as np
import argparse
import imutils
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True, help = "Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original", image)

# original, ugly translation code
#M = np.float32([[1, 0, 25], [0, 1, 50]])
#shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
#cv2.imshow("Shifted Down and Right", shifted)

#M = np.float32([[1, 0, -50], [0, 1, -90]])
#shifted = cv2.warpAffine(image, M, (image.shape[1], image.shape[0]))
#cv2.imshow("Shifted Up and Left", shifted)

# same thing with imutils convenience function
dr = imutils.translate(image, 25, 50)
cv2.imshow("Shifted Down and Right", dr)

cv2.waitKey(0)

ul = imutils.translate(image, -50, -90)
cv2.imshow("Shifted Up and Left", ul)

cv2.waitKey(0)
