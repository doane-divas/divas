import numpy as np
import argparse
import cv2

# load and show original image
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True, help = "Path to the image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
cv2.imshow("Original", image)

# apply not
neg = cv2.bitwise_not(image)
cv2.imshow("Negative", neg)

cv2.waitKey(0)
