import numpy as np
import cv2

# make a new image, all black
canvas = np.zeros((480, 640, 3), dtype = "uint8")

# draw lines
green = (0, 255, 0)
cv2.line(canvas, (0, 0), (639, 479), green)

red = (0, 0, 255)
cv2.line(canvas, (639, 0), (0, 479), red, 5)

# test display #1
cv2.imshow("drawing", canvas)
cv2.waitKey(0)

cv2.rectangle(canvas, (50, 100), (60, 200), green, -1)

# test display #2
cv2.imshow("drawing", canvas)
cv2.waitKey(0)

# concetric circles
canvas = np.zeros((300, 300, 3), dtype = "uint8")
(centerX, centerY) = (canvas.shape[1] // 2, canvas.shape[0] // 2)
white = (255, 255, 255)

for r in range(0, 175, 25):
	cv2.circle(canvas, (centerX, centerY), r, white)

cv2.imshow("Canvas", canvas)
cv2.waitKey(0)

# random, filled circles
for i in range(0, 25):
	radius = np.random.randint(5, high = 200)
	color = np.random.randint(0, high = 256, size = (3,)).tolist()
	pt = np.random.randint(0, high = 300, size = (2,))
	cv2.circle(canvas, tuple(pt), radius, color, -1)

cv2.imshow("Canvas", canvas)
cv2.waitKey(0)
