import cv2
import numpy as np
'''
# Load and display the original image
image = cv2.imread(filename = "titration.tiff")
cv2.namedWindow(winname = "original", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "original", mat = image)
cv2.waitKey(delay = 0)

# Create the basic black image 
mask = np.zeros(shape = image.shape, dtype = "uint8")

# Draw a white, filled rectangle on the mask image
cv2.rectangle(img = mask, pt1 = (283, 232), pt2 = (594,366), 
	color = (255, 255, 255), thickness = -1)

cv2.namedWindow("output", cv2.WINDOW_NORMAL)
cv2.imshow("output", mask)
cv2.waitKey(0)


'''

from PIL import Image
 
def crop(image_path, coords, saved_location):
    """
    @param image_path: The path to the image to edit
    @param coords: A tuple of x/y coordinates (x1, y1, x2, y2)
    @param saved_location: Path to save the cropped image
    """
    image_obj = Image.open(image_path)
    cropped_image = image_obj.crop(coords)
    cropped_image.save(saved_location)
    cropped_image.show()
 
 
if __name__ == '__main__':
    image = 'Titration25TimeLapse(1).wmv'
    crop(image, (283, 232,594,366 ), 'cropped.jpg')



