import cv2, sys
import os
import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

path = list(os.listdir('/home/diva/Desktop/divas/projects/028/BBBC010_v2_images'))

def cannyEdge():
    global image, minT, maxT
    edge = cv2.Canny(image = image, 
        threshold1 = minT, 
        threshold2 = maxT)

    cv2.imshow(winname = "edges", mat = edge)

def adjustMinT(v):
	global minT
	minT = v
	cannyEdge()

def adjustMaxT(v):
	global maxT
	maxT = v
	cannyEdge()

for filename in path:
	print('BBBC010_v2_images/' + filename)

	image = cv2.imread('BBBC010_v2_images/' + filename)

	cv2.imshow('original', image)

	cv2.waitKey(0)

	minT = 30
	maxT = 150

	# cv2.createTrackbar() does not support named parameters
	cv2.createTrackbar("minT", "edges", minT, 255, adjustMinT)
	cv2.createTrackbar("maxT", "edges", maxT, 255, adjustMaxT)

	cannyEdge()
	cv2.waitKey(delay = 0)

#w1's 0 was best
#w2's 5 was best
