import cv2, sys
import os
import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt

path = list(os.listdir('/home/diva/Desktop/divas/projects/028/BBBC010_v2_images'))

t = 0

for filename in path:

	if 'w1' in filename:

		print('BBBC010_v2_images/' + filename)

		img = cv2.imread('BBBC010_v2_images/' + filename)

		cv2.imshow('original', img)

		cv2.waitKey(0)

		#create binary image
		gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		blur = cv2.GaussianBlur(gray, (5, 5), 0)    
		(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
		new = cv2.merge([binary, binary, binary])

		cv2.imshow('binary', new)
		cv2.waitKey(0)
