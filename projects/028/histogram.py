import matplotlib
import matplotlib.pyplot as plt
import numpy as np

myresults = (6, 3, 3, 6, 3, 4, 2, 3, 6, 8, 2, 3, 13, 13, 13, 12, 13, 14, 12, 14, 13, 15, 12, 14)
theirresults = (3, 3, 2, 5, 3, 5, 3, 4, 3, 8, 5, 2, 14, 15, 15, 13, 14, 13, 12, 14, 14, 14, 11, 14)

ind = np.arange(len(myresults))  # the x locations for the groups
width = .45  # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(ind - width/2, myresults, width, label='My Results for # of Dead')
rects2 = ax.bar(ind + width/2, theirresults, width, label='Their Results for # of Dead')

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_ylabel('# Of Dead')
ax.set_title('Plate Ratio Comparison')
ax.set_xticks(ind)
ax.set_xticklabels(('A01', 'A02', 'A03', 'A04', 'A05', 'A06', 'A07', 'A08', 'A09', 'A10', 'A11', 'A12', 'A13', 'A14', 'A15', 'A16', 'A17', 'A18', 'A19', 'A20', 'A21', 'A22', 'A23', 'A24'))
ax.legend()


def autolabel(rects, xpos='center'):

    ha = {'center': 'center', 'right': 'left', 'left': 'right'}
    offset = {'center': 0, 'right': 1, 'left': -1}

    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(offset[xpos]*3, 3),  # use 3 points offset
                    textcoords="offset points",  # in both directions
                    ha=ha[xpos], va='bottom')

autolabel(rects1, "left")
autolabel(rects2, "right")

fig.tight_layout()

plt.show()

