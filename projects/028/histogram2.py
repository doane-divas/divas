import matplotlib
import matplotlib.pyplot as plt
import numpy as np

myresults = (9, 11, 12, 9, 12, 11, 12, 12, 9, 7, 13, 12, 1, 2, 2, 1, 1, 0, 0, 0, 1, 0, 0, 0)
theirresults = (12, 11, 13, 10, 12, 10, 11, 11, 12, 7, 10, 13, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0)

ind = np.arange(len(myresults))  # the x locations for the groups
width = .45  # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(ind - width/2, myresults, width, label='My Results for # of Alive')
rects2 = ax.bar(ind + width/2, theirresults, width, label='Their Results for # of Alive')

# Add some text for labels, title and custom x-axis tick labels, etc.
ax.set_ylabel('# Of Alive')
ax.set_title('Plate Ratio Comparison')
ax.set_xticks(ind)
ax.set_xticklabels(('A01', 'A02', 'A03', 'A04', 'A05', 'A06', 'A07', 'A08', 'A09', 'A10', 'A11', 'A12', 'A13', 'A14', 'A15', 'A16', 'A17', 'A18', 'A19', 'A20', 'A21', 'A22', 'A23', 'A24'))
ax.legend()


def autolabel(rects, xpos='center'):

    ha = {'center': 'center', 'right': 'left', 'left': 'right'}
    offset = {'center': 0, 'right': 1, 'left': -1}

    for rect in rects:
        height = rect.get_height()
        ax.annotate('{}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(offset[xpos]*3, 3),  # use 3 points offset
                    textcoords="offset points",  # in both directions
                    ha=ha[xpos], va='bottom')

autolabel(rects1, "left")
autolabel(rects2, "right")

fig.tight_layout()

plt.show()
