import cv2, sys
import os
import numpy as np
import cv2 as cv
import matplotlib.pyplot as plt

path = list(os.listdir('/home/diva/Desktop/divas/projects/028/BBBC010_v2_images'))

t = 5

for filename in path:

	if 'w1' in filename:

		print('BBBC010_v2_images/' + filename)

		img = cv2.imread('BBBC010_v2_images/' + filename)

		#cv2.imshow('original', img)

		#cv2.waitKey(0)

		#create binary image
		gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
		blur = cv2.GaussianBlur(gray, (5, 5), 0)    
		(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
		new = cv2.merge([binary, binary, binary])

		#find contours 
		(_,contours,_) = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

		dead = 0
		alive = 0

		#draw bounding boxes
		for c in contours:

			#creates the rotated rectangle
			rect = cv2.minAreaRect(c)
		
			#finds the four vertices of the rotated rect
			box = cv2.boxPoints(rect)
			box = np.int0(box)
		   		
			#draw rotated rectangle
			cv2.drawContours(img, [box], 0, (0, 0, 255))
		
			#print box coordinates
			print('box points: ', box)

			#declares points of rectangle
			(x, y), (width, height), angle = rect

			#we think it compares width to height of rotated rectangle 
			aspect_ratio = min(width, height) / max(width, height)

			#print('aspect ratio: ', aspect_ratio)
		
			#cutoff of dead or alive was generally at .15
			deathline = 0.15		
			
			#if less than .15 dead else alive
			if (aspect_ratio < deathline):
				dead += 1
				print('dead')
			
			else:
				alive += 1
				print('Alive')
			
		#draw contours
		cv2.drawContours(new, contours, -1, (0, 0, 255), 1)

		#display rotated rectangle
		cv2.namedWindow("Rotated Rectangle")
		cv2.imshow("Rotated Rectangle", img)
		cv2.waitKey(0)

outfile.close()

