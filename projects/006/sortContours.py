'''
 * Python program demonstraing how to sort contours using imutils.
 *
 * Mark Meysenburg, 5/26/2017
  
Modified by AdreAnna Earnest and Benjamin Zwiener on 5/27/2017
This program sorts the contours, numbers the contours, and finds the area
of each contour. After this, the area and index are added to a database
made from the pandas library. The database is printed for a visual and the
user is prompted to enter an index number. The inputted index number
pulls out the contour area to be added to csv file.
'''

import cv2
from imutils import contours as imc
import pandas as pd
#########################################################
####Function
####This function:
#########################################################
def findCentroid(contour):
    M = cv2.moments(contour)
    x = int(M['m10'] / M['m00'])
    y = int(M['m01'] / M['m00'])
    return (x, y)
#########################################################
####Function
####This function:
#########################################################
def sortContours(filename, contours, t):
    
    print('thresh number is:', t)
    print('press a key only once more')
    
    #load in csv datasheet for use in for loop later - this is used to write in
    #area data to the csv file
    datasheet = 'seedlingData.csv'
    Outfile = open(datasheet, "a")
    
    # read original image in color
    colorImage = cv2.imread(filename)
    
    # use imutils function to sort contours. See the source code at
    # https://github.com/jrosebr1/imutils/blob/master/imutils/contours.py
    # for details on the options for ordering
    (sortedContours, boundingBoxes) = imc.sort_contours(contours)
    
    #create a single contour for root (contour of interest)
    rootContour = pd.DataFrame(columns = ['Index', 'Area'])
    
    # draw sorted contours on the original image, one at a time, ignoring the 
    # small ones
    for (i, contour) in enumerate(sortedContours):
        
        cv2.drawContours(colorImage, sortedContours, i, (0, 0, 255), 5)
        
        # draw a black label with contour number
        cv2.putText(colorImage, str(i), findCentroid(contour), 
                    cv2.FONT_HERSHEY_PLAIN, 7.5, (0, 0, 0), 5)
        
        #print(i, "the length of c is:", len(contour))
        area = cv2.contourArea(contour)
        #print(i,"the area is:", area)
        
        #this line creates a dataframe file for each contour
        currentContour = pd.DataFrame(
                {'Index': [int(i)],
                'Area': [area]})
        
        rootContour = rootContour.append(currentContour)
        rootContour.reset_index()            
        
    # prepare a window for display
    cv2.imshow("image", colorImage)
    cv2.waitKey(0)
    print(rootContour)
    userInput = input('Enter Index Number: ')
    contourIndex = int(userInput)
    rootContour = rootContour[(rootContour['Index'] == contourIndex)]
    
    print(rootContour)
    selectedContour = rootContour.iloc[0].values
    print(selectedContour)
    area = selectedContour[0]
    print('Index 0', selectedContour[0])
    
    #save the area value for each contour into a csv file
    # to write to the data csv file
    Outfile.write('\n' + filename[-14:-11] + ',' + filename[-10:-7] + ',' + 
                 filename[-6:-4] + ',' + str(t) + ',' + str(area))
    
    Outfile.close()
    



        
