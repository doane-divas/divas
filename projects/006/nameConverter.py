#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 23 15:59:44 2017

This program will break apart a string using '-' as delimiters.


@author: Benjamin Zwiener & AdreAnna Ernest
"""

import sys

filename = sys.argv[1]

list = filename.split('-')

for label in list[1:-1]:
    file = list[0] + '-' + label + '-' + list[-1]
    print(file)