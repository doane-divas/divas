#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 24 11:59:45 2017

1) Create a threshold value for our individual test tube images
2) Find Contours for individual test tube images
3) Determine the number of pixels in the roots

YOU GUYS DO NOT REALLY NEED TO READ THIS FILE
YOU GUYS DO NOT REALLY NEED TO READ THIS FILE
YOU GUYS DO NOT REALLY NEED TO READ THIS FILE
YOU GUYS DO NOT REALLY NEED TO READ THIS FILE
YOU GUYS DO NOT REALLY NEED TO READ THIS FILE
YOU GUYS DO NOT REALLY NEED TO READ THIS FILE
YOU GUYS DO NOT REALLY NEED TO READ THIS FILE
YOU GUYS DO NOT REALLY NEED TO READ THIS FILE
YOU GUYS DO NOT REALLY NEED TO READ THIS FILE
YOU GUYS DO NOT REALLY NEED TO READ THIS FILE
YOU GUYS DO NOT REALLY NEED TO READ THIS FILE
YOU GUYS DO NOT REALLY NEED TO READ THIS FILE

@author: AdreAnna Ernest & Benjamin Zweiner
"""

import cv2, sys
import numpy as np
import imutils

#import images of individual test tube roots
filename = sys.argv[1]
t = 180

# read original image
img = cv2.imread(filename)

# create binary image
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(gray, (5, 5), 0)
(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
cv2.namedWindow("output", cv2.WINDOW_NORMAL)
cv2.imshow("output", binary)
cv2.waitKey(0)
new = cv2.merge([binary,binary,binary])


# find contours
(_, contours, _) = cv2.findContours(binary, cv2.RETR_TREE, 
    cv2.CHAIN_APPROX_SIMPLE)

new2 = cv2.merge([binary,binary,binary])

# print table of contours and sizes
print("Found %d objects." % len(contours))
for (i, c) in enumerate(contours):
    print("\tSize of contour %d: %d" % (i, len(c)))

# draw contours over original image
cv2.drawContours(new, contours, -1, (0, 0, 255), 5)
cv2.drawContours(new2, contours, -1, (0, 0, 255), 5)

# display original image with contours
cv2.namedWindow("output", cv2.WINDOW_NORMAL)
cv2.imshow("output", new)
cv2.waitKey(0)
cv2.namedWindow("output1", cv2.WINDOW_NORMAL)
cv2.imshow("output1", new2)
cv2.waitKey(0)