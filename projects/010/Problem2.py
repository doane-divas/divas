#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  6 21:01:18 2017

@author: M Tross and C Azaldegui
"""

#import tools
import sys, cv2
from imutils import contours as imc
import numpy as np

#set arguments for command line, this one inputs the image file we want to read
filename = sys.argv[1]

#read in image 
img = cv2.imread(filename, cv2.IMREAD_COLOR + cv2.IMREAD_ANYDEPTH)
cv2.namedWindow("96-well plate", cv2.WINDOW_NORMAL)
cv2.imshow("96-well plate", img)
cv2.waitKey(0)

#center coordinates of the well plate
coordinates = [(110,97), (184,97), (254,97), (324,97), (396,96), (466,97), 
               (537,96), (607,97), (680,96), (750,97), (820,96), (891,97), 
               (110,165),(184,165),(254,165),(324,165),(396,165),(466,165),
               (537,165),(607,165),(680,165),(750,166),(820,165),(891,165),
               (110,235),(184,235),(254,235),(324,235),(396,235),(466,235),
               (537,235),(607,235),(680,235),(750,235),(820,235),(891,235),
               (110,305),(184,305),(254,305),(324,305),(396,305),(466,305),
               (537,305),(607,305),(680,305),(750,305),(820,305),(891,305),
               (110,373),(184,373),(254,373),(324,373),(396,373),(466,373),
               (537,373),(607,373),(680,373),(750,372),(820,373),(891,373),
               (110,442),(184,442),(254,442),(324,442),(396,442),(466,442),
               (537,442),(607,442),(680,442),(750,443),(820,442),(891,442),
               (109,511),(184,511),(254,511),(324,511),(396,511),(466,511),
               (537,512),(607,512),(680,511),(750,511),(820,511),(891,511),
               (109,582),(184,582),(254,582),(324,581),(396,582),(466,582),
               (537,581),(607,582),(680,582),(750,581),(820,582),(891,582)]

#column names defined by coordinates in list               
H = coordinates[0:12]
G = coordinates[12:24]
F = coordinates[24:36]
E = coordinates[36:48]
D = coordinates[48:60]
C = coordinates[60:72]
B = coordinates[72:84]
A = coordinates[84:96]

# a lit of column names that each have the corresponding coordinates
columns = [H,G,F,E,D,C,B,A]               
                            
letters = ['H','G','F','E','D','C','B','A']               
 

#load in csv datasheet for use in for loop later - this is used to write in
#area data to the csv file
#datasheet = 'WellPlateData.csv'
#Outfile = open(datasheet, "a")
                           
#starting for-loop to go through coorinates and output RGB data
#start for lopp by going through each column               
for (e,column) in enumerate(columns):
    
    #for every column go through each point   
    for (i,point) in enumerate(column):
        
        #this defines the RGB data for each point
        RGBdata = img[point]
        
        #this splits the RGB data into individual points
        blue = RGBdata[0]
        green = RGBdata[1]
        red = RGBdata[2]
        
        #B = ('Column', e + 1, 'Row', i + 1, 'blue channel:', blue)
        #G = ('Column', e + 1, 'Row', i + 1, 'green channel:', green)
        #R = ('Column', e + 1, 'Row', i + 1, 'red channel:', red)
        
        #defining the output, couldn't get it to work******
        output = ('Column', e + 1, 'Row', i + 1)
        
        #prints the column by number, row number and channel with data
        print('Column', e + 1, 'Row', i + 1, 'blue channel:', blue)
        print('Column', e + 1, 'Row', i + 1, 'green channel:', green)
        print('Column', e + 1, 'Row', i + 1, 'red channel:', red)
        print()
            
        cv2.waitKey(0)
        
        
        #save the area value for each contour into a csv file
        # to write to the data csv file
        #Outfile.write('\n' + output + ',' + blue + ',' + green +',' + red + '\n')
        
        
    
#Outfile.close()
    
        
        

    

















