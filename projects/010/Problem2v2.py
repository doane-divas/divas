#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  6 21:01:18 2017

@author: M Tross and C Azaldegui
"""

#import tools
import sys, cv2
from imutils import contours as imc
import numpy as np
import math

#set arguments for command line, this one inputs the image file we want to read
filename = sys.argv[1]

#read in image 
img = cv2.imread(filename, cv2.IMREAD_COLOR + cv2.IMREAD_ANYDEPTH)
cv2.namedWindow("96-well plate", cv2.WINDOW_NORMAL)
cv2.imshow("96-well plate", img)
cv2.waitKey(0)

#center coordinates of the well plate
'''coordinates = [(110,97), (184,97), (254,97), (324,97), (396,96), (466,97), 
               (537,96), (607,97), (680,96), (750,97), (820,96), (891,97), 
               (110,165),(184,165),(254,165),(324,165),(396,165),(466,165),
               (537,165),(607,165),(680,165),(750,166),(820,165),(891,165),
               (110,235),(184,235),(254,235),(324,235),(396,235),(466,235),
               (537,235),(607,235),(680,235),(750,235),(820,235),(891,235),
               (110,305),(184,305),(254,305),(324,305),(396,305),(466,305),
               (537,305),(607,305),(680,305),(750,305),(820,305),(891,305),
               (110,373),(184,373),(254,373),(324,373),(396,373),(466,373),
               (537,373),(607,373),(680,373),(750,372),(820,373),(891,373),
               (110,442),(184,442),(254,442),(324,442),(396,442),(466,442),
               (537,442),(607,442),(680,442),(750,443),(820,442),(891,442),
               (109,511),(184,511),(254,511),(324,511),(396,511),(466,511),
               (537,512),(607,512),(680,511),(750,511),(820,511),(891,511),
               (109,582),(184,582),(254,582),(324,581),(396,582),(466,582),
               (537,581),(607,582),(680,582),(750,581),(820,582),(891,582)]'''
               
#coordinates for the wells beings used forthis problem             
wells = [(110,165),(184,165),(254,165),(324,165),(396,165),(466,165), #columnG 12-7
         (537,165),(607,165), #columnG 6-5
         (110,235),(184,235),(254,235),(324,235),(396,235),(466,235), #columnF 12-7
         (537,235),(607,235),#columnF 6-5
         (110,305),(184,305),(254,305),(324,305),(396,305),(466,305), #columnE 12-7
         (537,305),(607,305), #columnE 6-5
         (110,373),(184,373),(254,373),(324,373),(396,373),(466,373), #columnD 12-7
         (537,373),(607,373), #columnD 6-5
         (110,442),(184,442),(254,442),(324,442),(396,442),(466,442), #columnC 12-7
         (537,442),(607,442), #columnC 6-5
         (109,511),(184,511),(254,511),(324,511),(396,511),(466,511), #columnB 12-7
         (537,512),(607,512), #columnB 6-5
         (109,582),(184,582),(254,582),(324,581),(396,582),(466,582), #columnA 12-7
         (537,581),(607,582)] #columnA 6-5

#column names defined by coordinates in list               
G = wells[0:8]#lowest concentration
F = wells[8:16]
E = wells[16:24]
D = wells[24:32]
C = wells[32:40]
B = wells[40:48]
A = wells[48:56]#highest concentration



# a list of column names that each have the corresponding coordinates
columns = [G,F,E,D,C,B,A]               
                                          
#will let us make plots 
import matplotlib.pyplot as plt

#empty lists to put the R G and B values of the wells in
bluelist = []
greenlist = []       
redlist = []
                           
#starting for-loop to go through coorinates and output RGB data
#start for lopp by going through each column 
              
for (e,column) in enumerate(columns):
    
    #for every column go through each point   
    for (i,point) in enumerate(column):
        
        #this defines the RGB data for each point
        RGBdata = img[point]
        
        #this splits the RGB data into individual points
        blue = RGBdata[0]
        green = RGBdata[1]
        red = RGBdata[2]
     
        #put R G and B values into a list for that column      
        bluelist.append(blue)
        greenlist.append(green)
        redlist.append(red)
    
    
#print(bluelist)
#print(greenlist)
#print(redlist)
    
#for each color on the plate(2), take four values that correspond to that color, 
#take the average and put that in another list.average function below
Color1Blue = bluelist[0:4] + bluelist[8:12] + bluelist[16:20] + bluelist[24:28] + bluelist [32:36] + bluelist[40:44] + bluelist[48:52]
Color1Red = redlist[0:4] + redlist[8:12] + redlist[16:20] + redlist[24:28] + redlist [32:36] + redlist[40:44] + redlist[48:52]
Color1Green = greenlist[0:4] + greenlist[8:12] + greenlist[16:20] + greenlist[24:28] + greenlist [32:36] + greenlist[40:44] + greenlist[48:52]        

Color2Blue = bluelist[4:8] + bluelist[12:16] + bluelist[20:24] + bluelist[28:32] + bluelist [36:40] + bluelist[44:48] + bluelist[52:56]
Color2Red = redlist[4:8] + redlist[12:16] + redlist[20:24] + redlist[28:32] + redlist [36:40] + redlist[44:48] + redlist[52:56]
Color2Green = greenlist[4:8] + greenlist[12:16] + greenlist[20:24] + greenlist[28:32] + greenlist [36:40] + greenlist[44:48] + greenlist[52:56]        
   
#a list of the lists of R G and B values
channels = [Color1Blue,Color1Green,Color1Red,Color2Blue,Color2Green,Color2Red]

#list to put all the averaged R G and B values (42 total)
concentrations = []

#will need 6 plots, so runnign through 6 channels to get avg
for channel in channels:
    
    
    #getting the abs values to plot against concentrations
    C1 = - math.log((sum(channel[0:4]) / len(channel[0:4]))/65536)
    concentrations.append(C1)
    C2 = - math.log((sum(channel[4:8]) / len(channel[4:8]))/65536)
    concentrations.append(C2)
    C3 = - math.log((sum(channel[8:12]) / len(channel[8:12]))/65536)
    concentrations.append(C3)
    C4 = - math.log((sum(channel[12:16]) / len(channel[12:16]))/65536)
    concentrations.append(C4)
    C5 = - math.log((sum(channel[16:20]) / len(channel[16:20]))/65536)
    concentrations.append(C5)
    C6 = - math.log((sum(channel[20:24]) / len(channel[20:24]))/65536)
    concentrations.append(C6)
    C7 = - math.log((sum(channel[24:28]) / len(channel[24:28]))/65536)
    concentrations.append(C7)

#used to check that the number of objects was correct    
#print(concentrations)
#print(len(concentrations))

#defining lists that contain the abs values in correct order        
B1 = concentrations[0:7]
G1 = concentrations[7:14]
R1 = concentrations[14:21]
B2 = concentrations[21:28]
G2 = concentrations[28:35]
R2 = concentrations[35:42]   

print('1st Color Blue channel:',B1)
print('1st Color Green channel:',G1)
print('1st Color Red channel:',R1)
print()
print('2nd Color Blue channel:', B2)
print('2nd Color Green channel:', G2)        
print('2nd Color Red channel:', R2)           


      
     
#need to make plots with all info            
    
'''
            plt.plot([], [1,4,9,16], 'ro')
            plt.axis([0, 6, 0, 20])
            plt.show()
          '''  
    
        
        
        
        

    

















