# Assignment 3: Sorted Singly-Linked List

Modify the SLL class template (`SLL.h`) into a new class template called SortedSLL (saved in 
the file `SortedSLL.h`). Specifically:

* Change the `add()` method so that the elements of the list are always in non-decreasing order. 
For example, if the list contains ints, and we add the numbers [5, 4, 8, 0, 7, 6, 3, 1, 2, 9] to the 
list, in that order, the list looks like this at each stage:

```
[5]

[4, 5]

[4, 5, 8]

[0, 4, 5, 8]

[0, 4, 5, 7, 8]

[0, 4, 5, 6, 7, 8]

[0, 3, 4, 5, 6, 7, 8]

[0, 1, 3, 4, 5, 6, 7, 8]

[0, 1, 2, 3, 4, 5, 6, 7, 8]

[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```

* Change the `contains()` method so that it searches the list efficiently, i.e., it returns `-1` as 
soon as it can be sure that the value `key` is not found in the list.

* Modify the main function in `TestSLL.cpp`  to use the `SortedSLL` class instead of `SLL`.