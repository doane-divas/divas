# Assignment 1: PowerBall requency analysis

In this direcotry you will find a plain text file, *numbers.txt*, containing all of the winning 
Powerball lottery numbers from 11/1/1997 to 8/12/2015. Your task is to write a C++ program 
to perform some simple analysis on this data set.

Powerball lottery balls are numbered from 1 to 59 (inclusive), and in each drawing, six balls are 
chosen. The file has one line, with six integers, for each drawing. 

Write a program that reads input from the file. After all of the numbers have been read, the 
program should output the number of times each number *(1, 2, ..., 59)* occurred in the data 
set. The results you should see are in this directory as a second plain text file, *results.txt*. 
Note: the output should be one result per line, formatted like this:

i\tn

where *i* is the number *(1, 2, �, 59)*, *\t* is the tab character, and *n* is the number of 
times the ball *i* was seen in the input file.