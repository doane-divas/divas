'''
##############################################################################
The purpose of this code is to create a colormap image to easily estimate the 
concentration of free amines secreted by maize root. This quantification occurs
when the user compares the color of the maize root to the corresponding color 
on the intensity/concentration scale.

Truc Doan, AdreAnna Ernest, Danny Tran
##############################################################################
'''

#import libraries needed to run the code
import cv2, sys
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.ticker
import matplotlib as mpl
import numpy as np
import decimal

#enter image/filename into command line
filename = sys.argv[1]

#load the image and covert it to a grayscale image     
img = cv2.imread(filename)
    #grayscale = B*0.114 + G*0.587 + R*0.299
intensity = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)


"""
#Any pixel with concentration values less than concentration of 0 is configured
for row in fgray:
    for pixel in row:
        if int(pixel) >= 208.26:
                int(pixel) == 208.26
"""

#setting limits for heatmap
#intensity = np.clip(intensity, 0, 208)  

# Make plot with vertical (default) colorbar
fig, ax = plt.subplots()

colors = [(0,0,0), (1,0.2,0.2), (1,0.4,0.4), (1,0.6,0.2), (1,0.7,0.4), (1,1,0.2), (1,1,0.4), (0.6,1,0.2), (0.2,1,0.2), (0.2,1,0.6), (0.2,1,1), (0.2,0.6,1), (0.2,0.2,1), (0.6,0.2,1), (0.7,0.4,1), (0.8,0.6,1), (0.9,0.8,1), (1,1,1)]  # R -> G -> B
n_bins = 23  # Discretizes the interpolation into bins
cmap_name = 'my_list'

cm1 = mpl.colors.LinearSegmentedColormap.from_list(
    cmap_name, colors, N=n_bins)
    # Fewer bins will result in "coarser" colomap interpolation

cmap=cm1
# define the bins and normalize
bounds = np.linspace(115.6836, 212.6684, 23)
norm = mpl.colors.BoundaryNorm(bounds, cmap.N)


#create the colormap image - cmap.cm___ selects the color of the map you want
#to use
colormapImg = ax.imshow(intensity, interpolation='nearest', cmap=cmap, norm=norm)
ax.set_title('Concentration of Free Amines Detected By Ninhydrin')

#create two color bars - 1 for intensity and 1 for concentration 
#Acknowledgement to Mike

cs = plt.colorbar(colormapImg, spacing = 'proportional', drawedges=True, extend='both', format = '%1i', norm=norm) 
cs2 = plt.colorbar(colormapImg, spacing = 'proportional', drawedges=True, extend='both', format = '%1i', norm=norm)

#np.clip(intensity,120.092,208.26)

#Placing the equivalent cnocentration hat the given intensity value on the colorbar 
#and displaying the colormap

#for cs. Concentration
tick_loc = [208.26, 203.8516, 199.4432, 195.0348, 190.6264, 186.218, 181.8096,
            177.4012, 172.9928, 168.5844, 164.176, 159.7676, 155.3592, 150.9508, 
            146.5424, 142.134, 137.7256, 133.3127, 128.9088, 124.5004, 120.092]
tick_labels = [0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.2, 2.4, 2.6, 2.8, 3.0, 3.2, 3.4, 3.6, 3.8, 4.0]

#tick_loc = [208.26, 200.26, 192.26, 184.26, 176.26, 168.26, 160.26, 152.26, 144.26, 136.26, 128.26, 120.26, 112.26]
#tick_labels = [0.00, 0.36, 0.73, 1.09, 1.45, 1.81, 2.18, 2.54, 2.90, 3.27, 3.63, 3.99, 4.36 ]
cs.locator     = matplotlib.ticker.FixedLocator(tick_loc)
cs.formatter   = matplotlib.ticker.FixedFormatter(tick_labels)
cs.update_ticks()
print(tick_labels)

#for cs2. Intensity
tick_loc2 = [208.26, 175, 150, 125, 100, 75, 50, 25, 0]
tick_labels2 = [tick_loc2[0],tick_loc2[1],tick_loc2[2],tick_loc2[3],tick_loc2[4],tick_loc2[5],tick_loc2[6],tick_loc2[7],tick_loc2[8]]
cs2.locator     = matplotlib.ticker.FixedLocator(tick_loc2)
cs2.formatter   = matplotlib.ticker.FixedFormatter(tick_labels2)
cs2.update_ticks()
print(tick_labels2)


#Getting day number
day = filename[-5]

#Adding labels and displaying the image
cs2.ax.set_ylabel('Intensity Value')
cs.ax.set_ylabel('Concentration of free amines (mM)')
#Fliping y axis so 0 starts at the bottom
plt.gca().invert_yaxis()

plt.suptitle = ('Day ' + str(day) + ' ninhydrin paper heatmap')
plt.xlabel('X Coordinates')
plt.ylabel('Y Coordinates')
plt.show()
