#include <cstdlib>

#include <iostream>

#include <fstream>





int main() {

	using namespace std;

	int *counts = new int[60];

	for (int x = 0; x < 60; x++) {

		counts[x] = 0;
	}

	int num;

	fstream f("numbers.txt");

	while (f >> num) {

		if (num>0) {
			counts[num] += 1;
		}
	}

	f.close();

	ofstream myfile("results.txt");

	if (myfile.is_open())

	{

		for (int x = 1; x < 60; x++) {
			myfile << x << ": " << counts[x] << "\n";
		}

		myfile.close();

	}

	delete[] counts;

	return EXIT_SUCCESS;

}