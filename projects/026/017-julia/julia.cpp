#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <omp.h>    // <== needed for OpenMP

using namespace cv;
using namespace std;

/*
 * Map a value in one range to corresponding value in another range.
 *
 * value: original value
 * istart, istop: original value's range of possible values
 * ostart, ostop: output value's range of possible values
 *
 * returns value mapped to output ragne
 */
double map(double value, double istart, double istop, double ostart, double ostop) {
	return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
}

/*
 * Test a value to see if it belongs to the Julia set or not.
 *
 * x, y: coordinates of point to test
 *
 * returns integer in [0, 255], number of iterations before iteration
 * blows up
 */
int testPoint(double x, double y) {
	double a = x, b = y, ta;
	int i = 0;

	while (i < 255) {

		ta = (a * a) - (b * b) - 0.8;
		b = 2 * a * b + 0.156;
		a = ta;

		if (a < -2.0 || a > 2.0 || b < -2.0 || b > 2.0) {
			return i;
		}

		i++;
	} // while

	return i;
}

/*
 * Application entry point.
 */
int main() {

	// dimensions of image
	int width = 19200;
	int height = 10800;

	// build the julia set fractal image
	Mat img(height, width, CV_8UC3, Scalar(0));

	// start timing
	auto start = chrono::high_resolution_clock::now();

#pragma omp parallel
	{
		// perform julia set calculations
#pragma omp for
		for (int ix = 0; ix < width; ix++) {
			for (int iy = 0; iy < height; iy++) {
				double x = map(ix, 0.0, width - 1, -1.6, 1.6);
				double y = map(iy, 0.0, height - 1, -0.9, 0.9);

				// test the point in the image
				int k = testPoint(x, y);
				if (k == 255) {
					k = 0;
				}

				// color pixel in image based on k
				Vec3b color(k, 20, k);
				img.at<Vec3b>(Point(ix, iy)) = color;
			}
		}
	}

	// stop timing
	auto stop = chrono::high_resolution_clock::now();

	// write the completed image
	imwrite("test.jpg", img);

	// report timing information
	double nanos = chrono::duration_cast<chrono::nanoseconds>(stop - start).count();
	cout << "Image crerated in " <<
		(nanos / 1000000000.0) <<
		"s" << endl;

	return EXIT_SUCCESS;
}