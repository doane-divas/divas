#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc, char **argv) {

	String filename = argv[1];
	int k = atoi(argv[2]);

	//load in the image
	Mat img = imread(filename);// , CV_LOAD_IMAGE_UNCHANGED);
	Mat gaus;
	Mat blurred;

	if (img.empty()) //check whether the image is loaded or not
	{
		cout << "Error : Image cannot be loaded..!!" << endl;
		//system("pause"); //wait for a key press
		return -1;
	}

	//display original image
	namedWindow("Original", CV_WINDOW_AUTOSIZE); 
	imshow("Original", img); 

	waitKey(0); //wait infinite time for a keypress

	GaussianBlur(img, gaus, Size(k,k), 0, 0);
	blur(img, blurred, Size(k, k), Point(-1, -1));

	//display blurred image
	namedWindow("Gaussian Blur", CV_WINDOW_AUTOSIZE);
	imshow("Gaussian Blur", gaus);

	//display blurred image
	namedWindow("Average Blur", CV_WINDOW_AUTOSIZE);
	imshow("Average Blur", blurred);

	waitKey(0);

	destroyAllWindows();
}