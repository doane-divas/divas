#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc, char **argv) {

	String filename = argv[1];
	int k = atoi(argv[2]);
	int t = atoi(argv[3]);

	//load in the image and define images
	Mat img = imread(filename);// , CV_LOAD_IMAGE_UNCHANGED);
	Mat gaus;
	Mat gray;
	Mat mask;
	Mat maskLayer;
	Mat sel;

	if (img.empty()) //check whether the image is loaded or not
	{
		cout << "Error : Image cannot be loaded..!!" << endl;
		//system("pause"); //wait for a key press
		return -1;
	}
	//convert to gray and blur
	cvtColor(img, gray, CV_BGR2GRAY);
	GaussianBlur(gray, gaus, Size(k, k), 0, 0);

	//apply the threshold
	threshold(gaus, maskLayer, t, 255, THRESH_BINARY_INV);

	//making temp so masks can be merged together
	Mat temp[] = { maskLayer, maskLayer, maskLayer };

	merge(temp, 3, mask);

	//display mask
	namedWindow("mask", CV_WINDOW_NORMAL);
	imshow("mask", mask);

	waitKey(0);

	bitwise_and(img, mask, sel);

	namedWindow("Threshold", CV_WINDOW_NORMAL);
	imshow("Threshold", sel);
	waitKey(0);

	destroyAllWindows();


}