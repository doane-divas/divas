#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>
#include <chrono>


using namespace std;
using namespace cv;

int main(int argc, char **argv) {

	String filename = argv[1];

	//load in the image and define image variables
	Mat img = imread(filename);
	Mat gray;
	Mat black;

	if (img.empty()) //check whether the image is loaded or not
	{
		cout << "Error : Image cannot be loaded..!!" << endl;
		//system("pause"); //wait for a key press
		return -1;
	}
	
	//covert image to grayscale
	cvtColor(img, gray, COLOR_BGR2GRAY);

	//namedWindow("Grayscale", WINDOW_NORMAL);
	//imshow("Grayscale", gray);
	//waitKey(0);
	//destroyAllWindows();


	//Values for the standard ninhydrin paper - previously calculated
	double m2 = -22.042;
	double b2 = 208.96;

	//user inputs specify values that are adjusted
	double m1;
	double b1;
	cout << "input slope for nonstandard image";
	cin >> m1;
	cout << "\n" << "input y-intercept for nonstandard image";
	cin >> b1;

	// start timing
	auto start = chrono::high_resolution_clock::now();

	//make a black image the same size as the gray image. This will be used for the 
	//new, adjusted intesities to be written over
	black = cv::Mat::zeros(gray.size(), gray.type());

	//This for loop selects the area of the image that we want to adjust the pixel
	//intensities.It then applies an equation to normalize the unadjusted paper
	//so that the intensities and concentrations are comparable.
	for (int row = 0; row < gray.rows; row++) {
		for (int col = 0; col < gray.cols; col++) {
			
			double y1 = gray.at<uchar>(row, col);//y1 is equal to the pixel value at coordinate (row,col)
			Rect pixel(col, row, 1, 1);//defining area that will be adjusted

			double adjustedY = m2 * ((y1 - b1) / m1) + b2;

			black(pixel) = adjustedY;// update the black image
		}
	}

	// stop timing
	auto stop = chrono::high_resolution_clock::now();

	//write the new image to a file
	String name = "AdjustedIntensity_Day_" + filename.substr(0, 4) + ".tif";
	imwrite(name, black);

	// report timing information
	double nanos = chrono::duration_cast<chrono::nanoseconds>(stop - start).count();
	cout << "Image crerated in " <<
		(nanos / 1000000000.0) <<
		"s" << endl;

	namedWindow("Reformed gray", WINDOW_NORMAL);
	imshow("Reformed Gray", black);
	waitKey(0);

	destroyAllWindows();


}
