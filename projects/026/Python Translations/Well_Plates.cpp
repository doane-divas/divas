#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>

using namespace std;
using namespace cv;

int main() {

	//load in the image
	Mat img = imread("wellplateImg.tif");// , CV_LOAD_IMAGE_UNCHANGED);

	if (img.empty()) //check whether the image is loaded or not
	{
		cout << "Error : Image cannot be loaded..!!" << endl;
		//system("pause"); //wait for a key press
		return -1;
	}


	//define mask image
	//cv::Mat mask = cv::Mat::zeros(img.size(), img.type());
	cv::Mat mask(img.size(), img.type(), Scalar(0, 0, 0));

	//define destination image
	cv::Mat maskedImg = cv::Mat::zeros(img.size(), img.type());

	//draw circles over the wells

	int x0 = 91;//upper left well coordinates
	int y0 = 108;

	//space between wells
	int deltaX = 70;
	int deltaY = 72;

	//variables that will change 
	int x = x0;
	int y = y0;

	for (int row = 0; row < 12; row++) {
		//reset x to leftmost well
		x = x0;
		for (int col = 0; col < 8; col++) {
			circle(mask, Point(x, y), 16, Scalar(255, 255, 255), -1);
			x += deltaX;
		}
		y += deltaY;
	}

	//apply the mask
	bitwise_and(mask, img, maskedImg);

	namedWindow("Masked Image", CV_WINDOW_NORMAL); 
	imshow("Masked Image", maskedImg); 

	waitKey(0); 

	destroyAllWindows();

}
