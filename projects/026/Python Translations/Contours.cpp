#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc, char **argv) {

	String filename = argv[1];
	int k = 5;
	int t = atoi(argv[2]);

	//load in the image
	Mat img = imread(filename);
	Mat gray;
	Mat gaus;
	Mat binary;

	if (img.empty()) //check whether the image is loaded or not
	{
		cout << "Error : Image cannot be loaded..!!" << endl;
		//system("pause"); //wait for a key press
		return -1;
	}

	//blur and grayscale
	cvtColor(img, gray, COLOR_BGR2GRAY);
	GaussianBlur(gray, gaus, Size(k, k), 0, 0);
	threshold(gaus, binary, t, 255, THRESH_BINARY);

	//define destination for the contours
	vector<vector<Point> > contours;

	//find and draw contours
	findContours(binary, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
	
	drawContours(img, contours, -1, Scalar(0, 0, 255), 8);
	

	namedWindow("contours", CV_WINDOW_NORMAL);
	imshow("contours", img);
	waitKey(0);

	destroyAllWindows();
}