#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>

using namespace std;
using namespace cv;

int main() {

	//load in the image
	Mat img = imread("titration.tiff", CV_LOAD_IMAGE_UNCHANGED);

	//slice the image so we only get a 10x10 kernel
	Rect slice(270, 280, 10, 10);//not sure if these are best coordinates
	Mat sliced = img(slice);

	//find the average pixel intensity for each channel
	float bSum = 0;
	float gSum = 0;
	float rSum = 0;
	for (int row = 0; row < sliced.rows; row++) {
		for (int col = 0; col < sliced.cols; col++) {
			
			Vec3b intensity = sliced.at<Vec3b>(row,col);
			
			bSum += intensity.val[0];
			gSum += intensity.val[1];
			rSum += intensity.val[2];

		}
	}

	cout << "The blue intensity is: " << bSum / 100 <<'\n';
	cout << "The green intensity is: " << gSum / 100 << '\n';
	cout << "The red intensity is: " << rSum / 100 << '\n';

	return EXIT_SUCCESS;
}