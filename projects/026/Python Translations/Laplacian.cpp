#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc, char **argv) {

	String filename = argv[1];
	int k = atoi(argv[2]);
	int t = atoi(argv[3]);

	//load in the image
	Mat img = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
	Mat gaus;
	Mat binary;

	if (img.empty()) //check whether the image is loaded or not
	{
		cout << "Error : Image cannot be loaded..!!" << endl;
		//system("pause"); //wait for a key press
		return -1;
	}

	//blur and grayscale
	GaussianBlur(img, gaus, Size(k, k), 0, 0);
	threshold(gaus, binary, t, 255, THRESH_BINARY_INV);

	Mat edge;

	Laplacian(binary, edge, CV_16S);

	convertScaleAbs(edge, edge);
	

	namedWindow("Edge", CV_WINDOW_NORMAL);
	imshow("Edge", edge);
	waitKey(0);

	destroyAllWindows();





}