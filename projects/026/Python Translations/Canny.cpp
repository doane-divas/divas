#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>

using namespace std;
using namespace cv;

Mat img;

void adjustMinT(int minT, void *userData) {
	
	Mat dst;
	Mat draw;
	int maxT = *(static_cast<int*>(userData));

	Canny(img, dst, minT, maxT, 3);
	dst.convertTo(draw, CV_8U);

	imshow("edges", draw);

}

void adjustMaxT(int maxT, void *userData) {
	Mat dst; 
	Mat draw;
	int minT = *(static_cast<int*>(userData));

	Canny(img, dst, minT, maxT, 3);
	dst.convertTo(draw, CV_8U);

	imshow("edges", draw);
}

int main(int argc, char **argv) {

	String filename = argv[1];
	
	//load in the image
	img = imread(filename, CV_LOAD_IMAGE_GRAYSCALE);

	if (img.empty()) //check whether the image is loaded or not
	{
		cout << "Error : Image cannot be loaded..!!" << endl;
		//system("pause"); //wait for a key press
		return -1;
	}


	namedWindow("edges", CV_WINDOW_NORMAL);
	int minT = 30;
	int maxT = 150;

	createTrackbar("minT", "edges", &minT, 255, adjustMinT, &maxT);
	createTrackbar("maxT", "edges", &maxT, 255, adjustMaxT, &minT);

	
	waitKey(0);

	destroyAllWindows();

	return EXIT_SUCCESS;
}