# Assignment 2: manual thresholding

Write a C++ program that reads pixel information from a text file and stores it in a dynamically
created, 2D array of ints. Then, iterate through the 
values and turn off (set to zero) all of the values in the array that are lower than a threshold 
value *k*.  Finally, print the values in the array, as one long line, to an output file named 
*mt-out.txt*. A sample text file, *graygears.txt*, is included in this directory.

The parameters for a run of the program (filename, number of rows, number of columns, and
the threshold value *k*) should be input from the command line. For example, if the executable 
is named *MT.exe*, then the program should be executed from the command line like this:

```
MT.exe graygears.txt 567 756 128
```