#include <cstdlib>

#include <iostream>

#include <fstream>


int main(int argc, char **argv) {

	using namespace std;

	int ROWS = atoi(argv[2]);
	int COLUMNS = atoi(argv[3]);
	int t = atoi(argv[4]);

	int **img = new int*[ROWS];

	for (int r = 0; r < ROWS; ++r) {
		img[r] = new int[COLUMNS];
	}

	fstream infile(argv[1]);

	if (infile.is_open()) {

		for (int r = 0; r < ROWS; r++) {
			for (int c = 0; c < COLUMNS; c++) {
				infile >> img[r][c];

				if (img[r][c] < t) {
					img[r][c] = 0;
				}
			}
		}//first for
	}//if open

	infile.close();
	ofstream outfile("threshIMG.txt");

	for (int r = 0; r < ROWS; r++) {
		for (int c = 0; c < COLUMNS; c++) {
			outfile << img[r][c] << " ";
		}
	}
	outfile.close();
	for (int r = 0; r < ROWS; r++) {
		delete[] img[r];
	}
	delete[] img;
}

