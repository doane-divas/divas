# -*- coding: utf-8 -*-
"""
Created on Fri Aug  5 06:25:13 2016

@author: tessa
"""

#Usage:  python testscript.py --image /Directory/image.jpg

import numpy as np
import argparse
import cv2

# Set up script arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())

# Read in image, convert to grayscale, and change size.
image = cv2.imread(args["image"])
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Threshold image
(T, thresh) = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

# Crop image into four equal peices
(h, w) = thresh.shape[:2]
UL = thresh[:h/2, :w/2]
UR = thresh[:h/2, w/2:]
LL = thresh[h/2:, :w/2]
LR = thresh[h/2:, w/2:]

# Open output file
out_path = args["image"][:args["image"].rfind(".")]+"_out.csv"
output_file = open(out_path, 'w+')

# Count black pixels in each peice
blocks = [UL, UR, LL, LR]
rows = ["Upper Left", "Upper Right", "Lower Left", "Lower Right"]

for (row, block) in zip(rows, blocks):
    (h, w) = block.shape[:2]
    tot_size = h * w
    wht_cnt = int(np.sum(block)/255)
    blk_cnt = tot_size - wht_cnt
    output_file.write(row + "," + str(blk_cnt) + "\n")

# Close file
output_file.close()
