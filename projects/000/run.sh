#!/bin/bash
#
# Script to execute the testscript.py Python script on all the images in the 
# ./images directory.
#
# Usage:
#
# ./run.sh
#

# loop through all jpegs in the images directory
for FILE in images/*.jpg
do
	# let user watch progress 
	echo "processing $FILE"

	# execute script on file; output is per the script
	python testscript.py -i $FILE
done
