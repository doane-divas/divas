# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import cv2, sys, numpy as np
from imutils import contours as imc

# get the filename and kernel size from command line
filename = (sys.argv[1])
k = int(sys.argv[2])
t = int(sys.argv[3])




# read and display the original image
img = cv2.imread(filename)

#cv2.namedWindow("Original Image", cv2.WINDOW_NORMAL)
#cv2.imshow("Original Image", img)
#cv2.waitKey(0)

#create the basic black image
#mask = np.zeros(img.shape, dtype = "uint8")
#cv2.rectangle(mask, (288,6480), (11024,10800), (255, 255, 255), -1)

#use the mask to select the "interesting" part of the image
clip = img[6480:10800, 288:11024, :] #cv2.bitwise_and(img, mask)

cv2.namedWindow("Clipped Image", cv2.WINDOW_NORMAL)
cv2.imshow("Clipped Image", clip)
cv2.waitKey(0)

# blur and grayscale before thresholding
blur = cv2.cvtColor(clip, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(blur, (k,k), 0)

#perform inverse adaptive thresholding
(t, maskLayer) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

#make a mask suitablew for color image
mask = cv2.merge([maskLayer, maskLayer, maskLayer])
#mask1 = cv2.merge([maskLayer, maskLayer, maskLayer])

#cv2.namedWindow("mask", cv2.WINDOW_NORMAL)
#cv2.imshow("mask", mask1)
#cv2.waitKey(0)


# find contours
(_,contours,hierarchy) =cv2.findContours(maskLayer, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

(sortedContours, boundingBoxes) = imc.sort_contours(contours)

print("found", len(contours), "contours")


def findCentroid(contour):
    M = cv2.moments(contour)
    x = int(M['m10'] / M['m00'])
    y = int(M['m01'] / M['m00'])
    return (x, y)
# draw sorted contours on the original image, one at a time, ignoring the 
# small ones
for (i, contour) in enumerate(sortedContours):
    if len(contour) > 100:      # don't draw small contours
        print(contour)
    
        # draw the current contour in red

        cv2.drawContours(maskedImg, sortedContours, i, (0, 0, 255), 5)

        cv2.drawContours(clip, sortedContours, i, (0, 0, 255), 5)

        
        # draw a black label with contour number
        cv2.putText(maskedImg, str(i), findCentroid(contour), 
                    cv2.FONT_HERSHEY_PLAIN, 7.5, (0, 0, 0), 5)

cv2.namedWindow("final", cv2.WINDOW_NORMAL)
cv2.imshow("final", maskedImg)
cv2.waitKey(0)

avg = 0
for c in contours:
    avg += len(c)
    
avg /= len(contours)

for (i, c) in enumerate(contours):
     if len(c) > avg * 13:
        
        (x, y, w, h) = cv2.boundingRect(c)
        sub = img[y: y+h, x: x+w, :]
        
        # blur and grayscale before thresholding
        blur2 = cv2.cvtColor(sub, cv2.COLOR_BGR2GRAY)
        blur2 = cv2.GaussianBlur(blur2, (k, k), 0)
        # perform adaptive thresholding 
        (t, binary) = cv2.threshold(blur2, t, 255, cv2.THRESH_BINARY) 

        # make a mask suitable for color images
        blur2 = cv2.merge([binary, binary, binary])
        
        # keep only high-intensity pixels
        blur2[blur2 < 90] = 0
        blur2[blur2 >= 90] = 255



    
        cv2.namedWindow("sub--{0}.jpg".format(i), cv2.WINDOW_NORMAL)
        #cv2.imwrite("sub--{0}.jpg".format(i), sub)
        cv2.imshow("sub--{0}.jpg".format(i), blur2)
        cv2.waitKey(0)
        
        # print table of contours and sizes
        print("Found %d objects." % len(contours))
        for (i, c) in enumerate(contours):
            if len(c) >= 700:
                print("\tSize of contour %d: %d" % (i, len(c)))
               


    
=======
        cv2.imshow("selected", clip)
        cv2.waitKey(0)
>>>>>>> cc8375392a5eef759784ace9b9a2f3c31847f70f
