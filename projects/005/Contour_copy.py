
#-*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import cv2, sys, numpy as np
from imutils import contours as imc

# get the filename and kernel size from command line
filename = (sys.argv[1])
k = int(sys.argv[2])
t = int(sys.argv[3])


def findCentroid(contour):
    M = cv2.moments(contour)
    x = int(M['m10'] / M['m00'])
    y = int(M['m01'] / M['m00'])
    return (x, y)

# read and display the original image
img = cv2.imread(filename)

#cv2.namedWindow("Original Image", cv2.WINDOW_NORMAL)
#cv2.imshow("Original Image", img)
#cv2.waitKey(0)

#create the basic black image
#mask = np.zeros(img.shape, dtype = "uint8")
#cv2.rectangle(mask, (288,6480), (11024,10800), (255, 255, 255), -1)

#use the mask to select the "interesting" part of the image
clip = img[6480:10800, 288:11024, :] #cv2.bitwise_and(img, mask)

cv2.namedWindow("Clipped Image", cv2.WINDOW_NORMAL)
cv2.imshow("Clipped Image", clip)
cv2.waitKey(0)

# blur and grayscale before thresholding
blur = cv2.cvtColor(clip, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(blur, (k,k), 0)

#perform inverse adaptive thresholding
(t, maskLayer) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

#make a mask suitablew for color images
#mask1 = cv2.merge([maskLayer, maskLayer, maskLayer])

#cv2.namedWindow("mask", cv2.WINDOW_NORMAL)
#cv2.imshow("mask", mask1)
#cv2.waitKey(0)

# find contours
(_,contours,hierarchy) =cv2.findContours(maskLayer, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

(sortedContours, boundingBoxes) = imc.sort_contours(contours)

print("found", len(contours), "contours")

#dispay the result
cv2.namedWindow("selected", cv2.WINDOW_NORMAL)

# draw sorted contours on the original image, one at a time, ignoring the 
# small ones
for (i, contour) in enumerate(sortedContours):
    if len(contour) > 100:      # don't draw small contours
    
        # draw the current contour in red
        cv2.drawContours(clip, sortedContours, i, (0, 0, 255), 5)
        
        # draw a black label with contour number
        cv2.putText(img, str(i), findCentroid(contour), 
                    cv2.FONT_HERSHEY_PLAIN, 7.5, (0, 0, 0), 5)
        cv2.imshow("selected", clip)
        cv2.waitKey(0)