#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 20 13:28:00 2017

@author: Tu Doan
"""

import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
import pandas as pd
import cv2 

image = cv2.imread('10.tif')
image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)

square = [[1056,576],[1380,576],[1716,576],
          [1032,900],[1368,900],[1668,900],
          [1008,1244],[1356,1244],[1668,1244]]
         

column = []
for i in range(0, 3):
    
    k = i 
    
    for x in range(0,3):
        
        point = [square[k][0],square[k][1]]
        
        img_range = []
        for y in range(0,2):
            point[0] = point[0] + y
            for z in range(0,2):
                point[1] = point[1] + z
                img_range.append(image[point[0],point[1]])

        img = np.mean(img_range,0).astype(np.int64)
        
        #colour = ''.join(chr(c) for c in img).encode('hex')
        #print 'most frequent is %s (#%s)' % (img, colour)

        R_index = 0
        G_index = 1
        B_index = 2

        RGB = img
    
        
        column.append(RGB)#(np.log10(float(255)/RGB))

        k = k + 3
    
    if (i==0):
        column_1 = column 
        column = []    
    elif (i==1):
        column_2 = column 
        column = []    
    elif (i==2):
        column_3 = column 
        column = []    
    
        
plate = pd.DataFrame({1: list(column_1),
                      2: list(column_2),
                      3: list(column_3)})

color1_channel = plate.ix[:,0:3]
#print ("Color 1")                      
color1_channels = []
r_channels = []
g_channels = []
b_channels = []
for i in range(0,3):
    r_channel = []
    g_channel = []
    b_channel = []
    for column in color1_channel:
        row = color1_channel[column].ix[[i]]
        r_channel.append(row[i][0])
        g_channel.append(row[i][1])
        b_channel.append(row[i][2])
    r_channels.append(r_channel)
    g_channels.append(g_channel)
    b_channels.append(b_channel)
color1_channels.append(r_channels)
color1_channels.append(g_channels)
color1_channels.append(b_channels)

colors = ["RED", "GREEN", "BLUE"]
i = 0 
for channel in color1_channels:

    print (colors[i])
    i = i + 1

    plt.xticks(plate.index, ['5X', '10X', '14X'])

    index = [14,14,14,10,10,10,5,5,5]
    
    data = sum(channel, [])
    channel = pd.DataFrame({'RGB' : data}, index=index)
    channel.index.names = ['Concentration']
    print (channel)

    r_squared = 0 
    Y = channel['RGB'].values
    X = channel.index.values

    slope, intercept, r_value, p_value, std_err = stats.linregress(X, Y)
    r_squared = r_value**2
        
    print ("Equation: ",intercept," + ","(",slope,"* X)")
    print ("R-squared: ", r_squared)
    x_axis = np.array([5,10,14])
    best_fit = intercept + (slope * x_axis)
    plt.plot(X, Y, 'ro')
    plt.plot(x_axis, best_fit)
    plt.show()