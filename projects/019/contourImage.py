import cv2, sys

def findCentroid(contour):
    
    # find moments
    M = cv2.moments(contour)
    
    # use the moments to get the x value
    x = int(M['m10'] / M['m00'])
    
    # use the moments to get the y value
    y = int(M['m01'] / M['m00'])
    
    # return the x and y values
    return (x, y)

filename = sys.argv[1]
t = 0

img = cv2.imread(filename)
img2 = cv2.imread(filename)

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(gray, (5,5), 0)
(t, binary) = cv2.threshold(blur, t, 255,cv2.THRESH_BINARY + cv2.THRESH_OTSU)

cv2.namedWindow("output", cv2.WINDOW_NORMAL)
cv2.imshow("output", binary)
cv2.waitKey(0)

(_, contours, _) = cv2.findContours(binary, cv2.RETR_TREE,
    cv2.CHAIN_APPROX_NONE)
'''
# loop through contours to put numbers on them
for (i, contour) in enumerate(contours):
    
    cv2.drawContours(img, contours, i, (0,0,255), 5)
    
    # put numbers on image
    cv2.putText(img, str(i+1), findCentroid(contour), 
                cv2.FONT_HERSHEY_PLAIN, 5, (0, 255, 0), 8)

    cv2.namedWindow("output", cv2.WINDOW_NORMAL)
    cv2.imshow("output", img)
    cv2.waitKey(0)
'''
key = 20
    
cv2.drawContours(img2, contours, key, (0,0,255), 5)

cv2.putText(img2, str(key), findCentroid(contours[key]), cv2.FONT_HERSHEY_PLAIN, 5, (0,255,0), 5)

cv2.namedWindow("output", cv2.WINDOW_NORMAL)
cv2.imshow("output", img2)
cv2.waitKey(0)

num = cv2.matchShapes(contours[1], contours[20], 1, 0.0)

print(num)

# contour 0 and 19 are both boxes
num = cv2.matchShapes(contours[0], contours[19], 1, 0.0)

print(num)