'''
Measure the difference between the plants touched
and the plants not touched

By: Justin and Kiley
'''

'''
Jen and Grace's code to get the green seperate

http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_colorspaces/py_colorspaces.html#converting-colorspaces
'''
import cv2
import sys
import numpy as np
filename = sys.argv[1]
img = cv2.imread(filename)
k = 5 

# Convert BGR to HSV
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
# define range of green color in HSV
lower_green = np.array([30,60,20])
upper_green= np.array([75,255,255])
# Threshold the HSV image to get only green colors
mask = cv2.inRange(hsv, lower_green, upper_green)
# Bitwise-AND mask and original image
res = cv2.bitwise_and(img,img, mask= mask)

#Threshold to get rid of last bit

blur = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(blur, (k, k), 0)

(t, maskLayer) = cv2.threshold(blur, 50, 255, cv2.THRESH_BINARY)

mask = cv2.merge([maskLayer, maskLayer, maskLayer])
img_final = cv2.bitwise_and(img, mask)

'''
Find the average pixel count that isn't black for each row
'''

#convert to grayscale
gray_final = cv2.cvtColor(img_final, cv2.COLOR_BGR2GRAY)

#count non zero pixels by the slice
row1 = cv2.countNonZero(gray_final[:780, :])
row2 = cv2.countNonZero(gray_final[780:1530, :])
row3 = cv2.countNonZero(gray_final[1530:, :])

'''
Get the contours and find the area and perimeter 
'''

#find the contours and assign them by row
(_, cTop, _) = cv2.findContours(gray_final[:780,:], cv2.RETR_EXTERNAL, 
    cv2.CHAIN_APPROX_SIMPLE)
(_, cMid, _) = cv2.findContours(gray_final[780:1530, :], cv2.RETR_EXTERNAL, 
    cv2.CHAIN_APPROX_SIMPLE)
(_, cBot, _) = cv2.findContours(gray_final[1530:, :], cv2.RETR_EXTERNAL, 
    cv2.CHAIN_APPROX_SIMPLE)

#list of the contours by row
cList = [cTop,cMid,cBot]

#loop through the rows then the contours in the row
cAvgP = []
for contours in cList:
	avg = 0
	for c in contours:
		avg += cv2.arcLength(c,True) #finds the perimeter
	avg /= len(contours)
	cAvgP.append(avg)#adds the average to the new list(1 per row)

cAvgA = []
for contours in cList:
	avg = 0
	for c in contours:
		avg += cv2.contourArea(c) #finds the area
	avg /= len(contours)
	cAvgA.append(avg)

'''
Display image with contours outlined
'''
#cv2.drawContours(img[:780,:], cTop, -1, (0, 0, 255), 5)
#cv2.drawContours(img[780:1530,:], cMid, -1, (255, 0, 255), 5)
#cv2.drawContours(img[1530:,:], cBot, -1, (255, 0, 0), 5)
#cv2.namedWindow('img', cv2.WINDOW_NORMAL)
#cv2.imshow('img', img)
#cv2.waitKey(0)

'''
Make data readable for the file
'''
print(filename, int(row1/3), int(row2/3), int(row3/3), int(cAvgP[0]),int(cAvgP[1]), int(cAvgP[2]), int(cAvgA[0]), int(cAvgA[1]), int(cAvgA[2]), sep=",")








