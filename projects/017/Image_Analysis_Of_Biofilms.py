#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 25 14:46:19 2017

@author: Marisa Foster

"""
import cv2, sys, numpy as np

filename = sys.argv[1]
k = int(sys.argv[2])
t = int(sys.argv[3])

#for image in filename:   
img = cv2.imread(filename)
cv2.namedWindow('outcome', cv2.WINDOW_NORMAL)    
cv2.imshow('outcome', img)
cv2.waitKey(0)  

#coordinates for the clip image
clip = img[283:, 94: ]

#convert the image to grayscale 
gray = cv2.cvtColor(clip, cv2.COLOR_BGR2GRAY)

#blur the image
blur = cv2.GaussianBlur(gray, (k,k), 0)

# threshold the image using OTSU
(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)  
cv2.namedWindow('outcome', cv2.WINDOW_NORMAL)    
cv2.imshow('outcome', binary)
cv2.waitKey(0)  

# Create the basic black image 
mask = np.zeros(img.shape, dtype = "uint8")

# Draw a white, filled rectangle on the mask image
cv2.rectangle(mask, (561, 708), (675, 1017), (255, 255, 255), -1)

# Display constructed mask
cv2.namedWindow("Mask", cv2.WINDOW_NORMAL)
cv2.imshow("Mask", mask)
cv2.waitKey(0)

#display masked image
maskedImg = cv2.bitwise_and(img, mask)
cv2.namedWindow("Masked Image", cv2.WINDOW_NORMAL)
cv2.imshow("Masked Image", maskedImg)
cv2.waitKey(0)



