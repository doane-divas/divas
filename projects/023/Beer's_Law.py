#Authors: Jennifer and Brea 
#Date:6/11/2018

#Code is to find and sort concentrations on the plates 

#THINGS WE NEED TO FIX BRFORE RUNNING AGAIN
	#make sure to show the r2 when you do the best fit line 
	#make sure to shave off points so that it is a linar line and it is a better line to read.
	#make sure to get rid of the zeros in the line of concentrations.  
	#Also maybe only show the best fit line with the grater linar line of the color channels but at first do all color channels to find the best. 
#import tools 
import numpy as np
import cv2
import sys
from matplotlib import pyplot as plt

#Define image file to be able to read 
filename = sys.argv[1]

#open image in color 
img = cv2.imread(filename)

#convert to gray scale 
gimg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#scroll through each well and find the average values of blue, green, red, and gray in each well
x0,y0 = 91,108 #this is the center of the first well in the top left of the plate/image.
x,y = x0,y0
#Makes a list for each color channel.
blue_list=[]
green_list=[]
red_list=[]
gray_list = []
#Loop go down each column and then moves over to the next going through each row.
for col in range(8):
    y = y0
    for row in range(12):
        dpix = 8 #This is 1/2 the side of the sqaure being measured.
        B = np.mean(img[y - dpix: y + dpix, x - dpix: x + dpix,0])
        G = np.mean(img[y - dpix: y + dpix, x - dpix: x + dpix,1])
        R = np.mean(img[y - dpix: y + dpix, x - dpix: x + dpix,2])
        Gray = np.mean(gimg[y - dpix: y + dpix, x - dpix: x + dpix])

        blue_list.append(B)
        green_list.append(G)
        red_list.append(R)
        gray_list.append(Gray)

        y = y + 72 #the distance between the center and next well center.

    x = x + 70 #the distance between the center and the next well center. 
	

#group first 6 together for each concentration in each color channel to get
#the average color for each concentration

#grayscale grouping
sumwellgray = 0  
countgray = 1 
graywell_avg = [] #new list that contains the average of the wells with same concentration. 
for well in gray_list:
	
	if countgray < 6: #this sums up the first 5 wells 
		sumwellgray = sumwellgray + well
		
	elif countgray == 6: #this adds in the 6th and then finds the average 
		sumwellgrag = sumwellgray + well
		sumavg = sumwellgray/6
		graywell_avg.append(sumavg)
		#this resets the count of the sum and count when it moves to the next set of 6.  
		sumwellgray = 0 
		countgray = 0 

	countgray = countgray +1 
#blue grouping
sumwellblue = 0  
countblue = 1 
bluewell_avg = [] #new list that contains the average of the wells with same concentration.
for well in blue_list:
	
	if countblue < 6:#this sums up the first 5 wells 
		sumwellblue = sumwellblue + well
		
	elif countblue == 6:#this adds in the 6th and then finds the average 
		sumwellblue = sumwellblue + well
		sumavg = sumwellblue/6
		bluewell_avg.append(sumavg)
		#this resets the count of the sum and count when it moves to the next set of 6.
		sumwellblue = 0 
		countblue = 0 

	countblue = countblue +1
#green grouping
sumwellgreen = 0  
countgreen = 1 
greenwell_avg = [] #new list that contains the average of the wells with same concentration.
for well in green_list:
	
	if countgreen < 6:#this sums up the first 5 wells 
		sumwellgreen = sumwellgreen + well
		
	elif countgreen == 6:#this adds in the 6th and then finds the average 
		sumwellgreen = sumwellgreen+ well
		sumavg = sumwellgreen/6
		greenwell_avg.append(sumavg)
		#this resets the count of the sum and count when it moves to the next set of 6.
		sumwellgreen = 0 
		countgreen = 0 

	countgreen = countgreen +1
#red grouping	
sumwellred = 0  
countred = 1 
redwell_avg = [] #new list that contains the average of the wells with same concentration.
for well in red_list:
	
	if countred < 6:#this sums up the first 5 wells 
		sumwellred = sumwellred + well
		
	elif countred == 6:#this adds in the 6th and then finds the average 
		sumwellred = sumwellred + well
		sumavg = sumwellred/6
		redwell_avg.append(sumavg)
		#this resets the count of the sum and count when it moves to the next set of 6.
		sumwellred = 0 
		countred= 0 

	countred = countred +1 

#Create a list with the gray values assigned to a number that corresponds to a position on the plate
avggraylist=[] #this list is for the value with its corresponding position is put.
numbergray=1
for well in graywell_avg:
    avgwellnumlist=[] #this list is the well plus the value and then is appended to the avggraylist.

    avgwellnumlist.append(well)
    avgwellnumlist.append(numbergray)
    avggraylist.append(avgwellnumlist)

    numbergray=numbergray+1
#sort each color list so that the lowest color value is first for each color channel 
sortlistgray=sorted(avggraylist)
sortlistblue=sorted(bluewell_avg)
sortlistgreen=sorted(greenwell_avg)
sortlistred=sorted(redwell_avg)

#Labeling each well based on their gray value on the plate. 
#A=lowest value with the higher concentration. e=Highest value with the lowest concentration. e = empty  
alph=['A','B','C','D','E','F','G','H','I','J','e','e','e','e','e','e']
font=cv2.FONT_HERSHEY_SIMPLEX
#printing e=empty on the image
texted=cv2.putText(img,"e = empty",(160,950),font,1,(0,0,0),2,cv2.LINE_AA)
num=0


for well in sortlistgray:
    if well[1]==1:
        texted=cv2.putText(img,str(alph[num]),(80,120),font,1,(0,0,0),2,cv2.LINE_AA)#first well, first column
        num=num+1
    elif well[1]==2:
        texted=cv2.putText(img,str(alph[num]),(80,552),font,1,(0,0,0),2,cv2.LINE_AA)#7th well, first column
        num=num+1
    elif well[1]==3:
        texted=cv2.putText(img,str(alph[num]),(150,120),font,1,(0,0,0),2,cv2.LINE_AA)#first well, secondcolumn
        num=num+1
    elif well[1]==4:
        texted=cv2.putText(img,str(alph[num]),(150,552),font,1,(0,0,0),2,cv2.LINE_AA)#7th well, second column
        num=num+1
    elif well[1]==5:
        texted=cv2.putText(img,str(alph[num]),(220,120),font,1,(0,0,0),2,cv2.LINE_AA)#first well, third column
        num=num+1
    elif well[1]==6:
        texted=cv2.putText(img,str(alph[num]),(220,552),font,1,(0,0,0),2,cv2.LINE_AA)#7th well, third column
        num=num+1
    elif well[1]==7:
        texted=cv2.putText(img,str(alph[num]),(290,120),font,1,(0,0,0),2,cv2.LINE_AA)#first well, 4th column 
        num=num+1
    elif well[1]==8:
        texted=cv2.putText(img,str(alph[num]),(290,552),font,1,(0,0,0),2,cv2.LINE_AA)#7th well, 4th column
        num=num+1
    elif well[1]==9:
        texted=cv2.putText(img,str(alph[num]),(360,120),font,1,(0,0,0),2,cv2.LINE_AA)#1st well, 5th column
        num=num+1
    elif well[1]==10:
        texted=cv2.putText(img,str(alph[num]),(360,552),font,1,(0,0,0),2,cv2.LINE_AA)#7th well, 5th column
        num=num+1
    elif well[1]==11:
        texted=cv2.putText(img,str(alph[num]),(430,120),font,1,(0,0,0),2,cv2.LINE_AA)#1st well, 6th column
        num=num+1
    elif well[1]==12:
        texted=cv2.putText(img,str(alph[num]),(430,552),font,1,(0,0,0),2,cv2.LINE_AA)#7th well, 6th column
        num=num+1
    elif well[1]==13:
        texted=cv2.putText(img,str(alph[num]),(500,120),font,1,(0,0,0),2,cv2.LINE_AA)#1st well, 7th column
        num=num+1
    elif well[1]==14:
        texted=cv2.putText(img,str(alph[num]),(500,552),font,1,(0,0,0),2,cv2.LINE_AA)#7th well, 7th column
        num=num+1
    elif well[1]==15:
        texted=cv2.putText(img,str(alph[num]),(570,120),font,1,(0,0,0),2,cv2.LINE_AA)#1st well, 8th column
        num=num+1
    elif well[1]==16:
        texted=cv2.putText(img,str(alph[num]),(570,552),font,1,(0,0,0),2,cv2.LINE_AA)#7th well, 8th column 
        num=num+1

#print output of the labeled wells and the order of the concentrations from lowest to highest.     
cv2.namedWindow("output", cv2.WINDOW_NORMAL)
cv2.imshow("output", texted)
cv2.waitKey(0)
#create a new gray list without the number  for position
graysort = []#the new gray list. 
for i in range(16):# 16 number of possible concentrations 
    graysort.append(sortlistgray[i][0])
#plot each list vs. concentration and show it
conc =[1.00E-05,9.00E-06,8.00E-06,7.00E-06,6.00E-06,5.00E-06,4.00E-06,3.00E-06,2.00E-06,1.00E-06,0,0,0,0,0,0] 

#got from https://stackoverflow.com/questions/22239691/code-for-line-of-best-fit-of-a-scatter-plot-in-python to be able to define the function of creating a scatter plot and best fit line

def best_fit(X, Y):

	#finds the average per list
    xbar = sum(X)/len(X)
    ybar = sum(Y)/len(Y)
    n = len(X) # or len(Y)

    numer = sum([xi*yi for xi,yi in zip(X, Y)]) - n * xbar * ybar
    denum = sum([xi**2 for xi in X]) - n * xbar**2

    b = numer / denum
    a = ybar - b * xbar

    print('best fit line:\ny = {:.2f} + {:.2f}x'.format(a, b))

    return a, b

#PLOTTING THE BEST FIT LINE FOR EACH COLOR CHANNEL ON DIFFERENT GRAPHS TO SEE BEST RESULTS

#plotting the concentrations and the first 6 values of the GRAY CHANNEL
plt.plot(conc, graysort, color = 'gray', label = 'Gray')

#show points on graph with a line connecting the plots
plt.scatter(conc, graysort, s = 30, alpha = 0.15, marker = 'o')

#Determine the best fit line and put on scatter plot and label
a, b = best_fit(conc, graysort)

yfit = [a + b * xi for xi in conc]
plt.plot(conc, yfit)
plt.xlabel('concentration')
plt.ylabel('Color Value')
plt.title("Beer's Law")
plt.legend()
plt.show()

#plotting graph for the BLUE CHANNEL
plt.plot(conc, sortlistblue, color = 'blue', label = 'Blue')

#show points on graph with a line connecting the plots 
plt.scatter(conc, sortlistblue, s = 30, alpha = 0.15, marker = 'o')

#determine best fit line
a, b = best_fit(conc, sortlistblue)

#fit line to the scatter plot and label and plot
yfit = [a + b * xi for xi in conc]
plt.plot(conc, yfit)
plt.xlabel('concentration')
plt.ylabel('Color Value')
plt.title("Beer's Law")
plt.legend()
plt.show()

#plotting the concentrations and the first 6 values of the RED CHANNEL
plt.plot(conc, sortlistred, color = 'red', label = 'Red')

#show points on graph with a line connecting the plots
plt.scatter(conc, sortlistred, s = 30, alpha = 0.15, marker = 'o')

#Determine the best fit line and put on scatter plot and label
a, b = best_fit(conc, sortlistred)
yfit = [a + b * xi for xi in conc]
plt.plot(conc, yfit)
plt.xlabel('concentration')
plt.ylabel('Color Value')
plt.title("Beer's Law")
plt.legend()
plt.show()

#plotting the concentrations and the first 6 values of the GREEN CHANNEL
plt.plot(conc, sortlistgreen, color = 'green', label = 'Green')

#show points on graph with a line connecting the plots
plt.scatter(conc, sortlistgreen, s = 30, alpha = 0.15, marker = 'o')

#Determine the best fit line and put on scatter plot and label
a, b = best_fit(conc, sortlistgreen)
yfit = [a + b * xi for xi in conc]
plt.plot(conc, yfit)
plt.xlabel('concentration')
plt.ylabel('Color Value')
plt.title("Beer's Law")
plt.legend()
plt.show()

'''
KEY FOR CONCENTRATION 

A = 1.00E-05
B = 9.00E-06
C = 8.00E-06
D = 7.00E-06
E = 6.00E-06
F = 5.00E-06
G = 4.00E-06
H = 3.00E-06
I = 2.00E-06
J = 1.00E-06
'''




















