#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 10:16:36 2018

@author: diva
"""

import cv2
import numpy as np
import sys

k = 5

# read in original image and open the centers file
originalImage = cv2.imread(sys.argv[1])

centerFile = open('centers.txt')

# create the mask image
mask = np.zeros(originalImage.shape, dtype='uint8')

# iterate through the centers file...
for line in centerFile:
    # ... getting the coordinates of each well...
    tokens = line.split()
    x = int(tokens[0])
    y = int(tokens[1])

    # ... and drawing a white circle on the mask
    cv2.circle(mask, (x, y), (20), (255, 255, 255), -1) #measured diameter of a well in imagej = about 50 pix

# close the file after use
centerFile.close()

# apply the mask
maskedImage = np.bitwise_and(originalImage, mask)

#cv2.imwrite('CV1plate.tif', maskedImage)
cv2.namedWindow('mask', cv2.WINDOW_NORMAL)
cv2.imshow('mask', maskedImage)
cv2.waitKey(0)

'''used this piece of code from Brea and Jenn>>'''

#scroll through each well and find the color values
x0,y0 = 91,108 #center of first well

x,y = x0,y0

rgb_list = np.empty((3, 0)).tolist ()

for row in range(2):
    x = x0

    for col in range(8):
        dpix = 8

        #finds average colors within each well
        B = np.mean(maskedImage[y - dpix: y + dpix, x - dpix: x + dpix,0]) 
        G = np.mean(maskedImage[y - dpix: y + dpix, x - dpix: x + dpix,1])
        R = np.mean(maskedImage[y - dpix: y + dpix, x - dpix: x + dpix,2])
        red, green, blue = R,G,B
        rgb_list[2].append(red)
        rgb_list[1].append(green)
        rgb_list[0].append(blue)

        x = x + 70 #70 pixels between each well on x axis
        
        #rgb_list[0:3].sort(key=lambda rgb: lum(*rgb) ) #our thing


    y = y + 432 #stops count at 6 wells

colors = []
num = 1
ColorList = []
for i in range (16):
    colorlist = []
    colorlist.append(rgb_list[2][i])
    colorlist.append(rgb_list[1][i])
    colorlist.append(rgb_list[0][i])
    
    colors.append(colorlist)
#print (colors)

#find color averages
avgcolorlist=[]
number=1
for well in colors:
    sumcolor=0
    avgwellnumlist=[]

    for color in well:
        sumcolor=sumcolor+color
    avgwell=sumcolor/3

    avgwellnumlist.append(avgwell)
    avgwellnumlist.append(number)
    avgcolorlist.append(avgwellnumlist)

    number=number+1
#print(avgcolorlist)

#Labeling each well based on their color value on the plate. A:lowest value/ higher concentration. e:Highest value/ lowest concentration. e = empty  
#print(sorted(avgcolorlist))
acl=sorted(avgcolorlist)
dictionary = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'empty', 'empty', 
              'empty', 'empty', 'empty', 'empty']
# Adding concentration letters to color values
FinalList=[]
Almost=[]
Last = 0
num = 0
for plate in acl: 
    Almost.append(acl[Last])
    Almost.append(str(dictionary[num]))
    num = num +1
    Last = Last +1
FinalList.append(Almost)
    
print(FinalList)
    