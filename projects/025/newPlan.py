#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 10:57:09 2018

@author: diva
Kiley Taylor, Grace Su
"""

import cv2
import numpy as np
import sys
import matplotlib.pyplot as plt

# read in original image
img = cv2.imread(sys.argv[1])

'''used this piece of code from Brea and Jenn>>'''

#scroll through each well and find the color values
x0,y0 = 91,108 #center of first well

x,y = x0,y0 #updates position of next well

blue_list = []
green_list = []
red_list = []

for row in range(2): #goes through wells left to right then down
    x = x0

    for col in range(8):
        dpix = 8

        #finds color values for each channel within each well
        B = np.mean(img[y - dpix: y + dpix, x - dpix: x + dpix,0]) 
        G = np.mean(img[y - dpix: y + dpix, x - dpix: x + dpix,1])
        R = np.mean(img[y - dpix: y + dpix, x - dpix: x + dpix,2])
        red, green, blue = R,G,B
        red_list.append(red)
        green_list.append(green)
        blue_list.append(blue)

        x = x + 70 #70 pixels between each well on x axis

    y = y + 432 #stops count at 6 wells
    #only takes the top well of each concentration
'''end of jenn and breas code'''
    
#sort the color values within each channel
sortedRed = sorted(red_list)
sortedGreen = sorted(green_list)
sortedBlue = sorted(blue_list)

#give x and y coordinates for line on graph
#concentration values are x coordinates
#avg color values are y coordinates
concentration = [1.00E-05, 9.00E-06, 8.00E-06, 7.00E-06, 6.00E-06, 5.00E-06, 4.00E-06,
                 3.00E-06, 2.00E-06, 1.00E-06, 0, 0, 0, 0, 0 ,0]

#create function for best fit line
def best_fit(X, Y):

	#finds the average per list
    xbar = sum(X)/len(X)
    ybar = sum(Y)/len(Y)
    n = len(X) # or len(Y)

    numer = sum([xi*yi for xi,yi in zip(X, Y)]) - n * xbar * ybar
    denum = sum([xi**2 for xi in X]) - n * xbar**2

    b = numer / denum
    a = ybar - b * xbar

    print('best fit line:\ny = {:.2f} + {:.2f}x'.format(a, b))

    return a, b


#create line chart/graph for Red Channel
plt.plot(concentration[:10], sortedRed[:10], color='red',label="Red") #each plt.plot is  a different line for different color channels
#show points on graph
plt.scatter(concentration[:10], sortedRed[:10], s=30, alpha=0.15, marker='o')
# determine best fit line
a, b = best_fit(concentration[:10], sortedRed[:10])
# fit line to scattered points
yfit = [a + b * xi for xi in concentration]
plt.plot(concentration, yfit)

plt.xlabel('Concentration')
plt.ylabel('Color Value')
plt.title("Red Channel")
plt.legend()
plt.show()

#create line chart/graph for green channel
plt.plot(concentration[:10], sortedGreen[:10], color='green',label='Green')
#show points on graph
plt.scatter(concentration[:10], sortedGreen[:10], s=30, alpha=0.15, marker='o')
# determine best fit line
a, b = best_fit(concentration[:10], sortedGreen[:10])
# fit line to scattered points
yfit = [a + b * xi for xi in concentration]
plt.plot(concentration, yfit)

plt.xlabel('Concentration')
plt.ylabel('Color Value')
plt.title("Green Channel")
plt.legend()
plt.show()

#create line chart/graph for blue channel
plt.plot(concentration[:10],  sortedBlue[:10], color='blue',label='Blue')
#show points on graph
scatter = plt.scatter(concentration[:10], sortedBlue[:10], s=30, alpha=0.15, marker='o')
# determine best fit line
a, b = best_fit(concentration[:10], sortedBlue[:10])
# fit line to scattered points
yfit = [a + b * xi for xi in concentration]
plt.plot(concentration, yfit)

plt.xlabel('Concentration')
plt.ylabel('Color Value')
plt.title("Blue Channel")
plt.legend()
plt.show()

