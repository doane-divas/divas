import cv2 
import numpy as np
import os
import sys
import operator
import matplotlib.pyplot as plt

greens = np.loadtxt(fname = 'Greens.csv')

reds = np.loadtxt(fname = 'Reds.csv')

blues = np.loadtxt(fname = 'Blues.csv')

with open('Greens.csv') as lines:
	g = []
	for line in lines:
		sline = line.strip()
		sline = sline.split(',')
		g.append(float(sline[0]))

with open('Reds.csv') as lines:
	r = []
	for line in lines:
		sline = line.strip()
		sline = sline.split(',')
		r.append(float(sline[0]))

with open('Blues.csv') as lines:
	b = []
	for line in lines:
		sline = line.strip()
		sline = sline.split(',')
		b.append(float(sline[0]))

green_min = np.min(g)

frame = g.index(green_min)

sec = frame/15

usg = np.mean(g[0:frame])
sg = np.mean(g[frame:-1])

usr = np.mean(r[0:frame])
sr = np.mean(r[frame:-1])

usb = np.mean(b[0:frame])
sb = np.mean(b[frame:-1])

print('PH change is at: ', sec)

rgDiff = (usr - usg)
rbDiff = (usr - usb)
bgDiff = (usb - usg)

unstables = rgDiff + rbDiff + bgDiff

print('Unstable: ', unstables)

rgDiff2 = (sr - sg)
rbDiff2 = (sr - sb)
bgDiff2 = (sb - sg)

stables = rgDiff2 + rbDiff2 + bgDiff2

print('Stable: ', stables)

plt.xlabel('Frames')
plt.ylabel('Color Value')

plt.plot (r, 'r')
plt.plot (g, 'g')
plt.plot (b, 'b')
plt.ylim(0, 256)
plt.xlim(0, 455)
plt.show()




#ignore

'''
diff = reds - greens
diff2 = blues - greens
diff3 = reds - blues

rgDiff = np.mean(diff[frame:-1])
print('Stable red green difference: ', rgDiff)
bgDiff = np.mean(diff2[frame:-1])
print('Stable blue green Difference: ', bgDiff)
rbDiff = np.mean(diff3[frame:-1])
print('Stable red blue difference: ', rbDiff)

rgDiff2 = np.mean(diff[0:frame])
print('Unstable red green difference: ', rgDiff2)
bgDiff2 = np.mean(diff2[0:frame])
print('Unstable blue green Difference: ', bgDiff2)
rbDiff2 = np.mean(diff3[0:frame])
print('Unstable red blue difference: ', rbDiff2)

red = diff[frame:-1]
green = diff[frame:-1]
blue = diff[frame:-1]
'''

