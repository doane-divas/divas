import cv2
import numpy as np
import os
import sys
import csv

filename = sys.argv[1]

if '.png' in filename:

	img = cv2.imread(filename)

	mask = np.zeros(img.shape, 'uint8')

	cv2.rectangle(mask, (295, 240), (580, 355), (255, 255, 255), -1)

	maskedImg = cv2.bitwise_and(img, mask)

	arr = np.array(img).mean(axis=(0,1))

	avgRed = arr[0]
	avgGreen = arr[1]
	avgBlue = arr[2]

reds = open('Reds.csv', 'a')
greens = open('Greens.csv', 'a')
blues = open('Blues.csv', 'a')

reds.write(avgRed + '\n')
greens.write(avgGreen + '\n')
blues.write(avgBlue + '\n')

reds.close()
greens.close()
blues.close()



	

