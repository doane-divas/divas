"""

Created by Anna Korte and Lilly Shatford-Adams on June 5rd, 2019 

Goal: Create a python program that will identify the total number of c. elegans in a picture, the number of dead and alive c. elegans, and the percentages of each type. 

Note: Dead c. elegans are straight, have color variances within their bodies, and glow. 

Algorithm 

I. Find the total # of c. elegans (June 3rd)
	a. reading the image
	b. create a mask for the image
	b. binary-ing the image 
	c. find the contours of the image and print 

Usage: python Contours.py <filename> <threshold value> 

Example: python Contours.py test2w2.tif 4
"""

import cv2, sys, numpy as np
from skimage.morphology import skeletonize, thin 

#read user terminal input 
filename = sys.argv[1] 
#t = int(sys.argv[2]) (no point - currently) 

#read the original image, call it "image" 
image = cv2.imread(filename = filename) 

#display the BEFORE image 

cv2.namedWindow(winname = "before", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "before", mat = image)
cv2.waitKey(delay = 0)


#create binary image (binary) 
gray = cv2.cvtColor(src = image, code = cv2.COLOR_BGR2GRAY) 

""""
#display the GRAYSCALED IMAGE image 

cv2.namedWindow(winname = "grayscaled", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "grayscaled", mat = gray)
cv2.waitKey(delay = 0)

"""

#Blur the image with thresholding 
blur = cv2.GaussianBlur(src = gray, ksize = (5,5), sigmaX = 0)

(t, binary) = cv2.threshold(src = blur, thresh = 0, maxval = 255, type = cv2.THRESH_BINARY +cv2.THRESH_OTSU)


#display the BLURRED IMAGE image 

cv2.namedWindow(winname = "blurred", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "blurred", mat = blur)
cv2.waitKey(delay = 0)


#display the BINARY IMAGE image 

cv2.namedWindow(winname = "binary", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "binary", mat = binary)
cv2.waitKey(delay = 0)



# find contours
(_, contours,_) = cv2.findContours(image = binary, 
    mode = cv2.RETR_EXTERNAL,
    method = cv2.CHAIN_APPROX_NONE)

cnt = contours[0]


#trying curvature method
std::vector< float > vecCurvature( vecContourPoints.size() );

cv::Point2f posOld, posOlder;
cv::Point2f f1stDerivative, f2ndDerivative;   
for (size_t i = 0; i < vecContourPoints.size(); i++ )
{
    const cv::Point2f& pos = vecContourPoints[i];

    if ( i == 0 ){ posOld = posOlder = pos; }

    f1stDerivative.x =   pos.x -        posOld.x;
    f1stDerivative.y =   pos.y -        posOld.y;
    f2ndDerivative.x = - pos.x + 2.0f * posOld.x - posOlder.x;
    f2ndDerivative.y = - pos.y + 2.0f * posOld.y - posOlder.y;

    float curvature2D = 0.0f;
    if ( std::abs(f2ndDerivative.x) > 10e-4 && std::abs(f2ndDerivative.y) > 10e-4 )
    {
        curvature2D = sqrt( std::abs( 
            pow( f2ndDerivative.y*f1stDerivative.x - f2ndDerivative.x*f1stDerivative.y, 2.0f ) / 
            pow( f2ndDerivative.x + f2ndDerivative.y, 3.0 ) ) );
    }

    vecCurvature[i] = curvature2D;

    posOlder = posOld;
    posOld = pos;
}

