import cv2, sys

filename = sys.argv[1] 
#t = int(sys.argv[2]) (no point - currently) 

#read the original image, call it "image" 
img = cv2.imread(filename = filename) 

#display the BEFORE image 

cv2.namedWindow(winname = "before", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "before", mat = img)
cv2.waitKey(delay = 0)


#create binary image (binary) 
gray = cv2.cvtColor(src = img, code = cv2.COLOR_BGR2GRAY) 

#Blur the image with thresholding 
blur = cv2.GaussianBlur(src = gray, ksize = (5,5), sigmaX = 0)

(t, binary) = cv2.threshold(src = blur, thresh = 0, maxval = 255, type = cv2.THRESH_BINARY +cv2.THRESH_OTSU)

# find contours
(_, contours,_) = cv2.findContours(image = binary, 
    mode = cv2.RETR_EXTERNAL,
    method = cv2.CHAIN_APPROX_NONE)


# Display our contoured image 

cv2.namedWindow(winname = "maskedImage", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "maskedImage", mat = image)
cv2.waitKey(delay = 0)
