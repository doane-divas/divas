"""

Created by Anna Korte and Lilly Shatford-Adams on June 3rd, 2019 

Goal: Create a python program that will identify the total number of c. elegans in a picture, the number of dead and alive c. elegans, and the percentages of each type. 

Note: Dead c. elegans are straight, have color variances within their bodies, and glow. 

Algorithm 

I. Find the total # of c. elegans (June 3rd)
	a. reading the image
	b. create a mask for the image
	b. binary-ing the image 
	c. find the contours of the image and print 

Usage: python Contours.py <filename> <threshold value> 

Example: python Contours.py test2w2.tif 4
"""

import cv2, sys, numpy as np

#read user terminal input 
filename = sys.argv[1] 
#t = int(sys.argv[2]) (no point - currently) 

#read the original image, call it "image" 
image = cv2.imread(filename = filename) 

#display the BEFORE image 

cv2.namedWindow(winname = "before", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "before", mat = image)
cv2.waitKey(delay = 0)

#create binary image (binary) 
gray = cv2.cvtColor(src = image, code = cv2.COLOR_BGR2GRAY) 

#Blur the image with thresholding 
blur = cv2.GaussianBlur(src = gray, ksize = (5,5), sigmaX = 0)

(t, binary) = cv2.threshold(src = blur, thresh = 0, maxval = 255, type = cv2.THRESH_BINARY +cv2.THRESH_OTSU)

# find contours
(_, contours,_) = cv2.findContours(image = binary, 
    mode = cv2.RETR_EXTERNAL,
    method = cv2.CHAIN_APPROX_NONE)

cnt = contours[0]

#finding center of contour
for c in contours:
	M = cv2.moments(array = c)
	cx = int(M['m10'] / M['m00'])
	cy = int(M['m01'] / M['m00'])

print ("x-coordinate: ", cx, "y-coordinate: ", cy) 

#print contour number and size 
print("found %d c. elegans: " % len(contours))

#print("Found %d objects." % len(contours))
for (i, c) in enumerate(contours):
    print("\tSize of contour %d: %d" % (i, len(c)))

# draw contours over original image
cv2.drawContours(image = image, #image = image to HIDE contours and image = mask to SHOW contours 
    contours = contours, 
    contourIdx = -1, #draw them all 
    color = (0, 0, 255), #color that will be drawn
    thickness = 1) #line thicknes

# Display our contoured image 

cv2.namedWindow(winname = "maskedImage", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "maskedImage", mat = image)
cv2.waitKey(delay = 0)

M = cv.moments(cnt)
print( M )


#Creating a binding rectangle 

for (i, c) in enumerate(contours):
    (x, y, w, h) = cv2.boundingRect(c)
    subimage = image[y:y+h,x:x+w, :] #slices  every single, unique image - note: [y,x,colorchannels]
    
cv2.imwrite(filename = "cropped-{}.jpg".format(i), img = subimage)

#Find biggest curvature 

#for t in enumerate(contours): 
	#epsilon = 0.1 * cv2.arcLength(curve = t, closed = True)

#print (t) 

