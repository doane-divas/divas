'''
 * Python program to use contours to count the objects in an image.
 *
 * usage: python Contours.py <filename> <threshold>
'''
import cv2, sys
import numpy as np


# read command-line arguments
filename = sys.argv[1]
t = int(sys.argv[2]) #threshold

# read original image
image = cv2.imread(filename = filename)

# create binary image
gray = cv2.cvtColor(src = image, code = cv2.COLOR_BGR2GRAY) #converts the color 	
blur = cv2.GaussianBlur(src = gray, 
    ksize = (5, 5), #custom number
    sigmaX = 0)
(t, binary) = cv2.threshold(src = blur, 
    thresh = t, 
    maxval = 255, 
    type = cv2.THRESH_BINARY) #everything above t will be turned on - if you are running dots.jpg keep INV(white background), if you are running dice.jpg delete INV(black background)

# find contours
(_, contours,_) = cv2.findContours(image = binary, #underscores will ignore the return of the first and third one, the middle one is my data structure
	mode = cv2.RETR_EXTERNAL, #find contours that are on the OUTSIDE of objects only 
    method = cv2.CHAIN_APPROX_NONE) #NONE = accurate but bigger size, SIMPLE = speedier, less accurate 

# print table of contours and sizes
print("Found %d objects." % len(contours))
for (i, c) in enumerate(contours):
    print("\tSize of contour %d: %d" % (i, len(c)))

# draw contours over original image
cv2.drawContours(image = image, 
    contours = contours, 
    contourIdx = -1, #draw them all 
    color = (0, 0, 255), #color that will be drawn
    thickness = 5) #line thicknes

# display original image with contours
cv2.namedWindow(winname = "output", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "output", mat = image)
cv2.waitKey(delay = 0)

#Skeletonize


size = np.size(image)
skel = np.zeros(image.shape,np.uint8)

ret,image = cv2.threshold(image,127,255,0)
element = cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3))
done = False
 
while(not done):
    eroded = cv2.erode(img,element)
    temp = cv2.dilate(eroded,element)
    temp = cv2.subtract(img,temp)
    skel = cv2.bitwise_or(skel,temp)
    img = eroded.copy()
 
    zeros = size - cv2.countNonZero(img)
    if zeros==size:
        done = True

cv2.imshow("skel",skel)
cv2.waitKey(0)
