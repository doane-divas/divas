"""
This is an edited version that practices uses contour elements 


Created by Anna Korte and Lilly Shatford-Adams on June 3rd, 2019 

Goal: Create a python program that will identify the total number of c. elegans in a picture, the number of dead and alive c. elegans, and the percentages of each type. 

Note: Dead c. elegans are straight, have color variances within their bodies, and glow. 

Algorithm 

I. Find the total # of c. elegans (June 3rd)
	a. reading the image
	b. create a mask for the image
	b. binary-ing the image 
	c. find the contours of the image and print 

Usage: python Contours.py <filename> <threshold value> 

Example: python Contours.py test2w2.tif 4
"""

import cv2, sys, numpy as np

#read user terminal input 
filename = sys.argv[1] 
t = int(sys.argv[2]) 

#read the original image, call it "image" 
image = cv2.imread(filename = filename) 

#display the BEFORE image 

cv2.namedWindow(winname = "before", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "before", mat = image)
cv2.waitKey(delay = 0)

# Create the basic black image aka MASK 
mask = np.zeros(shape = image.shape, dtype = "uint8") 

cv2.circle(img = mask, center=
	(351, 307), radius = 200, color = (255,255,255), thickness = -1)

"""
# Draw a white, filled rectangle on the mask image
cv2.rectangle(img = mask, pt1 = (160, 100), pt2 = (500, 450),  
	color = (255,255,255), thickness = -1) 
	
"""

# Display constructed mask

maskedImg = cv2.bitwise_and(src1 = image, src2 = mask)


""""
cv2.namedWindow(winname = "maskedImage", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "maskedImage", mat = maskedImg)
cv2.waitKey(delay = 0)
"""

#create binary image (binary) 
gray = cv2.cvtColor(src = maskedImg, code = cv2.COLOR_BGR2GRAY) 
"""
#display the GRAYSCALED IMAGE image 

cv2.namedWindow(winname = "grayscaled", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "grayscaled", mat = gray)
cv2.waitKey(delay = 0)
"""

#Blur the image with thresholding 


blur = cv2.GaussianBlur(src = gray, ksize = (5,5), sigmaX = 0)
(t, binary) = cv2.threshold(src = blur, thresh = t, maxval = 255, type = cv2.THRESH_BINARY)
#white background = INV, blackground = reg

"""
#display the BLURRED IMAGE image 

cv2.namedWindow(winname = "blurred", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "blurred", mat = blur)
cv2.waitKey(delay = 0)
"""
#display the BINARY IMAGE image 

cv2.namedWindow(winname = "binary", flags = cv2.WINDOW_NORMAL)
cv2.imshow(winname = "binary", mat = binary)
cv2.waitKey(delay = 0)

# find contours
(_, contours,_) = cv2.findContours(image = binary, 
    mode = cv2.RETR_EXTERNAL,
    method = cv2.CHAIN_APPROX_NONE)


#print contour number and size 
print("found %d c. elegans: " % len(contours))

#print("Found %d objects." % len(contours))
for (i, c) in enumerate(contours):
    print("\tSize of contour %d: %d" % (i, len(c)))

cnt = contours[0]
for cnt in contours: 
	k = cv2.isContourConvex(cnt)
	print (k)

# draw contours over original image
cv2.drawContours(image = image, 
    contours = contours, 
    contourIdx = -1, #draw them all 
    color = (0, 0, 255), #color that will be drawn
    thickness = 5) #line thicknes

#display original image with contours 
cv2.namedWindow(winname = "output" , flags = cv2.WINDOW_NORMAL) 
cv2.imshow(winname = "output", mat = image) 
cv2.waitKey(delay = 0) 

