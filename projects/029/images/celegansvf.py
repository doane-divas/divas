
import cv2, sys, numpy as np

#read user terminal input 
filename = sys.argv[1] 
#t = int(sys.argv[2]) (no point - currently) 

#read the original image, call it "image" 
image = cv2.imread(filename = filename) 

#create binary image (binary) 
gray = cv2.cvtColor(src = image, code = cv2.COLOR_BGR2GRAY) 

#Blur the image with thresholding 
blur = cv2.GaussianBlur(src = gray, ksize = (5,5), sigmaX = 0)

(t, binary) = cv2.threshold(src = blur, thresh = 0, maxval = 255, type = cv2.THRESH_BINARY +cv2.THRESH_OTSU)

# find contours
(_, contours,_) = cv2.findContours(image = binary, 
    mode = cv2.RETR_EXTERNAL,
    method = cv2.CHAIN_APPROX_NONE)

cnt = contours[0]

#finding center of contour
for c in contours:
	M = cv2.moments(array = c)
	cx = int(M['m10'] / M['m00'])
	cy = int(M['m01'] / M['m00'])

#print ("x-coordinate: ", cx, "y-coordinate: ", cy) 

# Create the basic black image aka MASK 
mask = np.zeros(shape = image.shape, dtype = "uint8") 

cv2.circle(img = mask, center=
	(cx,cy), radius = 20, color = (255,255,255), thickness = -1)

# draw contours over original image
cv2.drawContours(image = image, #image = image to HIDE contours and image = mask to SHOW contours 
    contours = contours, 
    contourIdx = -1, #draw them all 
    color = (0, 0, 255), #color that will be drawn
    thickness = 1) #line thicknes

#find number of white

pixels = np.sum(image)
whitepixels = pixels/255 
percentagewhite = (whitepixels/pixels)*100

#print("total white pixels is: ", whitepixels) 
#print("total overall pixels are: ", pixels) 
print(filename,",",whitepixels,",",pixels,",",percentagewhite)

