import matplotlib.pyplot as plt
import numpy as np 

#Create a variable for the file name  
filename = "output3.csv"

#Open the file
infile = open(filename, 'r') 

lines = infile.readlines() 

percentagelist=[]

for line in lines:
	linesplit = line.split(',')  #separates line into a list of items, will split the lines at the commas
	line = linesplit[3] #makes the 4th column into the variable line 
	line = line.strip('\n') #takes away the "\n"
	line = int(line) #converts string to int 
	percentagelist.append(line) #takes all values in line and puts in a list
	#print(percentagelist) #prints the new lis

array = np.array(percentagelist)
print("oriiginal array is: ", array)
sortedArray = np.sort(array) 
print("sorted from smallest to biggest array is: ", sortedArray) 


plt.hist(sortedArray, facecolor = 'magenta', edgecolor = 'black', bins = 40) 
plt.xlabel('% White') 
plt.ylabel('Numbers of times it occurs')
plt.show() 



