import cv2
import matplotlib.pyplot as plt


fileName = ("pH.txt")

file = open(fileName, "r")


read = file.readlines()

pHlist = []
seconds = []
count = 0
for i in read:
	i = i.strip("\n")
	i = float(i)
	pHlist.append(i)
	seconds.append(count)
	if i == 7:
		neutralFrame = count
	count+=1

print(pHlist)
print(seconds)
print(neutralFrame)

x = seconds
y = pHlist


plt.plot(x, y)
plt.show()

fileName = ("green24.txt")

file = open(fileName, "r")


read = file.readlines()

greenList = []
seconds = []
count = 0
for i in read:
	i = i.strip("\n")
	i = float(i)
	greenList.append(i)
	seconds.append(count)
	count+=1

print(greenList)
print(seconds)

x = seconds
y = greenList


plt.plot(x, y)
plt.show()

