#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 14:57:31 2017

@author: diva
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  6 09:05:26 2017

@author: AdreAnna Ernest and Keeliann Mark

This script creates a graph for each channel of the first dye or the first four rows of
the well plate and finds the r*2.
"""
import math
import sys, cv2
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
#script reads file from assigned system argument in the terminal as anydepth--
#these images are 16bits (65000+ color range)
img = cv2.imread(sys.argv[1], cv2.IMREAD_COLOR + cv2.IMREAD_ANYDEPTH)

#these are the center points of the wells--centers are y,x
centers = [(110,165),(184,165),(254,165),(324,165),(396,165),(466,165), #columnG 12-7
         (537,165),(607,165), #columnG 6-5
         (110,235),(184,235),(254,235),(324,235),(396,235),(466,235), #columnF 12-7
         (537,235),(607,235),#columnF 6-5
         (110,305),(184,305),(254,305),(324,305),(396,305),(466,305), #columnE 12-7
         (537,305),(607,305), #columnE 6-5
         (110,373),(184,373),(254,373),(324,373),(396,373),(466,373), #columnD 12-7
         (537,373),(607,373), #columnD 6-5
         (110,442),(184,442),(254,442),(324,442),(396,442),(466,442), #columnC 12-7
         (537,442),(607,442), #columnC 6-5
         (109,511),(184,511),(254,511),(324,511),(396,511),(466,511), #columnB 12-7
         (537,512),(607,512), #columnB 6-5
         (109,582),(184,582),(254,582),(324,581),(396,582),(466,582), #columnA 12-7
         (537,581),(607,582)] #columnA 6-5

#these are empty one dimentional lists that will later include 
#the average color value within the assigned kernel size within each indivudal well
bAvgFilledWells = []
gAvgFilledWells = []
rAvgFilledWells = []
bAvgFilledWells2 = []
gAvgFilledWells2 = []
rAvgFilledWells2 = []
#this for loop finds the average color value within the kernel of each 
#well, excludnig the wells that are empty (first column, last 4 rows)
for center in centers:
    if center[1] > 132:
        if center[0] < 646:
            b = img[center[0]-4: center[0]+5, center[1]-4: center[1]+5, 0]
            g = img[center[0]-4: center[0]+5, center[1]-4: center[1]+5, 1]
            r = img[center[0]-4: center[0]+5, center[1]-4: center[1]+5, 2]
            bAvg = np.mean(b)
            gAvg = np.mean(g)
            rAvg = np.mean(r)
            
            #this adds the average color values within each well to a one dimentional list
            bAvgFilledWells.append(bAvg)
            gAvgFilledWells.append(gAvg)
            rAvgFilledWells.append(rAvg)
            bAvgFilledWells2.append(bAvg)
            gAvgFilledWells2.append(gAvg)
            rAvgFilledWells2.append(rAvg)
bcolumn = []
gcolumn = []
rcolumn = []

bcolumn2 = []
gcolumn2 = []
rcolumn2 = []

"""
###############################################################################################################################
##### For loop goes through the column of the first color and calculates the absorbance value
##### and adds it to a list
#####For Loop was created with the help of Ben Zwiener
################################################################################################################################
"""
#List of the average color value for each channel of each point found above
AvgFilledWells = [bAvgFilledWells, gAvgFilledWells, rAvgFilledWells]
#empty list that will hold the absorbance values of each column for each color channel
Color = [bcolumn, gcolumn, rcolumn]

for (list, col) in zip(AvgFilledWells, Color):
    for i in range(0,7):
        #average the color values of the column and convert to absorbance
        absorbance = -math.log10(np.mean(list[8*i:8*i+4])/65536)
        col.append(absorbance)
        
#x points - concentrations
concentrationsArray = np.array([0.00000195, 0.00000975, 0.0000195, 0.0000975, 0.000195, 0.000975, 0.00195])
concentrationsList = [0.00000195, 0.00000975, 0.0000195, 0.0000975, 0.000195, 0.000975, 0.00195]


"""
###############################################################################################################################
##### For loop plots the unedited points for the color channel then it looks at the rvalues to determine the LOL
##### if the graph fits Beer's law then it is shown.
##### rvalues are printed out along with the length of the list with absorbance and the concentrations list
#####
##### Graphs shown are in BGR order with the original graph first, then the adjusted graph if the r value > 0.99 and not
##### equal to 1.0
##############################################################################################################################
"""

for col in (Color):
    #define the x points in the for loop so that we start with the same list every time
    concentrationsArray = np.array([0.00000195, 0.00000975, 0.0000195, 0.0000975, 0.000195, 0.000975, 0.00195])
    concentrationsList = [0.00000195, 0.00000975, 0.0000195, 0.0000975, 0.000195, 0.000975, 0.00195]
    #plot the points in a scatter plot
    plt.plot(concentrationsArray, col, 'bo')
    #This plots the best fit line of the blue channel for the first color
    m, b = np.polyfit(concentrationsArray, col, 1)
    plt.plot(concentrationsArray, m*concentrationsArray + b, '-')
    plt.title('Original col Graph')
    plt.show()
    #this returns info abou the best fit line - slope, intercept, rvalue 
    out = stats.linregress(concentrationsArray, col) #FROM BEN Z AND MARISA F
    slope = out[0]
    intercept = out[1]
    rvalues = [out[2]]
    
    #this for loop determines the LOL based on r^2 values (must be > .99)
    for rvalue in rvalues:
        if rvalue**2 < 0.99:
            #delete the point with the greatest concetration  
            del col[-1]
            del concentrationsList[-1] 
            del rvalue
            #finds the new r value with the new points
            out = stats.linregress(concentrationsList, col)
            rvalues.append(out[2])
        #if there is only two points left, break the for loop and don't show 
        #the graph
        elif rvalue**2 == 1.0:
            break
        elif rvalue**2 >= 0.99:
            concentrationsArrayList = np.array(concentrationsList)
            #plot the points in a scatter plot
            plt.plot(concentrationsArrayList, col, 'bo')
        
            #This plots the best fit line of the blue channel for the first color
            m, b = np.polyfit(concentrationsArrayList, col, 1)
            plt.plot(concentrationsArrayList, m*concentrationsArrayList + b, '-')
            
            #Add labels to the graph
            plt.xlabel('concentrations')
            plt.ylabel('Absorbance')
            plt.title('Adjusted_col_channel')
                    
            #show the finished graph
            plt.show()
            #break out of the for lop when a best fit line fit's Beer's Law    
            break
    print("r_value for the channel", rvalue**2)
    print("Length of columns list:", len(col))
    print("concentrations list:", concentrationsList)
   

'''
###########################################################################
###########################################################################
###########################################################################
                    This Part IS for The Second 
                       Color In The Well Plate
############################################################################
############################################################################
############################################################################
'''
bcolumn2 = []
gcolumn2 = []
rcolumn2 = []

"""
###############################################################################
#####Graphs show original blue then adjusted blue, original green 
#####then adjusted green, original red and then adjusted red
###### For loop was created with the help of Ben Zwiener
###############################################################################
"""


AvgFilledWells2 = [bAvgFilledWells2, gAvgFilledWells2, rAvgFilledWells2]
ChannelColumn2 = [bcolumn2, gcolumn2, rcolumn2]

for (list, col2) in zip(AvgFilledWells2, ChannelColumn2):
    for i in range(0,7):
        color = -math.log10(np.mean(list[(8*i)+4:8*i+8])/65536)   #[(8*i)+4:8*i+8]
        col2.append(color)
 
concentrationsArray2 = np.array([0.00000195, 0.00000975, 0.0000195, 0.0000975, 0.000195, 0.000975, 0.00195])
concentrationsList2 = [0.00000195, 0.00000975, 0.0000195, 0.0000975, 0.000195, 0.000975, 0.00195]


"""
###############################################################################################################################
##### 
#####
################################################################################################################################
"""

for col2 in ChannelColumn2:
    concentrationsList2 = [0.00000195, 0.00000975, 0.0000195, 0.0000975, 0.000195, 0.000975, 0.00195]
    #plot the points in a scatter plot
    plt.plot(concentrationsArray2, col2, 'bo')
#This plots the best fit line of the blue channel for the first color
    m, b = np.polyfit(concentrationsArray2, col2, 1)
    plt.plot(concentrationsArray2, m*concentrationsArray2 + b, '-')
    plt.title('Original col Graph')
    plt.show()
#this returns info abou the best fit line - slope, intercept, rvalue 
    out = stats.linregress(concentrationsArray2, col2) #FROM BEN Z AND MARISA F
    slope = out[0]
    intercept = out[1]
    rvalues2 = [out[2]]

    #thsi for loop determines the LOL based on r^2 values (must be > .99)
    for rvalue in rvalues2:
        if rvalue**2 < 0.99:
            #delete the point with the greatest concetration  
            del col2[-1]
            del concentrationsList2[-1] 
            del rvalue      
            out = stats.linregress(concentrationsList2, col2)
            rvalues2.append(out[2])
        #break out of the for lop when a best fit line fit's Beer's Law    
        elif rvalue**2 == 1.0:
            break
        elif rvalue**2 >= 0.99:
            concentrationsArrayList2 = np.array(concentrationsList2)
            #plot the points in a scatter plot
            plt.plot(concentrationsArrayList2, col2, 'bo')
    
            #This plots the best fit line of the blue channel for the first color
            m, b = np.polyfit(concentrationsArrayList2, col2, 1)
            plt.plot(concentrationsArrayList2, m*concentrationsArrayList2 + b, '-')
                
            #Add labels to the graph
            plt.xlabel('concentrations')
            plt.ylabel('Absorbance')
            plt.title('Adjusted_col_channel')
                
            #show the finished graph
            plt.show()
            break
            
    print("r_value for the channel", rvalue**2)
    print("Length of columns list:", len(col2))
    print("concentrations list:", concentrationsList2)
    

 
'''
##############################################################################
##############################################################################
        CONGRATULATIONS...YOU HAVE MADE IT TO THE END OF OUR CODE.
            NOW GO TREAT YOURSELF TO A WALK AND ICE CREAM :)
##############################################################################
##############################################################################
'''
