#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 21 15:08:12 2017

@author: Michael Tross

Seed Dimensions
"""
'''
import cv2, sys
#from matplotlib import pyplot as plt

#read command-line arguments
filename = sys.argv[1]
t = int(sys.argv[2])

#gray scale
img = cv2.imread(sys.argv[1], cv2.IMREAD_GRAYSCALE)

#display image
cv2.namedWindow("Grayscale Image", cv2.WINDOW_NORMAL)
cv2.imshow("Grayscale Image", img)
cv2.waitKey (0)


#create binary image
gray = cv2.cvtColor (img, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(gray, (5,5), 0)
(t, binary) = cv2. threshold(blur, t, 255, cv2.THRESH_BINARY) 


# FIND CONTOURS
(_, contours, hierarachy) = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_APPROX_SIMPLE)

print()
'''

import numpy as np
import cv2, sys

filename = sys.argv[1]
k = int(sys.argv[2])
t = int(sys.argv[3])



img = cv2.imread ('DSC_0369.JPG')


#clip the intersting part of the image
clip = img[684:1352, 860:2344, :] # {y:y+h, x:x+w}

#display clippped image
cv2.namedWindow("clipped", cv2.WINDOW_NORMAL)
cv2.imshow("clippped", clip)
cv2.waitKey(0)




# Turn image into binary image
gray = cv2. cvtColor(clip, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(gray, (5, 5), 0) 
(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY)


#display binary image
cv2.namedWindow("Contour Image", cv2.WINDOW_NORMAL)
cv2.imshow("Contour Image",blur)
cv2.waitKey (0)

#find contours
(_, contours,_) = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

#draw contours
cv2.drawContours(clip,contours, -1, (0, 0, 255), 5)

cv2.namedWindow("output", cv2.WINDOW_NORMAL)
cv2.imshow("output", clip)
cv2.waitKey(0)


'''
imgray = cv2.cvtColor(im, cv2. COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(imgray, 127, 255, 0)
im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
'''
#Draw contours
#cv2.drawContours(im2, contours, -1, (0,255,0), 3)

#display image
#cv2.namedWindow("Contour Image", cv2.WINDOW_NORMAL)
#cv2.imshow("Contour Image", im2)
#cv2.waitKey (0)
