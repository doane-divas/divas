#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 24 09:40:20 2017
Objectives:
1. 

@author: AdreAnna Earnest & Benjamin Zwiener
"""

import cv2
import numpy as np
#########################################################
####Function
####This function:
#########################################################
def adjustContour():
    global img, blur, t, mask, contours, minimum, maximum
    #create contours
    mask2 = mask.copy()
    (_, contours, _) = cv2.findContours(mask2, cv2.RETR_TREE, 
    cv2.CHAIN_APPROX_SIMPLE)
    
    new_contours = []
    
    for contour in contours:
        
        if len(contour) > minimum and len(contour) < maximum:
            
            new_contours.append(contour)
    # draw contours over original image
    cv2.drawContours(img, new_contours, -1, (0, 0, 255), 5)
#########################################################
####Function
####This function:
#########################################################
def fixedThresh():
    global img, blur, t, mask, minimum, maximum
    (t, mask) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY)
    img = np.copy(img2)
    adjustContour()
    cv2.imshow('threshold image', mask)
    cv2.imshow('image', img)
#########################################################
####Function
####This function:
#########################################################
def adjustThresh(v):
    global img, blur, t, mask
    t = v
    fixedThresh()
    
def adjustMin(v):
    global minimum
    minimum = v
    fixedThresh()
    
def adjustMax(v):
    global maximum
    maximum = v
    fixedThresh()
    
#########################################################
####Function
####This function:
#########################################################   
#THIS IS THE MAIN FUNCTION OF THE FILE!!!!
def trackbarContour(filename,kernel):
    global img, blur, t, img2, minimum, maximum
    minimum = 60
    maximum = 85
    image = cv2.imread(filename)
    img = image
    img2 = image
    gray = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
    gray  = gray
    blur = cv2.GaussianBlur(gray, (kernel,kernel), 0)
    cv2.namedWindow('threshold image', cv2.WINDOW_NORMAL)
    cv2.namedWindow('image', cv2.WINDOW_NORMAL)
    (t, mask) = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    t = int(t)
    cv2.createTrackbar('thresh', 'threshold image', t, 255, adjustThresh)
    cv2.createTrackbar('minimum', 'threshold image', minimum, 130, adjustMin)
    cv2.createTrackbar('maximum', 'threshold image', maximum, 300, adjustMax) # change the 4th parameter to a higher number if needed
    fixedThresh()
    cv2.waitKey(0)
    #return contour values and threshold number
    return contours, t

import sys    

filename = sys.argv[1]
kernel = 5
    
trackbarContour(filename, kernel)

