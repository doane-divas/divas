#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 19:33:25 2017

@author: Michael Tross and Benjamin Zwiener
"""

#imports for this program
import sys, cv2
import tkinter as tk
import pandas as pd
from PIL import Image
from PIL import ImageTk
import tkinter.messagebox as tkmb

filename = sys.argv[1]
t = float(sys.argv[2])
minimum = int(sys.argv[3])
maximum = int(sys.argv[4])
k = 5

img = cv2.imread(filename)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
img_clip = img
gray_clip = gray

'''
Description: This function is used to find the contours using OTSU thresholding
             and then slim those contours down to the five largest.
             
Parameters:  slimmed_contours_list - list of top five larg. contours for each tube
             thres_list - list of the threshold num used for each tube
             subfilenames - the list created from get_subfilenames function
             
create_dataframe function algorithm:
    
    I.  intialize empty list root_dataframes
    II. loop through slimmed_contours_list, thres_list, and subfilenames
        1. create a dataframe called single_root_dataframe
        2. loop through the contours_list
            A. calculate the area of the contour
            B. create a row with index number, filename, area, and threshold number
            C. append that to the single_root_dataframe
            D. reset the index of single_root_dataframe
        3. append single_root_dataframe to root_dataframes
    III.return root_dataframes
'''
def create_dataframe(contours):
    
    # create a dataframe
    single_root_dataframe = pd.DataFrame(columns = ['Index','Area'])
    
    # loop through the three lists
    for (i, contour) in enumerate(contours):
     
        # calculate the area of the contour
        area = cv2.contourArea(contour)
        
        # create a row to add to single_root_dataframe
        current_contour = pd.DataFrame(
                        {'Index': [i+1],
                         'Area': [area]})
            
        # append current_contour
        single_root_dataframe = single_root_dataframe.append(current_contour)
        
        # reset the index
        single_root_dataframe.reset_index()
        
    
    # return root_dataframes list with all nine dataframes
    return single_root_dataframe
'''
Description: This function is used to find the center of the contour so a number
                can be placed there later
             
Parameters:  contour - this is the contour the function will find the center of
             
findCentroid function algorithm:
    
    I.  find the moments and set them equal to M
    II. find the x coordinate
    III.find the y coordinate
    IV. return the (x,y) coordinates
'''
def findCentroid(contour):
    
    # find moments
    M = cv2.moments(contour)
    
    # use the moments to get the x value
    x = int(M['m10'] / M['m00'])
    
    # use the moments to get the y value
    y = int(M['m01'] / M['m00'])
    
    # return the x and y values
    return (x, y)
    
'''
Description: This function is used to create the Graphical User Interface.
             It is more or less the main functionality of the script.
             
Parameters:  N/A
             
GUI function algorithm:
    
    I.  
'''
def GUI(dataframe, contours, image):
    
    # function binded with the get data button
    def get_data():
        
        # ask the user if they meant to press the button
        answer = tkmb.askquestion('Window', 'Did you mean to press that?')
        
        
        if answer == 'yes':
            
            index = get_data_entry_txtbox.get()
            
            seed_number = choose_seed_entry_txtbox.get()
                
            index = int(index)
    
            print('index:', index)
            print('seed number:',seed_number)
            
            data = dataframe[(dataframe['Index'] == index)]

            data = data.iloc[0].values

            area = float(data[0])
            
            datasheet = 'Seed_Areas.csv'

            Outfile = open(datasheet, "a")
            
            Outfile.write('\n' + seed_number + ',' + str(area))

            Outfile.close()
            
            tkmb.showinfo('Window', 'Your data has been saved. Exit the window.')
            
        else:
            
            tkmb.showinfo('Window', 'Okay. Exit and go back to window')

    # variables for width and height of widgets
    x_width = 825
    y_height = 550
        
    # initalize the window
    root = tk.Tk()
    
    # panel for normal image
    panelA = None
    # panel for contoured image
    panelB = None
    
    # draw the contours on the contoured_image
    cv2.drawContours(image, contours, -1, (0, 0, 255), 5)
    
    # loop through contours to put numbers on them
    for (i, contour) in enumerate(contours):
        
        # put numbers on image
        cv2.putText(image, str(i+1), findCentroid(contour), 
                    cv2.FONT_HERSHEY_PLAIN, 6, (0, 0, 0), 6)
    
    # turn images from BGR to RGB for tkinter
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    
    # make PIL Images from an array
    image = Image.fromarray(image)
    
    # resize the images684:1352, 860:2344]
    image = image.resize((x_width,y_height), Image.ANTIALIAS)
    
    # make the images PhotoImages
    image = ImageTk.PhotoImage(image)

    # the first panel wil684:1352, 860:2344]l store our original image
    panelA = tk.Frame(root, height=y_height, width=x_width/2)
    panelA.pack(side='left', padx=10, pady=10)
    
    # change contour button to be put in panelC
    get_data_btn = tk.Button(panelA, text='Get Data', command=get_data)
    get_data_btn.grid(row=0, pady=10)
    
    # label for data entry textbox
    data_entry_label = tk.Label(panelA, text="Index Number")
    data_entry_label.grid(row=1, column=0)
    
    # get data entry Entry to be put in panelC by get data button
    get_data_entry_txtbox = tk.Entry(panelA)
    get_data_entry_txtbox.grid(row=1, column=1, pady=10)
    
    # label for data entry textbox
    data_entry_label = tk.Label(panelA, text="Seed Number")
    data_entry_label.grid(row=2, column=0)
    
    # get data entry Entry to be put in panelC by get data button
    choose_seed_entry_txtbox = tk.Entry(panelA)
    choose_seed_entry_txtbox.grid(row=2, column=1, pady=10)
     
    # the second panel will store the original image with contours on it
    panelB = tk.Label(image=image)
    panelB.image = image
    panelB.pack(side="left", padx=10, pady=10)
    
    # kick off the GUI
    root.mainloop()
'''

'''
def Main():
    
    blur = cv2.GaussianBlur(gray_clip, (5, 5), 0) 
    (thres, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY)
    
    (_,contours,_) = cv2.findContours(binary,cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    new_contours = []
    
    for contour in contours:
        
        if len(contour) > minimum and len(contour) < maximum:
            
            new_contours.append(contour)
            
    dataframe = create_dataframe(contours=new_contours)
    
    print(dataframe)
    
    GUI(dataframe=dataframe, contours=new_contours, image=img_clip)
        
Main()