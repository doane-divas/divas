"""
*Authors: Danny Tran & Truc Doan
*Creating graph/s (Demonstrating Beer's law) with our given information
in another file
*6/6/2017
"""
#importing matplotlib to create a graph
import matplotlib.pyplot as plt
from Absorb import All_Absorb as aa
from Absorb import All_Absorb_2 as aa2

x = [1.95e-06, 9.75e-06, 1.95e-05, 9.75e-05, 1.95e-04, 9.75e-04, 1.95e-03] * 8 
y = aa
#ploting out the points while labeling the graph
plt.scatter(x,y, label='(concentration, Absorption)')
plt.xticks(np.arange(min(x), max(x)+1, 0.01))
plt.yticks(np.arange(0, max(x)+1, 0.01))
plt.title('Color One Scatter graph')
plt.ylabel('Absorption')
plt.xlabel('Concentration')
plt.grid(True)
plt.tight_layout()
plt.savefig('points.png')
plt.show()
"""
#inserting a slope for the line of best fit
slope, intercept = np.polyfit(x, y, 1)
abline_values = [slope *i +intercept for i in x]
plt.plot(x, y, '--' )
plt.plot(x, abline_values, 'b')
plt.show()
"""