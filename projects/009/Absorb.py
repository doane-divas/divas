#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#Authers: Danny and Truc
"""
*Authors: Danny Tran & Truc Doan
*6/7/2017
*Getting the values of absorption for each concentration
"""

import cv2, numpy as np, sys
import matplotlib.pyplot as plt 
from scipy import stats
from statistics import mean
#import operator
#from functools import reduce, partial

#filename will be taken from the command line and process as the image variable
filename = sys.argv[1]
img = cv2.imread(filename, cv2.IMREAD_COLOR + cv2.IMREAD_ANYDEPTH)

#centroids taken from Chris
wells = [img[110,165],img[184,165],img[254,165],img[324,165],img[396,165],img[466,165], #columnG 1-6
         img[537,165],img[607,165], #columnG 7 & 8
         img[110,235],img[184,235],img[254,235],img[324,235],img[396,235],img[466,235], #columnF 1-6
         img[537,235],img[607,235], #columnF 7 & 8
         img[110,305],img[184,305],img[254,305],img[324,305],img[396,305],img[466,305], #columnE 1-6
         img[537,305],img[607,305], #columnE 7 & 8
         img[110,373],img[184,373],img[254,373],img[324,373],img[396,373],img[466,373], #columnD 1-6
         img[537,373],img[607,373], #columnD 7 & 8
         img[110,442],img[184,442],img[254,442],img[324,442],img[396,442],img[466,442], #columnC 1-6
         img[537,442],img[607,442], #columnC 7 & 8
         img[109,511],img[184,511],img[254,511],img[324,511],img[396,511],img[466,511], #columnB 1-6
         img[537,512],img[607,512], #columnB 7 & 8
         img[109,582],img[184,582],img[254,582],img[324,581],img[396,582],img[466,582], #columnA 1-6
         img[537,581],img[607,582]] #columnA 7 & 8

"""
#rows
r1 = wells[[0],[8],[16],[24],[32],[40],[48]]
r2 = wells[[1],[9],[17],[25],[33],[41],[49]]
r3 = wells[[2],[10],[18],[26],[34],[42],[50]]
r4 = wells[[3],[11],[19],[27],[35],[43],[51]]
r5 = wells[[4],[12],[20],[28],[36],[44],[52]]
r6 = wells[[5],[13],[21],[29],[37],[45],[53]]
r7 = wells[[6],[14],[22],[30],[38],[46],[54]]
r8 = wells[[7],[15],[23],[31],[39],[47],[55]]

Color_row_1 = [r1, r2, r3, r4]
Color_row_2 = [r5, r6, r7, r8]
rows = [Color_row_1, Color_row_2]
"""

#acknowledgement to Adreanna and Keeliann
#listing averages from bgr averages starting in line 53-59
bAvgWells = []
gAvgWells = []
rAvgWells = []

#getting average color values in each well
for center in wells:
    b = img[center[0]-4: center[0]+5, center[1]-4: center[1]+5, 0]
    g = img[center[0]-4: center[0]+5, center[1]-4: center[1]+5, 1]
    r = img[center[0]-4: center[0]+5, center[1]-4: center[1]+5, 2]
    bAvg = np.mean(b)
    gAvg = np.mean(g)
    rAvg = np.mean(r)
    
    #Add each value from loop into list in line 54-56
    bAvgWells.append(bAvg)
    gAvgWells.append(gAvg)
    rAvgWells.append(rAvg)

    
#columns for blue values of top color
c_a_b = bAvgWells[48:51]
c_b_b = bAvgWells[40:43]
c_c_b = bAvgWells[32:35]
c_d_b = bAvgWells[24:27]
c_e_b = bAvgWells[16:19]
c_f_b = bAvgWells[8:11]
c_g_b = bAvgWells[0:3]

# columns for blue values of bottom color
c_a_b_2 = bAvgWells[52:55]
c_b_b_2 = bAvgWells[44:47]
c_c_b_2 = bAvgWells[36:39]
c_d_b_2 = bAvgWells[28:31]
c_e_b_2 = bAvgWells[20:23]
c_f_b_2 = bAvgWells[12:15]
c_g_b_2 = bAvgWells[4:7]

#columns for green values of top color
c_a_g = gAvgWells[48:51]
c_b_g = gAvgWells[40:43]
c_c_g = gAvgWells[32:35]
c_d_g = gAvgWells[24:27]
c_e_g = gAvgWells[16:19]
c_f_g = gAvgWells[8:11]
c_g_g = gAvgWells[0:3]

# columns for green values of bottom color
c_a_g_2 = gAvgWells[52:55]
c_b_g_2 = gAvgWells[44:47]
c_c_g_2 = gAvgWells[36:39]
c_d_g_2 = gAvgWells[28:31]
c_e_g_2 = gAvgWells[20:23]
c_f_g_2 = gAvgWells[12:15]
c_g_g_2 = gAvgWells[4:7]

#columns for red values of top color
c_a_r = rAvgWells[48:51]
c_b_r = rAvgWells[40:43]
c_c_r = rAvgWells[32:35]
c_d_r = rAvgWells[24:27]
c_e_r = rAvgWells[16:19]
c_f_r = rAvgWells[8:11]
c_g_r = rAvgWells[0:3]

# columns for red values of bottom color
c_a_r_2 = rAvgWells[52:55]
c_b_r_2 = rAvgWells[44:47]
c_c_r_2 = rAvgWells[36:39]
c_d_r_2 = rAvgWells[28:31]
c_e_r_2 = rAvgWells[20:23]
c_f_r_2 = rAvgWells[12:15]
c_g_r_2 = rAvgWells[4:7]
  
"""
#displaying the plate image as a reference for next step
cv2.namedWindow("Reference plate", cv2.WINDOW_NORMAL)
cv2.imshow("Reference plate", img)
cv2.waitKey(0)
"""

#values from one of the selections 
#will be put into these list and grabbed at the end
Avg_b = []
Avg_g = []
Avg_r = []
Avg_final = [Avg_b,Avg_g,Avg_r]

#person can now select wanted average of certain concentration
select_color = input('please type in "1" for top color or "2" for bottom value')
select_column = input('please type in wanted column letter (lowercase)')
while select_column == 'h':
    print('Error: Column h does not have any values, please try again')
    select_column = input('please type in wanted column letter (lowercase)')


if select_color == '1':
    if select_column == 'a':
        print('Average blue value:', np.mean(c_a_b))
        Avg_b.append(np.mean(c_a_b))
        print('Average green value:', np.mean(c_a_g))
        Avg_g.append(np.mean(c_a_g))
        print('Average red value:', np.mean(c_a_r))
        Avg_r.append(np.mean(c_a_r))

if select_color == '1':
    if select_column == 'b':
        print('Average blue value:', np.mean(c_b_b))
        Avg_b.append(np.mean(c_b_b))
        print('Average green value:', np.mean(c_b_g))
        Avg_g.append(np.mean(c_b_g))
        print('Average red value:', np.mean(c_b_r))
        Avg_r.append(np.mean(c_b_r))
if select_color == '1':
    if select_column == 'c':
        print('Average blue value:', np.mean(c_c_b))
        Avg_b.append(np.mean(c_c_b))
        print('Average green value:', np.mean(c_c_g))
        Avg_g.append(np.mean(c_c_g))
        print('Average red value:', np.mean(c_c_r))
        Avg_r.append(np.mean(c_c_r))
if select_color == '1':
    if select_column == 'd':
        print('Average blue value:', np.mean(c_d_b))
        Avg_b.append(np.mean(c_d_b))
        print('Average green value:', np.mean(c_d_g))
        Avg_g.append(np.mean(c_d_g))
        print('Average red value:', np.mean(c_d_r))
        Avg_r.append(np.mean(c_d_r))
if select_color == '1':
    if select_column == 'e':
        print('Average blue value:', np.mean(c_e_b))
        Avg_b.append(np.mean(c_e_b))
        print('Average green value:', np.mean(c_e_g))
        Avg_g.append(np.mean(c_e_g))
        print('Average red value:', np.mean(c_e_r))
        Avg_r.append(np.mean(c_e_r))
if select_color == '1':
    if select_column == 'f':
        print('Average blue value:', np.mean(c_f_b))
        Avg_b.append(np.mean(c_f_b))
        print('Average green value:', np.mean(c_f_g))
        Avg_g.append(np.mean(c_f_g))
        print('Average red value:', np.mean(c_f_r))
        Avg_r.append(np.mean(c_f_r))
if select_color == '1':
    if select_column == 'g':
        print('Average blue value:', np.mean(c_g_b))
        Avg_b.append(np.mean(c_g_b))
        print('Average green value:', np.mean(c_g_g))
        Avg_g.append(np.mean(c_g_g))
        print('Average red value:', np.mean(c_g_r))
        Avg_r.append(np.mean(c_g_r))
if select_color == '2':
    if select_column == 'a':
        print('Average blue value:', np.mean(c_a_b_2))
        Avg_b.append(np.mean(c_a_b_2))
        print('Average green value:', np.mean(c_a_g_2))
        Avg_g.append(np.mean(c_a_g))
        print('Average red value:', np.mean(c_a_r_2))
        Avg_r.append(np.mean(c_a_r_2))
if select_color == '2':
    if select_column == 'b':
        print('Average blue value:', np.mean(c_b_b_2))
        Avg_b.append(np.mean(c_b_b_2))
        print('Average green value:', np.mean(c_b_g_2))
        Avg_g.append(np.mean(c_b_g))
        print('Average red value:', np.mean(c_b_r_2))
        Avg_r.append(np.mean(c_b_r_2))
if select_color == '2':
    if select_column == 'c':
        print('Average blue value:', np.mean(c_c_b_2))
        Avg_b.append(np.mean(c_c_b_2))
        print('Average green value:', np.mean(c_c_g_2))
        Avg_g.append(np.mean(c_c_g))
        print('Average red value:', np.mean(c_c_r_2))
        Avg_r.append(np.mean(c_c_r_2))
if select_color == '2':
    if select_column == 'd':
        print('Average blue value:', np.mean(c_d_b_2))
        Avg_b.append(np.mean(c_d_b_2))
        print('Average green value:', np.mean(c_d_g_2))
        Avg_g.append(np.mean(c_d_g))
        print('Average red value:', np.mean(c_d_r_2))
        Avg_r.append(np.mean(c_d_r_2))
if select_color == '2':
    if select_column == 'e':
        print('Average blue value:', np.mean(c_e_b_2))
        Avg_b.append(np.mean(c_e_b_2))
        print('Average green value:', np.mean(c_e_g_2))
        Avg_g.append(np.mean(c_e_g))
        print('Average red value:', np.mean(c_e_r_2))
        Avg_r.append(np.mean(c_e_r_2))
if select_color == '2':
    if select_column == 'f':
        print('Average blue value:', np.mean(c_f_b_2))
        Avg_b.append(np.mean(c_f_b_2))
        print('Average green value:', np.mean(c_f_g_2))
        Avg_g.append(np.mean(c_a_g))
        print('Average red value:', np.mean(c_f_r_2))
        Avg_r.append(np.mean(c_f_r_2))
if select_color == '2':
    if select_column == 'g':
        print('Average blue value:', np.mean(c_g_b_2))
        Avg_b.append(np.mean(c_g_b_2))
        print('Average green value:', np.mean(c_g_g_2))
        Avg_g.append(np.mean(c_g_g))
        print('Average red value:', np.mean(c_g_r_2))
        Avg_r.append(np.mean(c_g_r_2))
    

#prints the single average of BGR values
result = np.mean(Avg_final)
print('Mean value of all BGR averages:', result)

Absorb = np.log10(result/(2**8))
Final = Absorb * -1
print('Absorption value is:', Final)


#simply regrouping all the columns and dividing columns into a 
#single value
c_b = [np.mean(c_a_b),np.mean(c_b_b),np.mean(c_c_b),np.mean(c_d_b),np.mean(c_e_b),np.mean(c_f_b),np.mean(c_g_b)] #Color one, blue
c_g = [np.mean(c_a_g),np.mean(c_b_g),np.mean(c_c_g),np.mean(c_d_g),np.mean(c_e_g),np.mean(c_f_g),np.mean(c_g_g)] #Color one, green
c_r = [np.mean(c_a_r),np.mean(c_b_r),np.mean(c_c_r),np.mean(c_d_r),np.mean(c_e_r),np.mean(c_f_r),np.mean(c_g_r)] #Color one, red
c_b_2 = [np.mean(c_a_b_2),np.mean(c_b_b_2),np.mean(c_c_b_2),np.mean(c_d_b_2),np.mean(c_e_b_2),np.mean(c_f_b_2),np.mean(c_g_b_2)] #Color two, blue
c_g_2 = [np.mean(c_a_g_2),np.mean(c_b_g_2),np.mean(c_c_g_2),np.mean(c_d_g_2),np.mean(c_e_g_2),np.mean(c_f_g_2),np.mean(c_g_g_2)] #Color two, green
c_r_2 = [np.mean(c_a_r_2),np.mean(c_b_r_2),np.mean(c_c_r_2),np.mean(c_d_r_2),np.mean(c_e_r_2),np.mean(c_f_r_2),np.mean(c_g_r_2)] #color two, red

"""
#Values retrieved from line 284-288
avg_one = []
avg_two = []

def add(a, b, c):
    d = (a + b + c)
    return d
d = [3]*7
#Divdes the list together for averages of all columns
k = add(c_b, c_g, c_r)
j = list(map(partial(reduce, operator.truediv), zip(k,d)))
avg_one.append(j)
h = add(c_b_2, c_g_2, c_r_2)
l = list(map(partial(reduce, operator.truediv), zip(h,d)))
avg_two.append(l)
"""

#These values will be recalled as y coordinates for the graph
All_Absorb_b = []
All_Absorb_g = []
All_Absorb_r = []
All_Absorb_b2 = []
All_Absorb_g2 = []
All_Absorb_r2 = []
All_Absorb = [All_Absorb_b,All_Absorb_g,All_Absorb_r]
All_Absorb_2 = [All_Absorb_b2, All_Absorb_g2, All_Absorb_r2]
#avg_one_1 = np.array(avg_one)
#avg_two_2 = np.array(avg_two)
#Converting all averages into absorption values
for i in c_b:
    k = i/(2**8)
    total_final = (np.log10(k))
    final_1 = total_final * -1
    All_Absorb_b.append(final_1)
for i in c_g:
    k = i/(2**8)
    total_final_2 = (np.log10(k))
    final_2 = total_final_2 * -1
    All_Absorb_g.append(final_2)
for i in c_r:
    k = i/(2**8)
    total_final_2 = (np.log10(k))
    final_2 = total_final_2 * -1
    All_Absorb_r.append(final_2)
for i in c_b_2:
    k = i/(2**8)
    total_final_2 = (np.log10(k))
    final_2 = total_final_2 * -1
    All_Absorb_b2.append(final_2)
for i in c_g_2:
    k = i/(2**8)
    total_final_2 = (np.log10(k))
    final_2 = total_final_2 * -1
    All_Absorb_g2.append(final_2)
for i in c_r_2:
    k = i/(2**8)
    total_final_2 = (np.log10(k))
    final_2 = total_final_2 * -1
    All_Absorb_r2.append(final_2)

print('All absorption values for color 1 is:', All_Absorb)
print('All absorption values for color 2 is:', All_Absorb_2)

x = [1.95e-03, 9.75e-04, 1.95e-04, 9.75e-05, 1.95e-05, 9.75e-06, 1.95e-06]
y = All_Absorb
y2 = All_Absorb_2
m, b = np.polyfit(x, All_Absorb_b, 1)


plt.xlim([min(x), max(x)+min(x)])
plt.ylim([0.01,0.13]) # Change the max limit of y
plt.title('Color One Scatter Graph')
plt.ylabel('Absorption')
plt.xlabel('Concentration')
plt.grid(True)
plt.tight_layout()
plt.scatter(x, All_Absorb[0], color = 'blue', label = 'Blue')
plt.scatter(x, All_Absorb[1], color = 'green', label = 'Green')
plt.scatter(x, All_Absorb[2], color = 'red', label = 'Red')
#plt.plot(x, m*x+b, '-')
plt.savefig('points_color1.jpg')
plt.show()


plt.xlim([min(x), max(x)+min(x)])
plt.ylim([0.01,0.13]) # Change the max limit of y
plt.title('Color Two Scatter Graph')
plt.ylabel('Absorption')
plt.xlabel('Concentration')
plt.grid(True)
plt.tight_layout()
plt.scatter(x, All_Absorb_2[0], color = 'blue', label = 'Blue')
plt.scatter(x, All_Absorb_2[1], color = 'green', label = 'Green')
plt.scatter(x, All_Absorb_2[2], color = 'red', label = 'Red')
plt.savefig('points_color2.jpg')
plt.show()
