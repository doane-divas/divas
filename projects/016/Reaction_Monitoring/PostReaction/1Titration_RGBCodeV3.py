#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 14:29:07 2017

@author: Chris and Louise
"""
# import the necessary packages
import cv2
import pandas as pd
import numpy as np
import glob
import sys
import matplotlib.pyplot as plt
#use the glod tool to go into folder and look at the files/images in the folder
#'titration_images/*.jpg" is the path to folder

path_to_file = sys.argv[1]
forname = path_to_file[:-2]
print(forname)

filenames = [img for img in glob.glob(path_to_file)]
             
#this lines sorts all of the images in alphabetical order,
#therefore also in chronological order
#added this when the code was reading the images in a random order
filenames.sort()


#an empty list to store all of the images in
images = []

#reads all of the images and adds them into the empty list
#also prints all of the images added, good to check order of images
for img in filenames:
    n= cv2.imread(img)
    images.append(n)
    print (img)
    
    
    
#empty lists to store the RGB data in
blue = []
green = []
red = []

#goes through all the images in the the images list
for image in images:
        
        #this will take the RGB data from the specified region
        #may need to work on a contour code to better achieve this
        #since for every titration run the coordinates need to be changes
        b = image[600 - 25 : 600 + 25, 325 - 10 : 325 + 10, 0]
        g = image[600 - 25 : 600 + 25, 325 - 10 : 325 + 10, 1]
        r = image[600 - 25 : 600 + 25, 325 - 10 : 325 + 10, 2]
            
        #averages the RGB values of all the pixels in the above region
        bAvg = np.mean(b)
        gAvg = np.mean(g)
        rAvg = np.mean(r)
            
        # append RGB means into empty lists
        blue.append(bAvg)
        green.append(gAvg)
        red.append(rAvg)
        
time_list = list(range(len(blue)))

time = []
for i in time_list:
    i = i + 1
    
    time.append(i)
        
    
        
#builds dataframe based on the 3 different color channels
#each column is a channel, every row is a different image        
dataframe = pd.DataFrame(time, columns = ['Time (s)'])
dataframe['blue'] = blue
dataframe['green'] = green
dataframe['red'] = red
print(dataframe)        
        
#exports dataframe onto csv is same layout
dataframe.to_csv(forname + '_RGBdata.csv')   


#need to look for real time reading of images once they are placed in the folder
#plots a seperate graph for each color value
dataframe.plot(x = 0,y = ['blue','green','red'])
plt.ylabel("Color value (RGB)")
plt.title('RGB data vs. Time in seconds')
plt.show()