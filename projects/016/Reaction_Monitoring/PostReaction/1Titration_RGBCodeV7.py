#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 14:29:07 2017

@author: Chris and Louise
"""



# import the necessary packages
import cv2
import pandas as pd
import numpy as np
import glob
import sys
import matplotlib.pyplot as plt
import time #used to pause code from running in specific situations

#uses a command line argument to define what the directory being used is called
path_to_file = sys.argv[1]



#####used forcsv export file naming#######
#this cuts the folder name to something shorter for naming purposes
forname = path_to_file[:-2]
#print(forname)

################################

def fileread():
    

    #function that reads images from folder designated on command line
    #path_to_file is the folder defining argument
    #def readFolder(path_to_file):
    
    #empty list to store the images in
    images = []
    
    #use the glod tool to go into folder and look at the files/images in the folder
    #places all of the images into a list calle 'filenames'
    filenames = [img for img in glob.glob(path_to_file)]
        
    #this lines sorts all of the images in alphabetical order,
    #therefore also in chronological order
    #added this when the code was reading the images in a random order
    filenames.sort()
    n = cv2.imread(filenames[-1])
    
    images.append(n)
    

    #reads all of the sorted images and adds them into the empty list
    #also prints all of the images added, good to check order of images

     
    return n

''' 
    for img in filenames:
        n= cv2.imread(img)
        images.append(n)
        print (img)
     '''

    #images = readFolder(path_to_file)
    
#############################################################################    
       

#function to perform RGB image analysis on the list of images read
def RGBanalysis(n):    
    
    #empty lists to store the RGB data in
    #blue = []
    #green = []
    #red = []
 


    #this will take the RGB data from the specified region
    #talk to Louise to run atleast 3-4 titrations in a marked position
    #on the stirrer to find an average position range to for general use
    b = n[359 : 479 , 266 : 486 , 0]
    g = n[359 : 479 , 266 : 486 , 1]
    r = n[359 : 479 , 266 : 486 , 2]
                
    #averages the RGB values of all the pixels in the above region
    bAvg = np.mean(b)
    gAvg = np.mean(g)
    rAvg = np.mean(r)
                
    # append RGB means into empty lists
    blue = bAvg
    green = gAvg
    red = rAvg
        
        
        
    return blue,green,red
################################################################
################################################################        
    
def maincode():
    
    plt.axis([0,500,0,300])
    plt.ion()
    plt.show()  
    
    blue_list = []
    green_list = []
    red_list = []


    for (i,file) in enumerate(range(300)):
        
        fileread()
        images = fileread()
        
        RGBanalysis(images)
        blue, green, red = RGBanalysis(images)
        
        blue_list.append(blue)
        green_list.append(green)
        red_list.append(red)
        
        blue_array = np.asarray(blue_list)
        green_array = np.asarray(green_list)
        red_array = np.asarray(red_list)
        
        n = i + 1
        seconds = []
        seconds.append(n)
        seconds_array = np.asarray(seconds)
        
        dataframe = pd.DataFrame(seconds, columns = ['Time (s)'])
        dataframe['blue'] = blue_list
        dataframe['green'] = green_list
        dataframe['red'] = red_list
        print(dataframe)
        
        plt.plot(seconds_array, blue_array)
        plt.plot(seconds_array, green_array)
        plt.plot(seconds_array, red_array)
        plt.draw()
        plt.pause(.001)
        
        

        
        
        
maincode()
        
       

   
                

 
            
'''        
    #exports dataframe onto csv is same layout
    dataframe.to_csv(forname + '_RGBdata.csv')   


    #need to look for real time reading of images once they are placed in the folder
    #plots one graph for each color value
    
    dataframe.plot(x = 0,y = ['blue','green','red'])
    
    plt.ylabel("Color value (RGB)")
    plt.title('RGB data vs. Time in seconds')
    
    #this part allows the code to continue without having to closing the figure
    plt.show()
    
    
'''   
    
'''    
#Beginning of looping mechanic            
images = readFolder(path_to_file) 
RGBanalysis(images)
time.sleep(10)
false = bool
found = false
while (found==false):    
    images2 = readFolder(path_to_file)
    RGBanalysis(images2)
    
'''
    

    
    