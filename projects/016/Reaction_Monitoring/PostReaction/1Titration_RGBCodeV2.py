#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  3 12:50:37 2017

@author: diva
"""

# import the necessary packages
import cv2
import pandas as pd
import numpy as np
import glob
import sys
from imutils import contours as imc
import matplotlib.pyplot as plt
#use the glod tool to go into folder and look at the files/images in the folder
#'titration_images/*.jpg" is the path to folder

path_to_file = sys.argv[1]
forname = path_to_file[:-2]
print(forname)

filenames = [img for img in glob.glob(path_to_file)]

def findCentroid(contour):
    M = cv2.moments(contour)
    x = int(M['m10'] / M['m00'])
    y = int(M['m01'] / M['m00'])
    return (x, y)             
            
#this lines sorts all of the images in alphabetical order,
#therefore also in chronological order
#added this when the code was reading the images in a random order
filenames.sort()


#an empty list to store all of the images in
images = []

#reads all of the images and adds them into the empty list
#also prints all of the images added, good to check order of images
for img in filenames:
    n= cv2.imread(img)
    images.append(n)
    print (img)

#work through list of images to use contours to find the black box
# black box is what is used to collect RGB data
for img in images:
    
    k = 5
    t = 245
    
   # blur and grayscale before thresholding
    blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(blur, (k, k), 0)

     # perform thresholding 
    (t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY) 

    # make a mask suitable for color images
    mask = cv2.merge([binary, binary, binary])
    
    # displays the grayscale mask image
    cv2.namedWindow("mask", cv2.WINDOW_NORMAL)
    cv2.imshow("mask", mask)
    cv2.waitKey(0)
    
    # find contours, one contour per well. 
    # 96 total contours
    (_, contours, hierarchy) = cv2.findContours(blur, cv2.RETR_TREE, 
            cv2.CHAIN_APPROX_SIMPLE)
    
     # use imutils function to sort contours. See the source code at
    # https://github.com/jrosebr1/imutils/blob/master/imutils/contours.py
    # for details on the options for ordering
    (sortedContours, boundingBoxes) = imc.sort_contours(contours)


    # prepare a window for display
    cv2.namedWindow("display", cv2.WINDOW_NORMAL)
    
    # draw sorted contours on the original image, one at a time, ignoring the 
    # small ones
    for (i, contour) in enumerate(sortedContours):
        
        
    
    
            # draw the current contour in red
            cv2.drawContours(img, sortedContours, i, (0, 0, 255), 5)
        
            # draw a black label with contour number
            cv2.putText(img, str(i), findCentroid(contour), 
                        cv2.FONT_HERSHEY_PLAIN, 7.5, (0, 0, 0), 5)

            cv2.imshow("display", img)
            cv2.waitKey(0)

    
    
