#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 14:29:07 2017

@author: Chris and Louise
"""



# import the necessary packages
import cv2
import pandas as pd
import numpy as np
import glob
import sys
import matplotlib.pyplot as plt
import statistics as stats

#uses a command line argument to define what the directory being used is called
path_to_file = sys.argv[1]
################################
#defines location of directory
#function that reads images from folder designated on command line
def filepull():

    #path_to_file is the folder defining argument
    #def readFolder(path_to_file):
    path_to_file = sys.argv[1]
    

    #use the glod tool to go into folder and look at the files/images in the folder
    #places all of the images into a list calle 'filenames'
    filenames = [img for img in glob.glob(path_to_file)]
        
    #this lines sorts all of the images in alphabetical order,
    #therefore also in chronological order
    #added this when the code was reading the images in a random order
    #filenames.sort()
    
    #return the sorted list of filenames
    return filenames
##############################################################
##############################################################
    
#function to read the image in cv2
def fileread(filenames):
    
    image = filenames
    
    #empty list to store the images in
    read_image = cv2.imread(image)
    

    #returns wanted image
    return read_image     
###############################################################
###############################################################    
       
#function to perform RGB image analysis on image 'n'
def RGBanalysis(n):    
    
    #modifiable pixel coordinates for center of sampling area
    x_coord = 350
    y_coord = 450
 
    #this will take the RGB data from the specified region
    #talk to Louise to run atleast 3-4 titrations in a marked position
    #on the stirrer to find an average position range to for general use
    b = n[x_coord - 100 : x_coord + 100 ,y_coord - 100 : y_coord + 100, 0]
    g = n[x_coord - 100 : x_coord + 100 ,y_coord - 100 : y_coord + 100, 1]
    r = n[x_coord - 100 : x_coord + 100 ,y_coord - 100 : y_coord + 100, 2]
                
    #averages the RGB values of all the pixels in the above region
    bAvg = np.mean(b)
    gAvg = np.mean(g)
    rAvg = np.mean(r)
                
    #defines averages a their color values
    blue = bAvg
    green = gAvg
    red = rAvg
        
        
    #returns the three values that make up an RGB code   
    return blue,green,red
################################################################
################################################################   

#list of values to look at, this will be the list of 20 first green values
#on the plot to find the avg and stdev
def initial_analysis(green_list):
    
    #takes average of first 20 green-channel values
    average_knot = stats.mean(green_list[0:20])
    
    #takes standard deviation of first 20 green-channel values
    stdev_knot = stats.stdev(green_list[0:20])
    
    #returns the average and standard deviation
    return average_knot, stdev_knot     

#################################################################
#################################################################

#function to take mean and standard deviation of most 20 most recent image's
#green-channel value
def comparison_analysis(green_list):
    
    #defines average and standard deviation of 20 most recent values    
    new_Avg = stats.mean(green_list[-20:])
    new_stdev = stats.stdev(green_list[-20:])
    
    return new_Avg, new_stdev
        
###################################################################
###################################################################
    
#function to send emai alert
def send_email():
    
    #modue needed to use gmail
    import smtplib
    
    #sender email address and password for access, using proxy email
    gmail_user = 'christopher.azaldegui@gmail.com'
    gmail_pwd = 'burksresearch2017'
    #email of receiver
    recipient = 'azaldeguic@gmail.com'
    #email of sender
    FROM = 'christopher.azaldegui@gmail.com'
    #uses the recipient variable to make sure email sent
    TO = recipient if type(recipient) is list else [recipient]
    #Subject line and text body
    SUBJECT = 'Alert: Color change obtained'
    TEXT = 'hello'

    # Prepare actual message
    message = """From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(FROM, TO, message)
        server.close()
        print('successfully sent the mail')
    except:
        print("failed to send mail")
        
#################################################################
#################################################################

#function of code    
def maincode():
     
    #creates a plot with y-axis from 0 to 300
    #and x-axis from 0 to 100
    #axis can be changed to accomodate metrics
    fig = plt.figure()
    plt.axis([0,1000,0,300])
    plt.ion()
    plt.show() 
    
    #empty lists to hold data after analysis
    #used to create a datafram with all of the data
    #datafram will then be exported into csv
    images = []
    blue_list = []
    green_list = []
    red_list = []

    #list of averages and stdevs calculated as code progresses
    #these are values used to compare to fist calculated avg and stdev
    #to see if values have reached expected threshold value    
    New_avg_list = []
    New_stdev_list = []
    
    stop_alert = []
    
    #starts loop to read and analyze images
    #set range to be number of images intended to capture plus 100
    #make sure range value is smaller than amount of image files in directory
    for (i,cap) in enumerate(range(900)):
        
        #images are capture one a second
        #index starts at 0 so add 1 
        second = i + 1
        
        #defines file finding function
        filenames = filepull(i)
        print(filenames)
        
        #file reading fucntion
        image = fileread(filenames)
                
        
        #stores RGB values from analysis function
        blue,green,red = RGBanalysis(image)
        
        #uses empty lists to store data as they're obtained
        images.append(second)
        blue_list.append(blue)
        green_list.append(green)
        red_list.append(red)

        #defines the green value
        Green_point = green
        
        #begin analysis
        if i == 20:
            average_knot, stdev_knot = initial_analysis(green_list)
            print(average_knot, stdev_knot)
            
            
            xx = float(average_knot)
            yy = float(35)
            
        elif i > 20:
            new_Avg, new_stdev = comparison_analysis(green_list)
            
            mean = float(new_Avg)
            stdev = float(new_stdev)
            
            CI = xx - yy
            
            New_avg_list.append(new_Avg)
            New_stdev_list.append(new_stdev)
            
            if "reached" in stop_alert:
                blankspace = ''
            else:
                if CI - stdev <= mean <= CI + stdev:
                    print("THRESHOLD REACHED!!!!")
                    stop_alert.append("reached")
                    send_email()
        
        #plots the time(s) vs. the green color channel value
        #green circles for the points
        plt.plot([second],[Green_point],'g.')
        #draws the plot
        plt.draw()
        #pause added, needs to be in time with camera
        plt.pause(.02)
         
    fig.savefig('titration_curve.png')    
    #after analysis is done, data is placed into dataframe
    #esports data
    dataframe = pd.DataFrame(images, columns = ['Image #'])
    dataframe['blue'] = blue_list
    dataframe['green'] = green_list
    dataframe['red'] = red_list
    print(dataframe) 
    
    #export datafram onto as csv
    dataframe.to_csv('_data.csv')
        
#calls code
maincode()

       

   
                
    

    
    