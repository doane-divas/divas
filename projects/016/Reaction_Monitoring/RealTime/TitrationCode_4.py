#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 14:29:07 2017

@author: Chris and Louise
"""



# import the necessary packages
import cv2
import pandas as pd
import numpy as np
import glob
import sys
import matplotlib.pyplot as plt
import time #used to pause code from running in specific situations

#uses a command line argument to define what the directory being used is called
path_to_file = sys.argv[1]




################################
#uses commandline argument to know what directory to go into
def fileread():
    #defines location of directory
    #function that reads images from folder designated on command line
    #path_to_file is the folder defining argument
    #def readFolder(path_to_file):
    
    path_to_file = sys.argv[1]
    

    
    #empty list to store the images in
    images = []
    
    #use the glod tool to go into folder and look at the files/images in the folder
    #places all of the images into a list calle 'filenames'
    filenames = [img for img in glob.glob(path_to_file)]
        
    #this lines sorts all of the images in alphabetical order,
    #therefore also in chronological order
    #added this when the code was reading the images in a random order
    filenames.sort()
    
    #reads images after they've been sorted
    #appends the images into the list images[]
    for img in filenames:
        n= cv2.imread(img)
        images.append(n)
        
    #this prints the last filename on the sorted list
    #to make sure right file is going to be analyzed    
    print(filenames[-1])
        
        
    #selects last image on list after they've been read
    n = images[-1]

    

    #returns wanted file
    return n


    
#############################################################################    
       

#function to perform RGB image analysis on image 'n'
def RGBanalysis(n):    
    
 
    #this will take the RGB data from the specified region
    #talk to Louise to run atleast 3-4 titrations in a marked position
    #on the stirrer to find an average position range to for general use
    b = n[359 : 479 , 266 : 486 , 0]
    g = n[359 : 479 , 266 : 486 , 1]
    r = n[359 : 479 , 266 : 486 , 2]
                
    #averages the RGB values of all the pixels in the above region
    bAvg = np.mean(b)
    gAvg = np.mean(g)
    rAvg = np.mean(r)
                
    #defines averages a their color values
    blue = bAvg
    green = gAvg
    red = rAvg
        
        
     #returns the three values that make up an RGB code   
    return blue,green,red
################################################################
################################################################        


#function of code    
def maincode():
    
    
    #creates a plot with y-axis from 0 to 300
    #and x-axis from 0 to 500
    plt.axis([0,500,0,300])
    plt.ion()
    plt.show() 
    
    #empty lists to hold data after analysis
    #used to create a datafram with all of the data
    #datafram will then be exported into csv
    image = []
    blue_list = []
    green_list = []
    red_list = []

    
    
    #starts loop to read and analyze images
    #set range to be number of images intended to capture plus 100
    for (i,cap) in enumerate(range(5)):
        
        #images are capture one a second
        #index starts at 0 so add 1 
        second = i + 1
        
        #defines reading function
        n = fileread()
        
        #calls fileread() and run analysis
        RGBanalysis(n)
        
        #stores RGB values from analysis function
        blue,green,red = RGBanalysis(n)
        
        #uses empty lists to store data as they're obtained
        image.append(second)
        blue_list.append(blue)
        green_list.append(green)
        red_list.append(red)

        #defines the green value
        #not necessary        
        Green_point = green
        
        #plots the time(s) vs. the green color channel value
        #green circles for the points
        plt.plot([second],[Green_point],'go')
        #draws the plot
        plt.draw()
        #pause added, needs to be in time with camera
        plt.pause(.8)
    
        
        
    #after analysis is done, data is placed into dataframe
    #esports data
    dataframe = pd.DataFrame(image, columns = ['Image #'])
    dataframe['blue'] = blue_list
    dataframe['green'] = green_list
    dataframe['red'] = red_list
    print(dataframe) 
    
    #export datafram onto as csv
    dataframe.to_csv('_data.csv')



 

        
#calls code
maincode()

       

   
                
    

    
    