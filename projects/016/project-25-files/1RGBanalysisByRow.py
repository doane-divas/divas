#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 11 13:45:41 2017

@author: Chris Azaldegui
"""

#import tools
import sys, cv2
from imutils import contours as imc
import numpy as np
import pandas as pd
import scipy.stats as ss
import csv


#set arguments for command line
#choose what image you want to analyze
filename = sys.argv[1]

#read in image and display it to verify it is the correct one
img = cv2.imread(filename, cv2.IMREAD_COLOR + cv2.IMREAD_ANYDEPTH)
cv2.namedWindow("96-well plate",cv2.WINDOW_NORMAL)
cv2.imshow("96-well plate", img)
cv2.waitKey(0)


#these are all of the center coordinates for all 96 wells
#the order of them go down by crows, it starts on H12 and ends at A1
wells = [(110,97),(110,165),(110,235),(110,305),(110,373),(110,442),(110,511),(110,582),
         (184,97),(184,165),(184,235),(184,305),(184,373),(184,442),(184,511),(184,582),
         (254,97),(254,165),(254,235),(254,305),(254,373),(254,442),(254,511),(254,582),
         (324,97),(324,165),(324,235),(324,305),(324,373),(324,442),(324,511),(324,582),
         (396,97),(396,165),(396,235),(396,305),(396,373),(396,442),(396,511),(396,582),
         (466,97),(466,165),(466,235),(466,305),(466,373),(466,442),(466,511),(466,582),
         (537,97),(537,165),(110,235),(110,305),(110,373),(110,442),(110,511),(110,582),
         (607,97),(607,165),(607,235),(607,305),(607,373),(607,442),(607,511),(607,582),
         (680,97),(680,165),(680,235),(680,305),(680,373),(680,442),(680,511),(680,582),
         (750,97),(750,165),(750,235),(750,305),(750,373),(750,442),(750,511),(750,582),
         (820,97),(820,165),(820,235),(820,305),(820,373),(820,442),(820,511),(820,582),
         (891,97),(891,165),(891,235),(891,305),(891,373),(891,442),(891,511),(891,582)]
 
            

#rows names defined by coordinates in list               
row12 = wells[0:8]
row11 = wells[8:16]
row10 = wells[16:24]
row9 = wells[24:32]
row8 = wells[32:40]
row7 = wells[40:48]
row6 = wells[48:56]
row5 = wells[56:64]
row4 = wells[64:72]
row3 = wells[72:80]
row2 = wells[80:88]
row1 = wells[88:96]

            

#list of rows
rows = [row12, row11, row10, row9, row8, row7, row6, row5, row4, row3, row2, row1]

#empty lists that will store RGB data
blue = []
green = []
red = []
        
#loop set to go through every point in every column in the columnas list
for (i, row) in enumerate(rows):    
    for point in row:
        
        #defines the x and y coordinates for every point
        cy = point[0]
        cx = point[1]
            
        #find average color for 64 pixel kernel around centroid
        b = img[cy - 4 : cy + 4, cx - 4 : cx + 4, 0]
        g = img[cy - 4 : cy + 4, cx - 4 : cx + 4, 1]
        r = img[cy - 4 : cy + 4, cx - 4 : cx + 4, 2]

        #averages of the 225 pixel kernel RGB data
        bAvg = np.mean(b)
        gAvg = np.mean(g)
        rAvg = np.mean(r)
            
        #append RGB means into lists
        blue.append(bAvg)
        green.append(gAvg)
        red.append(rAvg)

#creates a dataframe with every column being a color channel        
dataframe = pd.DataFrame(blue, columns = ['blue'])
dataframe['green'] = green
dataframe['red'] = red
print(dataframe)        
        
#exports dataframe into csv in same layout
dataframe.to_csv(filename[0:-4] + '_RGBdata.csv')


#DATA ORGANIZATION PART OF CODE
#this organizes the RGB data by corresponding columns on the well plate
#this will help with data analysis when comparing by columns A-H
 
#puts list of RGB data into single list for looping purposes
RGB_data = [blue, green, red]

#list of strings to label csv with data for each channel
colors = ['blue', 'green', 'red']

#beginnig of loop
for (c, channel) in enumerate(RGB_data):
    #individualizes run throughs for naming purposes
    if c is 0:
        c = 'BlueRowData'
    elif c is 1: 
        c = 'GreenRowData'
    elif c is 2: 
        c = 'RedRowData'
    #the corresponding data points to their columns
    row_12 = channel[0:8]
    row_11 = channel[8:16]
    row_10 = channel[16:24]
    row_9 = channel[24:32]
    row_8 = channel[32:40]
    row_7 = channel[40:48]
    row_6 = channel[48:56]
    row_5 = channel[56:64]
    row_4 = channel[64:72]
    row_3 = channel[72:80]
    row_2 = channel[80:88]
    row_1 = channel[88:96]
    
 

    #placing the rows into a data frame
    Frame = pd.DataFrame(row_1, columns = ['Row 1'])
    Frame['Row 2'] = row_2
    Frame['Row 3'] = row_3
    Frame['Row 4'] = row_4
    Frame['Row 5'] = row_5
    Frame['Row 6'] = row_6
    Frame['Row 7'] = row_7
    Frame['Row 8'] = row_8
    Frame['Row 9'] = row_9
    Frame['Row 10'] = row_10
    Frame['Row 11'] = row_11
    Frame['Row 12'] = row_12

    Frame.to_csv(filename[0:-4] + '_%s.csv' % (c) )
