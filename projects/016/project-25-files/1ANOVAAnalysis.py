#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 10:21:04 2017

@author: dChris Azaldegui
"""

#Statisical analysis code


#import tools
import sys
import pandas as pd
import scipy.stats as stats
import csv


#set arguments for command line
datafile= sys.argv[1]
data = pd.read_csv(datafile)




newdataframe = pd.DataFrame(data)

print(newdataframe)
print()
print()

print('What test would you like to run? \n For one-way ANOVA: enter "ANOVA" \n For 2-sample Student t-test: enter "T2"')


response = input()

if response == "ANOVA":
    #ANOVA analysis
    A = newdataframe.iloc[:,1].tolist()
    B = newdataframe.iloc[:,2].tolist()
    C = newdataframe.iloc[:,3].tolist()
    D = newdataframe.iloc[:,4].tolist()
    E = newdataframe.iloc[:,5].tolist()
    F = newdataframe.iloc[:,6].tolist()
    G = newdataframe.iloc[:,7].tolist()
    H = newdataframe.iloc[:,8].tolist()
    
    f_val, p_val = stats.f_oneway(A,B,C,D,E,F,G,H)
    print('One-way ANOVA P =', p_val)

elif response == "T2":
    #Two-sample t-test assuming equal variances
    #make sure to correct this before releasing it, only using 1 and 2 index positions
    #to test that the code for t-test is working
    
    column1 = newdataframe.iloc[:,1].tolist()
    column2 = newdataframe.iloc[:,2].tolist()
    
    t_stat, p_val = stats.ttest_ind(a=column1, b=column2, equal_var = True)
    
    print('P =', p_val)
    print('T-statistic =', t_stat)

else:
    
    print("ERROR: rerun code")