#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  2 08:25:31 2017

@author: Chris Azaldegui
"""

#import tools
import sys, cv2
from imutils import contours as imc
import numpy as np
import pandas as pd
import scipy.stats as ss
import csv


#set arguments for command line
#choose what image you want to analyze
filename = sys.argv[1]

#read in image and display it to verify it is the correct one
img = cv2.imread(filename, cv2.IMREAD_COLOR + cv2.IMREAD_ANYDEPTH)
cv2.namedWindow("96-well plate",cv2.WINDOW_NORMAL)
cv2.imshow("96-well plate", img)
cv2.waitKey(0)


#these are all of the center coordinates for all 96 wells
#the order of them go down by columns, it starts on H12 and ends at A1
wells = [(110,97), (184,97), (254,97), (324,97), (396,96), (466,97), 
               (537,96), (607,97), (680,96), (750,97), (820,96), (891,97), 
               (110,165),(184,165),(254,165),(324,165),(396,165),(466,165),
               (537,165),(607,165),(680,165),(750,166),(820,165),(891,165),
               (110,235),(184,235),(254,235),(324,235),(396,235),(466,235),
               (537,235),(607,235),(680,235),(750,235),(820,235),(891,235),
               (110,305),(184,305),(254,305),(324,305),(396,305),(466,305),
               (537,305),(607,305),(680,305),(750,305),(820,305),(891,305),
               (110,373),(184,373),(254,373),(324,373),(396,373),(466,373),
               (537,373),(607,373),(680,373),(750,372),(820,373),(891,373),
               (110,442),(184,442),(254,442),(324,442),(396,442),(466,442),
               (537,442),(607,442),(680,442),(750,443),(820,442),(891,442),
               (109,511),(184,511),(254,511),(324,511),(396,511),(466,511),
               (537,512),(607,512),(680,511),(750,511),(820,511),(891,511),
               (109,582),(184,582),(254,582),(324,581),(396,582),(466,582),
               (537,581),(607,582),(680,582),(750,581),(820,582),(891,582)]

#column names defined by coordinates in list               
H = wells[0:12]
G = wells[12:24]
F = wells[24:36]
E = wells[36:48]
D = wells[48:60]
C = wells[60:72]
B = wells[72:84]
A = wells[84:96]               

#list of columns
columnas = [H, G, F, E, D, C, B, A]

#empty lists that will store RGB data
blue = []
green = []
red = []
        
#loop set to go through every point in every column in the columnas list
for (i, column) in enumerate(columnas):    
    for point in column:
        
        #defines the x and y coordinates for every point
        cy = point[0]
        cx = point[1]
            
        #find average color for 64 pixel kernel around centroid
        b = img[cy - 4 : cy + 4, cx - 4 : cx + 4, 0]
        g = img[cy - 4 : cy + 4, cx - 4 : cx + 4, 1]
        r = img[cy - 4 : cy + 4, cx - 4 : cx + 4, 2]

        #averages of the 225 pixel kernel RGB data
        bAvg = np.mean(b)
        gAvg = np.mean(g)
        rAvg = np.mean(r)
            
        #append RGB means into lists
        blue.append(bAvg)
        green.append(gAvg)
        red.append(rAvg)

#creates a dataframe with every column being a color channel        
dataframe = pd.DataFrame(blue, columns = ['blue'])
dataframe['green'] = green
dataframe['red'] = red
print(dataframe)        
        
#exports dataframe into csv in same layout
dataframe.to_csv(filename[0:-4] + '_RGBdata.csv')


#DATA ORGANIZATION PART OF CODE
#this organizes the RGB data by corresponding columns on the well plate
#this will help with data analysis when comparing by columns A-H
 
#puts list of RGB data into single list for looping purposes
RGB_data = [blue, green, red]

#list of strings to label csv with data for each channel
colors = ['blue', 'green', 'red']

#beginnig of loop
for (c, channel) in enumerate(RGB_data):
    #individualizes run throughs for naming purposes
    if c is 0:
        c = 'BlueColumnData'
    elif c is 1: 
        c = 'GreenColumnData'
    elif c is 2: 
        c = 'RedColumnData'
    #the corresponding data points to their columns
    columnA = channel[84:96]
    columnB = channel[72:84]
    columnC = channel[60:72]
    columnD = channel[48:60]
    columnE = channel[36:48]
    columnF = channel[24:36]
    columnG = channel[12:24]
    columnH = channel[0:12]

    #placing the columns into a data frame
    Frame = pd.DataFrame(columnA, columns = ['A'])
    Frame['B'] = columnB
    Frame['C'] = columnC
    Frame['D'] = columnD
    Frame['E'] = columnE
    Frame['F'] = columnF
    Frame['G'] = columnG
    Frame['H'] = columnH

    Frame.to_csv(filename[0:-4] + '_%s.csv' % (c) )

                


        

