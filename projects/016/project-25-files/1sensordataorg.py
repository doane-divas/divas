#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  5 11:45:51 2017

@author: Christopher Azaldegui

This code will organize sensor experiment data by sensor
"""


#import tools
import sys
import pandas as pd
import csv


#set arguments for command line
datafile= sys.argv[1]
datas = pd.read_csv(datafile)


print(datas)

#columns = ['Pixels','R-Mean','R-Mode','R-Median','G-Mean','G-Mode','G-Median','B-Mean','B-Mode','B-Median']
#newdataframe = pd.DataFrame(datas, columns=columns)

#print(newdataframe)
'''
Pixel_list = newdataframe['Pixels'].tolist()
Rmean_list = newdataframe['R-Mean'].tolist()
Rmode_list = newdataframe['R-Mode'].tolist()
Rmedian_list = newdataframe['R-Median'].tolist()
Gmean_list = newdataframe['G-Mean'].tolist()
Gmode_list = newdataframe['G-Mode'].tolist()
Gmedian_list = newdataframe['G-Median'].tolist()
Bmean_list = newdataframe['B-Mean'].tolist()
Bmode_list = newdataframe['B-Mode'].tolist()
Bmedian_list = newdataframe['B-Median'].tolist()

RGB = [Rmean_list,Gmean_list,Bmean_list]
'''
newdataframe = datas.reindex([0,12,24,36,48,60,72,84,1,13,25,37,49,61,73,85,
                              2,14,26,38,50,62,74,86,3,15,27,39,51,63,75,87,
                              4,16,28,40,52,64,76,88,5,17,29,41,53,65,77,89,
                              6,18,30,42,54,66,78,90,7,19,31,43,55,67,79,91,
                              8,20,32,44,56,68,80,92,9,21,33,45,57,69,81,93,
                              10,22,34,46,58,70,82,94,11,23,35,47,59,71,83,95])

print(newdataframe)

'''
water = datas.reindex([0,12,24,36,48,60,72,84])
thiodiglycol = datas.reindex([1,13,25,37,49,61,73,85])
aminopyridine = datas.reindex([2,14,26,38,50,62,74,86])
ammoniumnitrate = datas.reindex([3,15,27,39,51,63,75,87])
sodiumhypochlorite = datas.reindex([4,16,28,40,52,64,76,88])
hydrogenperoxide = datas.reindex([5,17,29,41,53,65,77,89])
phosphoricacid = datas.reindex([6,18,30,42,54,66,78,90])
potassiumchlorate = datas.reindex([7,19,31,43,55,67,79,91])
potassiumiodate = datas.reindex([8,20,32,44,56,68,80,92])
trifluoroaceticacid = datas.reindex([9,21,33,45,57,69,81,93])
dimethylphosphite = datas.reindex([10,22,34,46,58,70,82,94])
diethylcyanophosphonate = datas.reindex([11,23,35,47,59,71,83,95])

dataframe_list = [water,thiodiglycol,aminopyridine,ammoniumnitrate,
                  sodiumhypochlorite,hydrogenperoxide,phosphoricacid,potassiumchlorate,
                  potassiumiodate,trifluoroaceticacid,dimethylphosphite,diethylcyanophosphonate]
                  
'''
    
    
    
    

#exports dataframe onto csv is same layout
newdataframe.to_csv(datafile + '_Reorganized.csv')  