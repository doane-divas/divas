#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 12 08:49:23 2017

@author: diva
"""

#import tools
import sys, cv2
from imutils import contours as imc
import numpy as np
import pandas as pd
import scipy.stats as ss
import csv


#set arguments for command line
#choose what image you want to analyze
filename = sys.argv[1]

#read in image and display it to verify it is the correct one
img = cv2.imread(filename, cv2.IMREAD_COLOR + cv2.IMREAD_ANYDEPTH)
cv2.namedWindow("Color array",cv2.WINDOW_NORMAL)
cv2.imshow("Color array", img)
cv2.waitKey(0)




blue = []
green = []
red = []
x1 = 260
for pix in range(16):
    
    
    y1 = 100

    for pix in range(25):
        
        
        B = img[y1 - 3: y1 + 3, x1 - 3: x1 + 3,0]
        G = img[y1 - 3: y1 + 3, x1 - 3: x1 + 3,1]
        R = img[y1 - 3: y1 + 3, x1 - 3: x1 + 3,2]


        #averages of the 225 pixel kernel RGB data
        bAvg = np.mean(B)
        gAvg = np.mean(G)
        rAvg = np.mean(R)
        
        #append RGB means into lists
        blue.append(bAvg)
        green.append(gAvg)
        red.append(rAvg)
    
        y1 = y1 + 126
    
    x1 = x1 + 126
    
'''
for pix in range(16):
    y1 = 100

    for pix in range(25):
        
        x1 = 324
        
        B = img[y1 - 3: y1 + 3, x1 - 3: x1 + 3,0]
        G = img[y1 - 3: y1 + 3, x1 - 3: x1 + 3,1]
        R = img[y1 - 3: y1 + 3, x1 - 3: x1 + 3,2]


        #averages of the 225 pixel kernel RGB data
        bAvg = np.mean(B)
        gAvg = np.mean(G)
        rAvg = np.mean(R)
        
        #append RGB means into lists
        blue.append(bAvg)
        green.append(gAvg)
        red.append(rAvg)
    
        y1 = y1 + 128
    
    x1 = x1 + 124
    
for pix in range(16):
    
    
    y1 = 160

    for pix in range(25):
        
        x1 = 264
        
        B = img[y1 - 3: y1 + 3, x1 - 3: x1 + 3,0]
        G = img[y1 - 3: y1 + 3, x1 - 3: x1 + 3,1]
        R = img[y1 - 3: y1 + 3, x1 - 3: x1 + 3,2]


        #averages of the 225 pixel kernel RGB data
        bAvg = np.mean(B)
        gAvg = np.mean(G)
        rAvg = np.mean(R)
        
        #append RGB means into lists
        blue.append(bAvg)
        green.append(gAvg)
        red.append(rAvg)
    
        y1 = y1 + 128
    
    x1 = x1 + 124
    

for pix in range(16):
    
    
    y1 = 160

    for pix in range(25):
        
        x1 = 324
        
        B = img[y1 - 3: y1 + 3, x1 - 3: x1 + 3,0]
        G = img[y1 - 3: y1 + 3, x1 - 3: x1 + 3,1]
        R = img[y1 - 3: y1 + 3, x1 - 3: x1 + 3,2]


        #averages of the 225 pixel kernel RGB data
        bAvg = np.mean(B)
        gAvg = np.mean(G)
        rAvg = np.mean(R)
        
        #append RGB means into lists
        blue.append(bAvg)
        green.append(gAvg)
        red.append(rAvg)
    
        y1 = y1 + 128
    
    x1 = x1 + 124
'''    

#creates a dataframe with every column being a color channel        
dataframe = pd.DataFrame(blue, columns = ['blue'])
dataframe['green'] = green
dataframe['red'] = red
print(dataframe)        
        
#exports dataframe into csv in same layout
dataframe.to_csv(filename[0:-5] + '_RGBdata.csv')   
    
    
    


        
    
    
    
    
    
 

