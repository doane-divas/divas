#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 10:21:04 2017

@author: dChris Azaldegui
"""

#Statisical analysis code


#import tools
import sys
import pandas as pd
import scipy as sp
import scipy.stats as stats
import numpy as np
import csv


#set arguments for command line
datafile= sys.argv[1]
data = pd.read_csv(datafile)



#turns dataframe into new dataframe so nothing gets changed in the original
newdataframe = pd.DataFrame(data)

#prints out the new dataframe to confirm that the data is correct
print(newdataframe)
print()
print()

#asks what statistical test to run, only have two right now.
print('Hello master, what test would you like to run?')
print('For confidence interva: enter "CI"')
print('For one-way ANOVA: enter "ANOVA"')
print('For 1-sample Student t-test: enter "T1"')
print('For 2-sample Student t-test assuming equal variances: enter "T2 True"')
print('For 2-sample Student t-test assuming unequal variances: enter "T2 False"')


#response of user to choose the stat test
response = input()

#if statement to run a test based on the answer of the user

#One-way ANOVA test, compares if the means of multiple groups are the same
#I have it set up for 8 groups since I have been comparing the columns
#of a  96-well plate.
#can still work on this to be more flexible depending on the data input
if response == "ANOVA":
    
    #places the data in the dataframe columns into lists
    A = newdataframe.iloc[:,1].tolist()
    B = newdataframe.iloc[:,2].tolist()
    C = newdataframe.iloc[:,3].tolist()
    D = newdataframe.iloc[:,4].tolist()
    E = newdataframe.iloc[:,5].tolist()
    F = newdataframe.iloc[:,6].tolist()
    G = newdataframe.iloc[:,7].tolist()
    H = newdataframe.iloc[:,8].tolist()
    
    #runs the One-way ANOVA and outputs the p-value
    f_val, p_val = stats.f_oneway(A,B,C,D,E,F,G,H)
    print('One-way ANOVA P =', p_val)

#One-sample t-test
elif response == "T1":
    
    #asks what the expected value to compare to for the test
    print("What is the expected value for the null hypothesis?")
    
    #expected value response from the user
    E_val = input()
    E_val = float(E_val)
    
    #places data into a column
    column1 = newdataframe.iloc[:,1].tolist()
    
    #runs test and output t-stat and p-val
    t_stat, p_val = stats.ttest_1samp(column1,E_val)
    print('T-statistic =', t_stat)
    print('P =', p_val)
      
#confidence intervals
elif response == "CI":
    
    #ask what the level of confidence is?"
    print("What is the confidence level?")
    confidence = input()
    confidence = float(confidence)
    
    #places the data in the dataframe columns into lists
    A = newdataframe.iloc[:,1].tolist()
    B = newdataframe.iloc[:,2].tolist()
    C = newdataframe.iloc[:,3].tolist()
    D = newdataframe.iloc[:,4].tolist()
    E = newdataframe.iloc[:,5].tolist()
    F = newdataframe.iloc[:,6].tolist()
    G = newdataframe.iloc[:,7].tolist()
    H = newdataframe.iloc[:,8].tolist()
    
    columns = [A,B,C,D,E,F,G,H]
    
    m_list = []
    lower_val = []
    upper_val = []

    for column in columns:
        
        n = len(column)
        m, se = np.mean(column), stats.sem(column)
        h = se * stats.t._ppf((1+confidence)/2., n-1)
        
        m_list.append(m)
        lower_val.append(m-h)
        upper_val.append(m+h)
    
    column_names = ['A','B','C','D','E','F','G','H']

    Confidence_intervals = pd.DataFrame(column_names, columns = ['Dataset'])
    Confidence_intervals['Mean'] = m_list
    Confidence_intervals['Lower'] = lower_val
    Confidence_intervals['Upper'] = upper_val
    
    print(Confidence_intervals)        
        

    
#Two-sample t-test assuming equal variances
elif response == "T2 True":
    
    #make sure to correct this before releasing it, only using 1 and 2 index positions
    #to test that the code for t-test is working
    column1 = newdataframe.iloc[:,1].tolist()
    column2 = newdataframe.iloc[:,2].tolist()
    
    #runs test and outputs t stat and p-value
    t_stat, p_val = stats.ttest_ind(a=column1, b=column2, equal_var = True)
    print('P =', p_val)
    print('T-statistic =', t_stat)
    
    
#Two-sample t-test assuming unequal variances
elif response == "T2 False":
    
    #make sure to correct this before releasing it, only using 1 and 2 index positions
    #to test that the code for t-test is working
    column1 = newdataframe.iloc[:,1].tolist()
    column2 = newdataframe.iloc[:,2].tolist()
    
    #runs test and outputs t stat and p-value
    t_stat, p_val = stats.ttest_ind(a=column1, b=column2, equal_var = False)
    print('P =', p_val)
    print('T-statistic =', t_stat)
    
#if no stat command is input by user then code needs to be ran again
else:
    
    print("ERROR: rerun code")