#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  9 09:58:36 2017

@author: diva
"""

#import tools
import sys, cv2
from imutils import contours as imc
import numpy as np
import math
import pandas as pd

#set arguments for command line, this one inputs the image file we want to read
filename = sys.argv[1]

#read in image
#can't get the code to work when reading 48-bit, will look into that.
img = cv2.imread(filename)
cv2.namedWindow("96-well plate", cv2.WINDOW_NORMAL)
cv2.imshow("96-well plate", img)
cv2.waitKey(0)

#center coordinates of the well plate
wells = [(110,97), (184,97), (254,97), (324,97), (396,96), (466,97), 
               (537,96), (607,97), (680,96), (750,97), (820,96), (891,97), 
               (110,165),(184,165),(254,165),(324,165),(396,165),(466,165),
               (537,165),(607,165),(680,165),(750,166),(820,165),(891,165),
               (110,235),(184,235),(254,235),(324,235),(396,235),(466,235),
               (537,235),(607,235),(680,235),(750,235),(820,235),(891,235),
               (110,305),(184,305),(254,305),(324,305),(396,305),(466,305),
               (537,305),(607,305),(680,305),(750,305),(820,305),(891,305),
               (110,373),(184,373),(254,373),(324,373),(396,373),(466,373),
               (537,373),(607,373),(680,373),(750,372),(820,373),(891,373),
               (110,442),(184,442),(254,442),(324,442),(396,442),(466,442),
               (537,442),(607,442),(680,442),(750,443),(820,442),(891,442),
               (109,511),(184,511),(254,511),(324,511),(396,511),(466,511),
               (537,512),(607,512),(680,511),(750,511),(820,511),(891,511),
               (109,582),(184,582),(254,582),(324,581),(396,582),(466,582),
               (537,581),(607,582),(680,582),(750,581),(820,582),(891,582)]
 
#code below not needed now but may come in use when placing data into csv or directly running data analysis               
'''
#column names defined by coordinates in list               
H = wells[0:12]
G = wells[12:24]
F = wells[24:36]
E = wells[36:48]
D = wells[48:60]
C = wells[60:72]
B = wells[72:84]
A = wells[84:96]
# a list of column names that each have the corresponding coordinates
columns = [H,G,F,E,D,C,B,A]
'''

# Create the basic black image 
mask = np.zeros(img.shape, dtype = "uint8")

#run a loop across all wells to make white circles
for well in wells:
    x = well[1]
    y = well[0]
    

    # Draw a white, filled rectangle on the mask image, gets rid of upper part of image
    cv2.circle(mask, (x, y), 15, (255, 255, 255), -1)
    # Applies the mask
maskedImg = cv2.bitwise_and(img, mask)
    
#make and show the masked well plate image            
cv2.namedWindow("mask", cv2.WINDOW_NORMAL)
cv2.imshow("mask", maskedImg)
cv2.waitKey(0)


# blur and grayscale before thresholding
blur = cv2.cvtColor(maskedImg, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(blur, (5, 5), 0)

# find contours, one contour per well. 
# 96 total contours
(_, contours, hierarchy) = cv2.findContours(blur, cv2.RETR_TREE, 
        cv2.CHAIN_APPROX_SIMPLE)


#print(len(contours))


 

#empty lists to put the R G and B values of the wells in
bluelist = []
greenlist = []       
redlist = []

# loop to get RGB data from well contours             
for well in contours:             
              
        
    #this defines the RGB data for each point
    Bdata = img[0]
    Gdata = img[1]
    Rdata = img[2]
        
    # this takes the avg values of all the pixels in the contour
    blue = np.mean(Bdata)
    green = np.mean(Gdata)
    red = np.mean(Rdata)
     
    #put R G and B values into a list for that column      
    bluelist.append(blue)
    greenlist.append(green)
    redlist.append(red)
        
        
#print(len(bluelist))
#print(len(greenlist))
#print(len(redlist))

dataframe = pd.DataFrame(bluelist, columns = ['blue'])

dataframe['green'] = greenlist
dataframe['red'] = redlist
print(dataframe)        
        

dataframe.to_csv('MaskRGB_Results.csv')









