#import 
import numpy as np
import sys
import cv2

 #read and save command-line arguments 
filename = sys.argv[1]
k = int(sys.argv[2])
t = int(sys.argv[3])
'''
def fixedThresh():
	global img, blur,t, binary
	(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY)
	cv2.imshow('Threshold Image', binary)
'''    
#create binary image 
img = cv2.imread(filename)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#blur = cv2.GaussianBlur(img, (k, k), 0)

(t, binary) = cv2.threshold(gray, t, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
print ('Threshold value', t)

mask = cv2.merge([binary, binary, binary])

cv2.namedWindow('Threshold Image', cv2.WINDOW_NORMAL)
cv2.imshow('Threshold Image', binary)
cv2.waitKey(0)


#find contours 
(_, contours, hierarchy) = cv2.findContours(gray, cv2.RETR_TREE, 
    cv2.CHAIN_APPROX_SIMPLE)


contimg = cv2.drawContours(gray, contours, -1, (0,0,255), 2)

font=cv2.FONT_HERSHEY_SIMPLEX
texted = cv2.putText(contimg, str(len(contours) - 1) + 'Colonies', (0,300), font, 1, (0,0,0), 2, cv2.LINE_AA)
print(len(contours))     

cv2.namedWindow('output', cv2.WINDOW_NORMAL)
cv2.imshow('output', contimg)
cv2.waitKey(0)
