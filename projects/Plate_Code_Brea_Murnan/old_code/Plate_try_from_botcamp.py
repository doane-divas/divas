#import 
import numpy as np
import sys
import cv2

 #read and save command-line arguments 
filename = sys.argv[1]
k = int(sys.argv[2])
t = int(sys.argv[3])

def fixedThresh():
	global img, blur,t, binary
	(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY)
	cv2.imshow('Threshold Image', binary)
    
#create binary image 
img = cv2.imread(filename)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

blur = cv2.GaussianBlur(gray, (k, k), 0)
(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY)

cv2.namedWindow('Threshold Image', cv2.WINDOW_NORMAL)
cv2.imshow('Threshold Image', binary)
cv2.waitKey(0)

#find contours 
(_, contours, hierarchy) = cv2.findContours(binary, cv2.RETR_TREE, 
    cv2.CHAIN_APPROX_SIMPLE)

#print table of contours and sizes
#print('Found %d objects.' % len(contours))
#for (i, c) in enumerate(contours):
    #print(\tSize of contour %d' % (i, len(c)))
    
#determine average length of contours 
'''
avg = 0
contour = []
    
for c in contours:
    avg += len(c)
    if len(c) > avg / 4:
        contour.append(c)
    
avg /= len(contours)
'''
contimg = cv2.drawContours(img, contours, -1, (0,0,255), 2)

font=cv2.FONT_HERSHEY_SIMPLEX
texted = cv2.putText(contimg, str(len(contours) - 1) + 'Colonies', (0,300), font, 1, (0,0,0), 2, cv2.LINE_AA)
print(len(contours))     

cv2.namedWindow('output', cv2.WINDOW_NORMAL)
cv2.imshow('output', img)
cv2.waitKey(0)
        
        
    
#5 86 or 89



