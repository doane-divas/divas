#author: Brea Murnan 
#date: 7/5/2018
#this code is to find and count plaques on a plate from Jade in Erin Doyles lab. It also finds the perimeter and area of each plaque on the plate 

#import tools needed to run code 
import numpy as np
import sys
import cv2

#read and save command-line arguments 
filename = sys.argv[1]
k = int(sys.argv[2])
t = int(sys.argv[3])

#read image in and convert to gray scale 
img = cv2.imread(filename)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#show gray image
cv2.namedWindow('oriimage', cv2.WINDOW_NORMAL)
cv2.imshow('oriimage', gray)

# create a mask to block the edge of the plate by creating a circle mask around the plate
mask = np.zeros(gray.shape, dtype='uint8')
cv2.circle(mask, (369,236), (242), (255,255,255), -1)

#merge the mask and gray image into one for later tasks
masks = cv2.bitwise_and(mask, gray)

#show the masked image to compare it to the orignal gray image 
cv2.namedWindow('maskedimage', cv2.WINDOW_NORMAL)
cv2.imshow('maskedimage', masks)
cv2.waitKey()

#blur image so I can threshold the image 
blur = cv2.GaussianBlur(masks, (k, k), 0)

#show blur image 
cv2.namedWindow('blurred', cv2.WINDOW_NORMAL)
cv2.imshow('blurred', blur)
cv2.waitKey()

#Threshold image so plaques stick out and are easier to contour and count 
(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY)

#Show threshold image to make sure it is finding all the plaques and nothing extra
cv2.namedWindow('Threshold Image', cv2.WINDOW_NORMAL)
cv2.imshow('Threshold Image', binary)
cv2.waitKey(0)

#find contours on the plaques  
(_, contours, hierarchy) = cv2.findContours(binary, cv2.RETR_TREE, 
    cv2.CHAIN_APPROX_SIMPLE)

#draw the contours on the image so you can find the contours and make sure it has the right things contoured
contimg = cv2.drawContours(img, contours, -1, (0,0,255), 2)

#count contours and place label on image
font=cv2.FONT_HERSHEY_SIMPLEX
texted = cv2.putText(contimg, str(len(contours) - 1) + 'Colonies', (0,300), font, 1, (255,255,255), 2, cv2.LINE_AA) #the -1 is finding all the plaques and since the contours get the mask as a contours it subtracts so that one is not counted. The right count of plaques is printed on the final image.
#print label of how many plaques on the original image 
print(len(contours))     

#show final image 
cv2.namedWindow('Contours', cv2.WINDOW_NORMAL)
cv2.imshow('Contours', contimg)
cv2.waitKey()

#loop through each contour and calculate the perimeter and area of each plaque. It is measured in the amount of pixels per the area. The biggest contour of area and perimeter is the outline of the plate and is not a plaque.  
perimeter = []
for contour in contours:
	plaques = cv2.arcLength(contour,True) #finds the perimeter of each plaque with the amount of pixels in the area it measures.
	perimeter.append(plaques)#adds the average to the new list(perimeter)
	
area = []
for contour in contours:
	plaques2 = cv2.contourArea(contour) #finds the area of each plaque on the plate by using the pixel count inside the area of the contour of each plaque.
	area.append(plaques2) #adds the area to the new list of (area)

#prints out the outputs of loops above. list the filename and then the word perimeter. It then puts out the values in order it found them biggest to least. The sep means to seperate each value with a comma so it is easier to read.
print (filename, 'perimeter', perimeter, sep = ', ') 
print (filename, 'area', area, sep = ', ')

#values to use for each image 
#plate_image_less2 11/34
#plate_image_less3 11/165  
#plate_image_less4 11/73
#plate_image_less5 11/57
#image1.tif	  11/120
#plate_image_more1 11/105
#plate_image_more2 11/35 or38
#plate_image_more5 11/85  


