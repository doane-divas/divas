#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  9 13:27:10 2018

@author: Grace Su and Nick Iwata

Command line requires first the filename for the image
                      second is the concentrations seperated by ","
                      
Ex: image.jpg 100,20,40,60,80
"""
import cv2, sys
import numpy as np
from matplotlib import pyplot as plt
import math
import pickle

k=5
t=198

filename=sys.argv[1]
img = cv2.imread(filename)


#User inputs for grid info, should be recieved from bash file when automated
RowInput=6

ColumnInput=5
lengthSqurInput=34
concs=[]
SSTR=sys.argv[2].split(",")
InputConcs=SSTR
for conc in InputConcs:
    NumberVersion=int(conc)
    print (conc)
    concs.append(NumberVersion)
#RowInput=int(RowInput) #change these to numbers, not strings
#ColumnInput=int(ColumnInput)
#lengthSqurInput=int(lengthSqurInput)
print (concs)
height = np.size(img,0)
width = np.size(img,1)
print("This is the width ",width)
print("This is the height ",height)
#blur image, k is the size of how big the blur sections
#will be
blur = cv2.GaussianBlur(img, (k, k), 0)

#load grayscale image using blur image
gray = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)
'''
#display the grayscale blurred image
#you can comment this section out after mindexpo
cv2.namedWindow("gray and blur", cv2.WINDOW_NORMAL)
cv2.imshow("gray and blur", gray)
cv2.waitKey(0)
'''

#created a grayscale image from sampling later that is not blurred
sampleImg = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

#most of the pixels have values over 225
#which means they are mostly white or light gray

#perform inverse binary thresholding
(t, BlackDotsWhite)=cv2.threshold(gray, t, 255, cv2.THRESH_BINARY_INV)

#display the thresholded image
#you can comment this section out after mindexpo
cv2.namedWindow("threshold image", cv2.WINDOW_NORMAL)
cv2.imshow("threshold image", BlackDotsWhite)
cv2.waitKey(0)

#cv2.imwrite("threshold_goodimage.jpg", BlackDotsWhite)


#Find the contours of the white dots
(_, contours, _) = cv2.findContours(BlackDotsWhite, cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_SIMPLE)

#To see how many objects we get, print table of 
#contours and sizes

print("Found %d objects." % len(contours))
boxSized = 0
for c in contours:
	if 1600>cv2.contourArea(c)>982:
		boxSized +=1
print(boxSized)
#for (i, c) in enumerate(contours):
    #print("\tSize of contour %d: %d" % (i, len(c)))
'''
number=0

for (c) in contours:
    a=cv2.contourArea(c)
    number+=1
    print("Size of of object ",number," is ", a)
'''
#It finds objects, lets see where they are

#Draw contours over the original image
drawContours= cv2.drawContours(img, contours, -1, (0,0,255), 5)

#display original image with the contours
cv2.namedWindow("output of contour", cv2.WINDOW_NORMAL)
cv2.imshow("output of contour", drawContours)
cv2.imwrite("drawContours.jpg", drawContours)
cv2.waitKey(0)

#Now it needs to find the centers of the black boxes


#number of white boxes (which are really the black boxes)
boxCount = 1


#define variables for the center coordinates of anchors

cx1=0
cy1=0

#this code is to automate finding the centroids of each concentration square on image


#assign variables for filtering
edgeL=100
edgeR=width-100
edgeT=100
edgeB=height-100

Q4Left=width/2
Q4Top=height/2

PX1=0
PY1=0
PX2=0
PY2=0
PX3=0
PY3=0
PX4=0
PY4=0
PX5=0
PY5=0

#for each of the contours
for c in contours:
    
    #print (boxCount)
    #count and find center only for the correct boxes
    if boxCount == 1:
        
        if 1600>cv2.contourArea(c)> 982:
            M=cv2.moments(c)
            cx=int(M['m10']/M['m00'])
            cy=int(M['m01']/M['m00']) 
        
        #find the distance between the two boxes
        #dist = []
        #dist.append()
            if cx>edgeL:
                if cx<edgeR:
                    if cy>edgeT:
                        if cy<edgeB:
                            if cx>Q4Left:
                                if cy<Q4Top:
                                    
                                    print ("For object ", boxCount, " the xy coordinates are: ", cx, ", ",cy)
                                    boxCount=boxCount+1
                                    PX1=cx
                                    PY1=cy
    if boxCount == 2:
        
        if 1600>cv2.contourArea(c)> 982:
            M=cv2.moments(c)
            cx=int(M['m10']/M['m00'])
            cy=int(M['m01']/M['m00'])
        #find the distance between the two boxes
        #dist = []
        #dist.append()
            if cx != PX1:
                
                if cx>edgeL:
                    if cx<edgeR:
                        if cy>edgeT:
                            if cy<edgeB:
                                if cx>Q4Left:
                                    if cy<Q4Top:
                                        
                                        print ("For object ", boxCount, " the xy coordinates are: ", cx, ", ",cy)
                                        boxCount=boxCount+1
                                        PX2=cx
                                        PY2=cy
                                    
    if boxCount == 3:
        
        if 1600>cv2.contourArea(c)> 982:
            M=cv2.moments(c)
            cx=int(M['m10']/M['m00'])
            cy=int(M['m01']/M['m00'])
        #find the distance between the two boxes
        #dist = []
        #dist.append()
            if cx != PX2:
                
                if cx>edgeL:
                    if cx<edgeR:
                        if cy>edgeT:
                            if cy<edgeB:
                                if cx>Q4Left:
                                    if cy<Q4Top:
                                        
                                        print ("For object ", boxCount, " the xy coordinates are: ", cx, ", ",cy)
                                        boxCount=boxCount+1
                                        PX3=cx
                                        PY3=cy
    if boxCount == 4:
        
        if 1600>cv2.contourArea(c)> 982:
            M=cv2.moments(c)
            cx=int(M['m10']/M['m00'])
            cy=int(M['m01']/M['m00'])
        #find the distance between the two boxes
        #dist = []
        #dist.append()
            if cx != PX3:
                
                if cx>edgeL:
                    if cx<edgeR:
                        if cy>edgeT:
                            if cy<edgeB:
                                if cx>Q4Left:
                                    if cy<Q4Top:
                                        
                                        print ("For object ", boxCount, " the xy coordinates are: ", cx, ", ",cy)
                                        boxCount=boxCount+1
                                        PX4=cx
                                        PY4=cy
                                    
    if boxCount == 5:
        
        if 1600>cv2.contourArea(c)> 982:
            M=cv2.moments(c)
            cx=int(M['m10']/M['m00'])
            cy=int(M['m01']/M['m00'])
        #find the distance between the two boxes
        #dist = []
        #dist.append()
            if cx != PX4:
                
                if cx>edgeL:
                    if cx<edgeR:
                        if cy>edgeT:
                            if cy<edgeB:
                                if cx>Q4Left:
                                    if cy<Q4Top:
                                        
                                        print ("For object ", boxCount, " the xy coordinates are: ", cx, ", ",cy)
                                        boxCount=boxCount+1
                                        PX5=cx
                                        PY5=cy
                                        print("This is X and Y: ",PX1," ",PY1, "before comparison.")
                                        print("This is X and Y: ",PX2," ",PY2)
                                        print("This is X and Y: ",PX3," ",PY3)
                                        print("This is X and Y: ",PX4," ",PY4)
                                        print("This is X and Y: ",PX5," ",PY5)
                                    
    if boxCount == 6:
        if 1600>cv2.contourArea(c)>982:
            M=cv2.moments(c)
            cx=int(M['m10']/M['m00'])
            cy=int(M['m01']/M['m00'])
            
            if cx!= PX5:
                if cx>edgeL:
                    if cx<edgeR:
                        if cy>edgeT:
                            if cy<edgeB:
                                if cx>Q4Left:
                                    if cy<Q4Top:
                                        if cy<PY1+20:
                                            PX1=cx
                                            PY1=cy
                                            cy=cy+400
                                        if cy<PY2+20:
                                            PX2=cx
                                            PY2=cy
                                            cy=cy+200
                                        if cy<PY3+20:
                                            PX3=cx
                                            PY3=cy
                                            cy=cy+400
                                        if cy<PY4+20:
                                            PX4=cx
                                            PY4=cy
                                            cy=cy+400
                                        if cy<PY5+20:
                                            PX5=cx
                                            PY5=cy
                                            cy=cy+400

print("This is X and Y: ",PX1," ",PY1, "after comparison.")
print("This is X and Y: ",PX2," ",PY2)
print("This is X and Y: ",PX3," ",PY3)
print("This is X and Y: ",PX4," ",PY4)
print("This is X and Y: ",PX5," ",PY5)
Anchors=[]
A1=[PX1,PY1]
Anchors.append(A1)
A2=[PX2,PY2]
A3=[PX3,PY3]
A4=[PX4,PY4]
A5=[PX5,PY5]
Anchors.append(A2)
Anchors.append(A3)
Anchors.append(A4)
Anchors.append(A5)


for k in Anchors:
    Top_x=k[0]-10
    Top_y=k[1]-10
    Bottom_x=k[0]+10
    Bottom_y=k[1]+10
   
    #puts a red square on image for each kernel used, uses exact size and location of each kernel
    #The cv2.rectangle stuff must be below the np.median stuff or the color values
    #will all be black because it picks up the black square
    cv2.rectangle(blur, (Top_x,Top_y), (Bottom_x,Bottom_y), (0, 0, 255), -1)
    

cv2.namedWindow("Anchor_image", cv2.WINDOW_NORMAL)
cv2.imshow("Anchor_image", blur)
cv2.imwrite("Anchor_imageRow.jpg",blur)
cv2.waitKey(0)

#I am printng the list Anchors so I can compare to the original to the sorted
#list after I print the sorted list
print("This is Anchors: ", Anchors)

#Add code for sorting the list here
#sort the list in ascending order according to the sublists,
#and sort it according to x values
def sortFirst(val):
    return val[0]

Anchors.sort(key=sortFirst)
print("This is Anchors after sorting: ", Anchors)

#Find equal distance between the anchors
#I took the average distance between all of the X values amongst the anchors to get
#an equal distance value
EqualDistance=((Anchors[4][0]-Anchors[3][0])+(Anchors[3][0]-Anchors[2][0])+(Anchors[2][0]-Anchors[1][0])+(Anchors[1][0]-Anchors[0][0]))/4
#EqualDistance=145 #mindexpo and uncomment the line above after mind expo
#Take the integer of the average so we done have fractions of a pixel for the distance
EqualDistance=int(EqualDistance) #uncomment this line after mindexpo
print("EqualDistance = ", EqualDistance)

#here I am adding the pixel intensities to the PI dictionary
#this will just keep going on until you are done with each value for each of mock exudates
PI = {}

#now we need to sample all the boxes 400 times using a 5x5 kernel
#that goes up each column and across each row in the inter 100x100 square
for conc in concs:
    PI[conc]=[]
    print (conc) 
    #Get a number for which conc is running currently (1st one is actually valued at 0)
    index=concs.index(conc)
    #Run through the anchors to test each column and then row while working up from each anchor
    #Making a few edits and additions of r collecting some data for mindexpo, if it says mindexpo
    #after it, it can be deleted later and revert to the old line above it
    for anchor in Anchors:
        cx=anchor[0]
        cy=anchor[1]-(index*EqualDistance)
        #TL=[cx-50,cy-50]
        TL=[cx-25,cy-25] #MindExpo
        #for boxX in range (0,100,5):#this moves the kernel in the x direction every 5 pixels up to 100
        for boxX in range (0,50,5):#MindExpo
            #for boxY in range (0,100,5): #this moves the kernel in the y direction every 5 pixels up to 100
                #sample each of the little boxes (y,x)
            for boxY in range (0,50,5):#MindExpo
                S=np.mean(sampleImg[TL[1]+boxY:TL[1]+boxY+5, TL[0]+boxX:TL[0]+boxX+5])
                #append to the the dict. for each of the samples
                PI[conc].append(S)
                #puts a red square on image for each kernel used, uses exact size and location of each kernel
                #The cv2.rectangle stuff must be below the np.median stuff or the color values
                #will all be black because it picks up the black square
                RX=TL[0]+boxX
                RY=TL[1]+boxY
                R1X=TL[0]+boxX+5
                R1Y=TL[1]+boxY+5
                cv2.rectangle(blur, (RX,RY), (R1X,R1Y), (0, 0, 255), 1)
                
print ("This is PI: ", PI)
cv2.namedWindow("Anchor_image", cv2.WINDOW_NORMAL)
cv2.imshow("Anchor_image", blur)
cv2.imwrite("Anchor_image.jpg",blur)
cv2.waitKey(0)
   
#pickle function so PI can be opened in the regression program
pickleTest=open ("PI.pkl","wb")
pickle.dump(PI,pickleTest)
pickleTest.close()
