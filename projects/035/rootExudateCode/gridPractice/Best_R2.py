"""
Spyder Editor

Automation of linear regression line
Author: Danny Tran and Nick Iwata

User is asked in command line for the pickled file they want to run

Ex: Please type the file needed to be pickled: example.pkl
"""

import numpy as np
import matplotlib.pyplot as plt
import pickle

#Polynomial Regression
#Source: https://stackoverflow.com/questions/893657/how-do-i-calculate-r-squared-using-python-and-numpy
def polyfit(x, y, degree):
    results = {}

    coeffs = np.polyfit(x, y, degree)

    #r^2
    p = np.poly1d(coeffs)
    
    #fit values, and mean
    yhat = p(x)                         
    ybar = np.sum(y)/len(y)          
    ssreg = np.sum((yhat-ybar)**2)   
    sstot = np.sum((y - ybar)**2)    
    results = ssreg / sstot

    return results

#User input to enter the file needed to be pickled
pickle_file = input("Please type the file needed to be pickled: ")
pickle_file = str(pickle_file)

#using Nick's data and unpickling the values
dataFile = open(pickle_file,"rb")
ImportedPixels = pickle.load(dataFile)

#These are the points gathered according to Nick's dictionary
#The x values are the concentrations in millimolars
#The y values are the pixel intensities
x = []
y = []

#First it goes to the first concentration then gathers the pixel intensities of the exudates in that coloum
#It repeats this process for the next concentrations while appending the values as x and y
#values with the list on line 21 and 22
for conc in ImportedPixels.keys():
    for value in ImportedPixels[conc]:
        x.append(conc)
        y.append(value)
print(x)
print(y)

r2 = polyfit(x,y,1)
print(r2)

#This allows the script to automatically create the linear regression for our graph
fit = np.polyfit(x,y,1)
fit_fn = (np.poly1d(fit))

print(fit_fn)

#Making a font style for your text box
font = {'family': 'serif',
        'color':  'darkred',
        'weight': 'normal',
        'size': 16,
        }

#Creating the graph and adding labels
fig = plt.figure()
plt.plot(x,y, 'ro', x, fit_fn(x), '--k')
plt.ylabel('Average Pixel Intensity')
plt.xlabel('Percent Output of Ink')
plt.title('Paper Sensitivity For Free Amines')
plt.text(2, 185, str(fit_fn), fontdict=font)
plt.text(2, 190, str(r2), fontdict=font)
plt.show()

#Saving the image and stopping the script
fig.savefig('Linear_Regression_R2_Test.png')


"""
#####I DONT THINK WE NEED THIS######
#User input for desire boundary range
X_boundaries = input("Please enter in the boundary range: ")
X_boundaries = int(X_boundaries)


#Place holder and list for R2 and slopes
x_R2 = []
y_R2 = []
R2_List = []
R2_Slopes = []
Placehold = 0

#Getting the amount of repeating numbers 
for n in x:
    if n == x[0]:
        Placehold += 1        
    else:
        break

#These values will be used as index keys for the mathmatical munipluation of the intensities
X_Totals = int(Placehold)
X_Totals_Index = X_Totals-1
Placehold = 1

#Adding in X values without duplicates
for n in x:
    if  n not in x_R2:
        x_R2.append(n)

#Getting the average of the first number
while True:
    First = np.average(y[((Placehold * X_Totals)-X_Totals):X_Totals_Index])
    y_R2.append(First)
    Placehold += 1
    X_Totals_Index = int((X_Totals*Placehold)-1)
    if Placehold > int(len(x)/X_Totals):
        Placehold = 0
        break

#Now working to get the R^2 values
"""