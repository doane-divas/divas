#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 18:36:02 2019

@author: Nick Iwata

User will be asked which pickeled file they want to run

Ex: Please type the file needed to be pickled: example.pkl
"""

import numpy as np
import matplotlib.pyplot as plt
import pickle

#User input to enter the file needed to be pickled
pickle_file = input("Please type the file needed to be pickled: ")
pickle_file = str(pickle_file)

#using Nick's data and unpickling the values
dataFile = open(pickle_file,"rb")
ImportedPixels = pickle.load(dataFile)

#new dictionary that will be added to and then pickeled for the filtere
#data points. This will not include anything outside the 1.5x IQR range from the
#median
filteredPI={}

for conc in ImportedPixels.keys():
    array=[]
    filteredPI[conc]=[]
    for value in ImportedPixels[conc]:
        array.append(value)
    #np.percentile gets the percentile/quartile you are looking for in 
    #an array of your choice
    Q25=np.percentile(array,25)
    Q75=np.percentile(array,75)
    print ("This is for concentration: ", conc)
    print ("This is the 1st quartile: ", Q25)
    print ("This is the 3rd quartile: ", Q75)
    IQR= np.percentile(array,75)-np.percentile(array,25)
    print ("This is the IQR: ",IQR)
    #minimum value for outliers
    Min=Q25-(1.5*IQR)
    #maximum value for outliers
    Max=Q75+(1.5*IQR)
    
    for number in array:
        if Min<=number<=Max:
            #print(number)
            #I am trying to append to the new dictionary of the values that are not outliers
            filteredPI[conc].append(number)
#print(filteredPI)
#pickle function so PI can be opened in the regression program
pickleTest=open ("filteredPI.pkl","wb")
pickle.dump(filteredPI,pickleTest)
pickleTest.close()