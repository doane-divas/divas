# -*- coding: utf-8 -*-
"""
Spyder Editor

Automation of linear regression line
Author: Danny Tran and Nick Iwata
"""

import numpy as np
import matplotlib.pyplot as plt
import pickle

#Polynomial Regression
#Source: https://stackoverflow.com/questions/893657/how-do-i-calculate-r-squared-using-python-and-numpy
def polyfit(x, y, degree):
    results = {}

    coeffs = np.polyfit(x, y, degree)

    #r^2
    p = np.poly1d(coeffs)
    
    #fit values, and mean
    yhat = p(x)                         
    ybar = np.sum(y)/len(y)          
    ssreg = np.sum((yhat-ybar)**2)   
    sstot = np.sum((y - ybar)**2)    
    results = ssreg / sstot

    return results

#using Nick's data and unpickling the values
dataFile = open("pickleFileOfPixels.pkl","rb")
ImportedPixels = pickle.load(dataFile)

#These are the points gathered according to Nick's dictionary
#The x values are the concentrations in millimolars
#The y values are the pixel intensities
x = []
y = []

#First it goes to the first concentration then gathers the pixel intensities of the exudates in that coloum
#It repeats this process for the next concentrations while appending the values as x and y
#values with the list on line 21 and 22
for conc in ImportedPixels.keys():
    for value in ImportedPixels[conc]:
        x.append(conc)
        y.append(value)
print(x)
print(y)

r2 = polyfit(x,y,1)
print(r2)

#This allows the script to automatically create the linear regression for our graph
fit = np.polyfit(x,y,1)
fit_fn = (np.poly1d(fit))

#Making a font style for your text box
font = {'family': 'serif',
        'color':  'darkred',
        'weight': 'normal',
        'size': 16,
        }

#Creating the graph and adding labels
fig = plt.figure()
plt.plot(x,y, 'ro', x, fit_fn(x), '--k')
plt.ylabel('Average Pixel Intensity')
plt.xlabel('Free amine concentration (mM)')
plt.title('Paper Sensitivity For Free Amines')
plt.text(2, 185, str(fit_fn), fontdict=font)
plt.text(2, 190, str(r2), fontdict=font)
plt.show()

#Saving the image and stopping the script
fig.savefig('Linear_Regression.png')

