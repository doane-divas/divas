# -*- coding: utf-8 -*-
"""
Spyder Editor

@author: Keeliann Mark
"""

import cv2

cap = cv2.VideoCapture('Titration13_06-19-17.wmv')

"""
These are different locations and sizes of rectangles that I used. All rectangles
produced the "same" results

Coordinates for 90x55 rectangle in the middle of the liquid in the beaker
    center_rectangle = frame[460:515, 620:710, :]
    saved in TitrationC_images as Rec_FRamE####
Coordinates for 90x55 rectangle near the top of the liquid in the beaker
    center_rectangle = frame[400:455, 620:710, :]
    saved in TitrationT_images as Rec_FRamE####
Coordinates for 18x11 rectangle near the center of the liquid in the beaker
    center_rectangle = frame[460:471, 620:638, :]    
Coordinates for 18x11 rectangle near the top of the liquid in the beaker
    center_rectangle = frame[400:411, 620:638, :]
Coordinates for 180x110 rectangle 
    center_rectangle = frame[430:540, 620:800, :]
"""

c = 0

#credit to Ben Z. for the naming system
while(cap.isOpened()):
    #reads the video frame by frame
    ret, frame = cap.read()
    #clips the frame to a small rectangle in the middle of the beaker
    center_rectangle = frame[430:540, 620:800, :]
    #saves each clip for each frame as a jpg
    if(c < 100):
        
        cv2.imwrite('Rec_frameB00%d.jpg' % c, center_rectangle)
    
    elif(c < 1000):
        
         cv2.imwrite('Rec_frameB0%d.jpg' % c, center_rectangle)
        
    else:
        cv2.imwrite('Rec_frameB%d.jpg' % c, center_rectangle)
        
    c=c+1
    
    
# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()    

