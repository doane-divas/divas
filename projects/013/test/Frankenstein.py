# -*- coding: utf-8 -*-
"""
Created on Wed Jul  5 10:58:05 2017

@author: Keeliann
"""
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob, cv2


def FrameSaver(video):
    c = 0
    cap = cv2.VideoCapture(video) 
    while(cap.isOpened()):
        # Capture frame-by-frame
        ret, frame = cap.read()
        #top rectangle
        center_rectangle = frame[400:455, 620:710, :] 
        if(c < 100):        
            cv2.imwrite('Rec_frameT00%d.jpg' % c, center_rectangle)        
        elif(c < 1000):            
            cv2.imwrite('Rec_frameT0%d.jpg' % c, center_rectangle)            
        else:        
            cv2.imwrite('Rec_frameT%d.jpg' % c, center_rectangle)                
        c=c+1    
    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()

#def PlotColor(filenames):


#ameSaver('Titration13_06-19-17.wmv')

filenames = [img for img in glob.glob("test/")]
    #sorts the filenames 
filenames.sort() # ADD THIS LINE
images = []
    #reads and prints the name of each image
for img in filenames:
    n= cv2.imread(img)
    images.append(n)
    print (img)
    #temporary lists 
blue = []
green = []
red = []
for image in images:        
    # find color for image
        b = image[: , : , 0]        
        g = image[: , : , 1]
        r = image[: , : , 2]                
            # averages color
        bAvg = np.mean(b)
        gAvg = np.mean(g)
        rAvg = np.mean(r)                
        # append color value
        blue.append(bAvg)
        green.append(gAvg)
        red.append(rAvg)                        
     #creates dataframe       
dataframe = pd.DataFrame(blue, columns = ['blue'])
dataframe['green'] = green
dataframe['red'] = red
print(dataframe)            
dataframe.to_csv('titration13T_data.csv') 
    
df = dataframe
    #plots a seperate graph for each color value
df.plot(subplots=True)
plt.title('TitrationT13_Color_Values')
plt.show()



"""
cap = cv2.VideoCapture('Titration13_06-19-17.wmv') #

while(cap.isOpened()):
    # Capture frame-by-frame
    ret, frame = cap.read()
    #top rectangle
    center_rectangle = frame[400:455, 620:710, :] 

    cv2.imshow('frame', frame)
    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
"""