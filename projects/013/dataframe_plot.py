# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 16:23:06 2017

@author: Keeliann
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import glob
#pulls images from a folder
filenames = [img for img in glob.glob("titrationB_images/*")]
#sorts the filenames 
filenames.sort() # ADD THIS LINE

images = []

#reads and prints the name of each image
for img in filenames:
    n= cv2.imread(img)
    images.append(n)
    print (img)

#temporary lists 

blue = []
green = []
red = []

for image in images:
    
    # find color for image
        b = image[: , : , 0]
        g = image[: , : , 1]
        r = image[: , : , 2]
            
        # averages color
        bAvg = np.mean(b)
        gAvg = np.mean(g)
        rAvg = np.mean(r)
            
        # append color value
        blue.append(bAvg)
        green.append(gAvg)
        #if len(green) > 2:
         #   if gAvg < (green[-2])-10:
          #      print('Color change', img)
        red.append(rAvg)
        
        
 #creates dataframe       
dataframe = pd.DataFrame(blue, columns = ['blue'])
dataframe['green'] = green
dataframe['red'] = red
print(dataframe)        
        
dataframe.to_csv('titration13B_data.csv') 


#plots a seperate graph for each color value
dataframe.plot(subplots=True)
plt.title('Titration13B_Color_Values')
plt.show()













