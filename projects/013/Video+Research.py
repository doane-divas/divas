
# coding: utf-8

# In[10]:

import numpy as np
import cv2

cap = cv2.VideoCapture('test.mp4')
count = 0
while(cap.isOpened()):
    
    print("Pass")
    
    # Capture frame-by-frame
    ret, frame = cap.read()
    cv2.imwrite("frame%d.jpg" % count, frame)     # save frame as JPEG file
    
    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imwrite("frame grey%d.jpg" % count, gray)     # save frame as JPEG file
    
    count += 1 
    
    # Display the resulting frame
    cv2.imshow('frame',gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()


# In[ ]:



