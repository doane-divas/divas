#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  6 16:24:50 2017

@author: Sam and Tu
"""

import matplotlib.pyplot as plt
from scipy import stats
import numpy as np
import pandas as pd
import cv2 

image = cv2.imread('image2.tif')
image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)

wells = [[110,97], [184,97], [254,97], [324,97], [396,96], [466,97], #columnH 12-7
         [537,96], [607,97], [680,96], [750,97], [820,96], [891,97], #columnH 6-1
         [110,165],[184,165],[254,165],[324,165],[396,165],[466,165], #columnG 12-7
         [537,165],[607,165],[680,165],[750,166],[820,165],[891,165], #columnG 6-1
         [110,235],[184,235],[254,235],[324,235],[396,235],[466,235], #columnF 12-7
         [537,235],[607,235],[680,235],[750,235],[820,235],[891,235], #columnF 6-1
         [110,305],[184,305],[254,305],[324,305],[396,305],[466,305], #columnE 12-7
         [537,305],[607,305],[680,305],[750,305],[820,305],[891,305], #columnE 6-1
         [110,373],[184,373],[254,373],[324,373],[396,373],[466,373], #columnD 12-7
         [537,373],[607,373],[680,373],[750,372],[820,373],[891,373], #columnD 6-1
         [110,442],[184,442],[254,442],[324,442],[396,442],[466,442], #columnC 12-7
         [537,442],[607,442],[680,442],[750,443],[820,442],[891,442], #columnC 6-1
         [109,511],[184,511],[254,511],[324,511],[396,511],[466,511], #columnB 12-7
         [537,512],[607,512],[680,511],[750,511],[820,511],[891,511], #columnB 6-1
         [109,582],[184,582],[254,582],[324,581],[396,582],[466,582], #columnA 12-7
         [537,581],[607,582],[680,582],[750,581],[820,582],[891,582]] #columnA 6-1

column = []
for i in range(0, 12):
    
    k = i 
    
    for x in range(0,7):
        
        point = [wells[k][0],wells[k][1]]
        
        img_range = []
        for y in range(0,6):
            point[0] = point[0] + y
            for z in range(0,6):
                point[1] = point[1] + z
                img_range.append(image[point[0],point[1]])

        img = np.mean(img_range,0).astype(np.int64)
        
        #colour = ''.join(chr(c) for c in img).encode('hex')
        #print 'most frequent is %s (#%s)' % (img, colour)

        R_index = 0
        G_index = 1
        B_index = 2

        RGB = img
        
        column.append(np.log10(float(255)/RGB))

        k = k + 12
    
    if (i==0):
        column_1 = column 
        column = []    
    elif (i==1):
        column_2 = column 
        column = []    
    elif (i==2):
        column_3 = column 
        column = []    
    elif (i==3):
        column_4 = column 
        column = []    
    elif (i==4):
        column_5 = column 
        column = []    
    elif (i==5):
        column_6 = column 
        column = []    
    elif (i==6):
        column_7 = column 
        column = []    
    elif (i==7):
        column_8 = column 
        column = []    
    elif (i==8):
        column_9 = column 
        column = []    
    elif (i==9):
        column_10 = column 
        column = []    
    elif (i==10):
        column_11 = column 
        column = []    
    elif (i==11):
        column_12 = column 
        column = []
        
plate = pd.DataFrame({1: list(column_1),
                      2: list(column_2),
                      3: list(column_3),
                      4: list(column_4),
                      5: list(column_5),
                      6: list(column_6),
                      7: list(column_7),
                      8: list(column_8),
                      9: list(column_9),
                      10: list(column_10),
                      11: list(column_11),
                      12: list(column_12)})

for x in range (0,2):
    if x == 0:
        color1_channel = plate.ix[:,0:4]
        print ("Color 1")
    if x == 1:
        color1_channel = plate.ix[:,4:8]
        print ("Color 2")

    color1_channels = []
    r_channels = []
    g_channels = []
    b_channels = []
    for i in range(0,7):
        r_channel = []
        g_channel = []
        b_channel = []
        for column in color1_channel:
            row = color1_channel[column].ix[[i]]
            r_channel.append(row[i][0])
            g_channel.append(row[i][1])
            b_channel.append(row[i][2])
        r_channels.append(np.mean(r_channel))
        g_channels.append(np.mean(g_channel))
        b_channels.append(np.mean(b_channel))
    color1_channels.append(r_channels)
    color1_channels.append(g_channels)
    color1_channels.append(b_channels)

    colors = ["RED", "GREEN", "BLUE"]
    i = 0 
    for channel in color1_channels:

        print (colors[i])
        i = i + 1

        plt.xticks(plate.index, ['1.95E-06 (v/v)', '9.75E-06 (v/v)',
                                 '1.95E-05 (v/v)', '9.75E-05 (v/v)',
                                 '1.95E-04 (v/v)', '9.75E-04 (v/v)',
                                 '1.95E-03 (v/v)'])

        index = [(1.95 * (10**-6)), (9.75 * (10**-6)),
                 (1.95 * (10**-5)), (9.75 * (10**-5)),
                 (1.95 * (10**-4)), (9.75 * (10**-4)),
                 (1.95 * (10**-3))]

        channel = pd.DataFrame(channel, index=index, columns=['Absorbance'])
        channel.index.names = ['Concentration']
        print (channel)
        
        r_squared = 0 
        Y = channel['Absorbance'].values
        X = channel.index.values
        
        while (len(X)>5) and (r_squared > .99):
            Y = Y[:-1]
            X = X[:-1]
            slope, intercept, r_value, p_value, std_err = stats.linregress(X, Y)
            r_squared = r_value**2
            
        print ("Equation: ",intercept," + ","(",slope,"* X)")
        print ("R-squared: ", r_squared)
        best_fit = intercept + (slope * X)
        plt.plot(Y, 'ro')
        plt.plot(best_fit)
        plt.show()