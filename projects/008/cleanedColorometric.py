#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  7 11:42:43 2017

This script is created specifically for the green and blue plate tif image and
the yellow and red plate tif image. It averages the blue, green, and red
color channels individually using a 9x9 kernel in the center of each well.
The average values are put into a pandas dataframe and used alongside the
concentration list for linear regression to find a best fit line. The end of 
the file founded the limits of linearity using indices of the concentrate
list. The area of the code that plotted the images (line 141-146) was then
rewrote to only display - and save - the plots with the correct LOLs.

This script needs to be rewritten to a generic format that can be used for
different problems and this will be done in the future.

@authors: bzwiener and mfoster
"""

import sys, cv2
import numpy as np
import pandas as pd
import matplotlib.pylab as plt
import scipy.stats as ss

filename = sys.argv[1]

COLORS = [' ',' ']

if(filename == 'greenandblueplate.tif'):
    COLORS = ['green', 'blue']
else:
    COLORS = ['yellow', 'red']

img = cv2.imread(filename,cv2.IMREAD_COLOR + cv2.IMREAD_ANYDEPTH)

blueDataList = [[],[]]
greenDataList = [[],[]]
redDataList = [[],[]]

# top dye on plate
colG_row1_4 = [(110,165),(184,165),(254,165),(324,165)]
colF_row1_4 = [(110,235),(184,235),(254,235),(324,235)]
colE_row1_4 = [(110,305),(184,305),(254,305),(324,305)]
colD_row1_4 = [(110,373),(184,373),(254,373),(324,373)]
colC_row1_4 = [(110,442),(184,442),(254,442),(324,442)]
colB_row1_4 = [(110,511),(184,511),(254,511),(324,511)]
colA_row1_4 = [(110,582),(184,582),(254,582),(324,581)]

row1_4 = [colG_row1_4, colF_row1_4, colE_row1_4, colD_row1_4, colC_row1_4, colB_row1_4, colA_row1_4]

# bottom dye on plate
colG_row5_8 = [(396,165),(466,165),(537,165),(607,165)]
colF_row5_8 = [(396,235),(466,235),(537,235),(607,235)]
colE_row5_8 = [(396,305),(466,305),(537,305),(607,305)]
colD_row5_8 = [(396,373),(466,373),(537,373),(607,373)]
colC_row5_8 = [(396,442),(466,442),(537,442),(607,442)]
colB_row5_8 = [(396,511),(466,511),(537,511),(607,511)]
colA_row5_8 = [(396,582),(466,582),(537,582),(607,581)]

row5_8 = [colG_row5_8, colF_row5_8, colE_row5_8, colD_row5_8, colC_row5_8, colB_row5_8, colA_row5_8]

# combine the row lists
dyes = [row1_4, row5_8]

# loops through twice...first time being the top color and second the bottom color
for (i, color) in enumerate(dyes):
    
    # loops through the columns of each color
    for col in color:
        
        # temporary lists 
        blueTemp = []
        greenTemp = []
        redTemp = []
        
        for point in col:
            cy = point[0]
            cx = point[1]
            
            # find average color for 9 pixel kernel around centroid
            b = img[cy - 4 : cy + 5, cx - 4 : cx + 5, 0]
            g = img[cy - 4 : cy + 5, cx - 4 : cx + 5, 1]
            r = img[cy - 4 : cy + 5, cx - 4 : cx + 5, 2]
            # averages
            bAvg = np.mean(b)
            gAvg = np.mean(g)
            rAvg = np.mean(r)
            
            # append points 
            blueTemp.append(bAvg)
            greenTemp.append(gAvg)
            redTemp.append(rAvg)
        
        # turn the color lists to arrays to be averaged out
        blue = np.array(blueTemp)
        green = np.array(greenTemp)
        red = np.array(redTemp)
        # the color lists were values for each concentration
        bColAvg = np.mean(blue)
        gColAvg = np.mean(green)
        rColAvg = np.mean(red)
        
        bColAvg = np.log10(bColAvg/(2**16))*(-1)
        gColAvg = np.log10(gColAvg/(2**16))*(-1)
        rColAvg = np.log10(rColAvg/(2**16))*(-1)
        # add color column averages to the color lists
        blueDataList[i].append(bColAvg)
        greenDataList[i].append(gColAvg)
        redDataList[i].append(rColAvg)
        
# turn the blueDataList into a DataFrame
blueDataFrame = pd.DataFrame(np.array(blueDataList), 
                  columns = ['G', 'F', 'E', 'D', 'C', 'B', 'A'],
                  index = ['color1', 'color2'])
# turn the greenDataList into a DataFrame
greenDataFrame = pd.DataFrame(np.array(greenDataList), 
                  columns = ['G', 'F', 'E', 'D', 'C', 'B', 'A'],
                  index = ['color1', 'color2'])
# turn the redDataList into a DataFrame
redDataFrame = pd.DataFrame(np.array(redDataList), 
                  columns = ['G', 'F', 'E', 'D', 'C', 'B', 'A'],
                  index = ['color1', 'color2'])

print('blueDataFrame')
print(blueDataFrame)
print('\ngreenDataFrame')
print(greenDataFrame)
print('\nredDataFrame')
print(redDataFrame)

# concentration values used as independent variable
concentrate = [.00000195,.00000975,.0000195,.0000975,.000195,.000975,.00195]

# lists to hold r^2 values
blueR2values = [[],[]]
greenR2values = [[],[]]
redR2values = [[],[]]

blueSlopeValues = [[],[]]
greenSlopeValues = [[],[]]
redSlopeValues = [[],[]]

for i in range(4,8):
    
    for (j, row) in enumerate(blueDataFrame.itertuples()):
        plt.close()
        
        # find the linear regression
        out = ss.linregress(concentrate[:i], row[1:i+1])
        slope = round(out[0],3)
        intercept = round(out[1],3)
        rvalue = out[2]
        pvalue = out[3]
        se = out[4]
        
        linEqu = str(slope) + '*x + ' + str(intercept)
        
        plt.plot(concentrate[:i],row[1:i+1],linestyle='',marker='d',color = 'b', label=str(round(rvalue**2, 4)) + ' R^2')
        
        # Plot the theory
        tTheory = np.linspace(0,.002,50)
        PTheory = slope*tTheory + intercept
        plt.ylabel(r'absorbance = -log($I_o$/I)')
        plt.xlabel('concentration')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.plot(tTheory,PTheory, color = 'g', label=linEqu)
        plt.title(COLORS[j] + ': blue channel')
        plt.legend()
        
        # append the r^2 value to the list
        blueR2values[j].append(rvalue**2)
        blueSlopeValues[j].append(slope)
        
        # Plot the theory
        if (j == 0) and (COLORS[j] == "yellow") and (i == 5):
            plt.ylim(-.01, row[i]*1.1)
            plt.xlim(-.00001, concentrate[i-1]*1.1)
            conc = str(concentrate[i-1])
            plt.savefig('yellow_channel_blue_' + conc[2:] + '.png')
            plt.show()
        if (j == 0) and (COLORS[j] == "green") and (i == 7):
            plt.ylim(-.01, row[i]*1.1)
            plt.xlim(-.00001, concentrate[i-1]*1.1)
            conc = str(concentrate[i-1])
            plt.savefig('green_channel_blue_' + conc[2:] + '.png')
            plt.show()
        if (j == 1) and (COLORS[j] == "red") and (i == 7):
            plt.ylim(-.01, row[i]*1.1)
            plt.xlim(-.00001, concentrate[i-1]*1.1)
            conc = str(concentrate[i-1])
            plt.savefig('red_channel_blue_' + conc[2:] + '.png')
            plt.show()
        if (j == 1) and (COLORS[j] == "blue") and (i == 7):
            plt.ylim(-.01, row[i]*1.1)
            plt.xlim(-.00001, concentrate[i-1]*1.1)
            conc = str(concentrate[i-1])
            plt.savefig('blue_channel_blue_' + conc[2:] + '.png')
            plt.show()
        
      
    for (j, row) in enumerate(greenDataFrame.itertuples()):
        plt.close()
        
        # find the linear regression
        out = ss.linregress(concentrate[:i], row[1:i+1])
        slope = round(out[0],3)
        intercept = round(out[1],3)
        rvalue = out[2]
        pvalue = out[3]
        se = out[4]
        
        linEqu = str(slope) + '*x + ' + str(intercept)
        
        plt.plot(concentrate[:i],row[1:i+1],linestyle='',marker='d',color = 'g', label=str(round(rvalue**2, 4)) + ' R^2')
        
        # Plot the theory
        tTheory = np.linspace(0,.002,20)
        PTheory = slope*tTheory + intercept
        plt.ylabel(r'absorbance = -log($I_o$/I)')
        plt.xlabel('concentration')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.plot(tTheory,PTheory, color = 'r', label=linEqu)
        plt.title(COLORS[j] + ': green channel')
        plt.legend()
        
        # append the r^2 value to the list
        greenR2values[j].append(rvalue**2)
        greenSlopeValues[j].append(slope)
        
        if (j == 0) and (COLORS[j] == "yellow") and (i == 5):
            print('no good green channel plot')
            #plt.show()
        if (j == 0) and (COLORS[j] == "green") and (i == 7):
            plt.ylim(-.01, row[i]*1.1)
            plt.xlim(-.00001, concentrate[i-1]*1.1)
            conc = str(concentrate[i-1])
            plt.savefig('green_channel_green_' + conc[2:] + '.png')
            plt.show()
        if (j == 1) and (COLORS[j] == "red") and (i == 4):
            plt.ylim(-.01, row[i]*1.1)
            plt.xlim(-.00001, concentrate[i-1]*1.1)
            conc = str(concentrate[i-1])
            print(conc)
            plt.savefig('red_channel_green_' + conc[2:] + '.png')
            plt.show()
        if (j == 1) and (COLORS[j] == "blue") and (i == 7):
            plt.ylim(-.01, row[i]*1.1)
            plt.xlim(-.00001, concentrate[i-1]*1.1)
            conc = str(concentrate[i-1])
            plt.savefig('blue_channel_green_' + conc[2:] + '.png')
            plt.show()
    
    for (j, row) in enumerate(redDataFrame.itertuples()):
        plt.close()   
        
        # find the linear regression
        out = ss.linregress(concentrate[:i], row[1:i+1])
        slope = round(out[0],3)
        intercept = round(out[1],3)
        rvalue = out[2]
        pvalue = out[3]
        se = out[4]
        
        linEqu = str(slope) + '*x + ' + str(intercept)

        plt.plot(concentrate[:i],row[1:i+1],linestyle='',marker='d',color = 'r', label=str(round(rvalue**2, 4)) + ' R^2')

        # Plot the theory
        tTheory = np.linspace(0,.002,20)          
        PTheory = slope*tTheory + intercept
        plt.ylabel(r'absorbance = -log($I_o$/I)')
        plt.xlabel('concentration')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.plot(tTheory,PTheory, color = 'b', label=linEqu)
        plt.title(COLORS[j] + ': red channel')
        plt.legend()
        
        # append the r^2 value to the list
        redR2values[j].append(rvalue**2)
        redSlopeValues[j].append(slope)
        
        if (j == 0) and (COLORS[j] == "yellow") and (i == 5):
            print('no good red channel plot')
            #plt.show()
        if (j == 0) and (COLORS[j] == "green") and (i == 7):
            plt.ylim(-.01, row[i]*1.1)
            plt.xlim(-.00001, concentrate[i-1]*1.1)
            conc = str(concentrate[i-1])
            plt.savefig('green_channel_red_' + conc[2:] + '.png')
            plt.show()
        if (j == 1) and (COLORS[j] == "red") and (i == 4):
            print('no good red channel plot')
            #plt.show()
        if (j == 1) and (COLORS[j] == "blue") and (i == 6):
            plt.ylim(-.01, row[i]*1.1)
            plt.xlim(-.00001, concentrate[i-1]*1.1)
            conc = str(concentrate[i-1])
            plt.savefig('blue_channel_red_' + conc[2:] + '.png')
            plt.show()
    
    #show the plot for each LOL    
    

# turn the blueDataList into a DataFrame
blueR2DataFrame = pd.DataFrame(np.array(blueR2values),
                    columns = ['4', '5', '6', '7'],
                    index = ['color1', 'color2'])
# turn the greenDataList into a DataFrame
greenR2DataFrame = pd.DataFrame(np.array(greenR2values),
                    columns = ['4', '5', '6', '7'],
                    index = ['color1', 'color2'])
# turn the redDataList into a DataFrame
redR2DataFrame = pd.DataFrame(np.array(redR2values),
                    columns = ['4', '5', '6', '7'],
                    index = ['color1', 'color2'])

colors = [[],[]]

for (j, row) in enumerate(blueR2DataFrame.itertuples()):
    colors[j].append(row[1:])
    
for (j, row) in enumerate(greenR2DataFrame.itertuples()):
    colors[j].append(row[1:])
    
for (j, row) in enumerate(redR2DataFrame.itertuples()):
    colors[j].append(row[1:])

color1DataFrame = pd.DataFrame(np.array(colors[0]),
                    columns = ['4', '5', '6', '7'],
                    index = ['blue', 'green', 'red'])

color2DataFrame = pd.DataFrame(np.array(colors[1]),
                    columns = ['4', '5', '6', '7'],
                    index = ['blue', 'green', 'red'])

print(color1DataFrame)
print(color2DataFrame)

colorDataFrame = [color1DataFrame, color2DataFrame]
colorDataFrame = pd.concat(colorDataFrame)
print(colorDataFrame)
#colorDataFrame.to_csv(path_or_buf='2' + filename[:-3] + 'csv')

LOL = []

for row in colorDataFrame.itertuples():
    
    for (i,data) in enumerate(reversed(row)):
        
        if(i == 4):
            print("no good r squared value was found")
            LOL.append(-1)
            break
        
        if(data >= 0.99):
            LOL.append(7-i)
            break

# prints r g b index of first color then r g b of second color        
print(LOL)
            
"""
need to create a table 
"""        