import cv2, sys

img = cv2.imread(sys.argv[1])
mask = cv2.imread("mask.tif")
cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
cv2.namedWindow("Mask", cv2.WINDOW_NORMAL)
cv2.namedWindow("Final", cv2.WINDOW_NORMAL)
cv2.imshow("Image", img)
cv2.imshow("Mask", mask)
final = cv2.bitwise_and(img, mask)
cv2.imshow("Final", final)
cv2.waitKey(0)