'''
* Python colorometry test script
'''
import cv2, sys, imutils
from operator import itemgetter

# read image and apply mask
image = cv2.imread(sys.argv[1])
mask = cv2.imread("mask1.tif")
image = cv2.bitwise_and(image, mask)

# blur and threshold mask
gray = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(gray, (5, 5), 0)
thresh = cv2.threshold(blurred, 60, 255, cv2.THRESH_BINARY)[1]

# find contours in the thresholded image
(_, cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)
	
# create a list to hold center coordinates
centers = []

# loop over the contours
for c in cnts:

	# ignore small contours
	if cv2.contourArea(c) < 100:
		continue
	
	# compute the center of the contour
	M = cv2.moments(c)
	cX = int(M["m10"] / M["m00"])
	cY = int(M["m01"] / M["m00"])
 
	# add to list
	centers.append((cX, cY))
	# draw the contour and center of the shape on the image
	cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
	cv2.circle(image, (cX, cY), 7, (255, 255, 255), -1)
	cv2.putText(image, "center", (cX - 20, cY - 20),
		cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
 
# show the image
cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
cv2.imshow("Image", image)
cv2.waitKey(0)
