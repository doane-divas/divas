'''
 * Programmatically make a mask for a well plate.
'''
import cv2, numpy as np

orig = cv2.imread("plate-03.tif")
mask = np.zeros(orig.shape, dtype="uint8")
for y in range(234, 1938, 142):
	for x in range(196, 1316, 140):
		cv2.circle(mask, (x, y), 45, (255, 255, 255), -1)
		
cv2.imwrite("mask1.tif", mask)
