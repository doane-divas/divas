'''
 * Seedling root growth morphology example.
 *
 * How fast does the root grow?
 *
'''
import cv2, sys, getopt, numpy as np

def findEdges(img, k, t1, t2):
	img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	img = cv2.GaussianBlur(img, (k, k), 0)
	img = cv2.Canny(img, t1, t2)
	return img

# try block to catch malformed arguments
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:k:L:H:")
	if len(options) != 4:
		raise getopt.GetoptError("Improper arguments")

	# assign variables based on command-line arguments
	for opt, arg in options:
		# filename 
		if opt == "-f":
			filename = arg
		# blur kernel size
		elif opt == "-k":
			k = int(arg)
			if k % 2 != 1:
				raise getopt.GetoptError("Blur kernel size must be odd")
		# low threshold values
		elif opt == "-L":
			t1 = int(arg)
		elif opt == "-H":
			t2 = int(arg)

	img = cv2.imread(filename)	
	'''
	img = img[353:, :, :]
	img = findEdges(img, k, t1, t1)
	cv2.imshow("edges", img)
	'''
	img1 = img[353:740, 58:134, :]
	img2 = img[353:740, 134:294, :]
	img3 = img[353:740, 294:455, :]
	img4 = img[353:740, 455:631, :]
	img5 = img[353:740, 631:756, :]
	
	img1e = findEdges(img1, k, t1, t2)
	img2e = findEdges(img2, k, t1, t2)
	img3e = findEdges(img3, k, t1, t2)
	img4e = findEdges(img4, k, t1, t2)
	img5e = findEdges(img5, k, t1, t2)

	cv2.imshow("1", img1e)
	cv2.moveWindow("1", 0, 0)
	cv2.imshow("2", img2e)
	cv2.moveWindow("2", 200, 0)
	cv2.imshow("3", img3e)
	cv2.moveWindow("3", 400, 0)
	cv2.imshow("4", img4e)
	cv2.moveWindow("4", 600, 0)
	cv2.imshow("5", img5e)
	cv2.moveWindow("5", 800, 0)
	
	cv2.waitKey(0)
	
	(_, contours, _) = cv2.findContours(img4e.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	print("{} contours in img4".format(len(contours)))
	lines = img4.copy()
	cv2.drawContours(lines, contours, -1, (0, 255, 0), 2)
	cv2.imshow("contours", lines)
	cv2.waitKey(0)
	
	print(contours)

except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python Morph01.py -f filename -k kernel-size -L " +
		"low-thresh -H hi-thresh")
	sys.exit(-1)
