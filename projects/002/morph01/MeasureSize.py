'''
 * Example for measuring object size. For the sample images
 * included here, workable prarmeters are:
 * -k 7 -L 30 -H 150
 * 
'''
import cv2, sys, getopt, numpy as np
from imutils import perspective
from imutils import contours
from scipy.spatial import distance as dist

'''
 * Determine midpoint between two points.
'''
def midpoint(ptA, ptB):
    return ((ptA[0] + ptB[0]) * 0.5, (ptA[1] + ptB[1]) * 0.5)
    
# try block to catch malformed arguments
try:
    # parse command line arguments, ignoring the first
    options, arguments = getopt.getopt(sys.argv[1:], "f:k:L:H:d")
    if len(options) < 4 or len(options) > 5:
        raise getopt.GetoptError("Improper number of arguments")

    # assign variables based on command-line arguments
    wantDisplay = False
    for opt, arg in options:
        # filename 
        if opt == "-f":
            filename = arg
        # blur kernel size
        elif opt == "-k":
            k = int(arg)
            if k % 2 != 1:
                raise getopt.GetoptError("Blur kernel size must be odd")
        # threshold values
        elif opt == "-L":
            t1 = int(arg)
        elif opt == "-H":
            t2 = int(arg)
        elif opt == "-d":
            wantDisplay = True

    # read and display input image, based on -f filename parameter
    img = cv2.imread(filename)
    # crop out image flaws
    img = img[266:736, 70:735, :]
    blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(blur, (k, k), 0)
 
    if wantDisplay:
        cv2.imshow("blurred", blur)
        cv2.waitKey(0)
    
    # edge detection
    edge = cv2.Canny(blur, t1, t2)
    edge = cv2.dilate(edge, None, iterations = 3)
    edge = cv2.erode(edge, None, iterations = 3)
 
    if wantDisplay:
        cv2.imshow("edges", edge)
        cv2.waitKey(0)
    
    # find contours
    (_, cnts, _) = cv2.findContours(edge.copy(), cv2.RETR_EXTERNAL, 
        cv2.CHAIN_APPROX_SIMPLE)
    # sort contours from left to right
    (cnts, _) = contours.sort_contours(cnts)
    
    pixelsPerUnit = 72.05 # px / cm, 183.33 px / in
 
    dimList = []
    
    # draw bounding boxes
    for (i, c) in enumerate(cnts):
        # discard small contours
        if cv2.contourArea(c) < 500:
            continue
            
        # compute the rotated bounding box of the contour
        box = cv2.minAreaRect(c)
        box = cv2.boxPoints(box)
        box = np.array(box, dtype="int")        

        # order the points in the contour such that they appear
        # in top-left, top-right, bottom-right, and bottom-left
        # order, then draw the outline of the rotated bounding
        # box
        box = perspective.order_points(box)
        if wantDisplay:
            cv2.drawContours(img, [box.astype("int")], -1, (0, 255, 0), 2)
            # loop over the original points and draw them
            for (x, y) in box:
                cv2.circle(img, (int(x), int(y)), 5, (0, 0, 255), -1)
      
        # unpack the ordered bounding box, then compute the midpoint
        # between the top-left and top-right coordinates, followed by
        # the midpoint between bottom-left and bottom-right coordinates
        (tl, tr, br, bl) = box
        (tltrX, tltrY) = midpoint(tl, tr)
        (blbrX, blbrY) = midpoint(bl, br)
     
        # compute the midpoint between the top-left and top-right points,
        # followed by the midpoint between the top-righ and bottom-right
        (tlblX, tlblY) = midpoint(tl, bl)
        (trbrX, trbrY) = midpoint(tr, br)
     
        # draw the midpoints on the image
        if wantDisplay:
            cv2.circle(img, (int(tltrX), int(tltrY)), 5, (255, 0, 0), -1)
            cv2.circle(img, (int(blbrX), int(blbrY)), 5, (255, 0, 0), -1)
            cv2.circle(img, (int(tlblX), int(tlblY)), 5, (255, 0, 0), -1)
            cv2.circle(img, (int(trbrX), int(trbrY)), 5, (255, 0, 0), -1)
     
            # draw lines between the midpoints
            cv2.line(img, (int(tltrX), int(tltrY)), (int(blbrX), int(blbrY)),
                (255, 0, 255), 2)
            cv2.line(img, (int(tlblX), int(tlblY)), (int(trbrX), int(trbrY)),
                (255, 0, 255), 2)

        # compute the Euclidean distance between the midpoints
        dA = dist.euclidean((tltrX, tltrY), (blbrX, blbrY))
        dB = dist.euclidean((tlblX, tlblY), (trbrX, trbrY))

        # compute the size of the object
        dimA = dA / pixelsPerUnit
        dimB = dB / pixelsPerUnit

        # add dimensions to output list
        dimList.append(dimA)

        if wantDisplay:     
            # draw the object sizes on the image
            cv2.putText(img, "{:.1f}cm".format(dimA),
                (int(tltrX - 15), int(tltrY - 10)), cv2.FONT_HERSHEY_SIMPLEX,
                0.65, (255, 255, 255), 2)
            cv2.putText(img, "{:.1f}cm".format(dimB),
                (int(trbrX + 10), int(trbrY)), cv2.FONT_HERSHEY_SIMPLEX,
                0.65, (255, 255, 255), 2)

    if wantDisplay:
        # show the image
        cv2.imshow("Bounding boxes", img)
        cv2.waitKey(0)    

    # output dimension list
    print(dimList)    
    
except getopt.GetoptError:
    # if we have errors in # or type of command-line arguments,
    # print a usage message and exit.
    print("usage: python MeasureSize.py -f filename -k kernel-size -L low -H high [-d]")
    sys.exit(-1)
