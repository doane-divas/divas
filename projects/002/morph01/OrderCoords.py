'''
 * Example for ordering bounding box coordinates
 * 
'''
import imutils, cv2, sys, getopt, numpy as np
from imutils import perspective
from imutils import contours

def order_points_old(pts):
	'''
	 * Don't use this one! It has a flaw -- if the sum 
	 * is equal, ordering can be incorrect
	 *
	'''
	# initialize a list of coordinates that will be ordered
	# such that the first entry in the list is the top-left,
	# the second entry is the top-right, the third is the
	# bottom-right, and the fourth is the bottom-left
	rect = np.zeros((4, 2), dtype="float32")
 
	# the top-left point will have the smallest sum, whereas
	# the bottom-right point will have the largest sum
	s = pts.sum(axis=1)
	rect[0] = pts[np.argmin(s)]
	rect[2] = pts[np.argmax(s)]
 
	# now, compute the difference between the points, the
	# top-right point will have the smallest difference,
	# whereas the bottom-left will have the largest difference
	diff = np.diff(pts, axis=1)
	rect[1] = pts[np.argmin(diff)]
	rect[3] = pts[np.argmax(diff)]
 
	# return the ordered coordinates
	return rect
	
# read input file name
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:k:L:H:t:")
	if len(options) != 5:
		raise getopt.GetoptError("Improper number of arguments")

	# assign variables based on command-line arguments
	for opt, arg in options:
		# filename 
		if opt == "-f":
			filename = arg
		# blur kernel size
		elif opt == "-k":
			k = int(arg)
			if k % 2 != 1:
				raise getopt.GetoptError("Blur kernel size must be odd")
		# threshold values
		elif opt == "-L":
			t1 = int(arg)
		elif opt == "-H":
			t2 = int(arg)
		elif opt == "-t":
			if arg == "bad":
				bbType = arg
			else:
				bbType = "good"

	# read and display input image, based on -f filename parameter
	img = cv2.imread(filename)
	# crop out image flaws
	img = img[266:736, 70:735, :]
	blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	blur = cv2.GaussianBlur(blur, (k, k), 0)
	cv2.imshow("blurred", blur)
	cv2.waitKey(0)
	
	# edge detection
	edge = cv2.Canny(blur, t1, t2)
	edge = cv2.dilate(edge, None, iterations = 3)
	edge = cv2.erode(edge, None, iterations = 3)
	cv2.imshow("edges", edge)
	cv2.waitKey(0)
	
	# find contours
	(_, cnts, _) = cv2.findContours(edge.copy(), cv2.RETR_EXTERNAL, 
		cv2.CHAIN_APPROX_SIMPLE)
	(cnts, _) = contours.sort_contours(cnts)
	bbColors = ((0, 0, 255), (240, 0, 159), (255, 0, 0), (255, 255, 0))
	
	# draw bounding boxes
	for (i, c) in enumerate(cnts):
		# discard small contours
		if cv2.contourArea(c) < 100:
			continue
			
		# compute the rotated bounding box of the contour, then
		# draw the contours
		box = cv2.minAreaRect(c)
		box = cv2.boxPoints(box)
		box = np.array(box, dtype="int")
		cv2.drawContours(img, [box], -1, (0, 255, 0), 2)		

		# order the points in the contour such that they appear
		# in top-left, top-right, bottom-right, and bottom-left
		# order, then draw the outline of the rotated bounding
		# box
		if bbType == "bad":
			rect = order_points_old(box)
		else:
			rect = perspective.order_points(box)
		
		# loop over the original points and draw them
		for ((x, y), color) in zip(rect, bbColors):
			cv2.circle(img, (int(x), int(y)), 5, color, -1)
	 
		# draw the object num at the top-left corner
		cv2.putText(img, "Object #{}".format(i + 1),
			(int(rect[0][0] - 15), int(rect[0][1] - 15)),
			cv2.FONT_HERSHEY_SIMPLEX, 0.55, (255, 255, 255), 2)
 
	# show the image
	cv2.imshow("Bounding boxes", img)
	cv2.waitKey(0)		
	
except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python OrderCoords.py -f filename -k kernel-size -L low -H high -t [bad|good]")
	sys.exit(-1)