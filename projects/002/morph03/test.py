import cv2

# second param 0 causes grayscale
img = cv2.imread("trial-16.jpg", 0)
cv2.namedWindow("raw", cv2.WINDOW_NORMAL)
cv2.imshow("raw", img)
print(img.shape)
cv2.waitKey(0)

ret,thresh = cv2.threshold(img, 127, 255, 0)
cv2.namedWindow("thresh", cv2.WINDOW_NORMAL)
cv2.imshow("thresh", thresh)
cv2.waitKey(0)

(_, contours, hierarchy) = cv2.findContours(thresh, 1, 2)
cnt = contours[0]
area = cv2.contourArea(cnt)
print(area)