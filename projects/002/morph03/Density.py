'''
 * Example for measuring mature root density.
 * 
'''
import cv2, sys, getopt

# try block to catch malformed arguments
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:k:L:H:i:d")
	if len(options) < 5 or len(options) > 6:
		raise getopt.GetoptError("Improper number of arguments")
		
	# assign variables based on command-line arguments
	wantDisplay = False
	for opt, arg in options:
		# filename 
		if opt == "-f":
			filename = arg
		# blur kernel size
		elif opt == "-k":
			k = int(arg)
			if k % 2 != 1:
				raise getopt.GetoptError("Blur kernel size must be odd")
		# threshold values
		elif opt == "-L":
			t1 = int(arg)
		elif opt == "-H":
			t2 = int(arg)
		# edge detection iterations
		elif opt == "-i":
			iter = int(arg)
		# display or text only?
		elif opt == "-d":
			wantDisplay = True

	# read and display input image, based on -f filename parameter
	img = cv2.imread(filename)
	blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	blur = cv2.GaussianBlur(blur, (k, k), 0)
 
	if wantDisplay:
		cv2.imshow("blurred", blur)
		cv2.waitKey(0)
		
	# perform thresholding
	maskLayer = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 3)
 
	if wantDisplay:
		cv2.imshow("thresh", maskLayer)
		cv2.waitKey(0)
	
except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python Density.py -f filename -k kernel-size -L low -H high [-d]")
	sys.exit(-1)
