'''
* Python script to cap the color level in an image.
'''
import cv2, sys, getopt

def checkParameters(args):
	'''
	 * function to validate command-line arguments
	'''
	try:
		options, arguments = getopt.getopt(args, "f:")
		return options[0][1]
	except getopt.GetoptError:
		print("usage: python CapImg.py -f filename")
		sys.exit(-1)

# read input image, based on -f filename parameter
filename = checkParameters(sys.argv[1:])		
img = cv2.imread(filename)
	
# display original image
cv2.imshow("original img", img)
cv2.waitKey(0)

# cap color values at 96
img[img > 96] = 96
		
# display capped image
cv2.imshow("capped img", img)
cv2.waitKey(0)