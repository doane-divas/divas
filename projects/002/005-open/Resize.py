'''
 * Python program to read an image, resize it, and save it
 * under a different name.
'''
import cv2

# read in image
image = cv2.imread(filename = "chicago.jpg")

# resize the image
small = cv2.resize(src = image, dsize = None, fx = 0.5, fy = 0.5)

# write out image
cv2.imwrite(filename = "resized.jpg", img = small)
