PImage img;

void setup() {
  size(767, 614);
  img = loadImage("06-junk-before.jpg");
  stroke(0);
  smooth();
}

void draw() {
  image(img, 0, 0);
  line(0, 0, width - 1, 0);
  line(width - 1, 0, width - 1, height - 1);
  line(width - 1, height - 1, 0, height - 1);
  line(0, height - 1, 0, 0);
}

void mouseClicked() {
  save("06-junk-before.jpg");
  exit();
}