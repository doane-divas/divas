PImage orig, blur;

final color WHITE = color(255, 255, 255), RED = color(255, 0, 0);

int bX, bY, idx;

void drawOrig() {
  color c = WHITE;
  noFill();
  stroke(c);
  rect(0, 0, 252, 252);
  for (int y = 0; y < orig.height; y++) {
    for (int x = 0; x < orig.width; x++) {
      c = orig.get(x, y);
      fill(c);
      stroke(c);
      rect(x * 10 + 1, y * 10 + 1, 10, 10);
    }
  }
}

void drawDemo() {
  color c = WHITE;
  noFill();
  stroke(c);
  rect(270, 0, 252, 252);
  for (int y = 0; y < orig.height; y++) {
    for (int x = 0; x < orig.width; x++) {
      if(x == bX && y == bY) {
        c = RED;
      } else if ((x >= bX - 3) && (x <= bX + 3) && (y >= bY - 3) && (y <= bY + 3)) {
        c = orig.get(x, y);
        c = lerpColor(c, color(255, 255, 255), 0.3);
      } else {
        c = orig.get(x, y);
      }
      fill(c);
      stroke(c);
      rect(x * 10 + 1 + 270, y * 10 + 1, 10, 10);
    }
  }
}

void drawBlur() {
  color c = WHITE;
  noFill();
  stroke(c);
  rect(538, 0, 252, 252);
  for (int y = 0; y < orig.height; y++) {
    for (int x = 0; x < orig.width; x++) {
      int i = y * 25 + x;
      if (i <= idx) { 
        c = blur.get(x, y);
        fill(c);
        stroke(c);
        rect(x * 10 + 1 + 538, y * 10 + 1, 10, 10);
      } else {
      }
    }
  }
}

void setup() {
  size(791, 253);

  orig = loadImage("cat-eye.jpg");

  blur = loadImage("blur-cat-eye.jpg");

  bX = bY = 0;

  smooth();
  
  frameRate(1);
}

void draw() {
  background(0);
  drawOrig();
  drawDemo();
  drawBlur();
  bX++;
  if(bX >= blur.width) {
    bX = 0;
    bY++;
  }
  save("blurdemo" + String.format("%03d", idx) + ".jpg");
  idx++;
  if(bY >= blur.height) {
    exit();
  }
}