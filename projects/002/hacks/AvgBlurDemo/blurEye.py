#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  5 12:52:46 2017

@author: mark
"""

import cv2, sys

filename = sys.argv[1]

img = cv2.imread(filename)

bimg = cv2.blur(img, (7, 7))

cv2.namedWindow("img", cv2.WINDOW_NORMAL)
cv2.imshow("img", bimg)
cv2.waitKey(0)
cv2.imwrite("blur-cat-eye.jpg", bimg)