PImage img = loadImage("cat.jpg");
for(int y = 0; y < 4; y++) {
  for(int x = 0; x < 4; x++) {
    color c = img.get(x, y);
    print(String.format("%3d ", int(blue(c))));
  }
  println();
}

exit();