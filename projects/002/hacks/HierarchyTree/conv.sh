#!/bin/bash
# convert pdf to png

# remove existing png
rm *.png

# use ImageMagick convert
for f in *.pdf
do
	convert -density 200 $f -quality 100 $f.png
done
