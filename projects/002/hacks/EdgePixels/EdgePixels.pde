PImage img;
int xLoc = 397;
int yLoc = 280;

void setup() {
  size(200, 200);
  img = loadImage("07-bw.jpg");
  smooth();
  //rectMode(CENTER);
}

void draw() {
  for(int y = 0; y < 20; y++) {
    for(int x = 0; x < 20; x++) {
      color c = img.get(xLoc + x, yLoc + y);
      fill(c);
      stroke(c);
      rect(x * 10, y * 10, 10, 10);
    }
  } // for y
}

void keyPressed() {
  save("07-bw-edge-pixels.jpg");
  exit();
}