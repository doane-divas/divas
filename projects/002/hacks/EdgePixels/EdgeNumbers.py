'''
 * dump numbers obtained by Sobel edge detection
'''
import cv2, sys

filename = sys.argv[1]

img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)

blur = cv2.GaussianBlur(img, (3, 3), 0)
(t, binary) = cv2.threshold(blur, 210, 255, cv2.THRESH_BINARY_INV)

edgeX = cv2.Sobel(binary, cv2.CV_16S, 1, 0)


edgeX = edgeX[280:300, 397:417]

print(edgeX)