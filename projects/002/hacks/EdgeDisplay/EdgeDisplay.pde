PImage img;

void setup() {
  size(832, 618);
  img = loadImage("07-bw.jpg");
  smooth();
  noFill();
  stroke(255, 0, 0);
  textAlign(CENTER, CENTER);
}

void draw() {
  image(img, 0, 0);
  rect(mouseX, mouseY, 20, 20);
  text("(" + mouseX + ", " + mouseY + ")", 30, height - 30);
}