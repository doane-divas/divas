import cv2, numpy as np, matplotlib.pyplot as plt

color1Conc1 = [(110, 165), (184, 165), (254, 165), (324, 165)]
color1Conc2 = [(110, 235), (184, 235), (254, 235), (324, 235)]
color1Conc3 = [(110, 305), (184, 305), (254, 305), (324, 305)]
color1Conc4 = [(110, 373), (184, 373), (254, 373), (324, 373)]
color1Conc5 = [(110, 442), (184, 442), (254, 442), (324, 442)]
color1Conc6 = [(109, 511), (184, 511), (254, 511), (324, 511)]
color1Conc7 = [(109, 582), (184, 582), (254, 582), (324, 581)]

color2Conc1 = [(396, 165), (466, 165), (537, 165), (607, 165)]
color2Conc2 = [(396, 235), (466, 235), (537, 235), (607, 235)]
color2Conc3 = [(396, 305), (466, 305), (537, 305), (607, 305)]
color2Conc4 = [(396, 373), (466, 373), (537, 373), (607, 373)]
color2Conc5 = [(396, 442), (466, 442), (537, 442), (607, 442)]
color2Conc6 = [(396, 511), (466, 511), (537, 512), (607, 512)]
color2Conc7 = [(396, 582), (466, 582), (537, 581), (607, 582)]

concentrations = [1.95e-6, 9.75e-6, 1.95e-5, 9.75e-5, 1.95e-4, 9.75e-4, 1.95e-3]         

def avgWellChannelValue(channel, center):
    print(np.min(channel[center[1] - 3 : center[1] + 4, 
                           center[0] - 3 : center[0] + 4]),
        np.max(channel[center[1] - 3 : center[1] + 4, 
                           center[0] - 3 : center[0] + 4]),
        np.std(channel[center[1] - 3 : center[1] + 4, 
                           center[0] - 3 : center[0] + 4]))
    return np.mean(channel[center[1] - 3 : center[1] + 4, 
                           center[0] - 3 : center[0] + 4])  

def avgConcChannelValue(channel, centers):
    wellAverages = []
    for center in centers:
        wellAverages.append(avgWellChannelValue(channel, center))
    return np.mean(wellAverages)

# read plate images         
gbImage = cv2.imread("greenandblueplate.tif", 
                     cv2.IMREAD_COLOR + cv2.IMREAD_ANYDEPTH)

yrImage = cv2.imread("yellowandredplate.tif", 
                     cv2.IMREAD_COLOR + cv2.IMREAD_ANYDEPTH)

# split into color channels
gbImageBlue = gbImage[:, :, 0]
gbImageGreen = gbImage[:, :, 1]
gbImageRed = gbImage[:, :, 2]

yrImageBlue = yrImage[:, :, 0]
yrImageGreen = yrImage[:, :, 1]
yrImageRed = yrImage[:, :, 2]

gbGreen_BlueChannel_Averages = []
gbGreen_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc1))
gbGreen_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc2))
gbGreen_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc3))
gbGreen_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc4))
gbGreen_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc5))
gbGreen_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc6))
gbGreen_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc7))

gbGreen_GreenChannel_Averages = []
gbGreen_GreenChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc1))
gbGreen_GreenChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc2))
gbGreen_GreenChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc3))
gbGreen_GreenChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc4))
gbGreen_GreenChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc5))
gbGreen_GreenChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc6))
gbGreen_GreenChannel_Averages.append(avgConcChannelValue(gbImageBlue, color1Conc7))

gbGreen_RedChannel_Averages = []
gbGreen_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color1Conc1))
gbGreen_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color1Conc2))
gbGreen_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color1Conc3))
gbGreen_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color1Conc4))
gbGreen_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color1Conc5))
gbGreen_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color1Conc6))
gbGreen_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color1Conc7))

gbBlue_BlueChannel_Averages = []
gbBlue_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color2Conc1))
gbBlue_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color2Conc2))
gbBlue_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color2Conc3))
gbBlue_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color2Conc4))
gbBlue_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color2Conc5))
gbBlue_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color2Conc6))
gbBlue_BlueChannel_Averages.append(avgConcChannelValue(gbImageBlue, color2Conc7))

gbBlue_GreenChannel_Averages = []
gbBlue_GreenChannel_Averages.append(avgConcChannelValue(gbImageGreen, color2Conc1)) 
gbBlue_GreenChannel_Averages.append(avgConcChannelValue(gbImageGreen, color2Conc2)) 
gbBlue_GreenChannel_Averages.append(avgConcChannelValue(gbImageGreen, color2Conc3)) 
gbBlue_GreenChannel_Averages.append(avgConcChannelValue(gbImageGreen, color2Conc4)) 
gbBlue_GreenChannel_Averages.append(avgConcChannelValue(gbImageGreen, color2Conc5)) 
gbBlue_GreenChannel_Averages.append(avgConcChannelValue(gbImageGreen, color2Conc6)) 
gbBlue_GreenChannel_Averages.append(avgConcChannelValue(gbImageGreen, color2Conc7)) 

gbBlue_RedChannel_Averages = []
gbBlue_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color2Conc1))
gbBlue_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color2Conc2))
gbBlue_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color2Conc3))
gbBlue_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color2Conc4))
gbBlue_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color2Conc5))
gbBlue_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color2Conc6))
gbBlue_RedChannel_Averages.append(avgConcChannelValue(gbImageRed, color2Conc7))

yrYellow_BlueChannel_Averages = []
yrYellow_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color1Conc1))
yrYellow_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color1Conc2))
yrYellow_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color1Conc3))
yrYellow_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color1Conc4))
yrYellow_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color1Conc5))
yrYellow_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color1Conc6))
yrYellow_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color1Conc7))

yrYellow_GreenChannel_Averages = []
yrYellow_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color1Conc1))
yrYellow_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color1Conc2))
yrYellow_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color1Conc3))
yrYellow_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color1Conc4))
yrYellow_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color1Conc5))
yrYellow_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color1Conc6))
yrYellow_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color1Conc7))

yrYellow_RedChannel_Averages = []
yrYellow_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color1Conc1))
yrYellow_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color1Conc2))
yrYellow_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color1Conc3))
yrYellow_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color1Conc4))
yrYellow_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color1Conc5))
yrYellow_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color1Conc6))
yrYellow_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color1Conc7))

yrRed_BlueChannel_Averages = []
yrRed_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color2Conc1))
yrRed_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color2Conc2))
yrRed_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color2Conc3))
yrRed_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color2Conc4))
yrRed_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color2Conc5))
yrRed_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color2Conc6))
yrRed_BlueChannel_Averages.append(avgConcChannelValue(yrImageBlue, color2Conc7))

yrRed_GreenChannel_Averages = []
yrRed_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color2Conc1))
yrRed_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color2Conc2))
yrRed_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color2Conc3))
yrRed_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color2Conc4))
yrRed_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color2Conc5))
yrRed_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color2Conc6))
yrRed_GreenChannel_Averages.append(avgConcChannelValue(yrImageGreen, color2Conc7))

yrRed_RedChannel_Averages = []
yrRed_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color2Conc1))
yrRed_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color2Conc2))
yrRed_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color2Conc3))
yrRed_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color2Conc4))
yrRed_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color2Conc5))
yrRed_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color2Conc6))
yrRed_RedChannel_Averages.append(avgConcChannelValue(yrImageRed, color2Conc7))

plt.title("Green Well Intensities")
plt.ticklabel_format(style="sci", axis="x", scilimits=(0, 0))
plt.xlabel("concentrations")
plt.ylabel("mean channel value")
plt.plot(concentrations, gbGreen_BlueChannel_Averages, 'bo')
plt.plot(concentrations, gbGreen_GreenChannel_Averages, 'g^')
plt.plot(concentrations, gbGreen_RedChannel_Averages, 'rx')
plt.show()

plt.title("Blue Well Intensities")
plt.ticklabel_format(style="sci", axis="x", scilimits=(0, 0))
plt.xlabel("concentrations")
plt.ylabel("mean channel value")
plt.plot(concentrations, gbBlue_BlueChannel_Averages, 'bo')
plt.plot(concentrations, gbBlue_GreenChannel_Averages, 'g^')
plt.plot(concentrations, gbBlue_RedChannel_Averages, 'rx')
plt.show()

plt.title("Yellow Well Intensities")
plt.ticklabel_format(style="sci", axis="x", scilimits=(0, 0))
plt.xlabel("concentrations")
plt.ylabel("mean channel value")
plt.plot(concentrations, yrYellow_BlueChannel_Averages, 'bo')
plt.plot(concentrations, yrYellow_GreenChannel_Averages, 'g^')
plt.plot(concentrations, yrYellow_RedChannel_Averages, 'rx')
plt.show()

plt.title("Red Well Intensities")
plt.ticklabel_format(style="sci", axis="x", scilimits=(0, 0))
plt.xlabel("concentrations")
plt.ylabel("mean channel value")
plt.plot(concentrations, yrRed_BlueChannel_Averages, 'bo')
plt.plot(concentrations, yrRed_GreenChannel_Averages, 'g^')
plt.plot(concentrations, yrRed_RedChannel_Averages, 'rx')
plt.show()

'''
cv2.namedWindow("test", cv2.WINDOW_NORMAL)
cv2.imshow("test", gbImageBlue)
cv2.waitKey(0)
'''