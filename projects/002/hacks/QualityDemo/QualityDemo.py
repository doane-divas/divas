''' 
 * Python script to illustrate quality differences between 
 * uncompressed and compressed image formats. Read in a 
 * large .tif, and the equivalent in .jpg (exported with
 * low quality), extract the equivalent subimage in both.
 *
 * mmm, 2/2/2017
''' 
import cv2, sys, getopt

def subImage(image, x1, y1, x2, y2):
	''' 
	 * subimage function
	 *
	 * image: cv2 image to start with
	 * x1: x coordinate of upper left corner of subimage
	 * y1: y     "       "   "    "     "     "    "
	 * x2: x     "       " lower right  "     "    "
	 * y2: y     "       "   "    "     "     "    "
	 *
	 * returns a new cv2 image bounded by the coordinates
	'''
	return image[y1:y2, x1:x2]
	
def stripExtension(filename):
	'''
	 * stripExtension function
	 *
	 * filename: filename with extension, e.g., hi_res.tif; 
	 * assumes that the only dot in the name is right before
	 * the extension
	 *
	 * returns filename without the extension, e.g., hi_res
	'''
	return filename[0:filename.index('.')]

def main(argv):
	''' 
	 * main function 
	 *
	 * argv: list of command-line arguments passed in to 
	 * the program
	'''
	try:
		# read .tif and .jpg input file names from 
		# command-line arguments
		options, arguments = getopt.getopt(argv, "t:j:x:y:d:")
		if len(options) != 5:
			raise getopt.GetoptError("Improper arguments")
			
		# read the input images and arguments
		for opt, arg in options:
			if opt == "-t":
				tifImg = cv2.imread(arg)
				tifSmallName = stripExtension(arg) + "-small-tif.jpg"
			elif opt == "-j":
				jpgImg = cv2.imread(arg)
				jpgSmallName = stripExtension(arg) + "-small-jpg.jpg"
			elif opt == "-x":
				x = int(arg)
			elif opt == "-y":
				y = int(arg)
			elif opt == "-d":
				d = int(arg)
		
		# create and display subimages
		tifSmall = subImage(tifImg, x, y, x + d, y + d)
		tifSmall = cv2.resize(tifSmall, (500, 500), interpolation = cv2.INTER_AREA)
		cv2.imshow("TIF sample", tifSmall)
		
		jpgSmall = subImage(jpgImg, x, y, x + d, y + d)
		jpgSmall = cv2.resize(jpgSmall, (500, 500), interpolation = cv2.INTER_AREA)
		cv2.imshow("JPG sample", jpgSmall)
		
		cv2.waitKey(0)
		
		# dump the smaller images
		cv2.imwrite(tifSmallName, tifSmall)
		cv2.imwrite(jpgSmallName, jpgSmall)
			
	except getopt.GetoptError:
		# if we have errors in # or type of command-line arguments,
		# print a usage message and exit.
		print("usage: python QualityDemo.py -t file.tif -j file.jpg -x <x> -y <y> -d <d>")
		sys.exit(-1)
		
# launch the main function
if __name__ == "__main__":
	main(sys.argv[1:]) # ditch the first arg (app name)
	
