#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videoio.hpp"
#include <iostream>

int main()
{
        //display the original image
        IplImage* img = cvLoadImage("../test/chair.jpg");
        cvNamedWindow("MyWindow");
        cvShowImage("MyWindow", img);

        //erode and display the eroded image
        //cvErode(img, img, 0, 2);
	//cvDilate(img, img, 0, 2);
	cvNot(img, img);
        cvNamedWindow("Filter");
        cvShowImage("Filter", img);
       
        cvWaitKey(0);
       
        //cleaning up
        cvDestroyWindow("MyWindow");
        cvDestroyWindow("Filter");
        cvReleaseImage(&img);
       
        return 0;
}
