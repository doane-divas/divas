'''
 * Python program demonstraing how to sort contours using imutils.
 *
 * Mark Meysenburg, 5/26/2017
'''
import cv2
from imutils import contours as imc

'''
 * function to find the centroid of a contour
'''
def findCentroid(contour):
    M = cv2.moments(contour)
    x = int(M['m10'] / M['m00'])
    y = int(M['m01'] / M['m00'])
    return (x, y)

# read original image in color
colorImage = cv2.imread("junk.jpg")

# blur, grayscale, and threshold to produce a binary image
grayscaleImage = cv2.cvtColor(colorImage, cv2.COLOR_BGR2GRAY)
blurredImage = cv2.GaussianBlur(grayscaleImage, (5, 5), 0)
(t, binaryImage) = cv2.threshold(blurredImage, 210, 255, 
    cv2.THRESH_BINARY_INV)

# find contours in the binary image
(_, contours, _) = cv2.findContours(binaryImage, cv2.RETR_EXTERNAL,
    cv2.CHAIN_APPROX_SIMPLE)

# use imutils function to sort contours. See the source code at
# https://github.com/jrosebr1/imutils/blob/master/imutils/contours.py
# for details on the options for ordering
(sortedContours, boundingBoxes) = imc.sort_contours(contours)


# prepare a window for display
cv2.namedWindow("display", cv2.WINDOW_NORMAL)

# draw sorted contours on the original image, one at a time, ignoring the 
# small ones
for (i, contour) in enumerate(sortedContours):
    if len(contour) > 100:      # don't draw small contours
    
        # draw the current contour in red
        cv2.drawContours(colorImage, sortedContours, i, (0, 0, 255), 5)
        
        # draw a black label with contour number
        cv2.putText(colorImage, str(i), findCentroid(contour), 
                    cv2.FONT_HERSHEY_PLAIN, 7.5, (0, 0, 0), 5)
        
        cv2.imshow("display", colorImage)
        cv2.waitKey(0)
        
