size(640, 480);

PFont font = loadFont("Courier-Bold-54.vlw");
textFont(font);
textAlign(CENTER, CENTER);

fill(255);
stroke(255);

background(0);
text("Average Blur", width / 2, height / 2);

stroke(255, 0, 0);
fill(255, 0, 0);
strokeWeight(4);
line(0, 0, width, height / 2);

stroke(0, 255, 0);
fill(0, 255, 0);
line(0, height / 2, width, 0);

stroke(0, 0, 255);
fill(0, 0, 255);
ellipse(width / 2, 3 * height / 4, 150, 150);

save("AverageTarget.png");
exit();