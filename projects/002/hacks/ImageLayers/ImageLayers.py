'''
 * ImageLayers.py
 *
 * Python script to illustrate how images are stored in NumPy arrays.
 *
 * mmm, 2/2/2017
'''

import cv2, sys, getopt, numpy

def main(argv):
	''' 
	 * main function 
	 *
	 * argv: list of command-line arguments passed in to 
	 * the program
	'''

	# read input file name
	try:
		options, arguments = getopt.getopt(argv, "f:")
		if len(options) != 1:
			raise getopt.GetoptError("Improper arguments")

		# read input image
		filename = options[0][1]
		orig = cv2.imread(filename)
	
		# pull out color layers
		blue = numpy.zeros(orig.shape, dtype="uint8")
		blue[:, :, 0] = orig[:, :, 0]

		green = numpy.zeros(orig.shape, dtype="uint8")
		green[:, :, 1] = orig[:, :, 1]

		red = numpy.zeros(orig.shape, dtype="uint8")
		red[:, :, 2] = orig[:, :, 2]
	
		# display images
		cv2.imshow("original", orig)
		cv2.imshow("blue", blue)
		cv2.imshow("green", green)
		cv2.imshow("red", red)
	
		cv2.waitKey(0)

		# write image layers
		cv2.imwrite("blue.jpg", blue)
		cv2.imwrite("green.jpg", green)
		cv2.imwrite("red.jpg", red)
			
	except getopt.GetoptError:
		# if we have errors in # or type of command-line arguments,
		# print a usage message and exit.
		print("usage: python ImageLayers.py -f filename")
		sys.exit(-1)
	
# launch the main function
if __name__ == "__main__":
	main(sys.argv[1:]) # ditch the first arg (app name)