PImage img;

void setup() {
  size(800, 600);
  img = loadImage("02-board.jpg");
  smooth();
  noFill();
  stroke(0);
  PFont font = loadFont("DialogInput.bolditalic-12.vlw");
  textFont(font);
  textAlign(CENTER, CENTER);
}

void draw() {
  image(img, 0, 0);
  noFill();
  rect(135, 60, (480 - 150), (150 - 60));
  fill(0);
  text("(135, 60)", 100, 65);
  text("(480, 150)", 505, 145);
}

void keyPressed() {
  save("02-board-coordinates.jpg");
}