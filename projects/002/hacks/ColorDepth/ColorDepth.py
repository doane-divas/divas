#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Simple script to load an image in any color depth, instead of just 24-bit.

Created on Tue Jun  6 09:55:27 2017

@author: Mark Meysenburg
"""

import cv2, sys, numpy as np

img = cv2.imread(sys.argv[1], cv2.IMREAD_COLOR + cv2.IMREAD_ANYDEPTH)

print("maximum value in any channel:", np.max(img))
print("minimum value in any channel:", np.min(img))

cv2.namedWindow("image", cv2.WINDOW_NORMAL)
cv2.imshow("image", img)
cv2.waitKey(0)