PImage img;

void setup() {
  size(463, 624);
  img = loadImage("cat.jpg");
  stroke(255);
  strokeWeight(3);
  noFill();
  smooth();
}

void draw() {
  image(img, 0, 0);
  rect(mouseX, mouseY, 25, 25);
}

void keyPressed() {
  save("cat-snap.jpg");
  int x = mouseX;
  int y = mouseY;

  PImage eye = img.get(x, y, 25, 25);
  eye.save("cat-eye.jpg");
  
  exit();
}