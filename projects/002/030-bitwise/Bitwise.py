'''
 * Python script to demonstrate image bitwise operations and masking
'''
import cv2, sys, getopt, numpy as np

def checkParameters(args):
	'''
	 * function to validate command-line arguments
	'''
	try:
		options, arguments = getopt.getopt(args, "f:")
		return options[0][1]
	except getopt.GetoptError:
		print("usage: python Bitwise.py -f filename")
		sys.exit(-1)

# read and display input image, based on -f filename parameter
img = cv2.imread(checkParameters(sys.argv[1:]))
cv2.imshow("original", img)
cv2.waitKey(0)

# determine mask coordinates, inset by 1/8 of image width / height
x1 = int(img.shape[1] / 8)
y1 = int(img.shape[0] / 8)
x2 = int(img.shape[1] - x1)
y2 = int(img.shape[0] - y1)

# create and display the mask 
white = (255, 255, 255)
mask = np.zeros(img.shape, dtype = "uint8")
cv2.rectangle(mask, (x1, y1), (x2, y2), white, -1)

cv2.imshow("mask", mask)
cv2.waitKey(0)

# demonstrate bitwise and
cv2.destroyWindow("mask")
andImg = cv2.bitwise_and(img, mask)
cv2.imshow("and", andImg)
cv2.waitKey(0)

# demonstrate bitwise or
cv2.destroyWindow("and")
orImg = cv2.bitwise_or(img, mask)
cv2.imshow("or", orImg)
cv2.waitKey(0)

# bitwise exclusive or
cv2.destroyWindow("or")
xorImg = cv2.bitwise_xor(img, mask)
cv2.imshow("xor", xorImg)
cv2.waitKey(0)

# bitwise inversion
cv2.destroyWindow("xor")
notImg = cv2.bitwise_not(img)
cv2.imshow("inverted", notImg)
cv2.waitKey(0)