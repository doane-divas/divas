line = input().split()
a = int(line[0])
b = int(line[1])

# is a even or odd? 
if a % 2 == 0:
	a = a + 1

sum = 0
for i in range(a, b + 1, 2):
	sum = sum + i

print(sum)
