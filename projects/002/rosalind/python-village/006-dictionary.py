wordcount = { }

words = input().split()

for word in words:
	if word not in wordcount:
		wordcount[word] = 1
	else:
		n = wordcount[word]
		wordcount[word] = n + 1

for word in wordcount.keys():
	print(word + " " + str(wordcount[word]))
