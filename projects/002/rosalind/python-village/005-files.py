infile = open("005-in.txt", "r")
outfile = open("005-out.txt", "w")

n = 1
for line in infile:
	if n % 2 == 0:
		outfile.write(line)
	n = n + 1

outfile.close()
infile.close()

