# read line of input data
line = input().split()

# parse the two numbers into a and b
a = int(line[0])
b = int(line[1])

# determine square of hypotenuse
c2 = a**2 + b**2

# output result
print(c2)
