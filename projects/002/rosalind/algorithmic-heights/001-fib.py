line = input().split()
n = int(line[0])

fn1 = 1
fn2 = 0

for i in range(2, n + 1):
	f = fn1 + fn2
	fn2 = fn1
	fn1 = f

print(f)
