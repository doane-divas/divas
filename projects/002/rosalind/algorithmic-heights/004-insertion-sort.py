def insertionSort(A, n):
	swaps = 0
	for i in range(2, n + 1):
		k = i
		while k > 1 and A[k] < A[k - 1]:
			t = A[k - 1]
			A[k - 1] = A[k]
			A[k] = t
			k = k - 1
			swaps = swaps + 1
	return swaps

line = input().split()	# read n
n = int(line[0])

A = [-9999]		# ignore spot 0
line = input().split() 	# read data
for v in line:
	A.append(int(v))

swaps = insertionSort(A, n)

print(swaps)

