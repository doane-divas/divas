import sys

graph = {}
line = input() # eat first line, it's not relevant 
for line in sys.stdin:
	tokens = line.split()
	v1 = int(tokens[0])
	v2 = int(tokens[1])

	if v1 not in graph:
		graph[v1] = []
		graph[v1].append(v2)
	else:
		graph[v1].append(v2)

	if v2 not in graph:
		graph[v2] = []
		graph[v2].append(v1)
	else:
		graph[v2].append(v1)

vertices = []
for v in graph.keys():
	vertices.append(v)

vertices.sort()
for v in vertices:
	neighbors = graph[v]
#	print(v, neighbors)
	sys.stdout.write(str(len(neighbors)) + " ")

sys.stdout.write("\n")
sys.stdout.flush()


