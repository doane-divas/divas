import sys

def binSearch(A, i, j, k):
	m = (i + j) // 2
	if A[m] == k:
		return m
	elif j < i:
		return -1
	else:
		if k < A[m]:
			return binSearch(A, i, m - 1, k)
		else:
			return binSearch(A, m + 1, j, k)

line = input().split()
n = int(line[0])

line = input().split()
m = int(line[0])

line = input().split()
A = [-9999]
for s in line:
	A.append(int(s))

line = input().split()
M = []
for s in line:
	M.append(int(s))

for m in M:
	sys.stdout.write(str(binSearch(A, 1, n, m)) + " ")

sys.stdout.write("\n")
sys.stdout.flush()
