'''
 * Python script to illustrate contour detection 
 * and element counting
'''
import cv2, sys, getopt

# try block to catch malformed arguments
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:k:L:H:")
	if len(options) != 4:
		raise getopt.GetoptError("Improper number of arguments")

	# assign variables based on command-line arguments
	for opt, arg in options:
		# filename 
		if opt == "-f":
			filename = arg
		# blur kernel size
		elif opt == "-k":
			k = int(arg)
			if k % 2 != 1:
				raise getopt.GetoptError("Blur kernel size must be odd")
		# threshold values
		elif opt == "-L":
			t1 = int(arg)
		elif opt == "-H":
			t2 = int(arg)

	# read and display input image, based on -f filename parameter
	img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
	img = cv2.GaussianBlur(img, (k, k), 0)
	cv2.imshow("original", img)
	cv2.waitKey(0)
	
	# use Canny method to detect edges
	canny = cv2.Canny(img, t1, t2)
	cv2.imshow("Canny edge detection", canny)
	cv2.waitKey(0)
	
	# get contours
	(_, cnts, _) = cv2.findContours(canny.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	print("{} pens".format(len(cnts)))
	
	pens = img.copy()
	cv2.drawContours(pens, cnts, -1, (0, 255, 0), 2)
	cv2.imshow("contours", pens)
	cv2.waitKey(0)
	
except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python Countour.py -f filename -k kernel-size -L low -H high")
	sys.exit(-1)