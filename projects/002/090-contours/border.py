import cv2, sys

filename = sys.argv[1]

image = cv2.imread(filename)

cv2.rectangle(image, (0, 0), (image.shape[1] - 1, image.shape[0] - 1), (0,0,0), 1)

cv2.imwrite('out.jpg', image)
