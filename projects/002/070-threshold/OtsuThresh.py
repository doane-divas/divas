'''
 * Python script to illustrate thresholding via 
 * Otsu's method
'''
import cv2, sys, getopt, mahotas

# read input file name
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:k:")
	if len(options) != 2:
		raise getopt.GetoptError("Improper number of arguments")
		
	# assign variables based on command-line arguments
	for opt, arg in options:
		# filename 
		if opt == "-f":
			filename = arg
		# blur kernel size
		elif opt == "-k":
			k = int(arg)
			if k % 2 != 1:
				raise getopt.GetoptError("Blur kernel size must be odd")
				
	# read and display original image		
	img = cv2.imread(filename)
	imgG = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	blur = cv2.GaussianBlur(imgG, (k, k), 0)
	cv2.namedWindow("original", cv2.WINDOW_NORMAL)
	cv2.imshow("original", blur)
	cv2.waitKey(0)

	# use Otsu's method to create a mask
	T = mahotas.thresholding.otsu(blur)
	maskLayer = imgG.copy()
	maskLayer[maskLayer > T] = 255
	maskLayer[maskLayer < T] = 0
	maskLayer = cv2.bitwise_not(maskLayer)
	mask=cv2.merge([maskLayer, maskLayer, maskLayer])
	cv2.namedWindow("Otsu mask", cv2.WINDOW_NORMAL)
	cv2.imshow("Otsu mask", mask)
	cv2.waitKey(0)
	
	# show the selected area
	sel = cv2.bitwise_and(img, mask)
	cv2.namedWindow("Otsu thresh sel", cv2.WINDOW_NORMAL)
	cv2.imshow("Otsu thresh sel", sel)
	cv2.waitKey(0)	
	
except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python OtsuThresh.py -f filename -k kernel-size")
	sys.exit(-1)
