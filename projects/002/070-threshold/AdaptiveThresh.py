'''
 * Python script to demonstrate adaptive thresholding to mask
 * an image
'''
import cv2, sys, getopt

# try block to catch malformed arguments
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:k:c:n:t:")
	if len(options) != 5:
		raise getopt.GetoptError("Improper number of arguments")
		
	# assign variables based on command-line arguments
	for opt, arg in options:
		# filename 
		if opt == "-f":
			filename = arg
		# blur kernel size
		elif opt == "-k":
			k = int(arg)
			if k % 2 != 1:
				raise getopt.GetoptError("Blur kernel size must be odd")
		# C parameter
		elif opt == "-c":
			C = int(arg)
		# threshold neighborhood size
		elif opt == "-n":
			N = int(arg)
			if N % 2 != 1:
				raise getopt.GetoptError("Threshold neighborhood must be odd")
		# thresholding type
		elif opt == "-t":
			type = arg
			if type not in ["mean", "gauss"]:
				raise getopt.GetoptError("Threshold type must be 'mean' or 'gauss'")
				
	# read and display original image
	img = cv2.imread(filename)
	cv2.namedWindow("original", cv2.WINDOW_NORMAL)
	cv2.imshow("original", img)
	cv2.waitKey(0)

	# blur the image 
	blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	blur = cv2.GaussianBlur(blur, (k, k), 0)		
	
	# perform adaptive thresholding to create a mask
	if type == "mean":
		maskLayer = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, N, C)
	elif type == "gauss":
		maskLayer = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C	, cv2.THRESH_BINARY_INV, N, C)

	# do masking and display the result
	mask = cv2.merge([maskLayer, maskLayer, maskLayer])
	sel = cv2.bitwise_and(img, mask)
	cv2.namedWindow("thresh sel", cv2.WINDOW_NORMAL)
	cv2.imshow("thresh sel", sel)
	cv2.waitKey(0)
	
except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python AdaptiveThresh.py -f filename -k kernel-size -n neighborhood-size -c C -t [mean | gauss]")
	sys.exit(-1)
