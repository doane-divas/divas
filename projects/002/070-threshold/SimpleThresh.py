'''
 * Python script to demonstrate simple thresholding to mask
 * an image
'''
import cv2, sys, getopt

# try block to catch malformed arguments
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:k:t:")
	if len(options) != 3:
		raise getopt.GetoptError("Improper number of arguments")
		
	# assign variables based on command-line arguments
	for opt, arg in options:
		# filename
		if opt == "-f":
			filename = arg
		# blur kernel size
		elif opt == "-k":
			k = int(arg)
			if k % 2 != 1:
				raise getopt.GetoptError("Kernel size must be odd")
		# threshold value
		elif opt == "-t":
			T = int(arg)
				
	# read and display original image
	img = cv2.imread(filename)
	cv2.imshow("original", img)
	cv2.waitKey(0)
	
	# perform binary thresholding to create a mask
	blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	blur = cv2.GaussianBlur(blur, (k, k), 0)		
	(T, maskLayer) = cv2.threshold(blur, T, 255, cv2.THRESH_BINARY_INV)
	mask = cv2.bitwise_not(cv2.merge([maskLayer, maskLayer, maskLayer]))
	
	cv2.imshow("binary threshold mask", mask)
	cv2.waitKey(0)
	
	# use mask to select the "interesting" part of the image
	sel = cv2.bitwise_and(img, mask)
	cv2.imshow("selected area", sel)
	cv2.waitKey(0)

except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python SimpleThresh.py -f filename -k kernel-size -t threshold")
	sys.exit(-1)