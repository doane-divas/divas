'''
 * Python script to illustrate channel separation.
'''
import cv2, sys, getopt

# try block to catch malformed arguments
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:")
	if len(options) != 1:
		raise getopt.GetoptError("Improper arguments")

	# read input image, based on -f filename parameter
	img = cv2.imread(options[0][1])
	cv2.imshow("original", img)
	cv2.waitKey(0)
	
	# separate the channels and display
	(B, G, R) = cv2.split(img)

	cv2.imshow("red", R)
	cv2.imshow("green", G)
	cv2.imshow("blue", B)
	cv2.waitKey(0)
	
except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python ImgSplit.py -f filename")
	sys.exit(-1)