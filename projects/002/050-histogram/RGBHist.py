'''
 * Python script to calculate and display a RGB histogram.
 *
'''
import cv2, sys, getopt
from matplotlib import pyplot as plt

# try block to catch malformed arguments
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:")
	if len(options) != 1:
		raise getopt.GetoptError("Improper arguments")

	# read and display input image, based on -f filename parameter
	img = cv2.imread(options[0][1])
	cv2.imshow("original", img)
	cv2.waitKey(0)

	# split into channels
	channels = cv2.split(img)
	colors = ("b", "g", "r")
	
	# draw figure with three plots
	for(chan, col) in zip(channels, colors):
		hist = cv2.calcHist([chan], [0], None, [256], [0, 256])
		plt.plot(hist, color = col)
		plt.xlim([0, 256])
	plt.xlabel("color value")
	plt.ylabel("pixels")
	plt.show()
	cv2.waitKey(0)
	
except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python RGBHist.py -f filename")
	sys.exit(-1)