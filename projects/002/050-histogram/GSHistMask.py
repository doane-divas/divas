'''
 * Python script to calculate and display a grayscale histogram, with
 * a masked area.
 *
'''
import cv2, sys, getopt, numpy as np
from matplotlib import pyplot as plt

# try block to catch malformed arguments
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:")
	if len(options) != 1:
		raise getopt.GetoptError("Improper arguments")

	# read input image, based on -f filename parameter
	img = cv2.imread(options[0][1], cv2.IMREAD_GRAYSCALE)
	
	# display
	cv2.imshow("original", img)
	cv2.waitKey(0)
	
	# create and display mask
	mask = np.zeros(img.shape, dtype = "uint8")
	cv2.rectangle(mask, (410, 199), (485, 384), (255, 255, 255), -1)
	cv2.imshow("mask", mask)
	cv2.waitKey(0)
	
	# create histogram of unmasked area
	hist = cv2.calcHist([img], [0], mask, [256], [0, 256])
	
	# draw histogram figure
	plt.figure()
	plt.title("Masked seedling histogram")
	plt.xlabel("grayscale value")
	plt.ylabel("pixels")
	plt.plot(hist)
	plt.xlim([0, 256])
	plt.show()
	
	cv2.waitKey(0)
	
except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python GSHistMask.py -f filename")
	sys.exit(-1)