'''
 * Python script to calculate and display a RGB histogram, with
 * a masked area.
 *
'''
import cv2, sys, getopt, numpy as np
from matplotlib import pyplot as plt

# try block to catch malformed arguments
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:")
	if len(options) != 1:
		raise getopt.GetoptError("Improper arguments")

	# read and display input image, based on -f filename parameter
	img = cv2.imread(options[0][1])
	cv2.imshow("original", img)
	cv2.waitKey(0)

	# split into channels
	channels = cv2.split(img)
	colors = ("b", "g", "r")
	
	# create mask based on blue channel shape
	mask = np.zeros(channels[0].shape, dtype = "uint8")
	cv2.rectangle(mask, (410, 199), (485, 384), (255, 255, 255), -1)

	# draw figure with three plots
	for(chan, col) in zip(channels, colors):
		hist = cv2.calcHist([chan], [0], mask, [256], [0, 256])
		plt.plot(hist, color = col)
		plt.xlim([0, 256])
	plt.show()
	cv2.waitKey(0)
	
except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python RGBHistMask.py -f filename")
	sys.exit(-1)