'''
 * Python script to calculate and display a grayscale histogram.
 *
'''
import cv2, sys, getopt
from matplotlib import pyplot as plt

# try block to catch malformed arguments
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:")
	if len(options) != 1:
		raise getopt.GetoptError("Improper arguments")

	# read input image, based on -f filename parameter
	# read as grayscale when loading
	img = cv2.imread(options[0][1], cv2.IMREAD_GRAYSCALE)
	
	# display image
	cv2.imshow("original", img)
	cv2.waitKey(0)
	
	# create histogram
	hist = cv2.calcHist([img], [0], None, [256], [0, 256])
	
	# draw histogram figure
	plt.figure()
	plt.title("Seedling histogram")
	plt.xlabel("grayscale value")
	plt.ylabel("pixels")
	plt.plot(hist)
	plt.xlim([0, 256])
	plt.show()
	
	cv2.waitKey(0)
	
except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python GSHist.py -f filename")
	sys.exit(-1)