'''
 * Python script to calculate and display a 2D RGB histogram.
 *
'''
import cv2, sys, getopt
from matplotlib import pyplot as plt

# try block to catch malformed arguments
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:")
	if len(options) != 1:
		raise getopt.GetoptError("Improper arguments")

	# read and display input image, based on -f filename parameter
	img = cv2.imread(options[0][1])
	cv2.imshow("original", img)
	cv2.waitKey(0)

	# split into channels
	channels = cv2.split(img)
	colors = ("b", "g", "r")

	fig = plt.figure()
	
	# 2D histogram for green and blue
	ax = fig.add_subplot(131)
	hist = cv2.calcHist([channels[1], channels[0]], [0, 1], None, [32, 32], [0, 256, 0, 256])
	p = ax.imshow(hist, interpolation = "nearest")
	ax.set_title("2D, G/B")
	plt.colorbar(p)

	# 2D histogram for green and red
	ax = fig.add_subplot(132)
	hist = cv2.calcHist([channels[1], channels[2]], [0, 1], None, [32, 32], [0, 256, 0, 256])
	p = ax.imshow(hist, interpolation = "nearest")
	ax.set_title("2D, G/R")
	plt.colorbar(p)

	# 2D histogram for blue and red
	ax = fig.add_subplot(133)
	hist = cv2.calcHist([channels[0], channels[2]], [0, 1], None, [32, 32], [0, 256, 0, 256])
	p = ax.imshow(hist, interpolation = "nearest")
	ax.set_title("2D, B/R")
	plt.colorbar(p)
	
	plt.show()
	
	cv2.waitKey(0)
	
except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python RGBHist2D.py -f filename")
	sys.exit(-1)