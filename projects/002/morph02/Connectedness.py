'''
 * Example for measuring connectedness of SEM nanoparticle images.
 * 
'''
import cv2, sys, getopt

# try block to catch malformed arguments
try:
    # parse command line arguments, ignoring the first
    options, arguments = getopt.getopt(sys.argv[1:], "f:k:L:H:i:d")
    if len(options) < 5 or len(options) > 6:
        raise getopt.GetoptError("Improper number of arguments")
        
    # assign variables based on command-line arguments
    wantDisplay = False
    for opt, arg in options:
        # filename 
        if opt == "-f":
            filename = arg
        # blur kernel size
        elif opt == "-k":
            k = int(arg)
            if k % 2 != 1:
                raise getopt.GetoptError("Blur kernel size must be odd")
        # threshold values
        elif opt == "-L":
            t1 = int(arg)
        elif opt == "-H":
            t2 = int(arg)
		# edge detection iterations
        elif opt == "-i":
            iter = int(arg)
		# display or text only?
        elif opt == "-d":
            wantDisplay = True

    # read and display input image, based on -f filename parameter
    img = cv2.imread(filename)
    blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(blur, (k, k), 0)
 
    if wantDisplay:
        cv2.imshow("blurred", blur)
        cv2.waitKey(0)
        
    # edge detection
    edge = cv2.Canny(blur, t1, t2)
    edge = cv2.dilate(edge, None, iterations = iter)
    edge = cv2.erode(edge, None, iterations = iter)
 
    if wantDisplay:
        cv2.imshow("edges", edge)
        cv2.waitKey(0)
    
    # find contours
    (_, cnts, _) = cv2.findContours(edge.copy(), cv2.RETR_EXTERNAL, 
        cv2.CHAIN_APPROX_SIMPLE)
        
    # output number of contours 
    print(len(cnts))
        
except getopt.GetoptError:
    # if we have errors in # or type of command-line arguments,
    # print a usage message and exit.
    print("usage: python Connectedness.py -f filename -k kernel-size -L low -H high -i iter [-d]")
    sys.exit(-1)
