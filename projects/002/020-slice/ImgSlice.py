'''
 * Python script demonstrating image modification and creation via 
 * NumPy array slicing.
'''

import cv2

'''
 * main script
 *
'''
img = cv2.imread("board.jpg")

# extract, display, and save subimage
clip = img[60:150, 135:450, :]
cv2.imshow("clip", clip)
cv2.imwrite("clip.tif", clip)
cv2.waitKey(0)

# replace clipped area with sampled color
c = img[330, 90]
img[60:150, 135:450] = c
cv2.imshow("modified", img)
cv2.waitKey(0)