'''
 * Python script to demonstrate blurring
'''
import cv2, sys, getopt

# try block to catch malformed arguments
try:
	# parse command line arguments, ignoring the first
	options, arguments = getopt.getopt(sys.argv[1:], "f:k:t:")
	if len(options) != 3:
		raise getopt.GetoptError("Improper number of arguments")

	# assign variables based on command-line arguments
	for opt, arg in options:
		# filename
		if opt == "-f":
			filename = arg
		# type of blurring
		elif opt == "-t":
			if arg == "avg":
				blurType = "avg"
			elif arg == "gauss":
				blurType = "gauss"
			elif arg == "med":
				blurType = "med"
			else:
				raise getopt.GetoptError("Invalid blur type")
		# blur kernel size
		elif opt == "-k":
			k = int(arg)
			if k % 2 != 1:
				raise getopt.GetoptError("Kernel size must be odd")
			kernelTitle = "(" + arg + ", " + arg + ")"
			
	# read and display original image
	img = cv2.imread(filename)
	cv2.imshow("original", img)
	cv2.waitKey(0)
	
	# blur  and diaplay image 
	if blurType == "avg":
		blur = cv2.blur(img, (k, k))
	elif blurType == "gauss":
		blur = cv2.GaussianBlur(img, (k, k), 0)
	elif blurType == "med":
		blur = cv2.medianBlur(img, k)			
	
	cv2.imshow(blurType + ", " + kernelTitle, blur)
	cv2.waitKey(0)
	
except getopt.GetoptError:
	# if we have errors in # or type of command-line arguments,
	# print a usage message and exit.
	print("usage: python Blur.py -f filename -t [avg|gauss|med] -k kernel-size")
	sys.exit(-1)