#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 23 15:55:29 2017

@author: diva
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 23 09:20:58 2017

@author: diva

"""

import cv2, sys, numpy as np
filenames = ['002-001-017-040-056-079-095-111-127-149-D2.tif', '003-008-023-038-059-074-090-106-129-145-D2.tif', '001-007-022-037-052-073-105-128-144-160-D2.tif']

t = int(sys.argv[1])
k = int(sys.argv[2])

for filename in filenames:
#read the file in the list  
    img = cv2.imread(filename)
#blur the image, k = 7, then use adaptive threshold and combine layers
    blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(blur, (k,k), 0)
    (t, maskLayer) = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    threshmask = cv2.merge([maskLayer, maskLayer, maskLayer])
    sel = cv2.bitwise_and(img, threshmask)
    (_, contours, _) = cv2.findContours(maskLayer, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(img, contours, -1, (0, 0, 255), 5)
    
    #mask = np.zeros(img.shape, dtype = "uint8")
    #for c in contours:
     #   (x, y, w, h) = cv2.boundingRect(c)
      #  cv2.rectangle(threshmask, (x, y), (x + w, y + h), (255, 255, 255), -1)
       # img = cv2.bitwise_and(img, threshmask)
    cv2.namedWindow('output', cv2.WINDOW_NORMAL)    
    cv2.imshow('output', img)
    cv2.waitKey(0)
    
   

#we need to add the write function into the loop as soon
#as we are comfortable with the threshold given
"""
blackmask = np.zeros(img.shape, dtype = "unt8")

cv2.rectangle(blackmask, (), (), (255, 255, 255), -1)

maskedimg = cv2.bitwise_and(img, blackmask)
"""
 
