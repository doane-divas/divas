#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 24 09:40:20 2017

@author: diva
"""

import cv2, sys

def fixedThresh():
    global img, blur, thresh
    (t, mask) = cv2.threshold(blur, thresh, 255, cv2.THRESH_BINARY)
    cv2.imshow('image', mask)
    
def adjustThresh(v):
    global thresh
    thresh = v
    fixedThresh()
    
    
filename = sys.argv[1]
k = int(sys.argv[2])
img = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
blur = cv2.GaussianBlur(img, (k,k), 0)
cv2.namedWindow('image', cv2.WINDOW_NORMAL)
thresh = 128
cv2.createTrackbar('thresh', 'image', thresh, 255, adjustThresh)
fixedThresh()
cv2.waitKey(0)


