#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
<<<<<<< HEAD
Created on Tue May 23 15:55:29 2017

@author: diva
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 23 09:20:58 2017

@author: Marisa Foster and Keeliann Mark

Length refers to the whole contour of the roots, not the actual length

"""

import cv2, sys, numpy as np
filenames = ['002-001-017-040-056-079-095-111-127-149-D2.tif', '003-008-023-038-059-074-090-106-129-145-D2.tif', '001-007-022-037-052-073-105-128-144-160-D2.tif']
k = int(sys.argv[1]) #k equals 5
t = int(sys.argv[2]) #t equals 83

lengths = []

for filename in filenames:
    img = cv2.imread(filename)
    #crop image to only show half of the kernel and the roots
    clip = img[6437:10670, 284:11012]  
#blur the image, then use adaptive threshold and combine layers
    gray = cv2.cvtColor(clip, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (k,k), 0)
    (t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)   
    threshimg = binary   
#need hierarchy in order to get a better contour of the roots
    (_, contours, hierarchy) = cv2.findContours(threshimg, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(clip, contours, -1, (0, 0, 255), 5)   
#if statement to remove the very tiny contours 
    avg = 0
    for c in contours:
        avg += len(c)
    avg /= len(contours)
    for c in contours:
        if len(c) > avg / 2:
            lengths.append(len(c))
    cv2.namedWindow('outcome', cv2.WINDOW_NORMAL)    
    cv2.imshow('outcome', clip)
    cv2.waitKey(0)  
#convert length from pixels to centimeters
for length in lengths:
    lengthincm = length ** 37.795276
    print('the length of the root in centimeters is', length)
=======
Created on Wed May 31 09:35:34 2017

@author: Keeliann Mark and Marisa Foster

This code draws the contours of the roots onto a black image and prints the area
inside of those contours.
"""
# functions that need to be imported for the program
import cv2, sys, numpy as np
from imutils import contours as imc

filenames = ['002-001-017-040-056-079-095-111-127-149-D2.tif', '003-008-023-038-059-074-090-106-129-145-D2.tif', '001-007-022-037-052-073-105-128-144-160-D2.jpg']

k = int(sys.argv[1]) #k equals 5
t = int(sys.argv[2]) #t equals 83
# the area in which the order of contour will be placed
def findCentroid(contour):
    M = cv2.moments(contour)
    x = int(M['m10'] / M['m00'])
    y = int(M['m01'] / M['m00'])
    return (x, y)
#this was a part of a for loop, but to save time we are only looking at one 
#image at a time
for filename in filenames:
    img = cv2.imread(filename)
#crop image to only show half of the kernel and the roots
    clip = img[6765:10442, 300:10994, :]  

#blur the image, then use adaptive threshold and combine layers
    gray = cv2.cvtColor(clip, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (k,k), 0)
    (t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)   

#need hierarchy in order to get a better contour of the roots
    (_, contours, hierarchy) = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# added mask to draw the contours on it
    mask = np.zeros(clip.shape, dtype = "uint8")
#this sorts the contours, so that we can know the order they appear
    (sortedContours, boundingBoxes) = imc.sort_contours(contours)
#if statement to remove the very tiny contours 

    cv2.namedWindow("display", cv2.WINDOW_NORMAL)



    for (i, contour) in enumerate(sortedContours):
    #this removes all of the little contours
        if len(contour) > 120: 
        # draw the current contour in red on the all black image
            cv2.drawContours(mask, sortedContours, i, (0, 0, 255), 5)    
        # draw a black label with contour number
            cv2.putText(mask, str(i), findCentroid(contour), 
                cv2.FONT_HERSHEY_PLAIN, 7.5, (255, 255, 255), 5)
       
       #this displays the contours one at a time
            cv2.imshow("display", mask)
            cv2.waitKey(0)
       #this finds the area inside of the contour
            area = cv2.contourArea(contour)
            print("for contour number", i)
            print("the area inside of the contour is", area)
            print("the height of the bounding box is", boundingBoxes[i][3])
       
       
       
       
       
       
       
       
       
       #print("The area inside of the contour is", area)
       #print(boundingBoxes)
    
   
    


    
                        

    


>>>>>>> cb13419af6eaeaa6dd77590c70839f781493c809

 
 
