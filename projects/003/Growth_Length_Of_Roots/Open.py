'''
 * Python program to open, display, and save an image.
 *
'''
import cv2

# read image 
img = cv2.imread("KYRIE.jpg")
small = cv2.resize(img, None, fx = 0.5, fy = 0.5)

#display image and wait for keypress, using a resizable window
cv2.namedWindow("image", cv2.WINDOW_NORMAL)
cv2.imshow("image", small)
cv2.waitKey(0)

# save a new version in .tif format
cv2.imwrite("KYRIE.tif", small)
