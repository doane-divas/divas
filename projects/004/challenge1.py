#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 23 13:32:22 2017

@author: diva
"""

import cv2,sys 
import numpy as np
# read command-line arguments
filename = sys.argv[1]
k= int(sys.argv[2])
t= int(sys.argv[3])
# Load the original image
img = cv2.imread(filename)

# Create the basic black image 
mask = np.zeros(img.shape, dtype = "uint8")
# Draw a white, filled rectangle on the mask image
cv2.rectangle(mask, (320,6414), (11008, 10672), (255, 255, 255), -1)
# Apply the mask and display the result
maskedImg = cv2.bitwise_and(img, mask)

#up to black mask above roots

# blur and grayscale before thresholding
blur = cv2.cvtColor(maskedImg, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(blur, (k, k), 0)
# perform adaptive thresholding 
(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY) 

# make a mask suitable for color images
mask = cv2.merge([binary, binary, binary])

# display the mask image
cv2.namedWindow("mask", cv2.WINDOW_NORMAL)
cv2.imshow("mask", mask)
cv2.waitKey(0)

#masked image of roots, still displays bits of higher intensity along with
# roots.

#Above codes works. First, masking and turn the image into binary(blur, threshold the mask)

# find contours
(_, contours, hierarchy) = cv2.findContours(binary, cv2.RETR_TREE, 
        cv2.CHAIN_APPROX_SIMPLE)


# draw contours over original image
cv2.drawContours(mask, contours, -1, (0, 0, 255), 5)


# determine average length of contours
avg = 0
for c in contours:
    avg += len(c)
    
avg /= len(contours)

for (i, c) in enumerate(contours):
    
    # ... only work with contours associated with actual dots
    if len(c) > avg * 13:
        
        (x, y, w, h) = cv2.boundingRect(c)
        sub = img[y: y+h, x: x+w, :]
        
        # blur and grayscale before thresholding
        blur2 = cv2.cvtColor(sub, cv2.COLOR_BGR2GRAY)
        blur2 = cv2.GaussianBlur(blur2, (k, k), 0)
        # perform adaptive thresholding 
        (t, binary) = cv2.threshold(blur2, t, 255, cv2.THRESH_BINARY) 

        # make a mask suitable for color images
        blur2 = cv2.merge([binary, binary, binary])
        
        # keep only high-intensity pixels
        blur2[blur2 < 90] = 0
        blur2[blur2 >= 90] = 255



    
        cv2.namedWindow("sub--{0}.jpg".format(i), cv2.WINDOW_NORMAL)
        #cv2.imwrite("sub--{0}.jpg".format(i), sub)
        cv2.imshow("sub--{0}.jpg".format(i), blur2)
        cv2.waitKey(0)
        
        # print table of contours and sizes
        print("Found %d objects." % len(contours))
        for (i, c) in enumerate(contours):
            if len(c) >= 700:
                print("\tSize of contour %d: %d" % (i, len(c)))
               









