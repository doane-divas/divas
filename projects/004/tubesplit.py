#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 30 12:04:03 2017

@author: diva
"""


import sys, cv2
from imutils import contours as imc
import numpy as np
import os


def findCentroid(contour):
    M = cv2.moments(contour)
    x = int(M['m10'] / M['m00'])
    y = int(M['m01'] / M['m00'])
    return (x, y)

filename = sys.argv[1]
k = 5
t = 95
#########################################################
####Section
####This section: loads the img as a BGR image; pulls the label out as BGR;
####loads the img as a grayscale image; pulls the label out as grayscale
#########################################################
#load image and pull our label
img = cv2.imread(filename)
labelColor = img[1900:3550,:]
#load grayscale image and pull out label
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
label = gray[1900:3550,:]
#########################################################
####Section
####This section: blurs image; thresholds image with OTSU; finds image 
####contours; then most importantly, determines a contour point for first tube
#########################################################
# blur image and use simple inverse binary thresholding to create
# a binary image
blur = cv2.GaussianBlur(label, (k, k), 0)
(t, binary) = cv2.threshold(blur, t, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

#find contours
(_, contours, _) = cv2.findContours(binary, cv2.RETR_EXTERNAL, 
    cv2.CHAIN_APPROX_SIMPLE)

#this determines the contour point to make first test tube image based on x
#value to draw vertical line to slice first image
#   ***this is done on the white label of each plate image***
#this for loops through each contour in the image
for contour in contours:
    #this for loops through each point in contour
    for points in contour:
        #this for loops through the points x/y values
        for point in points:
            y = point[1]
            if y > 1340 and y < 1375:
                x = point[0]
                if x < 600 and x  > 100:
                    xOriginPixel = point

#prints xOriginPixel for checking                    
print("the xOriginPixel is:", xOriginPixel)
#########################################################
####Section
####This section: splits up initial filename; makes new filenames; 
####splits up testtubes and saves them as individual images
#########################################################
#split up filename and put it in a list 
#(e.g. 001-022-045-267-D2.tif --> [001,022,045,267,D2.tif]
list = filename.split('-')

#initialize subfilenames as an empty list
subfilenames = []

#use the list to create each pictures new file name
for label in list[1:-1]:
    file = list[0] + '-' + label + '-' + list[-1]
    #e.g. file = '001-045-D2.tif'
    subfilenames.append(file)

#this selects the x value of the xOriginPixel
xPoint = xOriginPixel[0]

#splice the first test tube out of img using the x/y values below
testTube = img[6500: -1000, xPoint-10 : xPoint + 1200]

#write the testTube to a new image file using constructed name and testTube
cv2.imwrite(subfilenames[0], testTube)

#loop to save each test tube as a different file
#this does what was done above for each test tube   
for i in range(1, 9):
    #increment xPoint by 1200 since test tubes were around that wide
    xPoint = xPoint + 1200
    #this allows us to get individual test tube images
    #we overlapped images by about 40 pixels  
    #this was done to make up for potential error in test tube edge detection
    testTube = img[6500:-1000, xPoint-10 : xPoint + 1240]
    cv2.imwrite(subfilenames[i], testTube)
    
    
#datasheet = 'seedlingData.csv'
#Outfile = open(datasheet, "a")
    
    
for testTube in subfilenames:
    img = cv2.imread(testTube)
    
    # blur and grayscale before thresholding
    blur = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(blur, (k, k), 0)

    # perform thresholding 
    (t, binary) = cv2.threshold(blur, 95, 255, cv2.THRESH_BINARY) 

    # make a mask suitable for color images
    mask = cv2.merge([binary, binary, binary])
    
    # displays the grayscale mask image
    #cv2.namedWindow("mask", cv2.WINDOW_NORMAL)
    #cv2.imshow("mask", mask)
    cv2.waitKey(0)
    
    #shows masked imaged of roots + other high intensity bits in subimages
    #works up to this point
    
    (_, contours, hierarchy) = cv2.findContours(binary, cv2.RETR_TREE, 
        cv2.CHAIN_APPROX_SIMPLE)
    
    
    # use imutils function to sort contours. See the source code at
    # https://github.com/jrosebr1/imutils/blob/master/imutils/contours.py
    # for details on the options for ordering
    (sortedContours, boundingBoxes) = imc.sort_contours(contours)


    # prepare a window for display
    cv2.namedWindow("display", cv2.WINDOW_NORMAL)

    
    

    
    
    # draw sorted contours on the original image, one at a time, ignoring the 
    # small ones
    for (i, contour) in enumerate(sortedContours):
        
        
        if len(contour) > 100:      # don't draw small contours
    
            # draw the current contour in red
            cv2.drawContours(img, sortedContours, i, (0, 0, 255), 5)
        
            # draw a black label with contour number
            cv2.putText(img, str(i), findCentroid(contour), 
                        cv2.FONT_HERSHEY_PLAIN, 7.5, (0, 0, 0), 5)

            cv2.imshow("display", img)
            cv2.waitKey(0)
            
            #finds area of contours
            area = cv2.contourArea(contour)
            
            #print number of objects found
            print("Found %d objects." % len(sortedContours))
            #print the len of the contour
            print("\tSize of contour %d: %d" % (i, len(sortedContours))) 
            #prnti the area of the contour
            print("\tArea of contour", i, ":", area)
            
'''
            number_contours = ("Found %d objects." % len(sortedContours))
            size_contours = ("Size of contour %d: %d" % (i, len(sortedContours)))
            area_contours = ("Area of contour", i, ";", area)
            
        Outfile.write(number_contours + ',' + size_contours + ',' + area_contours +
        '\n' )
        
Outfile.close()

'''
            
        
        
        
        