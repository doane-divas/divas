#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 23 15:09:59 2017

@author: diva
"""
import cv2,sys
import Mahotas as mh
import numpy as np

filename = sys.argv[1]
image = mh.imread(filename)

# Make a copy to play with the indices.
img = np.copy(image)

 # Replace places with 3rd coordinate less than 100 with the white-color
 # vector [255, 255, 255].
inds = img[:,:,2] < 100
img[inds] = [0,0,0]

cv2.namedWindow("Image", cv2.WINDOW_NORMAL)
cv2.imshow("Image", img)
cv2.waitKey(0)