#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 24 09:08:42 2017

@author: diva
"""


import cv2,sys 
import numpy as np
# read command-line arguments
filename = sys.argv[1]

# Load the original image
img = cv2.imread(filename)


x = 192
y = 2016
w = 1168
h = 9216
for tube in img:
    (x, y, w, h) = cv2.boundingRect(tube)
    sub = img[y: y + h, x: x + w, :]
    y = y + h
    x = x + w


    cv2.namedWindow("sub--{0}.jpg", cv2.WINDOW_NORMAL)
    cv2.imshow("sub--{0}.jpg", sub)
    cv2.waitKey(0)