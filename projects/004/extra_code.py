#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 31 14:11:42 2017

@author: diva
"""

'''
    # draw contours over original image
    #this was done to check what the extra contours were and where they were at
    cv2.drawContours(mask, contours, -1, (0, 0, 255), 5)

    # determine average length of points that make up contours
    #avg = 0
    #for c in contours:
     #   avg += len(c)
    
    #avg /= len(contours)

    for (i, c) in enumerate(contours):
    
        # ... only work with contours associated with actual dots
        #this makes sure to only look at bigger contours by looking at avg size
        if len(c) > 350:
        
            #makes rectangle around target contour
            #(x, y, w, h) = cv2.boundingRect(c)
            #sub = img[y: y+h, x: x+w, :]
            
            # blur and grayscale before thresholding
            #blur2 = cv2.cvtColor(sub, cv2.COLOR_BGR2GRAY)
            #blur2 = cv2.GaussianBlur(blur2, (k, k), 0)
            # perform thresholding 
            #(t, binary) = cv2.threshold(blur2, t, 255, cv2.THRESH_BINARY) 
        
            # skeletonize the image
            #skeleton = im.skeletonize(blur2, size=(3, 3))
            #cv2.imshow("Skeleton", skeleton)
            
            # make a mask suitable for color images
            #blur2 = cv2.merge([binary, binary, binary])
            
            # contrast the contour vs the back ground.
            #contour turned white and background image turned black
            mask[mask < 95] = 0
            mask[mask >= 95] = 255
    
            #extracts rectangles with contours and turns them into own image.
            cv2.namedWindow("sub--{0}.jpg".format(i), cv2.WINDOW_NORMAL)
            #cv2.imwrite("sub--{0}.jpg".format(i), sub)
            cv2.imshow("sub--{0}.jpg".format(i), mask)
            cv2.waitKey(0)
    print("Found %d objects." % len(contours))
    for (i, c) in enumerate(contours):
        if len(c) >= 350:
            print("\tSize of contour %d: %d" % (i, len(c)))
            
            '''  