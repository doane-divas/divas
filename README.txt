*******************************************************************************
* Doane DIVAS README                                                          *
*******************************************************************************

+-----------------------------------------------------------------------------+
| Introduction                                                                |
+-----------------------------------------------------------------------------+

This git repo contains the code and miscellany associated with the Doane DIVAS
project. 

+-----------------------------------------------------------------------------+
| git workflow                                                                |
+-----------------------------------------------------------------------------+

FIRST OFF: when you first get the VirtualBox image up and running, open a 
terminal and do the following to configure git. 

git config --global user.name "Jane Smith"
git config --global user.email "jane.smith@doane.edu"
git config --global core.editor "gedit -s"

Of course, change Jane's information to your own name and email address.
The third command is optional; it configures GEdit as the editor to use when
you are resolving conflicts or adding commit messages.
